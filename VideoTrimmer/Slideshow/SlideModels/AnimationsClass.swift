//
//  AnimationsClass.swift
//  VideoEditorial
//
//  Created by iApp on 10/04/19.
//  Copyright © 2019 iApp. All rights reserved.
//

import UIKit

enum ANIMATIONS_CATEGORY : String {
    case DEFAULT        =  "DEFAULT"
    case FADE           =  "FADE"
    case ZOOM           =  "ZOOM"
    case SLOWZOOM       =  "SLOWOM"
    case SHAKE          =  "SHAKE"
    case CINEMATIC      =  "CINEMATIC"
    case RESHAPECIRCLE  =  "DROP"
    case SHAPEBOUNCE    =  "BOUNCER"
    case CHEERFUL       =  "CHEERFUL"
    case JOYFULL        =  "JOYFULL"
    case BIRDSFLY       =  "BIRDSFLY"
    case SNAKE_HORIZ    =  "JOURNEY"
    case SNAKE_VERT     =  "RIVERS"
    case SNAKE_DIAG     =  "SHAKES"
    case SNAKE_MASKING  =  "SPIRIT"
    case SNAKE_RANDOM   =  "TRAVEL"
    case STRIPES_VERT   =  "FLIPO"
    case BIRTHDAY       =  "BIRTHDAY"
    case MAGICAL        =  "MAGICAL"
    case ROMANCE        =  "ROMANCE"
    case CROSS          =  "CROSS"
    case RAINBOW        =  "RAINBOW"
    case BABY           =  "BABY"
    case FAMILY         =  "FAMILY"
    case CELTIC         =  "CELTIC"
    case FIREWORK       =  "FIREWORK"
    case SNOW           =  "SNOW"
    case FLAME          =  "FLAME"
    case LAUGH          =  "LAUGH"
    case FIELD          =  "FIELD"
    case NOIR           =  "NOIR"
    case COOL           =  "COOL"
    case DIVERT         =  "DIVERT"
    case PIZZA          =  "PIZZA"
    case INK            =  "INK"
    case BRUSH          =  "BRUSH"
    case HEART          =  "HEART"
    case RAINING        =  "RAINING"
    case SLICES         =  "SLICES"
    case DUMBLE         =  "DUMBLE"
    case ARCHERY        =  "ARCHERY"
    case MANIA          =  "MANIA"
    case ADVENTURE      =  "ADVENT"
    case BARS           =  "BARS"
    case SHAPE          =  "SHAPE"
    case ROLL           =  "ROLL"
    case CURTAINS       =  "CURTAINS"
    case DOTS           =  "DOTS"
    case PIXEL          =  "PIXEL"
    case SHADOW         =  "SHADOW"
    case FRIENDS        =  "FRIENDS"
    case AXIS           =  "AXIS"
    case SLIDE          =  "SLIDE"
    case CLASSY         =  "CLASSY"

}

enum SNAKE_ANIMATIONS {
    case HORIZONTAL
    case VERTICAL
    case DIAGONAL
}
enum EMITTER_ANIMATIONS {
    case FLAME
    case FIREWORK
    case SNOW
    case RANING
}

var globalMenuAnimationsArray : [ANIMATIONS_CATEGORY] = [
                                                         .CLASSY,
                                                         .SLIDE,
                                                         .AXIS,
                                                         .FRIENDS,
                                                         .DUMBLE,
                                                         .INK,
                                                         .CHEERFUL,
                                                         .PIXEL,
                                                         .CURTAINS,
                                                         .HEART,
                                                         .SHAPE,
                                                         .BARS,
                                                         .ADVENTURE,
                                                         .MANIA,
                                                         .ARCHERY,
                                                         .SHADOW,
                                                         .SLICES,
                                                         .RAINING,
                                                         .BRUSH,
                                                         .DOTS,
                                                         .PIZZA,
                                                         .COOL,
                                                         .CELTIC,
                                                         .FIELD,
                                                         .DIVERT,
                                                         .FAMILY,
                                                         .RAINBOW,
                                                         .CROSS,
                                                         .MAGICAL,
                                                         .SNAKE_HORIZ,
                                                         .BIRDSFLY,
                                                         .ROLL,
                                                         .SNAKE_DIAG,
                                                         .NOIR,
                                                         .CINEMATIC,
                                                         .SHAPEBOUNCE,
                                                         .SNAKE_VERT,
                                                         .BABY,
                                                         .SNAKE_MASKING,
                                                         .ROMANCE,
                                                         .RESHAPECIRCLE,
                                                         .STRIPES_VERT,
                                                         .BIRTHDAY,
                                                         .SNAKE_RANDOM,
                                                         .JOYFULL,
                                                         .LAUGH,
                                                         .SNOW,
                                                         .CURTAINS
                                                            ]
class AnimationsClass: NSObject {

    static var shared = AnimationsClass()
    
    func addAnimation(masterlayer : CALayer, images : [UIImage] , index : Int , totalCount : Int , isSaving : Bool  ,animateValue: ANIMATIONS_CATEGORY){
        
        var customValue : ANIMATIONS_CATEGORY = .FADE
        customValue = animateValue
//        customValue = .MAGICAL
        
        switch customValue {
            
        case  .CLASSY :
            classyAnimation(masterlayer: masterlayer, images : images, index: index, totalCount: totalCount, isSaving: isSaving)
            break

        case .SLIDE:
            slideAnimation(masterlayer: masterlayer, images : images, index: index, totalCount: totalCount, isSaving: isSaving)
            break
        case .AXIS:
            axisAnimation(masterlayer: masterlayer, images : images, index: index, totalCount: totalCount, isSaving: isSaving)
            break
        case .FRIENDS:
            friendsAnimation(masterlayer: masterlayer, images : images, index: index, totalCount: totalCount, isSaving: isSaving)
            break
        case .SHADOW:
            shadowAnimation(masterlayer: masterlayer, images : images, index: index, totalCount: totalCount, isSaving: isSaving)
            
            break
        case .PIXEL:
            pixelAnimation(masterlayer: masterlayer, images : images, index: index, totalCount: totalCount, isSaving: isSaving)
            
            break
        case .DOTS:
            dotsAnimation(masterlayer: masterlayer, images : images, index: index, totalCount: totalCount, isSaving: isSaving)
            
            break
        case .CURTAINS:
            curtainsAnimation(masterlayer: masterlayer, images : images, index: index, totalCount: totalCount, isSaving: isSaving)

            break
        case .ROLL:
            rollAnimation(masterlayer: masterlayer, images : images, index: index, totalCount: totalCount, isSaving: isSaving)
            
            break
        case .SHAPE:
            shapeAnimation(masterlayer: masterlayer, images : images, index: index, totalCount: totalCount, isSaving: isSaving)
            
            break
        case .BARS:
            barsAnimation(masterlayer: masterlayer, images : images, index: index, totalCount: totalCount, isSaving: isSaving)
            
            break
            
        case .ADVENTURE:
            adventureAnimation(masterlayer: masterlayer, images : images, index: index, totalCount: totalCount, isSaving: isSaving)
            
            break

        case .MANIA:
            maniaAnimation(masterlayer: masterlayer, images : images, index: index, totalCount: totalCount, isSaving: isSaving)
            
            break
        case .ARCHERY:
            archeryAnimation(masterlayer: masterlayer, images : images, index: index, totalCount: totalCount, isSaving: isSaving)
            
            break
        case .DUMBLE:
            dumbleAnimation(masterlayer: masterlayer, images : images, index: index, totalCount: totalCount, isSaving: isSaving)
            
            break
        case .SLICES:
            slicesAnimation(masterlayer: masterlayer, images : images, index: index, totalCount: totalCount, isSaving: isSaving)
            
            break
        case .HEART:
            heartAnimation(masterlayer: masterlayer, images : images, index: index, totalCount: totalCount, isSaving: isSaving)
            break
            
        case .BRUSH:
            brushAnimation(masterlayer: masterlayer, images : images, index: index, totalCount: totalCount, isSaving: isSaving)
            break
            
        case .INK:
            inkAnimation(masterlayer: masterlayer, images : images, index: index, totalCount: totalCount, isSaving: isSaving)
            break
        case .PIZZA:
            pizzaAnimation(masterlayer: masterlayer, images : images, index: index, totalCount: totalCount, isSaving: isSaving)
            break
        case .DIVERT:
            divertAnimation(masterlayer: masterlayer, images : images, index: index, totalCount: totalCount, isSaving: isSaving)
            break
            
        case .COOL:
            coolAnimation(masterlayer: masterlayer, images : images, index: index, totalCount: totalCount, isSaving: isSaving)
            break
            
        case .NOIR:
            noirAnimation(masterlayer: masterlayer, images : images, index: index, totalCount: totalCount, isSaving: isSaving)
            
            break
            
        case .LAUGH:
            laughAnimation(masterlayer: masterlayer, images : images, index: index, totalCount: totalCount, isSaving: isSaving)
            break
        case .FIELD:
            fieldAnimation(masterlayer: masterlayer, images : images, index: index, totalCount: totalCount, isSaving: isSaving)
            break
            
        case .FIREWORK:
            emitterAnimation(masterlayer: masterlayer, images : images, index: index, totalCount: totalCount,emitterType : EMITTER_ANIMATIONS.FIREWORK, isSaving: isSaving)
            
            break
        case .SNOW:
            emitterAnimation(masterlayer: masterlayer, images : images, index: index, totalCount: totalCount,emitterType : EMITTER_ANIMATIONS.SNOW, isSaving: isSaving)
            
            break
        case .FLAME:
            emitterAnimation(masterlayer: masterlayer, images : images, index: index, totalCount: totalCount,emitterType : EMITTER_ANIMATIONS.FLAME, isSaving: isSaving)
            
            break
        case .RAINING:
            emitterAnimation(masterlayer: masterlayer, images : images, index: index, totalCount: totalCount,emitterType : EMITTER_ANIMATIONS.RANING, isSaving: isSaving)
            
            break

        case .CELTIC:
            celticAnimation(masterlayer: masterlayer, images : images, index: index, totalCount: totalCount, isSaving: isSaving)
            
            break
            
        case .FAMILY:
            familyAnimation(masterlayer: masterlayer, image: images[index], index: index, totalCount: totalCount, isSaving: isSaving)
            
            break
            
        case .BABY:
            babyAnimation(masterlayer: masterlayer, images : images, index: index, totalCount: totalCount, isSaving: isSaving)
            
            break
        case .RAINBOW:
            rainbowAnimation(masterlayer: masterlayer, images : images, index: index, totalCount: totalCount, isSaving: isSaving)
            
            
            break
        case .CROSS:
            crossAnimation(masterlayer: masterlayer, images : images, index: index, totalCount: totalCount, isSaving: isSaving)
            
            break
        case .ROMANCE:
            romanceAnimation(masterlayer: masterlayer, images : images, index: index, totalCount: totalCount, isSaving: isSaving)
            
            break
            
        case .MAGICAL:
            magicalGrayAnimation(masterlayer: masterlayer, images : images, index: index, totalCount: totalCount, isSaving: isSaving)
            
            break
            
        case .SNAKE_RANDOM:
            
            self.randomAnimation(masterlayer: masterlayer, images : images, index: index, totalCount: totalCount, isSaving: isSaving)
            
            break
        case .BIRTHDAY:
            birthdayAnimation(masterlayer: masterlayer, images : images, index: index, totalCount: totalCount, isSaving: isSaving)
            
            break
            
        case .STRIPES_VERT:
            stripeVertAnimation(masterlayer: masterlayer, images : images, index: index, totalCount: totalCount, isSaving: isSaving)
            
            break
            
        case .SNAKE_MASKING:
            snakeMaskingAnimation(masterlayer: masterlayer, images : images, index: index, totalCount: totalCount, isSaving: isSaving)
            
            break
            
        case .CHEERFUL:
            cheerfulAnimation(masterlayer: masterlayer, images : images, index: index, totalCount: totalCount, isSaving: isSaving)
            
            break
            
        case .JOYFULL:
            joyfullAnimation(masterlayer: masterlayer, images : images, index: index, totalCount: totalCount, isSaving: isSaving)
            
            break
            
        case .CINEMATIC:
            addCinematicAnimation(masterlayer: masterlayer, image: images[index], index: index, totalCount: totalCount, isSaving: isSaving)
            
            break
            
        case .SHAPEBOUNCE:
            shapeBounceAnimation(masterlayer: masterlayer, image: images[index], index: index, totalCount: totalCount, isSaving: isSaving)
            
            break
            
        case .SNAKE_HORIZ:
            snakeMasterAnimation(masterlayer: masterlayer, images : images, index: index, totalCount: totalCount, isSaving: isSaving,snakeAnimationType: .HORIZONTAL)
            
            break
        case .SNAKE_VERT:
            snakeMasterAnimation(masterlayer: masterlayer, images : images, index: index, totalCount: totalCount, isSaving: isSaving,snakeAnimationType: .VERTICAL)
            
            break
        case .SNAKE_DIAG:
            snakeMasterAnimation(masterlayer: masterlayer, images : images, index: index, totalCount: totalCount, isSaving: isSaving,snakeAnimationType: .DIAGONAL)
            
            break
        case .RESHAPECIRCLE:
            reshapeCircleAnimation(masterlayer: masterlayer, image: images[index], index: index, totalCount: totalCount, isSaving: isSaving)
            
            break
            
        case .ZOOM:
            addZoomAnimation(masterlayer: masterlayer, image: images[index], index: index, totalCount: totalCount, isSaving: isSaving)
            
            break
            
        case .SLOWZOOM:
            addSlowZoomOutAnimation(masterlayer: masterlayer, image: images[index], index: index, totalCount: totalCount, isSaving: isSaving)
            
            break
            
        case .SHAKE:
            addShakeAnimation(masterlayer: masterlayer, image: images[index], index: index, totalCount: totalCount, isSaving: isSaving)
            
            break
            
        case .BIRDSFLY:
            birdsFlyAnimation(masterlayer: masterlayer, images : images, index: index, totalCount: totalCount, isSaving: isSaving)
            
            break
        case .FADE:
            addFadeAnimation(masterlayer: masterlayer, image: images[index], index: index, totalCount: totalCount, isSaving: isSaving)
            
            break
            
            
        default:
            addFadeAnimation(masterlayer: masterlayer, image: images[index], index: index, totalCount: totalCount, isSaving: isSaving)
            
            break
            
        }
        
    }
    func testSampleAnimation(masterlayer : CALayer, images : [UIImage] , index : Int , totalCount : Int , isSaving : Bool){
        
        var image = images[index]
        image = image.squareimage(size: masterlayer.bounds.size)!
        
        let sampleLayer = CALayer()
        sampleLayer.frame = CGRect(x: 0, y: CGFloat(index) * masterlayer.bounds.size.height , width: masterlayer.bounds.size.width , height: masterlayer.bounds.size.height)
        sampleLayer.contents = image.cgImage
        sampleLayer.opacity = 1.0
        sampleLayer.contentsGravity = VideoEdVideoMakeManager.contentsGravity
        masterlayer.addSublayer(sampleLayer)
        
        sampleLayer.anchorPoint = CGPoint(x: 0.5, y: 0)
        let beginTime = AVCoreAnimationBeginTimeAtZero + CFTimeInterval(CGFloat(index+1) * VideoEdVideoMakeManager.durationForOnePic)
        
        let animation1 = CAKeyframeAnimation(keyPath: "position.y")
        animation1.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
        animation1.duration = CFTimeInterval(VideoEdVideoMakeManager.durationForOnePic) * 0.50
        animation1.beginTime = AVCoreAnimationBeginTimeAtZero + CFTimeInterval(CGFloat(index+1) * VideoEdVideoMakeManager.durationForOnePic) -  CFTimeInterval( VideoEdVideoMakeManager.durationForOnePic) * 0.5
        animation1.beginTime = 2
        if index == 0 {
            animation1.values = [CGFloat(index) * masterlayer.frame.height ,  0 - CGFloat(totalCount - index) * masterlayer.frame.height ]
            animation1.keyTimes = [0 , 1]

        }else{
            animation1.values = [ masterlayer.frame.height ,0,0, 0 - CGFloat(totalCount - index) * masterlayer.frame.height ]
            animation1.keyTimes = [0,0.5,0.75, 1]

        }
        
        
        animation1.isRemovedOnCompletion = false
        animation1.fillMode = CAMediaTimingFillMode.both
        
        sampleLayer.add(animation1, forKey: "position.y" + "\(index)")
        


        
        if isSaving {
            masterlayer.isGeometryFlipped = true
        }
        
    }
    func classyAnimation(masterlayer : CALayer, images : [UIImage] , index : Int , totalCount : Int , isSaving : Bool){
        
        
        var image = images[index]
        image = image.squareimage(size: masterlayer.bounds.size)!
        
        let sampleLayer = CALayer()
        sampleLayer.frame = masterlayer.frame
        sampleLayer.contents = image.cgImage
        sampleLayer.opacity = 1.0
        sampleLayer.contentsGravity = VideoEdVideoMakeManager.contentsGravity
        masterlayer.insertSublayer(sampleLayer, at: 0)
        
        let beginTime = AVCoreAnimationBeginTimeAtZero + CFTimeInterval(CGFloat(index+1) * VideoEdVideoMakeManager.durationForOnePic)
        let tenPercent = CGFloat(1200) / 10
        let imageNew = #imageLiteral(resourceName: "theme_10")
        
        let replicatorLayer = CAReplicatorLayer() // Parent Layer
        replicatorLayer.frame = CGRect(x: 0, y: 0, width: sampleLayer.bounds.size.width * 2, height: sampleLayer.bounds.size.width * 2)
        //            replicatorLayer.masksToBounds = true
        let instanceCount = 2 * sampleLayer.bounds.size.width / tenPercent
        replicatorLayer.instanceCount = Int(ceil(instanceCount))
        replicatorLayer.preservesDepth = true
        replicatorLayer.instanceTransform = CATransform3DMakeTranslation(
            tenPercent,tenPercent*0.5, 0
        )
        let imageLayer = CALayer() // Horizontal Layer
        imageLayer.frame = CGRect(x: 0, y: 0.0, width: tenPercent, height: tenPercent)
        imageLayer.contents = imageNew.cgImage
        replicatorLayer.addSublayer(imageLayer)
        
        let animation = CABasicAnimation(keyPath: "bounds.size.height")
        animation.beginTime =  beginTime - 0.5
        animation.duration = 1
        animation.toValue = 0.0
        animation.fillMode = .forwards
        animation.isRemovedOnCompletion = false
        imageLayer.add(animation, forKey: "transform.scale\(index)")
        
        let animationCornerRadius = CABasicAnimation(keyPath: "cornerRadius")
        animationCornerRadius.beginTime =  beginTime - Double(VideoEdVideoMakeManager.durationForOnePic * 0.25)
        animationCornerRadius.duration = CFTimeInterval(VideoEdVideoMakeManager.durationForOnePic * 0.5)
        animationCornerRadius.toValue = 20
        animationCornerRadius.timingFunction = CAMediaTimingFunction(name: .easeInEaseOut)
        animationCornerRadius.fillMode = .forwards
        animationCornerRadius.isRemovedOnCompletion = false
        imageLayer.add(animationCornerRadius , forKey: "cornerRadius\(index)")
        
        let verticalReplicatorLayer = CAReplicatorLayer()
        verticalReplicatorLayer.frame = CGRect(x: sampleLayer.bounds.size.width * -0, y: sampleLayer.bounds.size.width * -0.5, width: sampleLayer.bounds.size.width * 2.0, height: sampleLayer.bounds.size.width * 2.5)
        verticalReplicatorLayer.masksToBounds = true
        verticalReplicatorLayer.instanceCount = Int(ceil(instanceCount))
        verticalReplicatorLayer.instanceTransform = CATransform3DMakeTranslation(
            0, tenPercent, 0
        )
        verticalReplicatorLayer.addSublayer(replicatorLayer)
        if totalCount - 1 > index{
            
            sampleLayer.mask = verticalReplicatorLayer
        }
        
        
        if isSaving {
            masterlayer.isGeometryFlipped = true
        }
        
    }
    func slideAnimation(masterlayer : CALayer, images : [UIImage] , index : Int , totalCount : Int , isSaving : Bool){
        
        var image = images[index]
        
        let sampleLayer = CALayer()
        sampleLayer.frame = masterlayer.frame
        sampleLayer.contents = image.cgImage
        sampleLayer.opacity = 1.0
        sampleLayer.contentsGravity = VideoEdVideoMakeManager.contentsGravity
        masterlayer.insertSublayer(sampleLayer, at: 0)
        
        let beginTime = AVCoreAnimationBeginTimeAtZero + CFTimeInterval(CGFloat(index+1) * VideoEdVideoMakeManager.durationForOnePic)
        
        let animation = CABasicAnimation(keyPath: "transform")
        animation.beginTime = beginTime
        var transform = CATransform3DIdentity
        transform.m34 = -0.002
        animation.duration = 1.25
        animation.fillMode = .both
        animation.isRemovedOnCompletion = false
        
        if(image.size.height > image.size.width){
            
            debugPrint(sampleLayer.position)
            let calHeight = image.size.height / image.size.width * masterlayer.frame.width
            sampleLayer.frame = CGRect(origin: .zero, size: CGSize(width: masterlayer.frame.width, height: calHeight))
            debugPrint(sampleLayer.position)
            sampleLayer.position = CGPoint(x: masterlayer.frame.width/2, y: masterlayer.frame.height/2)
            debugPrint(sampleLayer.position)
            
            
            let scaleAnimation = CAKeyframeAnimation(keyPath: "position.y")
            scaleAnimation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
            scaleAnimation.beginTime = AVCoreAnimationBeginTimeAtZero + CFTimeInterval(CGFloat(index) * VideoEdVideoMakeManager.durationForOnePic)
            scaleAnimation.duration =  CFTimeInterval(VideoEdVideoMakeManager.durationForOnePic)
            if totalCount-1 > index {
                scaleAnimation.values = [masterlayer.bounds.size.height*0.5,masterlayer.bounds.size.height*0.60,sampleLayer.frame.size.height + masterlayer.bounds.size.height*0.5]
            }else{
                scaleAnimation.values = [masterlayer.bounds.size.height*0.5,masterlayer.bounds.size.height*0.60, sampleLayer.frame.size.height  - masterlayer.bounds.size.height ]
            }
            
            scaleAnimation.keyTimes = [0.0,0.5,1.0]
            scaleAnimation.isRemovedOnCompletion = false
            scaleAnimation.fillMode = CAMediaTimingFillMode.forwards
            sampleLayer.add(scaleAnimation, forKey: "position.y\(index)")
            
        }else{
            
            sampleLayer.position = CGPoint(x: masterlayer.frame.width/2, y: masterlayer.frame.height/2)
            let calWidth = image.size.width / image.size.height * masterlayer.frame.height
            sampleLayer.frame = CGRect(origin: .zero, size: CGSize(width: calWidth , height: masterlayer.frame.height))
            
            let scaleAnimation = CAKeyframeAnimation(keyPath: "position.x")
            scaleAnimation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
            scaleAnimation.beginTime = AVCoreAnimationBeginTimeAtZero + CFTimeInterval(CGFloat(index) * VideoEdVideoMakeManager.durationForOnePic)
            scaleAnimation.duration =  CFTimeInterval(VideoEdVideoMakeManager.durationForOnePic)
            if totalCount-1 > index {
                scaleAnimation.values = [masterlayer.bounds.size.width*0.5,masterlayer.bounds.size.width*0.60,sampleLayer.frame.size.width + masterlayer.bounds.size.width*0.5]
            }else{
                scaleAnimation.values = [masterlayer.bounds.size.width*0.5,masterlayer.bounds.size.width*0.60, sampleLayer.frame.size.width  - masterlayer.bounds.size.width ]
            }
            
            scaleAnimation.keyTimes = [0.0,0.5,1.0]
            scaleAnimation.isRemovedOnCompletion = false
            scaleAnimation.fillMode = CAMediaTimingFillMode.forwards
            sampleLayer.add(scaleAnimation, forKey: "position.x\(index)")
            
        }
        
        
        if totalCount-1 > index {
            
            if index % 3 == 0 {
                animation.toValue = CATransform3DRotate(transform, CGFloat(90 * Double.pi / 180.0), 1, 0, 0)
                
            }else if index % 3 == 1{
                animation.toValue = CATransform3DRotate(transform, CGFloat(90 * Double.pi / 180.0), 1, 1, 0)
                
            } else{
                animation.toValue = CATransform3DRotate(transform, CGFloat(90 * Double.pi / 180.0), 0, 1, 0)
            }
            //            sampleLayer.add(animation, forKey: "transform\(index)")
            
        }
        
        DispatchQueue.main.async {
            
            sampleLayer.needsDisplay()
            masterlayer.needsDisplay()
        }
        if !isSaving {
            masterlayer.isGeometryFlipped = true
        }
        
    }
    
    func axisAnimation(masterlayer : CALayer, images : [UIImage] , index : Int , totalCount : Int , isSaving : Bool){
        
        var image = images[index]
        
        let sampleLayer = CALayer()
        sampleLayer.frame = masterlayer.frame
        sampleLayer.contents = image.cgImage
        sampleLayer.opacity = 1.0
        sampleLayer.contentsGravity = VideoEdVideoMakeManager.contentsGravity
        sampleLayer.zPosition = CGFloat(index)*1000*(-1)
        //        masterlayer.insertSublayer(sampleLayer, at: 0)
        masterlayer.addSublayer(sampleLayer)
        
        
        let beginTime = AVCoreAnimationBeginTimeAtZero + CFTimeInterval(CGFloat(index+1) * VideoEdVideoMakeManager.durationForOnePic)
        
        let animation = CABasicAnimation(keyPath: "transform")
        animation.beginTime = beginTime
        var transform = CATransform3DIdentity
        transform.m34 = -0.002
        animation.duration = 1.25
        animation.fillMode = .both
        animation.isRemovedOnCompletion = false
        
        let scaleanimation = CABasicAnimation(keyPath: "transform.scale")
        scaleanimation.beginTime =  beginTime - Double(VideoEdVideoMakeManager.durationForOnePic * 0.25)
        scaleanimation.duration = CFTimeInterval(VideoEdVideoMakeManager.durationForOnePic * 0.5)
        scaleanimation.fromValue = 1
        scaleanimation.toValue = 0
        scaleanimation.timingFunction = CAMediaTimingFunction(name: .easeInEaseOut)
        scaleanimation.fillMode = .both
        scaleanimation.isRemovedOnCompletion = false
        
        
        if totalCount-1 > index {
            
            if index % 2 == 0 {
                animation.toValue = CATransform3DRotate(transform, CGFloat(90 * Double.pi / 180.0), 1, 0, 0)
                
            } else{
                animation.toValue = CATransform3DRotate(transform, CGFloat(90 * Double.pi / 180.0), 0, 1, 0)
            }
            sampleLayer.add(animation, forKey: "transform\(index)")
            sampleLayer.add(scaleanimation , forKey: "size.width\(index)")
            
        }
        
        DispatchQueue.main.async {
            
            sampleLayer.needsDisplay()
            masterlayer.needsDisplay()
        }
        if isSaving {
            masterlayer.isGeometryFlipped = true
        }
        
    }
    
    func friendsAnimation(masterlayer : CALayer, images : [UIImage] , index : Int , totalCount : Int , isSaving : Bool){
        
        let image = images[index]
        let newMasterLayerForIndex = CALayer()
        newMasterLayerForIndex.frame = masterlayer.frame
        newMasterLayerForIndex.contents = image.cgImage
        newMasterLayerForIndex.opacity = 1.0
        newMasterLayerForIndex.contentsGravity = VideoEdVideoMakeManager.contentsGravity
        newMasterLayerForIndex.masksToBounds = true
        
        if index >= 1 {
        
            let scale =  newMasterLayerForIndex.bounds.size.width / 100
            
            let bezierPath = UIBezierPath()
            bezierPath.move(to: CGPoint(x: 0, y: 50 * scale))
            bezierPath.addLine(to: CGPoint(x: 100 * scale, y: 50 * scale))
            UIColor.black.setStroke()
            bezierPath.lineWidth = 1
            bezierPath.stroke()

            
            let maskLayer = CAShapeLayer()
            maskLayer.fillColor = UIColor.clear.cgColor
            maskLayer.strokeColor = UIColor.black.cgColor
            
            maskLayer.path = bezierPath.cgPath
            maskLayer.lineWidth = newMasterLayerForIndex.bounds.width / 4
            maskLayer.strokeEnd = 0.0
            
            newMasterLayerForIndex.mask = maskLayer

            let animationDuration : Double = Double(VideoEdVideoMakeManager.durationForOnePic * 0.20)
            let strokeEndanimation = CABasicAnimation(keyPath: "strokeEnd")
            strokeEndanimation.timingFunction = CAMediaTimingFunction(name: .easeOut)
            strokeEndanimation.duration = animationDuration
            strokeEndanimation.fromValue = 0
            strokeEndanimation.toValue = 1.0
            strokeEndanimation.beginTime =  AVCoreAnimationBeginTimeAtZero + CFTimeInterval( CGFloat(index) * VideoEdVideoMakeManager.durationForOnePic)
            strokeEndanimation.isRemovedOnCompletion = false
            strokeEndanimation.fillMode = .forwards
            maskLayer.add(strokeEndanimation, forKey: "strokeEnd" + "\(index)")

            
            let animation = CABasicAnimation(keyPath: "lineWidth")
            animation.timingFunction = CAMediaTimingFunction(name: .easeIn)
            animation.duration = animationDuration
            animation.fromValue = newMasterLayerForIndex.bounds.width / 4
            animation.toValue = newMasterLayerForIndex.bounds.width
            animation.beginTime =  AVCoreAnimationBeginTimeAtZero + CFTimeInterval( CGFloat(index) * VideoEdVideoMakeManager.durationForOnePic) + animationDuration
            animation.isRemovedOnCompletion = false
            animation.fillMode = .forwards
            maskLayer.add(animation, forKey: "strokeWidth" + "\(index)")

            
        }
//        masterlayer.insertSublayer(newMasterLayerForIndex, at: 0)
        masterlayer.addSublayer(newMasterLayerForIndex)

        if !isSaving {
            masterlayer.isGeometryFlipped = true
        }
        
        
    }

    func shadowAnimation(masterlayer : CALayer, images : [UIImage] , index : Int , totalCount : Int , isSaving : Bool){
        
        var image = images[index]
        image = image.squareimage(size: masterlayer.bounds.size)!
        
        let sampleLayer = CALayer()
        sampleLayer.frame = masterlayer.frame
        sampleLayer.contents = image.cgImage
        sampleLayer.opacity = 1.0
        sampleLayer.contentsGravity = VideoEdVideoMakeManager.contentsGravity
        
        masterlayer.insertSublayer(sampleLayer, at: 0)
        let beginTime = AVCoreAnimationBeginTimeAtZero + CFTimeInterval(CGFloat(index+1) * VideoEdVideoMakeManager.durationForOnePic)
        
        let tenPercent = sampleLayer.bounds.size.width / 30
        let instanceCount = sampleLayer.bounds.size.width / tenPercent
        
        let imageNew = #imageLiteral(resourceName: "theme_10")
        
        let replicatorLayer = CAReplicatorLayer() // Parent Layer
        replicatorLayer.frame.size = sampleLayer.bounds.size
        replicatorLayer.instanceCount = Int(ceil(instanceCount))
        //        replicatorLayer.instanceDelay = 0.001
        replicatorLayer.preservesDepth = true
        replicatorLayer.instanceTransform = CATransform3DMakeTranslation(
            tenPercent,0, 0
        )
        
        // CGFloat(index) * 50
        let imageLayer = CALayer() // Horizontal Layer
        imageLayer.frame = CGRect(x: 0, y: 0.0, width: tenPercent, height: tenPercent)
        imageLayer.contents = imageNew.cgImage
        imageLayer.masksToBounds = true
        replicatorLayer.addSublayer(imageLayer)
        
        
        let animation = CABasicAnimation(keyPath: "transform.scale")
        animation.beginTime =  beginTime - Double(VideoEdVideoMakeManager.durationForOnePic * 0.25)
        animation.duration = CFTimeInterval(VideoEdVideoMakeManager.durationForOnePic * 0.5)
        animation.fromValue = 1
        animation.toValue = 0
        animation.timingFunction = CAMediaTimingFunction(name: .easeInEaseOut)
        animation.fillMode = .forwards
        animation.isRemovedOnCompletion = false
        imageLayer.add(animation , forKey: "size.width\(index)")
        
        let animationCornerRadius = CABasicAnimation(keyPath: "cornerRadius")
        animationCornerRadius.beginTime =  beginTime - Double(VideoEdVideoMakeManager.durationForOnePic * 0.25)
        animationCornerRadius.duration = CFTimeInterval(VideoEdVideoMakeManager.durationForOnePic * 0.5)
        animationCornerRadius.fromValue = 1
        animationCornerRadius.toValue = tenPercent / 2
        animationCornerRadius.timingFunction = CAMediaTimingFunction(name: .easeInEaseOut)
        animationCornerRadius.fillMode = .forwards
        animationCornerRadius.isRemovedOnCompletion = false
        imageLayer.add(animationCornerRadius , forKey: "cornerRadius\(index)")
        
        
        if (index % 2 == 0){
            
            let animationPosition = CABasicAnimation(keyPath: "position.y")
            animationPosition.beginTime =  beginTime - Double(VideoEdVideoMakeManager.durationForOnePic * 0.25)
            animationPosition.duration = CFTimeInterval(VideoEdVideoMakeManager.durationForOnePic * 0.5)
            
            if (index % 4 == 0){
                animationPosition.toValue = masterlayer.frame.width
            }else{
                animationPosition.toValue = -1 * masterlayer.frame.width
                
            }
            animationPosition.timingFunction = CAMediaTimingFunction(name: .easeInEaseOut)
            animationPosition.fillMode = .forwards
            animationPosition.isRemovedOnCompletion = false
            imageLayer.add(animationPosition , forKey: "position.x\(index)")
            
        }else{
            
            let animationPosition = CABasicAnimation(keyPath: "position.x")
            animationPosition.beginTime =  beginTime - Double(VideoEdVideoMakeManager.durationForOnePic * 0.25)
            animationPosition.duration = CFTimeInterval(VideoEdVideoMakeManager.durationForOnePic * 0.5)
            if (arc4random() % 4 == 0){
                animationPosition.toValue = masterlayer.frame.width
            }else{
                animationPosition.toValue = -1 * masterlayer.frame.width
            }
            animationPosition.timingFunction = CAMediaTimingFunction(name: .easeInEaseOut)
            animationPosition.fillMode = .forwards
            animationPosition.isRemovedOnCompletion = false
            imageLayer.add(animationPosition , forKey: "position.x\(index)")
        }
        
        
        let verticalReplicatorLayer = CAReplicatorLayer()
        verticalReplicatorLayer.frame.size = sampleLayer.bounds.size
        verticalReplicatorLayer.masksToBounds = true
        
        let verticalInstanceCount =  sampleLayer.bounds.size.width / tenPercent
        verticalReplicatorLayer.instanceCount = Int(ceil(verticalInstanceCount))
        
        verticalReplicatorLayer.instanceTransform = CATransform3DMakeTranslation(
            0, tenPercent, 0
        )
        
        verticalReplicatorLayer.addSublayer(replicatorLayer)
        
        if totalCount - 1 > index{
            
            sampleLayer.mask = verticalReplicatorLayer
        }
        
        
        if isSaving {
            masterlayer.isGeometryFlipped = true
        }
        
    }

    func pixelAnimation(masterlayer : CALayer, images : [UIImage] , index : Int , totalCount : Int , isSaving : Bool){

        var image = images[index]
        image = image.squareimage(size: masterlayer.bounds.size)!

        let sampleLayer = CALayer()
        sampleLayer.frame = masterlayer.frame
        sampleLayer.contents = image.cgImage
        sampleLayer.opacity = 1.0
        sampleLayer.contentsGravity = VideoEdVideoMakeManager.contentsGravity

        masterlayer.insertSublayer(sampleLayer, at: 0)
        let beginTime = AVCoreAnimationBeginTimeAtZero + CFTimeInterval(CGFloat(index+1) * VideoEdVideoMakeManager.durationForOnePic)

        let tenPercent = sampleLayer.bounds.size.width / 80
        let instanceCount = sampleLayer.bounds.size.width / tenPercent

        let imageNew = #imageLiteral(resourceName: "theme_10")

        let replicatorLayer = CAReplicatorLayer() // Parent Layer
        replicatorLayer.frame.size = sampleLayer.bounds.size
        replicatorLayer.instanceCount = Int(ceil(instanceCount))
//        replicatorLayer.instanceDelay = 0.001
        replicatorLayer.preservesDepth = true
        replicatorLayer.instanceTransform = CATransform3DMakeTranslation(
            tenPercent,0, 0
        )

        // CGFloat(index) * 50
        let imageLayer = CALayer() // Horizontal Layer
        imageLayer.frame = CGRect(x: 0, y: 0.0, width: tenPercent, height: tenPercent)
        imageLayer.contents = imageNew.cgImage
        imageLayer.masksToBounds = true
        replicatorLayer.addSublayer(imageLayer)


        let animation = CABasicAnimation(keyPath: "transform.scale")
        animation.beginTime =  beginTime - Double(VideoEdVideoMakeManager.durationForOnePic * 0.25)
        animation.duration = CFTimeInterval(VideoEdVideoMakeManager.durationForOnePic * 0.5)
        animation.fromValue = 1
        animation.toValue = 0
        animation.timingFunction = CAMediaTimingFunction(name: .easeInEaseOut)
        animation.fillMode = .forwards
        animation.isRemovedOnCompletion = false
        imageLayer.add(animation , forKey: "size.width\(index)")

        let animationCornerRadius = CABasicAnimation(keyPath: "cornerRadius")
        animationCornerRadius.beginTime =  beginTime - Double(VideoEdVideoMakeManager.durationForOnePic * 0.25)
        animationCornerRadius.duration = CFTimeInterval(VideoEdVideoMakeManager.durationForOnePic * 0.5)
        animationCornerRadius.fromValue = 1
        animationCornerRadius.toValue = tenPercent / 2
        animationCornerRadius.timingFunction = CAMediaTimingFunction(name: .easeInEaseOut)
        animationCornerRadius.fillMode = .forwards
        animationCornerRadius.isRemovedOnCompletion = false
        imageLayer.add(animationCornerRadius , forKey: "cornerRadius\(index)")

        let verticalReplicatorLayer = CAReplicatorLayer()
        verticalReplicatorLayer.frame.size = sampleLayer.bounds.size
        verticalReplicatorLayer.masksToBounds = true

        let verticalInstanceCount =  sampleLayer.bounds.size.width / tenPercent
        verticalReplicatorLayer.instanceCount = Int(ceil(verticalInstanceCount))

        verticalReplicatorLayer.instanceTransform = CATransform3DMakeTranslation(
            0, tenPercent, 0
        )

        verticalReplicatorLayer.addSublayer(replicatorLayer)

        if totalCount - 1 > index{

            sampleLayer.mask = verticalReplicatorLayer
        }


        if isSaving {
            masterlayer.isGeometryFlipped = true
        }

    }

    func dotsAnimation(masterlayer : CALayer, images : [UIImage] , index : Int , totalCount : Int , isSaving : Bool){
        
        var image = images[index]
        image = image.squareimage(size: masterlayer.bounds.size)!
        
        let sampleLayer = CALayer()
        sampleLayer.frame = masterlayer.frame
        sampleLayer.contents = image.cgImage
        sampleLayer.opacity = 1.0
        sampleLayer.contentsGravity = VideoEdVideoMakeManager.contentsGravity

        masterlayer.insertSublayer(sampleLayer, at: 0)
        let beginTime = AVCoreAnimationBeginTimeAtZero + CFTimeInterval(CGFloat(index+1) * VideoEdVideoMakeManager.durationForOnePic)
        let tenPercent = sampleLayer.bounds.size.width / 2
        
        let imageNew = #imageLiteral(resourceName: "theme_10")
        
        let replicatorLayer = CAReplicatorLayer() // Parent Layer
        replicatorLayer.frame.size = sampleLayer.bounds.size
        let instanceCount = sampleLayer.bounds.size.width / tenPercent
        replicatorLayer.instanceCount = Int(ceil(instanceCount))
        replicatorLayer.preservesDepth = true
        replicatorLayer.instanceTransform = CATransform3DMakeTranslation(
            tenPercent,0, 0
        )
        
        // CGFloat(index) * 50
        let imageLayer = CALayer() // Horizontal Layer
        imageLayer.frame = CGRect(x: 0, y: 0.0, width: tenPercent, height: tenPercent)
        imageLayer.contents = imageNew.cgImage
        imageLayer.masksToBounds = true
        replicatorLayer.addSublayer(imageLayer)
        
        
        let animation = CABasicAnimation(keyPath: "transform.scale")
        animation.beginTime =  beginTime - Double(VideoEdVideoMakeManager.durationForOnePic * 0.25)
        animation.duration = CFTimeInterval(VideoEdVideoMakeManager.durationForOnePic * 0.5)
        animation.fromValue = 1
        animation.toValue = 0
        animation.timingFunction = CAMediaTimingFunction(name: .easeInEaseOut)
        animation.fillMode = .forwards
        animation.isRemovedOnCompletion = false
        imageLayer.add(animation , forKey: "size.width\(index)")

        let animationCornerRadius = CABasicAnimation(keyPath: "cornerRadius")
        animationCornerRadius.beginTime =  beginTime - Double(VideoEdVideoMakeManager.durationForOnePic * 0.25)
        animationCornerRadius.duration = CFTimeInterval(VideoEdVideoMakeManager.durationForOnePic * 0.5)
        animationCornerRadius.fromValue = 1
        animationCornerRadius.toValue = tenPercent / 2
        animationCornerRadius.timingFunction = CAMediaTimingFunction(name: .easeInEaseOut)
        animationCornerRadius.fillMode = .forwards
        animationCornerRadius.isRemovedOnCompletion = false
        imageLayer.add(animationCornerRadius , forKey: "cornerRadius\(index)")
        
        let verticalReplicatorLayer = CAReplicatorLayer()
        verticalReplicatorLayer.frame.size = sampleLayer.bounds.size
        verticalReplicatorLayer.masksToBounds = true
        
        let verticalInstanceCount =  sampleLayer.bounds.size.width / tenPercent
        verticalReplicatorLayer.instanceCount = Int(ceil(verticalInstanceCount))
        
        verticalReplicatorLayer.instanceTransform = CATransform3DMakeTranslation(
            0, tenPercent, 0
        )

        verticalReplicatorLayer.addSublayer(replicatorLayer)
        
        if totalCount - 1 > index{
            
            sampleLayer.mask = verticalReplicatorLayer
        }
        
        
        if isSaving {
            masterlayer.isGeometryFlipped = true
        }
        
    }
    func curtainsAnimation(masterlayer : CALayer, images : [UIImage] , index : Int , totalCount : Int , isSaving : Bool){
        
        var image = images[index]
        image = image.squareimage(size: masterlayer.bounds.size)!
        
        let sampleLayer = CALayer()
        sampleLayer.frame = masterlayer.frame
        sampleLayer.contents = image.cgImage
        sampleLayer.opacity = 1.0
        sampleLayer.contentsGravity = VideoEdVideoMakeManager.contentsGravity
        masterlayer.insertSublayer(sampleLayer, at: 0)
        
        let beginTime = AVCoreAnimationBeginTimeAtZero + CFTimeInterval(CGFloat(index+1) * VideoEdVideoMakeManager.durationForOnePic)
        
        let tenPercent = sampleLayer.bounds.size.width / 10
        
        let imageNew = #imageLiteral(resourceName: "theme_10")
        
        let replicatorLayer = CAReplicatorLayer() // Parent Layer
        replicatorLayer.frame.size = sampleLayer.bounds.size
        let instanceCount = sampleLayer.bounds.size.width / tenPercent
        replicatorLayer.instanceCount = Int(ceil(instanceCount))
        replicatorLayer.preservesDepth = true
        replicatorLayer.instanceTransform = CATransform3DMakeTranslation(
            tenPercent,0, 0
        )
        
        // CGFloat(index) * 50
        let imageLayer = CALayer() // Horizontal Layer
        imageLayer.frame = CGRect(x: 0, y: 0.0, width: tenPercent, height: tenPercent)
        imageLayer.contents = imageNew.cgImage
        replicatorLayer.addSublayer(imageLayer)
        
        
        //            addNormalOpacityAnimation(sampleLayer: newMasterLayerForIndex, index: index, totalCount: totalCount, isSaving: isSaving)
        let animation = CABasicAnimation(keyPath: "bounds.size.width")
        animation.beginTime =  beginTime - Double(VideoEdVideoMakeManager.durationForOnePic * 0.25)
        animation.duration = CFTimeInterval(VideoEdVideoMakeManager.durationForOnePic * 0.5)
        animation.fromValue = tenPercent
        animation.toValue = 0
        animation.timingFunction = CAMediaTimingFunction(name: .easeInEaseOut)
        animation.fillMode = .forwards
        animation.isRemovedOnCompletion = false
        imageLayer.add(animation, forKey: "transform.scale\(index)")
        
        let verticalReplicatorLayer = CAReplicatorLayer()
        verticalReplicatorLayer.frame.size = sampleLayer.bounds.size
        verticalReplicatorLayer.masksToBounds = true
        //            verticalReplicatorLayer.instanceBlueOffset = colorOffset
        
        let verticalInstanceCount =  sampleLayer.bounds.size.width / tenPercent
        verticalReplicatorLayer.instanceCount = Int(ceil(verticalInstanceCount))
        
        verticalReplicatorLayer.instanceTransform = CATransform3DMakeTranslation(
            0, tenPercent, 0
        )
        
        verticalReplicatorLayer.addSublayer(replicatorLayer)
        
        if totalCount - 1 > index{
            
            sampleLayer.mask = verticalReplicatorLayer
        }
        
        
        if isSaving {
            masterlayer.isGeometryFlipped = true
        }
        
    }

    func rollAnimation(masterlayer : CALayer, images : [UIImage] , index : Int , totalCount : Int , isSaving : Bool){
        
        var image = images[index]
        image = image.squareimage(size: masterlayer.bounds.size)!
        
        let sampleLayer = CALayer()
        sampleLayer.frame = masterlayer.frame
        sampleLayer.contents = image.cgImage
        sampleLayer.opacity = 1.0
        sampleLayer.contentsGravity = VideoEdVideoMakeManager.contentsGravity
        masterlayer.insertSublayer(sampleLayer, at: 0)
        
        let beginTime = AVCoreAnimationBeginTimeAtZero + CFTimeInterval(CGFloat(index+1) * VideoEdVideoMakeManager.durationForOnePic)
        let tenPercent = sampleLayer.bounds.size.width / 10
        let imageNew = #imageLiteral(resourceName: "theme_10")
        
        let replicatorLayer = CAReplicatorLayer() // Parent Layer
        replicatorLayer.frame.size = sampleLayer.bounds.size
        //            replicatorLayer.masksToBounds = true
        let instanceCount = sampleLayer.bounds.size.width / tenPercent
        replicatorLayer.instanceCount = Int(ceil(instanceCount))
        replicatorLayer.preservesDepth = true
        replicatorLayer.instanceTransform = CATransform3DMakeTranslation(
            tenPercent,0, 0
        )
        let imageLayer = CALayer() // Horizontal Layer
        imageLayer.frame = CGRect(x: 0, y: 0.0, width: tenPercent, height: tenPercent)
        imageLayer.contents = imageNew.cgImage
        replicatorLayer.addSublayer(imageLayer)
        
        let animation = CABasicAnimation(keyPath: "transform.scale")
        animation.beginTime =  beginTime - 0.5
        animation.duration = 1
        animation.fromValue = 1
        animation.toValue = 0.0
        //            animation.autoreverses = true
        //            animation.repeatCount = .infinity
        animation.fillMode = .forwards
        animation.isRemovedOnCompletion = false
        imageLayer.add(animation, forKey: "transform.scale\(index)")
        
        let verticalReplicatorLayer = CAReplicatorLayer()
        verticalReplicatorLayer.frame.size = sampleLayer.bounds.size
        verticalReplicatorLayer.masksToBounds = true
        
        let verticalInstanceCount =  sampleLayer.bounds.size.width / tenPercent
        verticalReplicatorLayer.instanceCount = Int(ceil(verticalInstanceCount))
        
        verticalReplicatorLayer.instanceTransform = CATransform3DMakeTranslation(
            0, tenPercent, 0
        )
        verticalReplicatorLayer.addSublayer(replicatorLayer)
        if totalCount - 1 > index{
            
            sampleLayer.mask = verticalReplicatorLayer
        }
        
        
        if isSaving {
            masterlayer.isGeometryFlipped = true
        }
        
    }
    func shapeBezierPath11(sampleLayer : CALayer, index : Int)-> CGPath{
        let scale = sampleLayer.bounds.width / 100
        
        let ovalPath = UIBezierPath(ovalIn: CGRect(x: 31, y: 16, width: 5, height: 5))
        UIColor.gray.setFill()
        ovalPath.fill()
        return ovalPath.cgPath.resizeCGPath(Fitin: CGRect(x: 0, y: 0, width: 5, height: 5).scaleRect(scale: scale))
        
    }
    
    func shapeBezierPath12(sampleLayer : CALayer, index : Int)-> CGPath{
        let scale = sampleLayer.bounds.width / 100
        
        let ovalPath = UIBezierPath(ovalIn: CGRect(x: -19, y: -27, width: 100, height: 100))
        UIColor.gray.setFill()
        ovalPath.fill()
        return ovalPath.cgPath.resizeCGPath(Fitin: CGRect(x: 0, y: 0, width: 100, height:100).scaleRect(scale: scale))
    }
    func shapeBezierPath21(sampleLayer : CALayer, index : Int)-> CGPath{
        let scale = sampleLayer.bounds.width / 100
        
        let ovalPath = UIBezierPath(ovalIn: CGRect(x: 87, y: 12, width: 5, height: 5))
        UIColor.gray.setFill()
        ovalPath.fill()
        return ovalPath.cgPath.resizeCGPath(Fitin: CGRect(x: 0, y: 0, width: 5, height: 5).scaleRect(scale: scale))
        
    }
    
    func shapeBezierPath22(sampleLayer : CALayer, index : Int)-> CGPath{
        let scale = sampleLayer.bounds.width / 100
        
        let ovalPath = UIBezierPath(ovalIn: CGRect(x: 50, y: -25, width: 100, height: 100))
        UIColor.gray.setFill()
        ovalPath.fill()
        return ovalPath.cgPath.resizeCGPath(Fitin: CGRect(x: 0, y: 0, width: 100, height:100).scaleRect(scale: scale))
    }
    func shapeBezierPath31(sampleLayer : CALayer, index : Int)-> CGPath{
        let scale = sampleLayer.bounds.width / 100
        
        let ovalPath = UIBezierPath(ovalIn: CGRect(x: 45, y: 95, width: 5, height: 5))
        UIColor.gray.setFill()
        ovalPath.fill()
        return ovalPath.cgPath.resizeCGPath(Fitin: CGRect(x: 0, y: 0, width: 5, height: 5).scaleRect(scale: scale))
        
    }
    
    func shapeBezierPath32(sampleLayer : CALayer, index : Int)-> CGPath{
        let scale = sampleLayer.bounds.width / 100
        
        let ovalPath = UIBezierPath(ovalIn: CGRect(x: -16, y: 34, width: 150, height: 150))
        UIColor.gray.setFill()
        ovalPath.fill()
        return ovalPath.cgPath.resizeCGPath(Fitin: CGRect(x: 0, y: 0, width: 150, height:150).scaleRect(scale: scale))
    }
    
    func shapeAnimation(masterlayer : CALayer, images : [UIImage] , index : Int , totalCount : Int , isSaving : Bool){
        
        var image = images[index]
        
        image = image.squareimage(size: masterlayer.bounds.size)!
        
        if index == 0{
            
            let newMasterLayerForIndex = CALayer()
            newMasterLayerForIndex.frame = masterlayer.frame
            newMasterLayerForIndex.contents = image.cgImage
            newMasterLayerForIndex.opacity = 1.0
            newMasterLayerForIndex.contentsGravity = VideoEdVideoMakeManager.contentsGravity
            newMasterLayerForIndex.masksToBounds = true
            
            let scaleAnimation = CAKeyframeAnimation(keyPath: "transform.scale")
            scaleAnimation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
            scaleAnimation.beginTime = AVCoreAnimationBeginTimeAtZero + CFTimeInterval(CGFloat(index) * VideoEdVideoMakeManager.durationForOnePic)
            scaleAnimation.duration =  CFTimeInterval(VideoEdVideoMakeManager.durationForOnePic)
            scaleAnimation.values = [1.0,1.1,1.08]
            scaleAnimation.keyTimes = [0.0,0.5,1.0]
            scaleAnimation.isRemovedOnCompletion = false
            scaleAnimation.fillMode = CAMediaTimingFillMode.forwards
            newMasterLayerForIndex.add(scaleAnimation, forKey: "transform.scale\(index)")
            masterlayer.addSublayer(newMasterLayerForIndex)
            
        }else{
            
            
            for  i in 0...2{
                
                let newMasterLayerForIndex = CALayer()
                newMasterLayerForIndex.frame = masterlayer.frame
                newMasterLayerForIndex.contents = image.cgImage
                newMasterLayerForIndex.opacity = 1.0
                newMasterLayerForIndex.contentsGravity = VideoEdVideoMakeManager.contentsGravity
                newMasterLayerForIndex.masksToBounds = true
                

                
                let scaleAnimation = CAKeyframeAnimation(keyPath: "path")
                scaleAnimation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
                scaleAnimation.beginTime = AVCoreAnimationBeginTimeAtZero + CFTimeInterval(CGFloat(index) * VideoEdVideoMakeManager.durationForOnePic)
                scaleAnimation.duration =  CFTimeInterval(VideoEdVideoMakeManager.durationForOnePic)
                if i == 0 {
                    
                    scaleAnimation.values = [shapeBezierPath11(sampleLayer: masterlayer , index : i) , shapeBezierPath12(sampleLayer: masterlayer , index : i)]
                }else if i == 1 {
                    scaleAnimation.values = [shapeBezierPath21(sampleLayer: masterlayer , index : i) , shapeBezierPath22(sampleLayer: masterlayer , index : i)]
                }else{
                    scaleAnimation.values = [shapeBezierPath31(sampleLayer: masterlayer , index : i) , shapeBezierPath32(sampleLayer: masterlayer , index : i)]

                }
                scaleAnimation.keyTimes = [0.0,0.5,1.0]
                scaleAnimation.isRemovedOnCompletion = false
                scaleAnimation.fillMode = CAMediaTimingFillMode.forwards

                let objShapeLayer = CAShapeLayer()

//                let colorStr : String = Source.getAllColorPaletteFromPlist()[i]
//                newMasterLayerForIndex.borderColor = color.cgColor
//                newMasterLayerForIndex.borderWidth = masterlayer.bounds.width * 0.02
                newMasterLayerForIndex.mask = objShapeLayer
                objShapeLayer.add(scaleAnimation, forKey: "transform.scale\(index)")
                masterlayer.addSublayer(newMasterLayerForIndex)
            }
        }
        
        if isSaving {
            masterlayer.isGeometryFlipped = true
        }
        
    }
    
    
    func barsBezierPath1(sampleLayer : CALayer, index : Int)-> CGPath{
        let scale = sampleLayer.bounds.width / 100
        
        let rectanglePath = UIBezierPath(rect: CGRect(x: CGFloat(index) * 6.668, y: 0, width: 6.69, height: 100))
        UIColor.gray.setFill()
        rectanglePath.fill()
        return rectanglePath.cgPath.resizeCGPath(Fitin: CGRect(x: 0, y: 0, width: 6.68  * scale, height: 100  * scale))

        
    }
    func barsAnimation(masterlayer : CALayer, images : [UIImage] , index : Int , totalCount : Int , isSaving : Bool){
        
        var image = images[index]
        
        image = image.squareimage(size: masterlayer.bounds.size)!
        
        if index == 0{
            
            let newMasterLayerForIndex = CALayer()
            newMasterLayerForIndex.frame = masterlayer.frame
            newMasterLayerForIndex.contents = image.cgImage
            newMasterLayerForIndex.opacity = 1.0
            newMasterLayerForIndex.contentsGravity = VideoEdVideoMakeManager.contentsGravity
            newMasterLayerForIndex.masksToBounds = true
            
            let scaleAnimation = CAKeyframeAnimation(keyPath: "transform.scale")
            scaleAnimation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
            scaleAnimation.beginTime = AVCoreAnimationBeginTimeAtZero + CFTimeInterval(CGFloat(index) * VideoEdVideoMakeManager.durationForOnePic)
            scaleAnimation.duration =  CFTimeInterval(VideoEdVideoMakeManager.durationForOnePic)
            scaleAnimation.values = [1.0,1.1,1.08]
            scaleAnimation.keyTimes = [0.0,0.5,1.0]
            scaleAnimation.isRemovedOnCompletion = false
            scaleAnimation.fillMode = CAMediaTimingFillMode.forwards
            newMasterLayerForIndex.add(scaleAnimation, forKey: "transform.scale\(index)")
            masterlayer.addSublayer(newMasterLayerForIndex)
            
        }else{
            
            
            for  i in 0...14{
                
                let newMasterLayerForIndex = CALayer()
                newMasterLayerForIndex.frame = masterlayer.frame
                newMasterLayerForIndex.contents = image.cgImage
                newMasterLayerForIndex.opacity = 1.0
                newMasterLayerForIndex.contentsGravity = VideoEdVideoMakeManager.contentsGravity
                newMasterLayerForIndex.masksToBounds = true
                
                let beginTime = AVCoreAnimationBeginTimeAtZero + CFTimeInterval(CGFloat(index) * VideoEdVideoMakeManager.durationForOnePic)
                
                let translationAnimation = CAKeyframeAnimation(keyPath: "position.x")
                translationAnimation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeOut)
                translationAnimation.beginTime = beginTime
                translationAnimation.duration =  CFTimeInterval(VideoEdVideoMakeManager.durationForOnePic)*0.5
                translationAnimation.values = [CGFloat(i) * newMasterLayerForIndex.bounds.width * 0.1 + (newMasterLayerForIndex.bounds.width),0 ]
                translationAnimation.keyTimes = [0.0,1.0]
                translationAnimation.isRemovedOnCompletion = false
                translationAnimation.fillMode = CAMediaTimingFillMode.both
                
                
                let objShapeLayer = CAShapeLayer()
                objShapeLayer.path = barsBezierPath1(sampleLayer: masterlayer , index : i)
                objShapeLayer.lineWidth = 0
                objShapeLayer.strokeEnd = 1.0
                newMasterLayerForIndex.mask = objShapeLayer
                
                objShapeLayer.add(translationAnimation, forKey: "origin.x\(index)")
                
                
                masterlayer.addSublayer(newMasterLayerForIndex)
            }
        }
        
        if isSaving {
            masterlayer.isGeometryFlipped = true
        }
        
    }
    
    func adventureBezierPath1(sampleLayer : CALayer, index : Int)-> CGPath{
        let scale = sampleLayer.bounds.width / 100
        
        
        if index == 0 {
            
            let bezierPath = UIBezierPath()
            bezierPath.move(to: CGPoint(x: 39.5, y: 74.5))
            bezierPath.addLine(to: CGPoint(x: 81.5, y: -0.5))
            bezierPath.addLine(to: CGPoint(x: 99.5, y: -0.5))
            bezierPath.addLine(to: CGPoint(x: 99.5, y: 62.5))
            bezierPath.addLine(to: CGPoint(x: 39.5, y: 74.5))
            bezierPath.close()
            UIColor.gray.setFill()
            bezierPath.fill()
            UIColor.black.setStroke()
            bezierPath.lineWidth = 1
            bezierPath.stroke()
            return bezierPath.cgPath.resizeCGPath(Fitin: CGRect(x: 0, y: 0, width: 60 * scale, height: 75  * scale))
            
            
        }else if index == 1 {
            
            let bezier2Path = UIBezierPath()
            bezier2Path.move(to: CGPoint(x: 57.5, y: 45.33))
            bezier2Path.addLine(to: CGPoint(x: 0.5, y: 19))
            bezier2Path.addLine(to: CGPoint(x: 0.5, y: 100))
            bezier2Path.addLine(to: CGPoint(x: 26.96, y: 100))
            bezier2Path.addLine(to: CGPoint(x: 57.5, y: 45.33))
            bezier2Path.close()
            UIColor.gray.setFill()
            bezier2Path.fill()
            UIColor.black.setStroke()
            bezier2Path.lineWidth = 1
            bezier2Path.stroke()
            
            return bezier2Path.cgPath.resizeCGPath(Fitin: CGRect(x: 0, y: 0, width: 57 * scale, height: 81  * scale))
            
        }else if index == 2{
            
            let bezier3Path = UIBezierPath()
            bezier3Path.move(to: CGPoint(x: 82.5, y: -0.5))
            bezier3Path.addLine(to: CGPoint(x: 0.5, y: -0.5))
            bezier3Path.addLine(to: CGPoint(x: 0.5, y: 20.5))
            bezier3Path.addLine(to: CGPoint(x: 56.5, y: 45.5))
            bezier3Path.addLine(to: CGPoint(x: 82.5, y: -0.5))
            bezier3Path.close()
            UIColor.gray.setFill()
            bezier3Path.fill()
            UIColor.black.setStroke()
            bezier3Path.lineWidth = 1
            bezier3Path.stroke()
            
            
            return bezier3Path.cgPath.resizeCGPath(Fitin: CGRect(x: 0, y: 0, width: 82 * scale, height: 46  * scale))
            
        }else {
            
            let bezier4Path = UIBezierPath()
            bezier4Path.move(to: CGPoint(x: 100, y: 100))
            bezier4Path.addLine(to: CGPoint(x: 25, y: 100))
            bezier4Path.addLine(to: CGPoint(x: 39.09, y: 73.33))
            bezier4Path.addLine(to: CGPoint(x: 100, y: 60))
            bezier4Path.addLine(to: CGPoint(x: 100, y: 100))
            bezier4Path.close()
            UIColor.gray.setFill()
            bezier4Path.fill()
            UIColor.black.setStroke()
            bezier4Path.lineWidth = 1
            bezier4Path.stroke()

            return bezier4Path.cgPath.resizeCGPath(Fitin: CGRect(x: 0, y: 0, width: 75 * scale, height: 40  * scale))
        }
        
    }
    func adventureAnimation(masterlayer : CALayer, images : [UIImage] , index : Int , totalCount : Int , isSaving : Bool){
        
        var image = images[index]
        
        image = image.squareimage(size: masterlayer.bounds.size)!
        
        if index == 0{
            
            let newMasterLayerForIndex = CALayer()
            newMasterLayerForIndex.frame = masterlayer.frame
            newMasterLayerForIndex.contents = image.cgImage
            newMasterLayerForIndex.opacity = 1.0
            newMasterLayerForIndex.contentsGravity = VideoEdVideoMakeManager.contentsGravity
            newMasterLayerForIndex.masksToBounds = true
            
            let scaleAnimation = CAKeyframeAnimation(keyPath: "transform.scale")
            scaleAnimation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
            scaleAnimation.beginTime = AVCoreAnimationBeginTimeAtZero + CFTimeInterval(CGFloat(index) * VideoEdVideoMakeManager.durationForOnePic)
            scaleAnimation.duration =  CFTimeInterval(VideoEdVideoMakeManager.durationForOnePic)
            scaleAnimation.values = [1.0,1.1,1.08]
            scaleAnimation.keyTimes = [0.0,0.5,1.0]
            scaleAnimation.isRemovedOnCompletion = false
            scaleAnimation.fillMode = CAMediaTimingFillMode.forwards
            newMasterLayerForIndex.add(scaleAnimation, forKey: "transform.scale\(index)")
            masterlayer.addSublayer(newMasterLayerForIndex)
            
        }else{
            
            
            for  i in 0...3{
                
                let newMasterLayerForIndex = CALayer()
                newMasterLayerForIndex.frame = masterlayer.frame
                newMasterLayerForIndex.contents = image.cgImage
                newMasterLayerForIndex.opacity = 1.0
                newMasterLayerForIndex.contentsGravity = VideoEdVideoMakeManager.contentsGravity
                newMasterLayerForIndex.masksToBounds = true
                
                let beginTime = AVCoreAnimationBeginTimeAtZero + CFTimeInterval(CGFloat(index) * VideoEdVideoMakeManager.durationForOnePic)
                
                let translationAnimation = CAKeyframeAnimation(keyPath: "position")
                translationAnimation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
                translationAnimation.beginTime = beginTime //+ Double(i) * 0.2
                translationAnimation.duration =  CFTimeInterval(VideoEdVideoMakeManager.durationForOnePic)*0.5
                
                if i == 0 {
                    translationAnimation.values = [CGPoint(x: newMasterLayerForIndex.frame.width, y: 0) , CGPoint(x: 0, y: 0) ]
                    
                }else if i == 1 {
                    translationAnimation.values = [CGPoint(x: newMasterLayerForIndex.frame.width * (-0.57), y: 0) , CGPoint(x: 0, y: 0) ]

                }  else  if i == 2{
                    translationAnimation.values = [CGPoint(x: 0, y: newMasterLayerForIndex.frame.width * (-0.5)) , CGPoint(x: 0, y: 0) ]

                } else{
                    translationAnimation.values = [CGPoint(x: newMasterLayerForIndex.frame.width * 0.25, y: newMasterLayerForIndex.frame.width) , CGPoint(x: 0, y: 0) ]
                    
                }
                
    
                translationAnimation.keyTimes = [0.0,0.85,1.0]
                translationAnimation.isRemovedOnCompletion = false
                translationAnimation.fillMode = CAMediaTimingFillMode.both
                
                
                let objShapeLayer = CAShapeLayer()
                objShapeLayer.path = adventureBezierPath1(sampleLayer: masterlayer , index : i)
                
                objShapeLayer.lineWidth = 0
                objShapeLayer.strokeEnd = 1.0
                
                newMasterLayerForIndex.mask = objShapeLayer
                
                objShapeLayer.add(translationAnimation, forKey: "origin.x\(index)")
                
                
                masterlayer.addSublayer(newMasterLayerForIndex)
                
            }
        }
        
        if isSaving {
            masterlayer.isGeometryFlipped = true
        }
        
    }
    
    
    func maniaBezierPath1(sampleLayer : CALayer, index : Int)-> CGPath{
        let scale = sampleLayer.bounds.width / 100
        
        
        if index == 0 {
            
            let bezierPath = UIBezierPath()
            bezierPath.move(to: CGPoint(x: 75, y: 0))
            bezierPath.addLine(to: CGPoint(x: 18, y: 100))
            bezierPath.addLine(to: CGPoint(x: 0, y: 100))
            bezierPath.addLine(to: CGPoint(x: 0, y: 0))
            bezierPath.addLine(to: CGPoint(x: 75, y: 0))
            bezierPath.close()
            UIColor.gray.setFill()
            bezierPath.fill()
            UIColor.black.setStroke()
            bezierPath.lineWidth = 1
            bezierPath.stroke()
            
            
            return bezierPath.cgPath.resizeCGPath(Fitin: CGRect(x: 0, y: 0, width: 75 * scale, height: 100  * scale))
            
            
        }else if index == 1 {
            
            let bezier2Path = UIBezierPath()
            bezier2Path.move(to: CGPoint(x: 44, y: 54))
            bezier2Path.addLine(to: CGPoint(x: 100, y: 80))
            bezier2Path.addLine(to: CGPoint(x: 100, y: 0))
            bezier2Path.addLine(to: CGPoint(x: 74, y: 0))
            bezier2Path.addLine(to: CGPoint(x: 44, y: 54))
            bezier2Path.close()
            UIColor.gray.setFill()
            bezier2Path.fill()
            UIColor.black.setStroke()
            bezier2Path.lineWidth = 1
            bezier2Path.stroke()
            
            
            return bezier2Path.cgPath.resizeCGPath(Fitin: CGRect(x: 0, y: 0, width: 56 * scale, height: 80  * scale))
            
        }else {
            
            let bezier3Path = UIBezierPath()
            bezier3Path.move(to: CGPoint(x: 18, y: 100))
            bezier3Path.addLine(to: CGPoint(x: 100, y: 100))
            bezier3Path.addLine(to: CGPoint(x: 100, y: 79))
            bezier3Path.addLine(to: CGPoint(x: 44, y: 54))
            bezier3Path.addLine(to: CGPoint(x: 18, y: 100))
            bezier3Path.close()
            UIColor.gray.setFill()
            bezier3Path.fill()
            UIColor.black.setStroke()
            bezier3Path.lineWidth = 1
            bezier3Path.stroke()
            
            return bezier3Path.cgPath.resizeCGPath(Fitin: CGRect(x: 0, y: 0, width: 82 * scale, height: 46  * scale))
            
        }
        
    }
    func maniaAnimation(masterlayer : CALayer, images : [UIImage] , index : Int , totalCount : Int , isSaving : Bool){
        
        var image = images[index]
        
        image = image.squareimage(size: masterlayer.bounds.size)!
        
        if index == 0{
            
            let newMasterLayerForIndex = CALayer()
            newMasterLayerForIndex.frame = masterlayer.frame
            newMasterLayerForIndex.contents = image.cgImage
            newMasterLayerForIndex.opacity = 1.0
            newMasterLayerForIndex.contentsGravity = VideoEdVideoMakeManager.contentsGravity
            newMasterLayerForIndex.masksToBounds = true
            
            let scaleAnimation = CAKeyframeAnimation(keyPath: "transform.scale")
            scaleAnimation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
            scaleAnimation.beginTime = AVCoreAnimationBeginTimeAtZero + CFTimeInterval(CGFloat(index) * VideoEdVideoMakeManager.durationForOnePic)
            scaleAnimation.duration =  CFTimeInterval(VideoEdVideoMakeManager.durationForOnePic) //+ 0.19
            scaleAnimation.values = [1.0,1.1,1.08] // zoomout
            scaleAnimation.keyTimes = [0.0,0.5,1.0]
            scaleAnimation.isRemovedOnCompletion = false
            scaleAnimation.fillMode = CAMediaTimingFillMode.forwards
            newMasterLayerForIndex.add(scaleAnimation, forKey: "transform.scale\(index)")
            masterlayer.addSublayer(newMasterLayerForIndex)
            
        }else{
            
            
            for  i in 0...2{
                
                let newMasterLayerForIndex = CALayer()
                newMasterLayerForIndex.frame = masterlayer.frame
                newMasterLayerForIndex.contents = image.cgImage
                newMasterLayerForIndex.opacity = 1.0
                newMasterLayerForIndex.contentsGravity = VideoEdVideoMakeManager.contentsGravity
                newMasterLayerForIndex.masksToBounds = true
                
                let beginTime = AVCoreAnimationBeginTimeAtZero + CFTimeInterval(CGFloat(index) * VideoEdVideoMakeManager.durationForOnePic)
                
                let translationAnimation = CAKeyframeAnimation(keyPath: "position")
                translationAnimation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
                translationAnimation.beginTime = beginTime //+ Double(i) * 0.2
                translationAnimation.duration =  CFTimeInterval(VideoEdVideoMakeManager.durationForOnePic)*0.5
                
                
                
                if i == 0 {
                    translationAnimation.values = [CGPoint(x: newMasterLayerForIndex.frame.width*(-1.55), y: newMasterLayerForIndex.frame.width*0.48) , CGPoint(x: newMasterLayerForIndex.frame.width/2, y: newMasterLayerForIndex.frame.width/2) ]
                    
                }else if i == 1 {
                    translationAnimation.values = [CGPoint(x: newMasterLayerForIndex.frame.width+20, y: 0) , CGPoint(x: newMasterLayerForIndex.frame.width/2, y: newMasterLayerForIndex.frame.width/2) ]
                    
                }  else{
                    translationAnimation.values = [CGPoint(x: newMasterLayerForIndex.frame.width*(0.18), y: newMasterLayerForIndex.frame.width) , CGPoint(x: newMasterLayerForIndex.frame.width/2, y: newMasterLayerForIndex.frame.width/2) ]
                    
                }
                
                
                translationAnimation.keyTimes = [0.0,0.85,1.0]
                translationAnimation.isRemovedOnCompletion = false
                translationAnimation.fillMode = CAMediaTimingFillMode.both
                
                
                let objShapeLayer = CAShapeLayer()
                objShapeLayer.path = maniaBezierPath1(sampleLayer: masterlayer , index : i)
                
                objShapeLayer.lineWidth = 0
                objShapeLayer.strokeEnd = 1.0
                
                newMasterLayerForIndex.mask = objShapeLayer
                
                newMasterLayerForIndex.add(translationAnimation, forKey: "origin.x\(index)")
                
                
                masterlayer.addSublayer(newMasterLayerForIndex)
                
            }
        }
        
        
        
        if !isSaving {
            //            masterlayer.isGeometryFlipped = true
        }
        
    }
    
    func archeryBezierPath1(sampleLayer : CALayer, index : Int)-> CGPath{
        let scale = sampleLayer.bounds.width / 100
        
        
        
        if index == 0 {
            
            let bezier5Path = UIBezierPath()
            bezier5Path.move(to: CGPoint(x: 52.5, y: -52.5))
            bezier5Path.addLine(to: CGPoint(x: -52.5, y: 50.5))
            bezier5Path.addLine(to: CGPoint(x: -39.5, y: 62.5))
            bezier5Path.addLine(to: CGPoint(x: 65.5, y: -40.5))
            bezier5Path.addLine(to: CGPoint(x: 52.5, y: -52.5))
            UIColor.black.setStroke()
            bezier5Path.lineWidth = 1
            bezier5Path.stroke()

            return bezier5Path.cgPath.resizeCGPath(Fitin: CGRect(x: 0, y: 0, width: 118 * scale, height: 115  * scale))

            
        }else if index == 1 {
            
            let bezier3Path = UIBezierPath()
            bezier3Path.move(to: CGPoint(x: 72, y: -27.5))
            bezier3Path.addLine(to: CGPoint(x: -33, y: 75.5))
            bezier3Path.addLine(to: CGPoint(x: -21, y: 86.5))
            bezier3Path.addLine(to: CGPoint(x: 45.71, y: 21.06))
            bezier3Path.addLine(to: CGPoint(x: 84, y: -16.5))
            bezier3Path.addLine(to: CGPoint(x: 72, y: -27.5))
            UIColor.black.setStroke()
            bezier3Path.lineWidth = 1
            bezier3Path.stroke()

            return bezier3Path.cgPath.resizeCGPath(Fitin: CGRect(x: 0, y: 0, width: 117 * scale, height: 114  * scale))
            
        }else if index == 2 {
            
            let bezierPath = UIBezierPath()
            bezierPath.move(to: CGPoint(x: 94.5, y: -6.5))
            bezierPath.addLine(to: CGPoint(x: -10.5, y: 96.5))
            bezierPath.addLine(to: CGPoint(x: 2.5, y: 108.5))
            bezierPath.addLine(to: CGPoint(x: 69.21, y: 43.06))
            bezierPath.addLine(to: CGPoint(x: 107.5, y: 5.5))
            bezierPath.addLine(to: CGPoint(x: 94.5, y: -6.5))
            bezierPath.close()
            UIColor.black.setStroke()
            bezierPath.lineWidth = 1
            bezierPath.stroke()
            return bezierPath.cgPath.resizeCGPath(Fitin: CGRect(x: 0, y: 0, width: 118 * scale, height: 115  * scale))
            
            
            
        }else  if index == 3{
            
            let bezier7Path = UIBezierPath()
            bezier7Path.move(to: CGPoint(x: 115.5, y: 18.5))
            bezier7Path.addLine(to: CGPoint(x: 10.5, y: 121.5))
            bezier7Path.addLine(to: CGPoint(x: 23.5, y: 133.5))
            bezier7Path.addLine(to: CGPoint(x: 90.21, y: 68.06))
            bezier7Path.addLine(to: CGPoint(x: 128.5, y: 30.5))
            bezier7Path.addLine(to: CGPoint(x: 115.5, y: 18.5))
            UIColor.black.setStroke()
            bezier7Path.lineWidth = 1
            bezier7Path.stroke()
            return bezier7Path.cgPath.resizeCGPath(Fitin: CGRect(x: 0, y: 0, width: 118 * scale, height: 115  * scale))
            
        }else  if index == 4{
            
            let bezier9Path = UIBezierPath()
            bezier9Path.move(to: CGPoint(x: 140.5, y: 41))
            bezier9Path.addLine(to: CGPoint(x: 35.5, y: 144))
            bezier9Path.addLine(to: CGPoint(x: 47.5, y: 155))
            bezier9Path.addLine(to: CGPoint(x: 114.21, y: 89.56))
            bezier9Path.addLine(to: CGPoint(x: 152.5, y: 52))
            bezier9Path.addLine(to: CGPoint(x: 140.5, y: 41))
            UIColor.black.setStroke()
            bezier9Path.lineWidth = 1
            bezier9Path.stroke()
            return bezier9Path.cgPath.resizeCGPath(Fitin: CGRect(x: 0, y: 0, width: 117 * scale, height: 114  * scale))
            
        }else  if index == 5{
            
            let bezier4Path = UIBezierPath()
            bezier4Path.move(to: CGPoint(x: 63.5, y: -39.5))
            bezier4Path.addLine(to: CGPoint(x: -41.5, y: 63.5))
            bezier4Path.addLine(to: CGPoint(x: -29.5, y: 74.5))
            bezier4Path.addLine(to: CGPoint(x: 37.21, y: 9.06))
            bezier4Path.addLine(to: CGPoint(x: 75.5, y: -28.5))
            bezier4Path.addLine(to: CGPoint(x: 63.5, y: -39.5))
            UIColor.black.setStroke()
            bezier4Path.lineWidth = 1
            bezier4Path.stroke()

            
            return bezier4Path.cgPath.resizeCGPath(Fitin: CGRect(x: 0, y: 0, width: 117 * scale, height: 114  * scale))
            
        }else  if index == 6{
            
            let bezier2Path = UIBezierPath()
            bezier2Path.move(to: CGPoint(x: 81, y: -14.5))
            bezier2Path.addLine(to: CGPoint(x: -24, y: 88.5))
            bezier2Path.addLine(to: CGPoint(x: -12, y: 99.5))
            bezier2Path.addLine(to: CGPoint(x: 54.71, y: 34.06))
            bezier2Path.addLine(to: CGPoint(x: 93, y: -3.5))
            bezier2Path.addLine(to: CGPoint(x: 81, y: -14.5))
            UIColor.black.setStroke()
            bezier2Path.lineWidth = 1
            bezier2Path.stroke()

            return bezier2Path.cgPath.resizeCGPath(Fitin: CGRect(x: 0, y: 0, width: 117 * scale, height: 114  * scale))
            
        }else if index == 7{
            
            let bezier6Path = UIBezierPath()
            bezier6Path.move(to: CGPoint(x: 105.5, y: 7))
            bezier6Path.addLine(to: CGPoint(x: 0.5, y: 110))
            bezier6Path.addLine(to: CGPoint(x: 12.5, y: 121))
            bezier6Path.addLine(to: CGPoint(x: 79.21, y: 55.56))
            bezier6Path.addLine(to: CGPoint(x: 117.5, y: 18))
            bezier6Path.addLine(to: CGPoint(x: 105.5, y: 7))
            UIColor.black.setStroke()
            bezier6Path.lineWidth = 1
            bezier6Path.stroke()
            return bezier6Path.cgPath.resizeCGPath(Fitin: CGRect(x: 0, y: 0, width: 117 * scale, height: 114  * scale))
            
        }else {
            
            let bezier8Path = UIBezierPath()
            bezier8Path.move(to: CGPoint(x: 128, y: 30.5))
            bezier8Path.addLine(to: CGPoint(x: 23, y: 133.5))
            bezier8Path.addLine(to: CGPoint(x: 35, y: 144.5))
            bezier8Path.addLine(to: CGPoint(x: 101.71, y: 79.06))
            bezier8Path.addLine(to: CGPoint(x: 140, y: 41.5))
            bezier8Path.addLine(to: CGPoint(x: 128, y: 30.5))
            UIColor.black.setStroke()
            bezier8Path.lineWidth = 1
            bezier8Path.stroke()

            return bezier8Path.cgPath.resizeCGPath(Fitin: CGRect(x: 0, y: 0, width: 117 * scale, height: 114  * scale))
            
        }
        
    }
    
    func archeryAnimation(masterlayer : CALayer, images : [UIImage] , index : Int , totalCount : Int , isSaving : Bool){
        
        var image = images[index]
        
        image = image.squareimage(size: masterlayer.bounds.size)!
        
        if index == 0{
            
            let newMasterLayerForIndex = CALayer()
            newMasterLayerForIndex.frame = masterlayer.frame
            newMasterLayerForIndex.contents = image.cgImage
            newMasterLayerForIndex.opacity = 1.0
            newMasterLayerForIndex.contentsGravity = VideoEdVideoMakeManager.contentsGravity
            newMasterLayerForIndex.masksToBounds = true
            
            let scaleAnimation = CAKeyframeAnimation(keyPath: "transform.scale")
            scaleAnimation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
            scaleAnimation.beginTime = AVCoreAnimationBeginTimeAtZero + CFTimeInterval(CGFloat(index) * VideoEdVideoMakeManager.durationForOnePic)
            scaleAnimation.duration =  CFTimeInterval(VideoEdVideoMakeManager.durationForOnePic) //+ 0.19
            scaleAnimation.values = [1.0,1.1,1.08] // zoomout
            scaleAnimation.keyTimes = [0.0,0.5,1.0]
            scaleAnimation.isRemovedOnCompletion = false
            scaleAnimation.fillMode = CAMediaTimingFillMode.forwards
            newMasterLayerForIndex.add(scaleAnimation, forKey: "transform.scale\(index)")
            masterlayer.addSublayer(newMasterLayerForIndex)
            
        }else{
            
            
            for  i in 0...8{
                
                let newMasterLayerForIndex = CALayer()
                newMasterLayerForIndex.frame = masterlayer.frame
                newMasterLayerForIndex.contents = image.cgImage
                newMasterLayerForIndex.opacity = 1.0
                newMasterLayerForIndex.contentsGravity = VideoEdVideoMakeManager.contentsGravity
                newMasterLayerForIndex.masksToBounds = true
                
                let beginTime = AVCoreAnimationBeginTimeAtZero + CFTimeInterval(CGFloat(index) * VideoEdVideoMakeManager.durationForOnePic)
                
                let translationAnimation = CAKeyframeAnimation(keyPath: "position")
                translationAnimation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
                translationAnimation.beginTime = beginTime //+ Double(i) * 0.2
                translationAnimation.duration =  CFTimeInterval(VideoEdVideoMakeManager.durationForOnePic)*0.5
                
                if i <= 4{
                    
                    
                    if i == 0 {
                        translationAnimation.values = [CGPoint(x: newMasterLayerForIndex.frame.width*(-1.55), y: newMasterLayerForIndex.frame.width*0.48) , CGPoint(x: newMasterLayerForIndex.frame.width/2, y: newMasterLayerForIndex.frame.width/2) ]

                    }else if i == 1 {
                        translationAnimation.values = [CGPoint(x: newMasterLayerForIndex.frame.width*(-1.35), y: newMasterLayerForIndex.frame.width*0.74) , CGPoint(x: newMasterLayerForIndex.frame.width/2, y: newMasterLayerForIndex.frame.width/2) ]
                        
                    } else if i == 2 {
                        translationAnimation.values = [CGPoint(x: newMasterLayerForIndex.frame.width*(-1.17), y: newMasterLayerForIndex.frame.width*0.99) , CGPoint(x: newMasterLayerForIndex.frame.width/2, y: newMasterLayerForIndex.frame.width/2) ]
                        
                    } else if i == 3 {
                        translationAnimation.values = [CGPoint(x: newMasterLayerForIndex.frame.width*(-0.92), y: newMasterLayerForIndex.frame.width*1.19) , CGPoint(x: newMasterLayerForIndex.frame.width/2, y: newMasterLayerForIndex.frame.width/2) ]
                        
                    } else{
                        translationAnimation.values = [CGPoint(x: newMasterLayerForIndex.frame.width*(-0.67), y: newMasterLayerForIndex.frame.width*1.42) , CGPoint(x: newMasterLayerForIndex.frame.width/2, y: newMasterLayerForIndex.frame.width/2) ]

                    }

                }else{

                    if i == 5 {
                        translationAnimation.values = [CGPoint(x: newMasterLayerForIndex.frame.width*(0.64), y: newMasterLayerForIndex.frame.width*(-1.46)) , CGPoint(x: newMasterLayerForIndex.frame.width/2, y: newMasterLayerForIndex.frame.width/2) ]
                        
                    }else if i == 6 {
                        translationAnimation.values = [CGPoint(x: newMasterLayerForIndex.frame.width*(0.82), y: newMasterLayerForIndex.frame.width*(-1.21)) , CGPoint(x: newMasterLayerForIndex.frame.width/2, y: newMasterLayerForIndex.frame.width/2) ]
                        
                    } else if i == 7 {
                        translationAnimation.values = [CGPoint(x: newMasterLayerForIndex.frame.width*(1.06), y: newMasterLayerForIndex.frame.width*(-1)) , CGPoint(x: newMasterLayerForIndex.frame.width/2, y: newMasterLayerForIndex.frame.width/2) ]
                        
                    } else{
                        translationAnimation.values = [CGPoint(x: newMasterLayerForIndex.frame.width*(1.29), y: newMasterLayerForIndex.frame.width*(-0.76)) , CGPoint(x: newMasterLayerForIndex.frame.width/2, y: newMasterLayerForIndex.frame.width/2) ]
                        
                    }
                }
                
                translationAnimation.keyTimes = [0.0,0.85,1.0]
                translationAnimation.isRemovedOnCompletion = false
                translationAnimation.fillMode = CAMediaTimingFillMode.both
                
                
                let objShapeLayer = CAShapeLayer()
                objShapeLayer.path = archeryBezierPath1(sampleLayer: masterlayer , index : i)
                
                objShapeLayer.lineWidth = 0
                objShapeLayer.strokeEnd = 1.0
                
                newMasterLayerForIndex.mask = objShapeLayer
                
                newMasterLayerForIndex.add(translationAnimation, forKey: "origin.x\(index)")
                
                
                masterlayer.addSublayer(newMasterLayerForIndex)
                
            }
        }
        
        
        
        if isSaving {
            masterlayer.isGeometryFlipped = true
        }
        
    }

    func dumbleBezierPath1(sampleLayer : CALayer, index : Int)-> CGPath{
        let scale = sampleLayer.bounds.width / 100
        
        
        
        if index == 0 {
            
            let bezier3Path = UIBezierPath()
            bezier3Path.move(to: CGPoint(x: 25, y: 0))
            bezier3Path.addCurve(to: CGPoint(x: 25, y: 29), controlPoint1: CGPoint(x: 25, y: 0), controlPoint2: CGPoint(x: 25, y: 29))
            bezier3Path.addLine(to: CGPoint(x: 22, y: 29))
            bezier3Path.addCurve(to: CGPoint(x: 15.18, y: 21.28), controlPoint1: CGPoint(x: 21.43, y: 25.22), controlPoint2: CGPoint(x: 18.7, y: 22.18))
            bezier3Path.addCurve(to: CGPoint(x: 12.98, y: 21), controlPoint1: CGPoint(x: 14.47, y: 21.1), controlPoint2: CGPoint(x: 13.74, y: 21))
            bezier3Path.addCurve(to: CGPoint(x: 3.96, y: 29), controlPoint1: CGPoint(x: 8.43, y: 21), controlPoint2: CGPoint(x: 4.65, y: 24.47))
            bezier3Path.addLine(to: CGPoint(x: 0, y: 29))
            bezier3Path.addLine(to: CGPoint(x: 0, y: 0))
            bezier3Path.addLine(to: CGPoint(x: 25, y: 0))
            bezier3Path.addLine(to: CGPoint(x: 25, y: 0))
            bezier3Path.close()
            UIColor.lightGray.setFill()
            bezier3Path.fill()
            return bezier3Path.cgPath.resizeCGPath(Fitin: CGRect(x: 0, y: 0, width: 25 * scale, height: 29  * scale))

            
        }else if index == 1 {
            
            let bezier5Path = UIBezierPath()
            bezier5Path.move(to: CGPoint(x: 40.39, y: 78.72))
            bezier5Path.addCurve(to: CGPoint(x: 47.76, y: 71), controlPoint1: CGPoint(x: 44.2, y: 77.82), controlPoint2: CGPoint(x: 47.14, y: 74.78))
            bezier5Path.addLine(to: CGPoint(x: 51, y: 71))
            bezier5Path.addLine(to: CGPoint(x: 51, y: 0))
            bezier5Path.addLine(to: CGPoint(x: 24, y: 0))
            bezier5Path.addLine(to: CGPoint(x: 24, y: 71))
            bezier5Path.addLine(to: CGPoint(x: 28.28, y: 71))
            bezier5Path.addCurve(to: CGPoint(x: 38.02, y: 79), controlPoint1: CGPoint(x: 29.02, y: 75.53), controlPoint2: CGPoint(x: 33.1, y: 79))
            bezier5Path.addCurve(to: CGPoint(x: 40.39, y: 78.72), controlPoint1: CGPoint(x: 38.84, y: 79), controlPoint2: CGPoint(x: 39.63, y: 78.9))
            bezier5Path.close()
            UIColor.gray.setFill()
            bezier5Path.fill()
            return bezier5Path.cgPath.resizeCGPath(Fitin: CGRect(x: 0, y: 0, width: 27 * scale, height: 79  * scale))
            
        }else if index == 2 {
            
            let bezier2Path = UIBezierPath()
            bezier2Path.move(to: CGPoint(x: 76, y: 0))
            bezier2Path.addCurve(to: CGPoint(x: 76, y: 29), controlPoint1: CGPoint(x: 76, y: 0), controlPoint2: CGPoint(x: 76, y: 29))
            bezier2Path.addLine(to: CGPoint(x: 72.88, y: 29))
            bezier2Path.addCurve(to: CGPoint(x: 65.78, y: 21.28), controlPoint1: CGPoint(x: 72.28, y: 25.22), controlPoint2: CGPoint(x: 69.45, y: 22.18))
            bezier2Path.addCurve(to: CGPoint(x: 63.5, y: 21), controlPoint1: CGPoint(x: 65.05, y: 21.1), controlPoint2: CGPoint(x: 64.29, y: 21))
            bezier2Path.addCurve(to: CGPoint(x: 54.12, y: 29), controlPoint1: CGPoint(x: 58.76, y: 21), controlPoint2: CGPoint(x: 54.84, y: 24.47))
            bezier2Path.addLine(to: CGPoint(x: 50, y: 29))
            bezier2Path.addLine(to: CGPoint(x: 50, y: 0))
            bezier2Path.addLine(to: CGPoint(x: 76, y: 0))
            bezier2Path.addLine(to: CGPoint(x: 76, y: 0))
            bezier2Path.close()
            UIColor.lightGray.setFill()
            bezier2Path.fill()
            return bezier2Path.cgPath.resizeCGPath(Fitin: CGRect(x: 0, y: 0, width: 26 * scale, height: 29  * scale))


            
        }else  if index == 3{
            
            let bezier7Path = UIBezierPath()
            bezier7Path.move(to: CGPoint(x: 90.18, y: 78.72))
            bezier7Path.addCurve(to: CGPoint(x: 97, y: 71), controlPoint1: CGPoint(x: 93.7, y: 77.82), controlPoint2: CGPoint(x: 96.43, y: 74.78))
            bezier7Path.addLine(to: CGPoint(x: 100, y: 71))
            bezier7Path.addLine(to: CGPoint(x: 100, y: 0))
            bezier7Path.addLine(to: CGPoint(x: 75, y: 0))
            bezier7Path.addLine(to: CGPoint(x: 75, y: 71))
            bezier7Path.addLine(to: CGPoint(x: 78.96, y: 71))
            bezier7Path.addCurve(to: CGPoint(x: 87.98, y: 79), controlPoint1: CGPoint(x: 79.65, y: 75.53), controlPoint2: CGPoint(x: 83.43, y: 79))
            bezier7Path.addCurve(to: CGPoint(x: 90.18, y: 78.72), controlPoint1: CGPoint(x: 88.74, y: 79), controlPoint2: CGPoint(x: 89.47, y: 78.9))
            bezier7Path.close()
            UIColor.gray.setFill()
            bezier7Path.fill()
            return bezier7Path.cgPath.resizeCGPath(Fitin: CGRect(x: 0, y: 0, width: 25 * scale, height: 79  * scale))

        }else  if index == 4{
            
            let bezier4Path = UIBezierPath()
            bezier4Path.move(to: CGPoint(x: 15.18, y: 20.28))
            bezier4Path.addCurve(to: CGPoint(x: 22, y: 28.1), controlPoint1: CGPoint(x: 18.7, y: 21.2), controlPoint2: CGPoint(x: 21.43, y: 24.27))
            bezier4Path.addLine(to: CGPoint(x: 25, y: 28.1))
            bezier4Path.addLine(to: CGPoint(x: 25, y: 100))
            bezier4Path.addLine(to: CGPoint(x: 0, y: 100))
            bezier4Path.addLine(to: CGPoint(x: 0, y: 28.1))
            bezier4Path.addLine(to: CGPoint(x: 3.96, y: 28.1))
            bezier4Path.addCurve(to: CGPoint(x: 12.98, y: 20), controlPoint1: CGPoint(x: 4.65, y: 23.51), controlPoint2: CGPoint(x: 8.43, y: 20))
            bezier4Path.addCurve(to: CGPoint(x: 15.18, y: 20.28), controlPoint1: CGPoint(x: 13.74, y: 20), controlPoint2: CGPoint(x: 14.47, y: 20.1))
            bezier4Path.close()
            UIColor.lightGray.setFill()
            bezier4Path.fill()
            return bezier4Path.cgPath.resizeCGPath(Fitin: CGRect(x: 0, y: 0, width: 25 * scale, height: 80  * scale))

        }else  if index == 5{
            
            let bezier6Path = UIBezierPath()
            bezier6Path.move(to: CGPoint(x: 51, y: 100))
            bezier6Path.addCurve(to: CGPoint(x: 51, y: 70), controlPoint1: CGPoint(x: 51, y: 100), controlPoint2: CGPoint(x: 51, y: 70))
            bezier6Path.addLine(to: CGPoint(x: 47.76, y: 70))
            bezier6Path.addCurve(to: CGPoint(x: 40.39, y: 77.99), controlPoint1: CGPoint(x: 47.14, y: 73.91), controlPoint2: CGPoint(x: 44.2, y: 77.05))
            bezier6Path.addCurve(to: CGPoint(x: 38.02, y: 78.28), controlPoint1: CGPoint(x: 39.63, y: 78.18), controlPoint2: CGPoint(x: 38.84, y: 78.28))
            bezier6Path.addCurve(to: CGPoint(x: 28.28, y: 70), controlPoint1: CGPoint(x: 33.1, y: 78.28), controlPoint2: CGPoint(x: 29.02, y: 74.69))
            bezier6Path.addLine(to: CGPoint(x: 24, y: 70))
            bezier6Path.addLine(to: CGPoint(x: 24, y: 100))
            bezier6Path.addLine(to: CGPoint(x: 51, y: 100))
            bezier6Path.addLine(to: CGPoint(x: 51, y: 100))
            bezier6Path.close()
            UIColor.gray.setFill()
            bezier6Path.fill()

            return bezier6Path.cgPath.resizeCGPath(Fitin: CGRect(x: 0, y: 0, width: 27 * scale, height: 30  * scale))

        }else  if index == 6{
            
            let bezierPath = UIBezierPath()
            bezierPath.move(to: CGPoint(x: 65.78, y: 20.28))
            bezierPath.addCurve(to: CGPoint(x: 72.88, y: 28.1), controlPoint1: CGPoint(x: 69.45, y: 21.2), controlPoint2: CGPoint(x: 72.28, y: 24.27))
            bezierPath.addLine(to: CGPoint(x: 76, y: 28.1))
            bezierPath.addLine(to: CGPoint(x: 76, y: 100))
            bezierPath.addLine(to: CGPoint(x: 50, y: 100))
            bezierPath.addLine(to: CGPoint(x: 50, y: 28.1))
            bezierPath.addLine(to: CGPoint(x: 54.12, y: 28.1))
            bezierPath.addCurve(to: CGPoint(x: 63.5, y: 20), controlPoint1: CGPoint(x: 54.84, y: 23.51), controlPoint2: CGPoint(x: 58.76, y: 20))
            bezierPath.addCurve(to: CGPoint(x: 65.78, y: 20.28), controlPoint1: CGPoint(x: 64.29, y: 20), controlPoint2: CGPoint(x: 65.05, y: 20.1))
            bezierPath.close()
            UIColor.lightGray.setFill()
            bezierPath.fill()
            return bezierPath.cgPath.resizeCGPath(Fitin: CGRect(x: 0, y: 0, width: 26 * scale, height: 80  * scale))

        }else {
            
            let bezier8Path = UIBezierPath()
            bezier8Path.move(to: CGPoint(x: 100, y: 100))
            bezier8Path.addCurve(to: CGPoint(x: 100, y: 70), controlPoint1: CGPoint(x: 100, y: 100), controlPoint2: CGPoint(x: 100, y: 70))
            bezier8Path.addLine(to: CGPoint(x: 97, y: 70))
            bezier8Path.addCurve(to: CGPoint(x: 90.18, y: 77.99), controlPoint1: CGPoint(x: 96.43, y: 73.91), controlPoint2: CGPoint(x: 93.7, y: 77.05))
            bezier8Path.addCurve(to: CGPoint(x: 87.98, y: 78.28), controlPoint1: CGPoint(x: 89.47, y: 78.18), controlPoint2: CGPoint(x: 88.74, y: 78.28))
            bezier8Path.addCurve(to: CGPoint(x: 78.96, y: 70), controlPoint1: CGPoint(x: 83.43, y: 78.28), controlPoint2: CGPoint(x: 79.65, y: 74.69))
            bezier8Path.addLine(to: CGPoint(x: 75, y: 70))
            bezier8Path.addLine(to: CGPoint(x: 75, y: 100))
            bezier8Path.addLine(to: CGPoint(x: 100, y: 100))
            bezier8Path.addLine(to: CGPoint(x: 100, y: 100))
            bezier8Path.close()
            UIColor.gray.setFill()
            bezier8Path.fill()
            return bezier8Path.cgPath.resizeCGPath(Fitin: CGRect(x: 0, y: 0, width: 25 * scale, height: 30  * scale))

        }
        
    }
    
    func dumbleAnimation(masterlayer : CALayer, images : [UIImage] , index : Int , totalCount : Int , isSaving : Bool){
        
        var image = images[index]
        
        image = image.squareimage(size: masterlayer.bounds.size)!
        
        if index == 0{
            
            let newMasterLayerForIndex = CALayer()
            newMasterLayerForIndex.frame = masterlayer.frame
            newMasterLayerForIndex.contents = image.cgImage
            newMasterLayerForIndex.opacity = 1.0
            newMasterLayerForIndex.contentsGravity = VideoEdVideoMakeManager.contentsGravity
            newMasterLayerForIndex.masksToBounds = true
            
            let scaleAnimation = CAKeyframeAnimation(keyPath: "transform.scale")
            scaleAnimation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
            scaleAnimation.beginTime = AVCoreAnimationBeginTimeAtZero + CFTimeInterval(CGFloat(index) * VideoEdVideoMakeManager.durationForOnePic)
            scaleAnimation.duration =  CFTimeInterval(VideoEdVideoMakeManager.durationForOnePic) //+ 0.19
            scaleAnimation.values = [1.0,1.1,1.08] // zoomout
            scaleAnimation.keyTimes = [0.0,0.5,1.0]
            scaleAnimation.isRemovedOnCompletion = false
            scaleAnimation.fillMode = CAMediaTimingFillMode.forwards
            newMasterLayerForIndex.add(scaleAnimation, forKey: "transform.scale\(index)")
            masterlayer.addSublayer(newMasterLayerForIndex)
            
        }else{
            
            
            for  i in 0...7{
                
                let newMasterLayerForIndex = CALayer()
                newMasterLayerForIndex.frame = masterlayer.frame
                newMasterLayerForIndex.contents = image.cgImage
                newMasterLayerForIndex.opacity = 1.0
                newMasterLayerForIndex.contentsGravity = VideoEdVideoMakeManager.contentsGravity
                newMasterLayerForIndex.masksToBounds = true
                
                let beginTime = AVCoreAnimationBeginTimeAtZero + CFTimeInterval(CGFloat(index) * VideoEdVideoMakeManager.durationForOnePic)
                
                let translationAnimation = CAKeyframeAnimation(keyPath: "position.y")
                translationAnimation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
                translationAnimation.beginTime = beginTime + Double(i) * 0.2
                translationAnimation.duration =  CFTimeInterval(VideoEdVideoMakeManager.durationForOnePic)*0.5
                
                if i <= 3{
                    
                    translationAnimation.values = [newMasterLayerForIndex.frame.width*(-1.5),newMasterLayerForIndex.frame.width/2-newMasterLayerForIndex.frame.width*0.05,newMasterLayerForIndex.frame.width/2]
                    
                }else{
                    translationAnimation.values = [newMasterLayerForIndex.frame.width*1.5,newMasterLayerForIndex.frame.width/2-newMasterLayerForIndex.frame.width*0.05,newMasterLayerForIndex.frame.width/2]
                    
                }
                
                translationAnimation.keyTimes = [0.0,0.85,1.0]
                translationAnimation.isRemovedOnCompletion = false
                translationAnimation.fillMode = CAMediaTimingFillMode.both
                
                
                let objShapeLayer = CAShapeLayer()
                objShapeLayer.path = dumbleBezierPath1(sampleLayer: masterlayer , index : i)
                
                objShapeLayer.lineWidth = 0
                objShapeLayer.strokeEnd = 1.0
                
                newMasterLayerForIndex.mask = objShapeLayer
                
                newMasterLayerForIndex.add(translationAnimation, forKey: "origin.x\(index)")
                
                
                masterlayer.addSublayer(newMasterLayerForIndex)
                
            }
        }
        
        
        

        
    }
    
    func slicesBezierPath1(sampleLayer : CALayer, index : Int)-> UIBezierPath{
        
        if index == 0 {
            
            let rectanglePath = UIBezierPath(rect: CGRect(x: 0, y: sampleLayer.frame.width *  0.75, width: sampleLayer.frame.width, height: sampleLayer.frame.width * 0.27))
            rectanglePath.fill()
            return rectanglePath

        }else if index == 1 {
            
            let rectangle2Path = UIBezierPath(rect: CGRect(x: 0, y: sampleLayer.frame.width *  0.5, width: sampleLayer.frame.width, height: sampleLayer.frame.width * 0.27))
            rectangle2Path.fill()
            return rectangle2Path

        }else if index == 2 {
            
            let rectangle3Path = UIBezierPath(rect: CGRect(x: 0, y: sampleLayer.frame.width *  0.25, width: sampleLayer.frame.width, height: sampleLayer.frame.width * 0.27))
            rectangle3Path.fill()
            return rectangle3Path

        }else  {
            
            let rectangle4Path = UIBezierPath(rect: CGRect(x: 0, y: sampleLayer.frame.width *  0, width: sampleLayer.frame.width, height: sampleLayer.frame.width * 0.26))
            rectangle4Path.fill()
            return rectangle4Path

        }

    }
    
    func slicesAnimation(masterlayer : CALayer, images : [UIImage] , index : Int , totalCount : Int , isSaving : Bool){
        
        var image = images[index]
        
        image = image.squareimage(size: masterlayer.bounds.size)!
        
        if index == 0{
            
            let newMasterLayerForIndex = CALayer()
            newMasterLayerForIndex.frame = masterlayer.frame
            newMasterLayerForIndex.contents = image.cgImage
            newMasterLayerForIndex.opacity = 1.0
            newMasterLayerForIndex.contentsGravity = VideoEdVideoMakeManager.contentsGravity
            newMasterLayerForIndex.masksToBounds = true
            
            let scaleAnimation = CAKeyframeAnimation(keyPath: "transform.scale")
            scaleAnimation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
            scaleAnimation.beginTime = AVCoreAnimationBeginTimeAtZero + CFTimeInterval(CGFloat(index) * VideoEdVideoMakeManager.durationForOnePic)
            scaleAnimation.duration =  CFTimeInterval(VideoEdVideoMakeManager.durationForOnePic) //+ 0.19
            scaleAnimation.values = [1.4,1.1,1.0] // zoomout
            scaleAnimation.keyTimes = [0.0,0.5,1.0]
            scaleAnimation.isRemovedOnCompletion = false
            scaleAnimation.fillMode = CAMediaTimingFillMode.forwards
            newMasterLayerForIndex.add(scaleAnimation, forKey: "transform.scale\(index)")
            masterlayer.addSublayer(newMasterLayerForIndex)
            
        }else{
            
            
            for  i in 0...3{
                
                let newMasterLayerForIndex = CALayer()
                newMasterLayerForIndex.frame = masterlayer.frame
                newMasterLayerForIndex.contents = image.cgImage
                newMasterLayerForIndex.opacity = 1.0
                newMasterLayerForIndex.contentsGravity = VideoEdVideoMakeManager.contentsGravity
                newMasterLayerForIndex.masksToBounds = true
                
                let beginTime = AVCoreAnimationBeginTimeAtZero + CFTimeInterval(CGFloat(index) * VideoEdVideoMakeManager.durationForOnePic)
                
                let translationAnimation = CAKeyframeAnimation(keyPath: "position.x")
                translationAnimation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
                translationAnimation.beginTime = beginTime + Double(i) * 0.2
                translationAnimation.duration =  CFTimeInterval(VideoEdVideoMakeManager.durationForOnePic)*0.3
                
                if i == 0 {
                    
                    translationAnimation.values = [newMasterLayerForIndex.frame.width*1.5,newMasterLayerForIndex.frame.width/2-newMasterLayerForIndex.frame.width*0.05,newMasterLayerForIndex.frame.width/2]
                    
                }else if i == 1 {
                    
                    translationAnimation.values = [newMasterLayerForIndex.frame.width*(-1.5),newMasterLayerForIndex.frame.width/2-newMasterLayerForIndex.frame.width*0.05,newMasterLayerForIndex.frame.width/2]
                    
                }else if i == 2 {
                    translationAnimation.values = [newMasterLayerForIndex.frame.width*(-1.5),newMasterLayerForIndex.frame.width/2-newMasterLayerForIndex.frame.width*0.05,newMasterLayerForIndex.frame.width/2]
                    
                    
                }else{
                    translationAnimation.values = [newMasterLayerForIndex.frame.width*1.5,newMasterLayerForIndex.frame.width/2-newMasterLayerForIndex.frame.width*0.05,newMasterLayerForIndex.frame.width/2]
                    
                }
                
                translationAnimation.keyTimes = [0.0,0.85,1.0]
                translationAnimation.isRemovedOnCompletion = false
                translationAnimation.fillMode = CAMediaTimingFillMode.both
                
                
                let objShapeLayer = CAShapeLayer()
                objShapeLayer.path = slicesBezierPath1(sampleLayer: masterlayer , index : i).cgPath
                
                objShapeLayer.lineWidth = 0
                objShapeLayer.strokeEnd = 1.0
                
                newMasterLayerForIndex.mask = objShapeLayer
                
                newMasterLayerForIndex.add(translationAnimation, forKey: "origin.x\(index)")
                
                
                masterlayer.addSublayer(newMasterLayerForIndex)
                
            }
        }
        
        
        
        if !isSaving {
            masterlayer.isGeometryFlipped = true
        }
        
    }

    func pathsForHeartAnimations(sampleLayer : CALayer) -> [CGPath]{
        let scale = sampleLayer.frame.width / 100
        let bezierPath0 = UIBezierPath()
        bezierPath0.move(to: CGPoint(x: 164 * scale, y: -5.74 * scale))
        bezierPath0.addCurve(to: CGPoint(x: 144.92 * scale, y: 50.51 * scale), controlPoint1: CGPoint(x: 164 * scale, y: 12.59 * scale), controlPoint2: CGPoint(x: 156.37 * scale, y: 34.44 * scale))
        bezierPath0.addCurve(to: CGPoint(x: 49.5 * scale, y: 163 * scale), controlPoint1: CGPoint(x: 130.59 * scale, y: 73.02 * scale), controlPoint2: CGPoint(x: 49.5 * scale, y: 163 * scale))
        bezierPath0.addCurve(to: CGPoint(x: -38.28 * scale, y: 50.51 * scale), controlPoint1: CGPoint(x: 49.5 * scale, y: 163 * scale), controlPoint2: CGPoint(x: -15.38 * scale, y: 82.65 * scale))
        bezierPath0.addCurve(to: CGPoint(x: -65 * scale, y: -5.74 * scale), controlPoint1: CGPoint(x: -49.73 * scale, y: 38.46 * scale), controlPoint2: CGPoint(x: -65 * scale, y: 16.81 * scale))
        bezierPath0.addCurve(to: CGPoint(x: -23.86 * scale, y: -63.58 * scale), controlPoint1: CGPoint(x: -65 * scale, y: -33.13 * scale), controlPoint2: CGPoint(x: -47.64 * scale, y: -56.25 * scale))
        bezierPath0.addCurve(to: CGPoint(x: -7.75 * scale, y: -66 * scale), controlPoint1: CGPoint(x: -18.75 * scale, y: -65.16 * scale), controlPoint2: CGPoint(x: -13.34 * scale, y: -66 * scale))
        bezierPath0.addCurve(to: CGPoint(x: 49.5 * scale, y: -17.79 * scale), controlPoint1: CGPoint(x: 23.87 * scale, y: -66 * scale), controlPoint2: CGPoint(x: 49.5 * scale, y: -26.65 * scale))
        bezierPath0.addCurve(to: CGPoint(x: 49.5 * scale, y: -5.74 * scale), controlPoint1: CGPoint(x: 49.5 * scale, y: -13.42 * scale), controlPoint2: CGPoint(x: 49.5 * scale, y: 10.69 * scale))
        bezierPath0.addCurve(to: CGPoint(x: 106.75 * scale, y: -66 * scale), controlPoint1: CGPoint(x: 49.5 * scale, y: -39.02 * scale), controlPoint2: CGPoint(x: 75.13 * scale, y: -66 * scale))
        bezierPath0.addCurve(to: CGPoint(x: 164 * scale, y: -5.74 * scale), controlPoint1: CGPoint(x: 138.37 * scale, y: -66 * scale), controlPoint2: CGPoint(x: 164 * scale, y: -39.02 * scale))
        bezierPath0.close()
        UIColor.gray.setFill()
        bezierPath0.fill()
        bezierPath0.stroke()
        
        
        let bezierPath1 = UIBezierPath()
        bezierPath1.move(to: CGPoint(x: 100 * scale, y: 26.32 * scale))
        bezierPath1.addCurve(to: CGPoint(x: 91.67 * scale, y: 50.88 * scale), controlPoint1: CGPoint(x: 100 * scale, y: 34.32 * scale), controlPoint2: CGPoint(x: 96.67 * scale, y: 43.86 * scale))
        bezierPath1.addCurve(to: CGPoint(x: 50 * scale, y: 100 * scale), controlPoint1: CGPoint(x: 85.41 * scale, y: 60.71 * scale), controlPoint2: CGPoint(x: 50 * scale, y: 100 * scale))
        bezierPath1.addCurve(to: CGPoint(x: 11.67 * scale, y: 50.88 * scale), controlPoint1: CGPoint(x: 50 * scale, y: 100 * scale), controlPoint2: CGPoint(x: 21.67 * scale, y: 64.91 * scale))
        bezierPath1.addCurve(to: CGPoint(x: 0, y: 26.32 * scale), controlPoint1: CGPoint(x: 6.67 * scale, y: 45.61 * scale), controlPoint2: CGPoint(x: 0, y: 36.16 * scale))
        bezierPath1.addCurve(to: CGPoint(x: 17.96 * scale, y: 1.06 * scale), controlPoint1: CGPoint(x: 0, y: 14.35 * scale), controlPoint2: CGPoint(x: 7.58 * scale, y: 4.26 * scale))
        bezierPath1.addCurve(to: CGPoint(x: 25 * scale, y: 0), controlPoint1: CGPoint(x: 20.2 * scale, y: 0.37 * scale), controlPoint2: CGPoint(x: 22.56 * scale, y: 0))
        bezierPath1.addCurve(to: CGPoint(x: 50 * scale, y: 21.05 * scale), controlPoint1: CGPoint(x: 38.81 * scale, y: 0), controlPoint2: CGPoint(x: 50 * scale, y: 17.18 * scale))
        bezierPath1.addCurve(to: CGPoint(x: 50 * scale, y: 26.32 * scale), controlPoint1: CGPoint(x: 50 * scale, y: 22.96 * scale), controlPoint2: CGPoint(x: 50 * scale, y: 33.49 * scale))
        bezierPath1.addCurve(to: CGPoint(x: 75 * scale, y: 0), controlPoint1: CGPoint(x: 50 * scale, y: 11.78 * scale), controlPoint2: CGPoint(x: 61.19 * scale, y: 0))
        bezierPath1.addCurve(to: CGPoint(x: 100 * scale, y: 26.32 * scale), controlPoint1: CGPoint(x: 88.81 * scale, y: 0), controlPoint2: CGPoint(x: 100 * scale, y: 11.78 * scale))
        bezierPath1.close()
        UIColor.gray.setFill()
        bezierPath1.fill()
        bezierPath1.stroke()
        
        let bezierPath2 = UIBezierPath()
        bezierPath2.move(to: CGPoint(x: 60 * scale, y: 38.26 * scale))
        bezierPath2.addCurve(to: CGPoint(x: 58.33 * scale, y: 43.18 * scale), controlPoint1: CGPoint(x: 60 * scale, y: 39.86 * scale), controlPoint2: CGPoint(x: 59.33 * scale, y: 41.77 * scale))
        bezierPath2.addCurve(to: CGPoint(x: 50 * scale, y: 53 * scale), controlPoint1: CGPoint(x: 57.08 * scale, y: 45.14 * scale), controlPoint2: CGPoint(x: 50 * scale, y: 53 * scale))
        bezierPath2.addCurve(to: CGPoint(x: 42.33 * scale, y: 43.18 * scale), controlPoint1: CGPoint(x: 50 * scale, y: 53 * scale), controlPoint2: CGPoint(x: 44.33 * scale, y: 45.98 * scale))
        bezierPath2.addCurve(to: CGPoint(x: 40 * scale, y: 38.26 * scale), controlPoint1: CGPoint(x: 41.33 * scale, y: 42.12 * scale), controlPoint2: CGPoint(x: 40 * scale, y: 40.23 * scale))
        bezierPath2.addCurve(to: CGPoint(x: 43.59 * scale, y: 33.21 * scale), controlPoint1: CGPoint(x: 40 * scale, y: 35.87 * scale), controlPoint2: CGPoint(x: 41.52 * scale, y: 33.85 * scale))
        bezierPath2.addCurve(to: CGPoint(x: 45 * scale, y: 33 * scale), controlPoint1: CGPoint(x: 44.04 * scale, y: 33.07 * scale), controlPoint2: CGPoint(x: 44.51 * scale, y: 33 * scale))
        bezierPath2.addCurve(to: CGPoint(x: 50 * scale, y: 37.21 * scale), controlPoint1: CGPoint(x: 47.76 * scale, y: 33 * scale), controlPoint2: CGPoint(x: 50 * scale, y: 36.44 * scale))
        bezierPath2.addCurve(to: CGPoint(x: 50 * scale, y: 38.26 * scale), controlPoint1: CGPoint(x: 50 * scale, y: 37.59 * scale), controlPoint2: CGPoint(x: 50 * scale, y: 39.7 * scale))
        bezierPath2.addCurve(to: CGPoint(x: 55 * scale, y: 33 * scale), controlPoint1: CGPoint(x: 50 * scale, y: 35.36 * scale), controlPoint2: CGPoint(x: 52.24 * scale, y: 33 * scale))
        bezierPath2.addCurve(to: CGPoint(x: 60 * scale, y: 38.26 * scale), controlPoint1: CGPoint(x: 57.76 * scale, y: 33 * scale), controlPoint2: CGPoint(x: 60 * scale, y: 35.36 * scale))
        bezierPath2.close()
        UIColor.gray.setFill()
        bezierPath2.fill()
        bezierPath2.stroke()

        let bezierPath3 = UIBezierPath()
        bezierPath3.move(to: CGPoint(x: 51 * scale, y: 50.26 * scale))
        bezierPath3.addCurve(to: CGPoint(x: 50.92 * scale, y: 50.51 * scale), controlPoint1: CGPoint(x: 51 * scale, y: 50.34 * scale), controlPoint2: CGPoint(x: 50.97 * scale, y: 50.44 * scale))
        bezierPath3.addCurve(to: CGPoint(x: 50.5 * scale, y: 51 * scale), controlPoint1: CGPoint(x: 50.85 * scale, y: 50.61 * scale), controlPoint2: CGPoint(x: 50.5 * scale, y: 51 * scale))
        bezierPath3.addCurve(to: CGPoint(x: 50.12 * scale, y: 50.51 * scale), controlPoint1: CGPoint(x: 50.5 * scale, y: 51 * scale), controlPoint2: CGPoint(x: 50.22 * scale, y: 50.65 * scale))
        bezierPath3.addCurve(to: CGPoint(x: 50 * scale, y: 50.26 * scale), controlPoint1: CGPoint(x: 50.07 * scale, y: 50.46 * scale), controlPoint2: CGPoint(x: 50 * scale, y: 50.36 * scale))
        bezierPath3.addCurve(to: CGPoint(x: 50.18 * scale, y: 50.01 * scale), controlPoint1: CGPoint(x: 50 * scale, y: 50.14 * scale), controlPoint2: CGPoint(x: 50.08 * scale, y: 50.04 * scale))
        bezierPath3.addCurve(to: CGPoint(x: 50.25 * scale, y: 50 * scale), controlPoint1: CGPoint(x: 50.2 * scale, y: 50 * scale), controlPoint2: CGPoint(x: 50.23 * scale, y: 50 * scale))
        bezierPath3.addCurve(to: CGPoint(x: 50.5 * scale, y: 50.21 * scale), controlPoint1: CGPoint(x: 50.39 * scale, y: 50 * scale), controlPoint2: CGPoint(x: 50.5 * scale, y: 50.17 * scale))
        bezierPath3.addCurve(to: CGPoint(x: 50.5 * scale, y: 50.26 * scale), controlPoint1: CGPoint(x: 50.5 * scale, y: 50.23 * scale), controlPoint2: CGPoint(x: 50.5 * scale, y: 50.33 * scale))
        bezierPath3.addCurve(to: CGPoint(x: 50.75 * scale, y: 50 * scale), controlPoint1: CGPoint(x: 50.5 * scale, y: 50.12 * scale), controlPoint2: CGPoint(x: 50.61 * scale, y: 50 * scale))
        bezierPath3.addCurve(to: CGPoint(x: 51 * scale, y: 50.26 * scale), controlPoint1: CGPoint(x: 50.89 * scale, y: 50 * scale), controlPoint2: CGPoint(x: 51 * scale, y: 50.12 * scale))
        bezierPath3.close()

        let bezierPath4 = UIBezierPath()
        bezierPath4.move(to: CGPoint(x: 50.5 * scale, y: 51 * scale))
        bezierPath4.addCurve(to: CGPoint(x: 50.5 * scale, y: 50.26 * scale), controlPoint1: CGPoint(x: 50.5 * scale, y: 51 * scale), controlPoint2: CGPoint(x: 50.5 * scale, y: 50.33 * scale))
        bezierPath4.addCurve(to: CGPoint(x: 50.5 * scale, y: 51 * scale), controlPoint1: CGPoint(x: 50.5 * scale, y: 50.12 * scale), controlPoint2: CGPoint(x: 50.5 * scale, y: 51 * scale))
        bezierPath4.close()

        
        return [bezierPath0.cgPath,bezierPath1.cgPath, bezierPath2.cgPath,bezierPath3.cgPath,bezierPath4.cgPath]

    }
    func heartAnimation(masterlayer : CALayer, images : [UIImage] , index : Int , totalCount : Int , isSaving : Bool){
        
        let image = images[index]
        let newMasterLayerForIndex = CALayer()
        newMasterLayerForIndex.frame = masterlayer.bounds
        newMasterLayerForIndex.contents = image.cgImage
        newMasterLayerForIndex.opacity = 1.0
        newMasterLayerForIndex.contentsGravity = VideoEdVideoMakeManager.contentsGravity
        newMasterLayerForIndex.masksToBounds = true
        
        let shapeNewLayer = CAShapeLayer()
        let animationPath = CAKeyframeAnimation(keyPath: "path")
        animationPath.duration = CFTimeInterval(VideoEdVideoMakeManager.durationForOnePic)*0.5
        animationPath.beginTime =  AVCoreAnimationBeginTimeAtZero + CFTimeInterval( CGFloat(index+1) * VideoEdVideoMakeManager.durationForOnePic) - CFTimeInterval(VideoEdVideoMakeManager.durationForOnePic)*0.25
        animationPath.values = pathsForHeartAnimations(sampleLayer: newMasterLayerForIndex)
        animationPath.keyTimes = [0.0,0.3,0.7,0.98,1.0]
        animationPath.fillMode = CAMediaTimingFillMode.both
        animationPath.isRemovedOnCompletion = false
        if totalCount-1 > index{
            
            shapeNewLayer.add(animationPath, forKey: "pathAnimation" + "\(index)")
            newMasterLayerForIndex.mask = shapeNewLayer
        }
        masterlayer.insertSublayer(newMasterLayerForIndex, at: 0)
        
        
        if isSaving {
            masterlayer.isGeometryFlipped = true
        }
        
        masterlayer.display()
    }

    func pathsForBrushAnimations(sampleLayer : CALayer) -> [CGPath]{
        let scale = sampleLayer.frame.width / 100

        let bezierPath0 = UIBezierPath()
        bezierPath0.move(to: CGPoint(x: 0 * scale, y: -6.1 * scale))
        bezierPath0.addCurve(to: CGPoint(x: 3.5 * scale, y: -4.1 * scale), controlPoint1: CGPoint(x: 3.5 * scale, y: -6.98 * scale), controlPoint2: CGPoint(x: -0.5 * scale, y: -4.1 * scale))
        bezierPath0.addCurve(to: CGPoint(x: 7.5 * scale, y: -4.1 * scale), controlPoint1: CGPoint(x: 7.5 * scale, y: -4.1 * scale), controlPoint2: CGPoint(x: 3.5 * scale, y: -1.45 * scale))
        bezierPath0.addCurve(to: CGPoint(x: 11.5 * scale, y: -1.45 * scale), controlPoint1: CGPoint(x: 9.58 * scale, y: -5.47 * scale), controlPoint2: CGPoint(x: 10.46 * scale, y: -1.4 * scale))
        bezierPath0.addCurve(to: CGPoint(x: 15.5 * scale, y: -4.1 * scale), controlPoint1: CGPoint(x: 12.46 * scale, y: -1.5 * scale), controlPoint2: CGPoint(x: 13.58 * scale, y: -5.79 * scale))
        bezierPath0.addCurve(to: CGPoint(x: 18.5 * scale, y: -1.45 * scale), controlPoint1: CGPoint(x: 19.5 * scale, y: -0.57 * scale), controlPoint2: CGPoint(x: 18.5 * scale, y: -1.45 * scale))
        bezierPath0.addLine(to: CGPoint(x: 21.5 * scale, y: -1.45 * scale))
        bezierPath0.addLine(to: CGPoint(x: 21.5 * scale, y: -0.57 * scale))
        bezierPath0.addLine(to: CGPoint(x: 27.5 * scale, y: -4.5 * scale))
        bezierPath0.addLine(to: CGPoint(x: 33.5 * scale, y: -1.5 * scale))
        bezierPath0.addLine(to: CGPoint(x: 38.5 * scale, y: -4.5 * scale))
        bezierPath0.addLine(to: CGPoint(x: 42.5 * scale, y: -0.5 * scale))
        bezierPath0.addLine(to: CGPoint(x: 47.5 * scale, y: -0.5 * scale))
        bezierPath0.addLine(to: CGPoint(x: 52.5 * scale, y: -4.5 * scale))
        bezierPath0.addLine(to: CGPoint(x: 57.5 * scale, y: -1.5 * scale))
        bezierPath0.addLine(to: CGPoint(x: 61.5 * scale, y: -0.5 * scale))
        bezierPath0.addLine(to: CGPoint(x: 65.5 * scale, y: -4.5 * scale))
        bezierPath0.addLine(to: CGPoint(x: 71.5 * scale, y: -0.5 * scale))
        bezierPath0.addLine(to: CGPoint(x: 75.5 * scale, y: -1.5 * scale))
        bezierPath0.addLine(to: CGPoint(x: 79.5 * scale, y: -4.5 * scale))
        bezierPath0.addLine(to: CGPoint(x: 84.5 * scale, y: -1.5 * scale))
        bezierPath0.addLine(to: CGPoint(x: 89.5 * scale, y: -1.5 * scale))
        bezierPath0.addLine(to: CGPoint(x: 93.5 * scale, y: -0.5 * scale))
        bezierPath0.addLine(to: CGPoint(x: 97.5 * scale, y: -1.5 * scale))
        bezierPath0.addLine(to: CGPoint(x: 101 * scale, y: -2.13 * scale))
        bezierPath0.addLine(to: CGPoint(x: 101.5 * scale, y: 100.5 * scale))
        bezierPath0.addLine(to: CGPoint(x: -0.5 * scale, y: 100.5 * scale))

        
        let bezierPath1 = UIBezierPath()
        bezierPath1.move(to: CGPoint(x: -1.5 * scale, y: 101.9 * scale))
        bezierPath1.addCurve(to: CGPoint(x: 3.5 * scale, y: 103.9 * scale), controlPoint1: CGPoint(x: 0.5 * scale, y: 101.02 * scale), controlPoint2: CGPoint(x: -0.5 * scale, y: 103.9 * scale))
        bezierPath1.addCurve(to: CGPoint(x: 7.5 * scale, y: 103.9 * scale), controlPoint1: CGPoint(x: 7.5 * scale, y: 103.9 * scale), controlPoint2: CGPoint(x: 3.5 * scale, y: 106.55 * scale))
        bezierPath1.addCurve(to: CGPoint(x: 11.5 * scale, y: 106.55 * scale), controlPoint1: CGPoint(x: 9.58 * scale, y: 102.53 * scale), controlPoint2: CGPoint(x: 10.46 * scale, y: 106.6 * scale))
        bezierPath1.addCurve(to: CGPoint(x: 15.5 * scale, y: 103.9 * scale), controlPoint1: CGPoint(x: 12.46 * scale, y: 106.5 * scale), controlPoint2: CGPoint(x: 13.58 * scale, y: 102.21 * scale))
        bezierPath1.addCurve(to: CGPoint(x: 18.5 * scale, y: 106.55 * scale), controlPoint1: CGPoint(x: 19.5 * scale, y: 107.43 * scale), controlPoint2: CGPoint(x: 18.5 * scale, y: 106.55 * scale))
        bezierPath1.addLine(to: CGPoint(x: 23.5 * scale, y: 106.55 * scale))
        bezierPath1.addLine(to: CGPoint(x: 27.5 * scale, y: 106.43 * scale))
        bezierPath1.addLine(to: CGPoint(x: 32.5 * scale, y: 101.5 * scale))
        bezierPath1.addLine(to: CGPoint(x: 38.5 * scale, y: 102.5 * scale))
        bezierPath1.addLine(to: CGPoint(x: 38.5 * scale, y: -4.5 * scale))
        bezierPath1.addLine(to: CGPoint(x: 42.5 * scale, y: -0.5 * scale))
        bezierPath1.addLine(to: CGPoint(x: 47.5 * scale, y: -0.5 * scale))
        bezierPath1.addLine(to: CGPoint(x: 52.5 * scale, y: -4.5 * scale))
        bezierPath1.addLine(to: CGPoint(x: 57.5 * scale, y: -1.5 * scale))
        bezierPath1.addLine(to: CGPoint(x: 61.5 * scale, y: -0.5 * scale))
        bezierPath1.addLine(to: CGPoint(x: 65.5 * scale, y: -4.5 * scale))
        bezierPath1.addLine(to: CGPoint(x: 71.5 * scale, y: -0.5 * scale))
        bezierPath1.addLine(to: CGPoint(x: 75.5 * scale, y: -1.5 * scale))
        bezierPath1.addLine(to: CGPoint(x: 79.5 * scale, y: -4.5 * scale))
        bezierPath1.addLine(to: CGPoint(x: 84.5 * scale, y: -1.5 * scale))
        bezierPath1.addLine(to: CGPoint(x: 89.5 * scale, y: -1.5 * scale))
        bezierPath1.addLine(to: CGPoint(x: 93.5 * scale, y: -0.5 * scale))
        bezierPath1.addLine(to: CGPoint(x: 97.5 * scale, y: -1.5 * scale))
        bezierPath1.addLine(to: CGPoint(x: 101 * scale, y: -2.13 * scale))
        bezierPath1.addLine(to: CGPoint(x: 101.5 * scale, y: 100.5 * scale))
        bezierPath1.addLine(to: CGPoint(x: -0.5 * scale, y: 100.5 * scale))
        bezierPath1.stroke()

        
        let bezierPath2 = UIBezierPath()
        bezierPath2.move(to: CGPoint(x: -1.5 * scale, y: 101.9 * scale))
        bezierPath2.addCurve(to: CGPoint(x: 3.5 * scale, y: 103.9 * scale), controlPoint1: CGPoint(x: 0.5 * scale, y: 101.02 * scale), controlPoint2: CGPoint(x: -0.5 * scale, y: 103.9 * scale))
        bezierPath2.addCurve(to: CGPoint(x: 7.5 * scale, y: 103.9 * scale), controlPoint1: CGPoint(x: 7.5 * scale, y: 103.9 * scale), controlPoint2: CGPoint(x: 3.5 * scale, y: 106.55 * scale))
        bezierPath2.addCurve(to: CGPoint(x: 11.5 * scale, y: 106.55 * scale), controlPoint1: CGPoint(x: 9.58 * scale, y: 102.53 * scale), controlPoint2: CGPoint(x: 10.46 * scale, y: 106.6 * scale))
        bezierPath2.addCurve(to: CGPoint(x: 15.5 * scale, y: 103.9 * scale), controlPoint1: CGPoint(x: 12.46 * scale, y: 106.5 * scale), controlPoint2: CGPoint(x: 13.58 * scale, y: 102.21 * scale))
        bezierPath2.addCurve(to: CGPoint(x: 18.5 * scale, y: 106.55 * scale), controlPoint1: CGPoint(x: 19.5 * scale, y: 107.43 * scale), controlPoint2: CGPoint(x: 18.5 * scale, y: 106.55 * scale))
        bezierPath2.addLine(to: CGPoint(x: 23.5 * scale, y: 106.55 * scale))
        bezierPath2.addLine(to: CGPoint(x: 27.5 * scale, y: 106.43 * scale))
        bezierPath2.addLine(to: CGPoint(x: 27.5 * scale, y: 101.5 * scale))
        bezierPath2.addLine(to: CGPoint(x: 33.5 * scale, y: 104.5 * scale))
        bezierPath2.addLine(to: CGPoint(x: 38.5 * scale, y: 101.5 * scale))
        bezierPath2.addLine(to: CGPoint(x: 42.5 * scale, y: 105.5 * scale))
        bezierPath2.addLine(to: CGPoint(x: 47.5 * scale, y: 105.5 * scale))
        bezierPath2.addLine(to: CGPoint(x: 52.5 * scale, y: 101.5 * scale))
        bezierPath2.addLine(to: CGPoint(x: 57.5 * scale, y: 104.5 * scale))
        bezierPath2.addLine(to: CGPoint(x: 61.5 * scale, y: 105.5 * scale))
        bezierPath2.addLine(to: CGPoint(x: 71.5 * scale, y: 101.5 * scale))
        
        bezierPath2.addLine(to: CGPoint(x: 71.5 * scale, y: -0.5 * scale))
        bezierPath2.addLine(to: CGPoint(x: 75.5 * scale, y: -1.5 * scale))
        bezierPath2.addLine(to: CGPoint(x: 79.5 * scale, y: -4.5 * scale))
        bezierPath2.addLine(to: CGPoint(x: 84.5 * scale, y: -1.5 * scale))
        bezierPath2.addLine(to: CGPoint(x: 89.5 * scale, y: -1.5 * scale))
        bezierPath2.addLine(to: CGPoint(x: 93.5 * scale, y: -0.5 * scale))
        bezierPath2.addLine(to: CGPoint(x: 97.5 * scale, y: -1.5 * scale))
        bezierPath2.addLine(to: CGPoint(x: 101 * scale, y: -2.13 * scale))
        bezierPath2.addLine(to: CGPoint(x: 101.5 * scale, y: 100.5 * scale))
        bezierPath2.addLine(to: CGPoint(x: -0.5 * scale, y: 100.5 * scale))
        bezierPath2.stroke()

        
        let bezierPath3 = UIBezierPath()
        bezierPath3.move(to: CGPoint(x: -1.5 * scale, y: 101.9 * scale))
        bezierPath3.addCurve(to: CGPoint(x: 3.5 * scale, y: 103.9 * scale), controlPoint1: CGPoint(x: 0.5 * scale, y: 101.02 * scale), controlPoint2: CGPoint(x: -0.5 * scale, y: 103.9 * scale))
        bezierPath3.addCurve(to: CGPoint(x: 7.5 * scale, y: 103.9 * scale), controlPoint1: CGPoint(x: 7.5 * scale, y: 103.9 * scale), controlPoint2: CGPoint(x: 3.5 * scale, y: 106.55 * scale))
        bezierPath3.addCurve(to: CGPoint(x: 11.5 * scale, y: 106.55 * scale), controlPoint1: CGPoint(x: 9.58 * scale, y: 102.53 * scale), controlPoint2: CGPoint(x: 10.46 * scale, y: 106.6 * scale))
        bezierPath3.addCurve(to: CGPoint(x: 15.5 * scale, y: 103.9 * scale), controlPoint1: CGPoint(x: 12.46 * scale, y: 106.5 * scale), controlPoint2: CGPoint(x: 13.58 * scale, y: 102.21 * scale))
        bezierPath3.addCurve(to: CGPoint(x: 18.5 * scale, y: 106.55 * scale), controlPoint1: CGPoint(x: 19.5 * scale, y: 107.43 * scale), controlPoint2: CGPoint(x: 18.5 * scale, y: 106.55 * scale))
        bezierPath3.addLine(to: CGPoint(x: 23.5 * scale, y: 106.55 * scale))
        bezierPath3.addLine(to: CGPoint(x: 27.5 * scale, y: 106.43 * scale))
        bezierPath3.addLine(to: CGPoint(x: 27.5 * scale, y: 101.5 * scale))
        bezierPath3.addLine(to: CGPoint(x: 33.5 * scale, y: 104.5 * scale))
        bezierPath3.addLine(to: CGPoint(x: 38.5 * scale, y: 101.5 * scale))
        bezierPath3.addLine(to: CGPoint(x: 42.5 * scale, y: 105.5 * scale))
        bezierPath3.addLine(to: CGPoint(x: 47.5 * scale, y: 105.5 * scale))
        bezierPath3.addLine(to: CGPoint(x: 52.5 * scale, y: 101.5 * scale))
        bezierPath3.addLine(to: CGPoint(x: 57.5 * scale, y: 104.5 * scale))
        bezierPath3.addLine(to: CGPoint(x: 61.5 * scale, y: 105.5 * scale))
        bezierPath3.addLine(to: CGPoint(x: 71.5 * scale, y: 101.5 * scale))
        
        bezierPath3.addLine(to: CGPoint(x: 71.5 * scale, y: 107.5 * scale))
        bezierPath3.addLine(to: CGPoint(x: 75.5 * scale, y: 106.5 * scale))
        bezierPath3.addLine(to: CGPoint(x: 79.5 * scale, y: 103.5 * scale))
        bezierPath3.addLine(to: CGPoint(x: 84.5 * scale, y: 106.5 * scale))
        bezierPath3.addLine(to: CGPoint(x: 89.5 * scale, y: 106.5 * scale))
        bezierPath3.addLine(to: CGPoint(x: 93.5 * scale, y: 107.5 * scale))
        bezierPath3.addLine(to: CGPoint(x: 97.5 * scale, y: 106.5 * scale))
        bezierPath3.addLine(to: CGPoint(x: 101 * scale, y: 105.87 * scale))
        bezierPath3.addLine(to: CGPoint(x: 101.5 * scale, y: 100.5 * scale))
        bezierPath3.addLine(to: CGPoint(x: -0.5 * scale, y: 100.5 * scale))
        bezierPath3.stroke()
        
        return [bezierPath0.cgPath,bezierPath1.cgPath, bezierPath2.cgPath,bezierPath3.cgPath]
        
        

    }


    func brushAnimation(masterlayer : CALayer, images : [UIImage] , index : Int , totalCount : Int , isSaving : Bool){
        
        let image = images[index]
        let newMasterLayerForIndex = CALayer()
        newMasterLayerForIndex.frame = masterlayer.bounds
        newMasterLayerForIndex.contents = image.cgImage
        newMasterLayerForIndex.opacity = 1.0
        newMasterLayerForIndex.contentsGravity = VideoEdVideoMakeManager.contentsGravity
        newMasterLayerForIndex.masksToBounds = true
        
        let shapeNewLayer = CAShapeLayer()
//        shapeNewLayer.path = inkBezierPath1(sampleLayer: masterlayer ).cgPath
        
        let animationPath = CAKeyframeAnimation(keyPath: "path")
        animationPath.duration = CFTimeInterval(VideoEdVideoMakeManager.durationForOnePic)*0.25
        animationPath.beginTime =  AVCoreAnimationBeginTimeAtZero + CFTimeInterval( CGFloat(index+1) * VideoEdVideoMakeManager.durationForOnePic) - animationPath.duration*0.5
        animationPath.values = pathsForBrushAnimations(sampleLayer: masterlayer)
        animationPath.keyTimes = [0.0,0.3,0.67,1.0]
        animationPath.fillMode = CAMediaTimingFillMode.both
        animationPath.isRemovedOnCompletion = false
        if totalCount-1 > index{
            
            shapeNewLayer.add(animationPath, forKey: "pathAnimation" + "\(index)")
            addNormalOpacityAnimation(sampleLayer: shapeNewLayer, index: index, totalCount: totalCount, isSaving: isSaving)
            newMasterLayerForIndex.mask = shapeNewLayer
        }
        masterlayer.insertSublayer(newMasterLayerForIndex, at: 0)
        
        
        
        if isSaving {
            masterlayer.isGeometryFlipped = true
        }
        
    }
    func pathsForINKAnimations(sampleLayer : CALayer) -> [CGPath]{
        
        let scale = sampleLayer.frame.width / 100
        let bezierPath0 = UIBezierPath()
        bezierPath0.move(to: CGPoint(x: -2.5 * scale, y: 100 * scale))
        bezierPath0.addLine(to: CGPoint(x: 100.78 * scale, y: 100 * scale))
        bezierPath0.addLine(to: CGPoint(x: 104.5 * scale, y: -7.05 * scale))
        bezierPath0.addLine(to: CGPoint(x: 92.4 * scale, y: -3.01 * scale))
        bezierPath0.addCurve(to: CGPoint(x: 78.45 * scale, y: -3.01 * scale), controlPoint1: CGPoint(x: 92.4 * scale, y: -3.01 * scale), controlPoint2: CGPoint(x: 84.03 * scale, y: -11.1 * scale))
        bezierPath0.addCurve(to: CGPoint(x: 65.42 * scale, y: -21.89 * scale), controlPoint1: CGPoint(x: 72.87 * scale, y: 5.09 * scale), controlPoint2: CGPoint(x: 75.66 * scale, y: -7.05 * scale))
        bezierPath0.addCurve(to: CGPoint(x: 47.74 * scale, y: -21.8 * scale), controlPoint1: CGPoint(x: 55.19 * scale, y: -36.73 * scale), controlPoint2: CGPoint(x: 56.12 * scale, y: -9.75 * scale))
        bezierPath0.addCurve(to: CGPoint(x: 31 * scale, y: -21.89 * scale), controlPoint1: CGPoint(x: 39.37 * scale, y: -34.03 * scale), controlPoint2: CGPoint(x: 31 * scale, y: -21.89 * scale))
        bezierPath0.addCurve(to: CGPoint(x: 11.46 * scale, y: -31.33 * scale), controlPoint1: CGPoint(x: 31 * scale, y: -21.89 * scale), controlPoint2: CGPoint(x: 17.97 * scale, y: -42.12 * scale))
        bezierPath0.addCurve(to: CGPoint(x: 4.01 * scale, y: -7.05 * scale), controlPoint1: CGPoint(x: 4.94 * scale, y: -20.54 * scale), controlPoint2: CGPoint(x: 8.67 * scale, y: 2.39 * scale))
        bezierPath0.addCurve(to: CGPoint(x: -2.5 * scale, y: -15.15 * scale), controlPoint1: CGPoint(x: -0.64 * scale, y: -16.5 * scale), controlPoint2: CGPoint(x: -2.5 * scale, y: -15.15 * scale))
        bezierPath0.addLine(to: CGPoint(x: -2.5 * scale, y: 100 * scale))
        bezierPath0.stroke()
        
        let bezierPath1 = UIBezierPath()
        bezierPath1.move(to: CGPoint(x: 0.5 * scale, y: 100 * scale))
        bezierPath1.addLine(to: CGPoint(x: 99.55 * scale, y: 100 * scale))
        bezierPath1.addLine(to: CGPoint(x: 99.55 * scale, y: 16.85 * scale))
        bezierPath1.addLine(to: CGPoint(x: 88.315 * scale, y: 19.945 * scale))
        bezierPath1.addCurve(to: CGPoint(x: 75.45 * scale, y: 19.945 * scale), controlPoint1: CGPoint(x: 88.315 * scale, y: 19.945 * scale), controlPoint2: CGPoint(x: 80.565 * scale, y: 13.665 * scale))
        bezierPath1.addCurve(to: CGPoint(x: 63.345 * scale, y: 5.295 * scale), controlPoint1: CGPoint(x: 70.235 * scale, y: 26.225 * scale), controlPoint2: CGPoint(x: 72.815 * scale, y: 16.85 * scale))
        bezierPath1.addCurve(to: CGPoint(x: 46.995 * scale, y: 5.295 * scale), controlPoint1: CGPoint(x: 53.875 * scale, y: -6.235 * scale), controlPoint2: CGPoint(x: 54.735 * scale, y: 14.715 * scale))
        bezierPath1.addCurve(to: CGPoint(x: 31.495 * scale, y: 5.295 * scale), controlPoint1: CGPoint(x: 39.245 * scale, y: -4.135 * scale), controlPoint2: CGPoint(x: 31.495 * scale, y: 5.295 * scale))
        bezierPath1.addCurve(to: CGPoint(x: 13.415 * scale, y: -2.045 * scale), controlPoint1: CGPoint(x: 31.495 * scale, y: 5.295 * scale), controlPoint2: CGPoint(x: 19.445 * scale, y: -10.425 * scale))
        bezierPath1.addCurve(to: CGPoint(x: 6.535 * scale, y: 16.85 * scale), controlPoint1: CGPoint(x: 7.395 * scale, y: 6.335 * scale), controlPoint2: CGPoint(x: 10.835 * scale, y: 24.135 * scale))
        bezierPath1.addCurve(to: CGPoint(x: 0.55 * scale, y: 10.525 * scale), controlPoint1: CGPoint(x: 2.225 * scale, y: 9.475 * scale), controlPoint2: CGPoint(x: 0.55 * scale, y: 10.525 * scale))
        bezierPath1.addLine(to: CGPoint(x: 0.55 * scale, y: 100 * scale))

        let bezierPath2 = UIBezierPath()
        bezierPath2.move(to: CGPoint(x: 0.5 * scale, y: 100 * scale))
        bezierPath2.addLine(to: CGPoint(x: 101.5 * scale, y: 100 * scale))
        bezierPath2.addLine(to: CGPoint(x: 101.5 * scale, y: 52.5 * scale))
        bezierPath2.addLine(to: CGPoint(x: 88.31 * scale, y: 32.18 * scale))
        bezierPath2.addCurve(to: CGPoint(x: 75.4 * scale, y: 32.18 * scale), controlPoint1: CGPoint(x: 88.31 * scale, y: 32.18 * scale), controlPoint2: CGPoint(x: 80.56 * scale, y: 26.87 * scale))
        bezierPath2.addCurve(to: CGPoint(x: 54.5 * scale, y: 9.5 * scale), controlPoint1: CGPoint(x: 70.23 * scale, y: 37.5 * scale), controlPoint2: CGPoint(x: 63.97 * scale, y: 19.24 * scale))
        bezierPath2.addCurve(to: CGPoint(x: 44.5 * scale, y: 45.5 * scale), controlPoint1: CGPoint(x: 45.03 * scale, y: -0.24 * scale), controlPoint2: CGPoint(x: 52.25 * scale, y: 53.47 * scale))
        bezierPath2.addCurve(to: CGPoint(x: 30.5 * scale, y: 38.5 * scale), controlPoint1: CGPoint(x: 36.75 * scale, y: 37.53 * scale), controlPoint2: CGPoint(x: 30.5 * scale, y: 38.5 * scale))
        bezierPath2.addCurve(to: CGPoint(x: 18.5 * scale, y: 52.5 * scale), controlPoint1: CGPoint(x: 30.5 * scale, y: 38.5 * scale), controlPoint2: CGPoint(x: 24.53 * scale, y: 45.41 * scale))
        bezierPath2.addCurve(to: CGPoint(x: 6.53 * scale, y: 29.52 * scale), controlPoint1: CGPoint(x: 12.47 * scale, y: 59.59 * scale), controlPoint2: CGPoint(x: 10.83 * scale, y: 35.72 * scale))
        bezierPath2.addCurve(to: CGPoint(x: 0.5 * scale, y: 45.5 * scale), controlPoint1: CGPoint(x: 2.22 * scale, y: 23.32 * scale), controlPoint2: CGPoint(x: 0.5 * scale, y: 45.5 * scale))
        bezierPath2.addLine(to: CGPoint(x: 0.5 * scale, y: 100 * scale))
        bezierPath2.stroke()

        let bezierPath3 = UIBezierPath()
        bezierPath3.move(to: CGPoint(x: 0.5 * scale, y: 100 * scale))
        bezierPath3.addLine(to: CGPoint(x: 101.5 * scale, y: 100 * scale))
        bezierPath3.addLine(to: CGPoint(x: 101.5 * scale, y: 75.5 * scale))
        bezierPath3.addLine(to: CGPoint(x: 91.5 * scale, y: 67.5 * scale))
        bezierPath3.addCurve(to: CGPoint(x: 76.5 * scale, y: 58.5 * scale), controlPoint1: CGPoint(x: 91.5 * scale, y: 67.5 * scale), controlPoint2: CGPoint(x: 81.67 * scale, y: 53.19 * scale))
        bezierPath3.addCurve(to: CGPoint(x: 59.5 * scale, y: 58.5 * scale), controlPoint1: CGPoint(x: 71.33 * scale, y: 63.81 * scale), controlPoint2: CGPoint(x: 68.97 * scale, y: 68.24 * scale))
        bezierPath3.addCurve(to: CGPoint(x: 42.5 * scale, y: 67.5 * scale), controlPoint1: CGPoint(x: 50.03 * scale, y: 48.76 * scale), controlPoint2: CGPoint(x: 50.25 * scale, y: 75.47 * scale))
        bezierPath3.addCurve(to: CGPoint(x: 29.5 * scale, y: 67.5 * scale), controlPoint1: CGPoint(x: 34.75 * scale, y: 59.53 * scale), controlPoint2: CGPoint(x: 29.5 * scale, y: 67.5 * scale))
        bezierPath3.addCurve(to: CGPoint(x: 23.5 * scale, y: 84.5 * scale), controlPoint1: CGPoint(x: 29.5 * scale, y: 67.5 * scale), controlPoint2: CGPoint(x: 29.53 * scale, y: 77.41 * scale))
        bezierPath3.addCurve(to: CGPoint(x: 9.5 * scale, y: 67.5 * scale), controlPoint1: CGPoint(x: 17.47 * scale, y: 91.59 * scale), controlPoint2: CGPoint(x: 13.8 * scale, y: 73.7 * scale))
        bezierPath3.addCurve(to: CGPoint(x: 0.5 * scale, y: 75.5 * scale), controlPoint1: CGPoint(x: 5.2 * scale, y: 61.3 * scale), controlPoint2: CGPoint(x: 0.5 * scale, y: 75.5 * scale))
        bezierPath3.addLine(to: CGPoint(x: 0.5 * scale, y: 100 * scale))
        bezierPath3.stroke()

        let bezierPath4 = UIBezierPath()
        bezierPath4.move(to: CGPoint(x: 0.5 * scale, y: 146.5 * scale))
        bezierPath4.addLine(to: CGPoint(x: 101.5 * scale, y: 146.5 * scale))
        bezierPath4.addLine(to: CGPoint(x: 101.5 * scale, y: 122.5 * scale))
        bezierPath4.addLine(to: CGPoint(x: 83.5 * scale, y: 125.5 * scale))
        bezierPath4.addCurve(to: CGPoint(x: 76.5 * scale, y: 105.5 * scale), controlPoint1: CGPoint(x: 83.5 * scale, y: 125.5 * scale), controlPoint2: CGPoint(x: 81.67 * scale, y: 100.19 * scale))
        bezierPath4.addCurve(to: CGPoint(x: 59.5 * scale, y: 105.5 * scale), controlPoint1: CGPoint(x: 71.33 * scale, y: 110.81 * scale), controlPoint2: CGPoint(x: 68.97 * scale, y: 115.24 * scale))
        bezierPath4.addCurve(to: CGPoint(x: 42.5 * scale, y: 114.5 * scale), controlPoint1: CGPoint(x: 50.03 * scale, y: 95.76 * scale), controlPoint2: CGPoint(x: 50.25 * scale, y: 122.47 * scale))
        bezierPath4.addCurve(to: CGPoint(x: 29.5 * scale, y: 114.5 * scale), controlPoint1: CGPoint(x: 34.75 * scale, y: 106.53 * scale), controlPoint2: CGPoint(x: 29.5 * scale, y: 114.5 * scale))
        bezierPath4.addCurve(to: CGPoint(x: 23.5 * scale, y: 131.5 * scale), controlPoint1: CGPoint(x: 29.5 * scale, y: 114.5 * scale), controlPoint2: CGPoint(x: 29.53 * scale, y: 124.41 * scale))
        bezierPath4.addCurve(to: CGPoint(x: 9.5 * scale, y: 114.5 * scale), controlPoint1: CGPoint(x: 17.47 * scale, y: 138.59 * scale), controlPoint2: CGPoint(x: 13.8 * scale, y: 120.7 * scale))
        bezierPath4.addCurve(to: CGPoint(x: 0.5 * scale, y: 122.5 * scale), controlPoint1: CGPoint(x: 5.2 * scale, y: 108.3 * scale), controlPoint2: CGPoint(x: 0.5 * scale, y: 122.5 * scale))
        bezierPath4.addLine(to: CGPoint(x: 0.5 * scale, y: 146.5 * scale))
        bezierPath4.stroke()

        
        return [bezierPath0.cgPath,bezierPath1.cgPath, bezierPath2.cgPath,bezierPath3.cgPath,bezierPath4.cgPath]
        
    }
    

    func inkAnimation(masterlayer : CALayer, images : [UIImage] , index : Int , totalCount : Int , isSaving : Bool){
        
        let image = images[index]
        let newMasterLayerForIndex = CALayer()
        newMasterLayerForIndex.frame = masterlayer.bounds
        newMasterLayerForIndex.contents = image.cgImage
        newMasterLayerForIndex.opacity = 1.0
        newMasterLayerForIndex.contentsGravity = VideoEdVideoMakeManager.contentsGravity
        newMasterLayerForIndex.masksToBounds = true
        
        let shapeNewLayer = CAShapeLayer()
        
        let animationPath = CAKeyframeAnimation(keyPath: "path")
        animationPath.duration = CFTimeInterval(VideoEdVideoMakeManager.durationForOnePic)*0.5
        animationPath.beginTime =  AVCoreAnimationBeginTimeAtZero + CFTimeInterval( CGFloat(index+1) * VideoEdVideoMakeManager.durationForOnePic) - CFTimeInterval(VideoEdVideoMakeManager.durationForOnePic)*0.25
        animationPath.values = pathsForINKAnimations(sampleLayer: masterlayer)
        animationPath.keyTimes = [0.0,0.25,0.5,0.75,1.0]
        animationPath.fillMode = CAMediaTimingFillMode.both
        animationPath.isRemovedOnCompletion = false
        if totalCount-1 > index{
            
            shapeNewLayer.add(animationPath, forKey: "pathAnimation" + "\(index)")
            addNormalOpacityAnimation(sampleLayer: shapeNewLayer, index: index, totalCount: totalCount, isSaving: isSaving)
            newMasterLayerForIndex.mask = shapeNewLayer
        }
        masterlayer.insertSublayer(newMasterLayerForIndex, at: 0)
        
        
        
        if isSaving {
            masterlayer.isGeometryFlipped = true
        }
        
    }
    func pizzaBezierPath1(sampleLayer : CALayer, index : Int)-> UIBezierPath{
        
        let scale = sampleLayer.frame.width / 100
        let bezierPath = UIBezierPath()
        if index == 0 {

            bezierPath.move(to: CGPoint(x: 0, y: 0))
            bezierPath.addLine(to: CGPoint(x: 101 * scale , y: 0))
            bezierPath.addLine(to: CGPoint(x: 50 * scale, y: 50 * scale))
            bezierPath.addLine(to: CGPoint(x: 0, y: 0))

        }else if index == 1 {
            
            bezierPath.move(to: CGPoint(x: 0, y: 100 * scale))
            bezierPath.addLine(to: CGPoint(x: 101 * scale, y: 101 * scale))
            bezierPath.addLine(to: CGPoint(x: 50 * scale, y: 50 * scale))
            bezierPath.addLine(to: CGPoint(x: 0, y: 100 * scale))

        }else if index == 2 {
            
            bezierPath.move(to: CGPoint(x: -1 * scale, y: 102 * scale))
            bezierPath.addLine(to: CGPoint(x: 50 * scale, y: 50 * scale))
            bezierPath.addLine(to: CGPoint(x: -1 * scale, y: -1 * scale))
            bezierPath.addLine(to: CGPoint(x: -1 * scale, y: 102 * scale))

        }else if index == 3 {
            
            bezierPath.move(to: CGPoint(x: 101 * scale, y: 101 * scale))
            bezierPath.addLine(to: CGPoint(x: 50 * scale, y: 50 * scale))
            bezierPath.addLine(to: CGPoint(x: 101 * scale, y: 0))
            bezierPath.addLine(to: CGPoint(x: 101 * scale, y: 101 * scale))

        }
        UIColor.red.setFill()
        UIColor.black.setStroke()
        bezierPath.fill()
        bezierPath.lineWidth = 0
        bezierPath.stroke()
        return bezierPath
    }

    func pizzaAnimation(masterlayer : CALayer, images : [UIImage] , index : Int , totalCount : Int , isSaving : Bool){
        
        var image = images[index]
        
        image = image.squareimage(size: masterlayer.bounds.size)!
        
        if index == 0{
            
            let newMasterLayerForIndex = CALayer()
            newMasterLayerForIndex.frame = masterlayer.frame
            newMasterLayerForIndex.contents = image.cgImage
            newMasterLayerForIndex.opacity = 1.0
            newMasterLayerForIndex.contentsGravity = VideoEdVideoMakeManager.contentsGravity
            newMasterLayerForIndex.masksToBounds = true
            
            let scaleAnimation = CAKeyframeAnimation(keyPath: "transform.scale")
            scaleAnimation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
            scaleAnimation.beginTime = AVCoreAnimationBeginTimeAtZero + CFTimeInterval(CGFloat(index) * VideoEdVideoMakeManager.durationForOnePic)
            scaleAnimation.duration =  CFTimeInterval(VideoEdVideoMakeManager.durationForOnePic) //+ 0.19
            scaleAnimation.values = [1.4,1.1,1.0] // zoomout
            scaleAnimation.keyTimes = [0.0,0.5,1.0]
            scaleAnimation.isRemovedOnCompletion = false
            scaleAnimation.fillMode = CAMediaTimingFillMode.forwards
            newMasterLayerForIndex.add(scaleAnimation, forKey: "transform.scale\(index)")
            masterlayer.addSublayer(newMasterLayerForIndex)
            
        }else{
        
            
            for  i in 0...3{
                
                let newMasterLayerForIndex = CALayer()
                newMasterLayerForIndex.frame = masterlayer.frame
                newMasterLayerForIndex.contents = image.cgImage
                newMasterLayerForIndex.opacity = 1.0
                newMasterLayerForIndex.contentsGravity = VideoEdVideoMakeManager.contentsGravity
                newMasterLayerForIndex.masksToBounds = true
                
                let beginTime = AVCoreAnimationBeginTimeAtZero + CFTimeInterval(CGFloat(index) * VideoEdVideoMakeManager.durationForOnePic)
                
                let translationAnimation = CAKeyframeAnimation(keyPath: "position.x")
                translationAnimation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
                translationAnimation.beginTime = beginTime + Double(i) * 0.2
                translationAnimation.duration =  CFTimeInterval(VideoEdVideoMakeManager.durationForOnePic)*0.2*Double(i)
                
                if i == 0 {
                    
                    translationAnimation.values = [newMasterLayerForIndex.frame.width*1.5,newMasterLayerForIndex.frame.width/2-newMasterLayerForIndex.frame.width*0.05,newMasterLayerForIndex.frame.width/2]

                }else if i == 1 {
                    
                    translationAnimation.values = [newMasterLayerForIndex.frame.width*(-1.5),newMasterLayerForIndex.frame.width/2-newMasterLayerForIndex.frame.width*0.05,newMasterLayerForIndex.frame.width/2]

                }else if i == 2 {
                    translationAnimation.values = [newMasterLayerForIndex.frame.width*(-1.5),newMasterLayerForIndex.frame.width/2-newMasterLayerForIndex.frame.width*0.05,newMasterLayerForIndex.frame.width/2]


                }else{
                    translationAnimation.values = [newMasterLayerForIndex.frame.width*1.5,newMasterLayerForIndex.frame.width/2-newMasterLayerForIndex.frame.width*0.05,newMasterLayerForIndex.frame.width/2]

                }
                
                translationAnimation.keyTimes = [0.0,0.85,1.0]
                translationAnimation.isRemovedOnCompletion = false
                translationAnimation.fillMode = CAMediaTimingFillMode.both
                
                
                let objShapeLayer = CAShapeLayer()
                objShapeLayer.path = pizzaBezierPath1(sampleLayer: masterlayer , index : i).cgPath
                
                objShapeLayer.lineWidth = 0
                objShapeLayer.strokeEnd = 1.0
                
                newMasterLayerForIndex.mask = objShapeLayer
                
                newMasterLayerForIndex.add(translationAnimation, forKey: "origin.x\(index)")
                
                
                masterlayer.addSublayer(newMasterLayerForIndex)
                
            }
        }
        


        if !isSaving {
            masterlayer.isGeometryFlipped = true
        }
        
    }
    func arrowBezierPath1(sampleLayer : CALayer, index : Int)-> UIBezierPath{
        
        let varPecent : CGFloat  = (4-CGFloat(index))/4 * 100
        let scale = sampleLayer.frame.width / 100
        let bezierPath = UIBezierPath()
        bezierPath.move(to: CGPoint(x: varPecent * scale, y: 0))
        bezierPath.addLine(to: CGPoint(x: (varPecent + 50) * scale, y: 50 * scale))
        bezierPath.addLine(to: CGPoint(x: varPecent * scale, y: 100 * scale))
        bezierPath.addLine(to: CGPoint(x: (varPecent - 30) * scale, y: 100 * scale))
        bezierPath.addLine(to: CGPoint(x: (varPecent + 20) * scale, y: 50 * scale))
        bezierPath.addLine(to: CGPoint(x: (varPecent - 30) * scale, y: 0))
        bezierPath.addLine(to: CGPoint(x: varPecent * scale, y: 0))
        bezierPath.stroke()
        return bezierPath
    }

    func divertAnimation(masterlayer : CALayer, images : [UIImage] , index : Int , totalCount : Int , isSaving : Bool){
        
        var image = images[index]
        
        image = image.squareimage(size: masterlayer.bounds.size)!

        
        if index == 0{
            
            let newMasterLayerForIndex = CALayer()
            newMasterLayerForIndex.frame = masterlayer.frame
            newMasterLayerForIndex.contents = image.cgImage
            newMasterLayerForIndex.opacity = 1.0
            newMasterLayerForIndex.contentsGravity = VideoEdVideoMakeManager.contentsGravity
            newMasterLayerForIndex.masksToBounds = true
            
            let scaleAnimation = CAKeyframeAnimation(keyPath: "transform.scale")
            scaleAnimation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
            scaleAnimation.beginTime = AVCoreAnimationBeginTimeAtZero + CFTimeInterval(CGFloat(index) * VideoEdVideoMakeManager.durationForOnePic)
            scaleAnimation.duration =  CFTimeInterval(VideoEdVideoMakeManager.durationForOnePic) //+ 0.19
            scaleAnimation.values = [1.4,1.1,1.0] // zoomout
            scaleAnimation.keyTimes = [0.0,0.5,1.0]
            scaleAnimation.isRemovedOnCompletion = false
            scaleAnimation.fillMode = CAMediaTimingFillMode.forwards
            newMasterLayerForIndex.add(scaleAnimation, forKey: "transform.scale\(index)")
            masterlayer.addSublayer(newMasterLayerForIndex)
            
        }else{
            
            
            for  i in 0..<6{
                
                let newMasterLayerForIndex = CALayer()
                newMasterLayerForIndex.frame = masterlayer.frame
                newMasterLayerForIndex.contents = image.cgImage
                newMasterLayerForIndex.opacity = 1.0
                newMasterLayerForIndex.contentsGravity = VideoEdVideoMakeManager.contentsGravity
                newMasterLayerForIndex.masksToBounds = true
                
                let beginTime = AVCoreAnimationBeginTimeAtZero + CFTimeInterval(CGFloat(index) * VideoEdVideoMakeManager.durationForOnePic)
                
                let translationAnimation = CAKeyframeAnimation(keyPath: "position.x")
                translationAnimation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
                translationAnimation.beginTime = beginTime + Double(i) * 0.2
                translationAnimation.duration =  CFTimeInterval(VideoEdVideoMakeManager.durationForOnePic)*0.1*Double(i)
                translationAnimation.values = [newMasterLayerForIndex.frame.width*1.5,newMasterLayerForIndex.frame.width/2-newMasterLayerForIndex.frame.width*0.05,newMasterLayerForIndex.frame.width/2]
                translationAnimation.keyTimes = [0.0,0.85,1.0]
                translationAnimation.isRemovedOnCompletion = false
                translationAnimation.fillMode = CAMediaTimingFillMode.both
                
                
                let objShapeLayer = CAShapeLayer()
                objShapeLayer.fillColor = UIColor.green.cgColor
                objShapeLayer.strokeColor = UIColor.black.cgColor
                
                objShapeLayer.path = arrowBezierPath1(sampleLayer: masterlayer , index : i).cgPath
                
                objShapeLayer.lineWidth = 0
                objShapeLayer.strokeEnd = 1.0
                
                newMasterLayerForIndex.mask = objShapeLayer
                
                newMasterLayerForIndex.add(translationAnimation, forKey: "origin.x\(index)")
                
                
                masterlayer.addSublayer(newMasterLayerForIndex)
                
            }
        }
        
        
        
        if !isSaving {
            masterlayer.isGeometryFlipped = true
        }
        
    }
    
    func coolAnimation(masterlayer : CALayer, images : [UIImage] , index : Int , totalCount : Int , isSaving : Bool){
        
        let image = images[index]
        let newMasterLayerForIndex = CALayer()
        newMasterLayerForIndex.frame = masterlayer.frame
        newMasterLayerForIndex.contents = image.cgImage
        newMasterLayerForIndex.opacity = 0.0
        newMasterLayerForIndex.contentsGravity = VideoEdVideoMakeManager.contentsGravity
        newMasterLayerForIndex.masksToBounds = true
        
        let beginTime = AVCoreAnimationBeginTimeAtZero + CFTimeInterval(CGFloat(index) * VideoEdVideoMakeManager.durationForOnePic)
        
        
        
        let translationAnimation = CAKeyframeAnimation(keyPath: "position.x")
        translationAnimation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
        translationAnimation.beginTime = beginTime
        translationAnimation.duration =  CFTimeInterval(VideoEdVideoMakeManager.durationForOnePic)*0.4
        translationAnimation.values = [-newMasterLayerForIndex.frame.width,newMasterLayerForIndex.frame.width/2-newMasterLayerForIndex.frame.width*0.05,newMasterLayerForIndex.frame.width/2]
        translationAnimation.keyTimes = [0.0,0.85,1.0]
        translationAnimation.isRemovedOnCompletion = false
        translationAnimation.fillMode = CAMediaTimingFillMode.both
        
        let animation2 = CAKeyframeAnimation(keyPath: "opacity")
        animation2.duration = CFTimeInterval(VideoEdVideoMakeManager.durationForOnePic)*0.55
        animation2.beginTime =  beginTime
        animation2.values = [0,1.0]
        animation2.keyTimes = [0.0,1.0]
        animation2.isRemovedOnCompletion = false
        animation2.fillMode = CAMediaTimingFillMode.both
        
        if index > 0 {
            
            newMasterLayerForIndex.add(translationAnimation, forKey: "origin.x\(index)")
            newMasterLayerForIndex.add(animation2, forKey: "animationReverseFade" + "\(index)")
            
        }else{
            newMasterLayerForIndex.opacity = 1.0
            
        }
        masterlayer.addSublayer(newMasterLayerForIndex)
        if !isSaving {
            masterlayer.isGeometryFlipped = true
        }
        
    }
    func noirAnimation(masterlayer : CALayer, images : [UIImage] , index : Int , totalCount : Int , isSaving : Bool){
        
        let image = images[index]
        let newMasterLayerForIndex = CALayer()
        newMasterLayerForIndex.frame = masterlayer.frame
        newMasterLayerForIndex.contents = image.noir?.cgImage
        newMasterLayerForIndex.opacity = 1.0
        newMasterLayerForIndex.contentsGravity = VideoEdVideoMakeManager.contentsGravity
        newMasterLayerForIndex.masksToBounds = true
        
        if index + 1 <= totalCount - 1 {
            
            noirLayer(duration: CFTimeInterval(VideoEdVideoMakeManager.durationForOnePic * 0.5) , filledLayer: newMasterLayerForIndex, timerFillDiameter: newMasterLayerForIndex.frame.width, begintime: AVCoreAnimationBeginTimeAtZero + CFTimeInterval( CGFloat(index) * VideoEdVideoMakeManager.durationForOnePic) , index : index)
            
            
        }
        masterlayer.insertSublayer(newMasterLayerForIndex, at: 0)
        
        
        let scaleAnimation = CAKeyframeAnimation(keyPath: "transform.scale")
        scaleAnimation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
        scaleAnimation.beginTime = AVCoreAnimationBeginTimeAtZero + CFTimeInterval(CGFloat(index) * VideoEdVideoMakeManager.durationForOnePic)
        scaleAnimation.duration =  CFTimeInterval(VideoEdVideoMakeManager.durationForOnePic) //+ 0.19
        if index % 2 == 0{
            scaleAnimation.values = [1.2,1.1,1.0]
        }else{
            scaleAnimation.values = [1.0,1.1,1.2]
        }
        scaleAnimation.keyTimes = [0.0,0.5,1.0]
        //        scaleAnimation.isAdditive = true
        scaleAnimation.isRemovedOnCompletion = false
        scaleAnimation.fillMode = CAMediaTimingFillMode.forwards
        masterlayer.add(scaleAnimation, forKey: "transform.scale\(index)")

        if !isSaving {
            masterlayer.isGeometryFlipped = true
        }
    }
    open func noirLayer(duration: CFTimeInterval, filledLayer : CALayer? , timerFillDiameter : CGFloat , begintime: CFTimeInterval , index : Int) {
        
        if let parentLayer = filledLayer {
            
            let maskLayer = CAShapeLayer()
            maskLayer.fillColor = UIColor.clear.cgColor
            maskLayer.strokeColor = UIColor.black.cgColor
            let circleRadius = timerFillDiameter
            
            maskLayer.path = noirPath(layersize: parentLayer.frame.size, index : index)
            maskLayer.lineWidth = 0
            maskLayer.strokeEnd = 1.0
            
            parentLayer.mask = maskLayer
            let animation = CABasicAnimation(keyPath: "lineWidth")
            animation.duration = duration
            animation.fromValue = circleRadius*2
            animation.toValue = 0
            animation.beginTime =  begintime
//            animation.fillMode = CAMediaTimingFillMode.both
            animation.isRemovedOnCompletion = false
            maskLayer.add(animation, forKey: "strokeWidth" + "\(begintime)")
         

        }
    }
    func noirPath(layersize : CGSize , index : Int) -> CGPath {
        
        if index % 2 == 0{
            
            let scale =  layersize.width / 100
            let bezierPath = UIBezierPath()
            bezierPath.move(to: CGPoint(x: 50 * scale, y: -4.5 * scale))
            bezierPath.addLine(to: CGPoint(x: 50 * scale, y: 102.5 * scale))
            UIColor.black.setStroke()
            bezierPath.stroke()
            return bezierPath.cgPath
            
        }else{
            
            let scale =  layersize.width / 100
            let bezierPath = UIBezierPath()
            bezierPath.move(to: CGPoint(x: 0 * scale, y: 50 * scale))
            bezierPath.addLine(to: CGPoint(x: 100 * scale, y: 50 * scale))
            UIColor.black.setStroke()
            bezierPath.stroke()
            return bezierPath.cgPath
        }
        
        
    }

    func fieldAnimation(masterlayer : CALayer, images : [UIImage] , index : Int , totalCount : Int , isSaving : Bool){
        let image = images[index]
        let newMasterLayerForIndex = CALayer()
        newMasterLayerForIndex.frame = masterlayer.frame
        newMasterLayerForIndex.contents = image.cgImage
        newMasterLayerForIndex.opacity = 1.0
        newMasterLayerForIndex.contentsGravity = VideoEdVideoMakeManager.contentsGravity
        newMasterLayerForIndex.masksToBounds = true
        
        let beginTime = AVCoreAnimationBeginTimeAtZero + CFTimeInterval(CGFloat(index) * VideoEdVideoMakeManager.durationForOnePic)
        if index + 1 <= totalCount - 1 {
            
            fieldMaskAnimation(duration: CFTimeInterval(VideoEdVideoMakeManager.durationForOnePic * 0.5) , filledLayer: newMasterLayerForIndex, timerFillDiameter: newMasterLayerForIndex.frame.width * 2, begintime: beginTime, index: index )
            
        }
        masterlayer.insertSublayer(newMasterLayerForIndex, at: 0)
        if !isSaving {
            //            masterlayer.isGeometryFlipped = true
        }
        
    }
    
    open func fieldMaskAnimation(duration: CFTimeInterval, filledLayer : CALayer? , timerFillDiameter : CGFloat , begintime: CFTimeInterval , index : Int) {
        
        if let parentLayer = filledLayer {
            
            let maskLayer = CAShapeLayer()
            maskLayer.frame = parentLayer.frame
            
            maskLayer.fillColor = UIColor.clear.cgColor
            maskLayer.strokeColor = UIColor.black.cgColor
            maskLayer.lineWidth = (filledLayer?.frame.width)! / 2
            
            let scale =  (filledLayer?.frame.width)! / 100
            
            let bezierPath = UIBezierPath()
            bezierPath.move(to: CGPoint(x: 25 * scale, y: 0))
            bezierPath.addLine(to: CGPoint(x: 25 * scale, y: 150 * scale))
            bezierPath.addLine(to: CGPoint(x: -204 * scale, y: -13 * scale))
            bezierPath.addLine(to: CGPoint(x: 75 * scale, y: -30 * scale))
            bezierPath.addLine(to: CGPoint(x: 75 * scale, y: 100 * scale))
            bezierPath.stroke()
            
            
            let path =  bezierPath
            maskLayer.path = path.cgPath
            maskLayer.strokeEnd = 0
            
            let animation = CABasicAnimation(keyPath: "strokeEnd")
            animation.duration = duration*2
            animation.beginTime =  begintime
            animation.fillMode = CAMediaTimingFillMode.both
            animation.isRemovedOnCompletion = false
            
            let animation2 = CABasicAnimation(keyPath: "strokeStart")
            animation2.duration = duration
            animation2.beginTime =  begintime
            animation2.fillMode = CAMediaTimingFillMode.both
            animation2.isRemovedOnCompletion = false
            
            
            animation.fromValue = 1.0
            animation.toValue = 0.0
            animation2.fromValue = 0
            animation2.toValue = 1.0
            
            
            maskLayer.add(animation, forKey: "strokeEnd\(begintime)")
            maskLayer.add(animation2, forKey: "strokeStart/(begintime)")
            
            parentLayer.mask = maskLayer
            
        }
    }

    func laughAnimation(masterlayer : CALayer, images : [UIImage] , index : Int , totalCount : Int , isSaving : Bool){
        let image = images[index]
        let newMasterLayerForIndex = CALayer()
        newMasterLayerForIndex.frame = masterlayer.frame
        newMasterLayerForIndex.contents = image.cgImage
        newMasterLayerForIndex.opacity = 1.0
        newMasterLayerForIndex.contentsGravity = VideoEdVideoMakeManager.contentsGravity
        newMasterLayerForIndex.masksToBounds = true
        
        let beginTime = AVCoreAnimationBeginTimeAtZero + CFTimeInterval(CGFloat(index) * VideoEdVideoMakeManager.durationForOnePic)
        if index + 1 <= totalCount - 1 {
            
            laughMaskAnimation(duration: CFTimeInterval(VideoEdVideoMakeManager.durationForOnePic * 0.5) , filledLayer: newMasterLayerForIndex, timerFillDiameter: newMasterLayerForIndex.frame.width * 2, begintime: beginTime, index: index )
            
        }
        masterlayer.insertSublayer(newMasterLayerForIndex, at: 0)
        if !isSaving {
            masterlayer.isGeometryFlipped = true
        }

    }
    
    open func laughMaskAnimation(duration: CFTimeInterval, filledLayer : CALayer? , timerFillDiameter : CGFloat , begintime: CFTimeInterval , index : Int) {
        
        if let parentLayer = filledLayer {
            
            let maskLayer = CAShapeLayer()
            maskLayer.frame = parentLayer.frame
            
            let circleRadius = timerFillDiameter * 2.5
            let circleHalfRadius = circleRadius * 0.5
            let circleBounds = CGRect(x: parentLayer.bounds.midX - circleHalfRadius, y: parentLayer.bounds.midY - circleHalfRadius, width: circleRadius, height: circleRadius)
            
            maskLayer.fillColor = UIColor.clear.cgColor
            maskLayer.strokeColor = UIColor.black.cgColor
            maskLayer.lineWidth = circleRadius
//            maskLayer.lineWidth = (filledLayer?.frame.width)! / 2

            let path =   UIBezierPath(roundedRect: circleBounds, cornerRadius: circleBounds.size.width * 2.5)
            maskLayer.path = path.cgPath
            maskLayer.strokeEnd = 0
            
            let animation = CABasicAnimation(keyPath: "strokeEnd")
            animation.duration = duration
            animation.beginTime =  begintime
            animation.fillMode = CAMediaTimingFillMode.both
            animation.isRemovedOnCompletion = false
            
            let animation2 = CABasicAnimation(keyPath: "strokeStart")
            animation2.duration = duration
            animation2.beginTime =  begintime
            animation2.fillMode = CAMediaTimingFillMode.both
            animation2.isRemovedOnCompletion = false


            animation.fromValue = 1.0
            animation.toValue = 0.0
            animation2.fromValue = 0
            animation2.toValue = 1.0


            maskLayer.add(animation, forKey: "strokeEnd\(begintime)")
            maskLayer.add(animation2, forKey: "strokeStart/(begintime)")

            parentLayer.mask = maskLayer

        }
    }

    func emitterAnimation(masterlayer : CALayer, images : [UIImage] , index : Int , totalCount : Int,emitterType :  EMITTER_ANIMATIONS, isSaving : Bool){
        
        let image = images[index]
        let sampleLayer = CALayer()
        sampleLayer.frame = masterlayer.frame
        sampleLayer.contents = image.cgImage
        sampleLayer.opacity = 0.0
        sampleLayer.contentsGravity = VideoEdVideoMakeManager.contentsGravity
        sampleLayer.masksToBounds = true
        
        addLittleFadeOpacityAnimation(sampleLayer: sampleLayer, index: index, totalCount: totalCount, isSaving: isSaving)
        
        switch emitterType {
            
        case .FIREWORK:
            let particleEmitter = flameEmittedLayer(parentLayer: masterlayer)
            particleEmitter.emitterCells = [setupFlameEmitterCell(parentLayer: masterlayer)]
            sampleLayer.addSublayer(particleEmitter)
            break
            
        case .SNOW:
            
//            let emitter = CAEmitterLayer()
//            emitter.masksToBounds = true
//            emitter.emitterShape = .line
//            emitter.emitterPosition = CGPoint(x: bounds.midX, y: 0)
//            emitter.emitterSize = CGSize(width: bounds.size.width, height: 1)
//
//            let near = makeEmmiterCell(color: UIColor(white: 1, alpha: 1), velocity: 100, scale: 0.3)
//            let middle = makeEmmiterCell(color: UIColor(white: 1, alpha: 0.66), velocity: 80, scale: 0.2)
//            let far = makeEmmiterCell(color: UIColor(white: 1, alpha: 0.33), velocity: 60, scale: 0.1)
//
//            emitter.emitterCells = [near, middle, far]
//            sampleLayer.addSublayer(emitter)

            let particleEmitter = snowEmittedLayer(parentLayer: masterlayer)
//            particleEmitter.emitterPosition = CGPoint(x: masterlayer.bounds.midX, y: 0)
//            particleEmitter.emitterSize = CGSize(width: masterlayer.bounds.size.width, height: 1)

            particleEmitter.emitterCells = [makeSnowflakeCell(size : masterlayer.frame.size)]
            sampleLayer.addSublayer(particleEmitter)
            break
            
        case .FLAME:
            let particleEmitter = fireworkEmittedLayer(parentLayer: masterlayer)
            let cell = setupEmitterCell(parentLayer: masterlayer, isSaving: isSaving)
            cell.emitterCells = [setupTrailCell(parentLayer: masterlayer, isSaving: isSaving), setupFireworkCell(parentLayer: masterlayer, isSaving: isSaving)]
            particleEmitter.emitterCells = [cell]
            sampleLayer.addSublayer(particleEmitter)
            break
        case .RANING:
            let particleEmitter = raningEmittedLayer(parentLayer: masterlayer)
            particleEmitter.emitterCells = [makeRaningCell(size : masterlayer.frame.size)]
            sampleLayer.addSublayer(particleEmitter)
            break

        }
        
        
        masterlayer.addSublayer(sampleLayer)
        if !isSaving{
            masterlayer.isGeometryFlipped = true
            
        }
        
    }
    func raningEmittedLayer(parentLayer : CALayer)-> CAEmitterLayer{
        let newEmitterLayer = CAEmitterLayer()
        newEmitterLayer.emitterSize = parentLayer.bounds.size
        newEmitterLayer.emitterShape = CAEmitterLayerEmitterShape.line
        newEmitterLayer.emitterPosition = CGPoint(x:(parentLayer.bounds.width) / 2, y: (parentLayer.bounds.height)*(1.5))
        return newEmitterLayer
    }
    
    func snowEmittedLayer(parentLayer : CALayer)-> CAEmitterLayer{
        let snowflakeEmitterLayer = CAEmitterLayer()
        
        snowflakeEmitterLayer.emitterSize = parentLayer.bounds.size
        snowflakeEmitterLayer.emitterShape = CAEmitterLayerEmitterShape.rectangle
        snowflakeEmitterLayer.emitterPosition = CGPoint(x:(parentLayer.bounds.width) / 2, y: (parentLayer.bounds.height))
        return snowflakeEmitterLayer
    }
    

    func makeEmitterCell(color: UIColor) -> CAEmitterCell {
        let cell = CAEmitterCell()
        cell.birthRate = 3
        cell.lifetime = 7.0
        cell.lifetimeRange = 0
        cell.color = color.cgColor
        cell.velocity = 1000
        cell.velocityRange = 50
        cell.emissionLongitude = CGFloat.pi
        cell.emissionRange = CGFloat.pi / 4
        cell.spin = 0.2
        cell.spinRange = 0
        cell.scaleRange = 0.5
        cell.scaleSpeed = -0.05
        let image = UIImage(named: "drop.png")
        cell.contents = image?.cgImage
        return cell
    }
    
//    func makeRaningCell(size : CGSize) -> CAEmitterCell{
//        var  image = UIImage(named: "longDrop.png")
//        image = image?.resizeImage(targetSize: CGSize(width: size.width * 0.05, height: size.width * 0.05))
//        let snowflakeEmitterCell = CAEmitterCell()
//        snowflakeEmitterCell.color = UIColor.white.cgColor
//        snowflakeEmitterCell.redRange = 0.8
//        snowflakeEmitterCell.contents = image?.cgImage
//        snowflakeEmitterCell.lifetime = 5.5
//        snowflakeEmitterCell.birthRate = 300
//        snowflakeEmitterCell.blueRange = 0.15
//        snowflakeEmitterCell.alphaRange = 0.4
//        snowflakeEmitterCell.velocity = 0
//        snowflakeEmitterCell.velocityRange = 500
//        snowflakeEmitterCell.scale = 0.4
//        snowflakeEmitterCell.scaleRange = 1.3
//        //        snowflakeEmitterCell.emissionRange = CGFloat.pi / 2
//        //        snowflakeEmitterCell.emissionLongitude = CGFloat.pi
//        snowflakeEmitterCell.yAcceleration = -100
//        snowflakeEmitterCell.scaleSpeed = -0.1
//        snowflakeEmitterCell.alphaSpeed = -0.05
//        return snowflakeEmitterCell
//
//    }
    
    func makeRaningCell(size : CGSize) -> CAEmitterCell{
        var  image = UIImage(named: "longDrop.png")
        image = image?.resizeImage(targetSize: CGSize(width: size.width * 0.05, height: size.width * 0.05))
        let snowflakeEmitterCell = CAEmitterCell()
        snowflakeEmitterCell.color = UIColor.white.cgColor
        snowflakeEmitterCell.redRange = 0.8
        snowflakeEmitterCell.contents = image?.cgImage
        snowflakeEmitterCell.lifetime = 5.5
        snowflakeEmitterCell.birthRate = 300
        snowflakeEmitterCell.blueRange = 0.15
        snowflakeEmitterCell.alphaRange = 0.4
        snowflakeEmitterCell.velocity = 600
        snowflakeEmitterCell.velocityRange = 500
        snowflakeEmitterCell.scale = 0.4
        snowflakeEmitterCell.scaleRange = 1.3
//        snowflakeEmitterCell.emissionRange = CGFloat.pi / 2
//        snowflakeEmitterCell.emissionLongitude = CGFloat.pi
        snowflakeEmitterCell.yAcceleration = -100
        snowflakeEmitterCell.scaleSpeed = -0.1
        snowflakeEmitterCell.alphaSpeed = -0.05
        return snowflakeEmitterCell

    }

    func makeSnowflakeCell(size : CGSize) -> CAEmitterCell{
        var  image = UIImage(named: "drop.png")
        image = image?.resizeImage(targetSize: CGSize(width: size.width * 0.05, height: size.width * 0.05))
        let snowflakeEmitterCell = CAEmitterCell()
        snowflakeEmitterCell.color = UIColor.white.cgColor
        snowflakeEmitterCell.contents = image?.cgImage
        snowflakeEmitterCell.lifetime = 5.5
        snowflakeEmitterCell.birthRate = 100
        snowflakeEmitterCell.blueRange = 0.15
        snowflakeEmitterCell.alphaRange = 0.4
        snowflakeEmitterCell.velocity = 10
        snowflakeEmitterCell.velocityRange = 300
        snowflakeEmitterCell.scale = 0.4
        snowflakeEmitterCell.scaleRange = 1.3
        snowflakeEmitterCell.emissionRange = CGFloat.pi / 2
        snowflakeEmitterCell.emissionLongitude = CGFloat.pi
        snowflakeEmitterCell.yAcceleration = -70
        snowflakeEmitterCell.scaleSpeed = -0.1
        snowflakeEmitterCell.alphaSpeed = -0.05
        return snowflakeEmitterCell

    }

    func flameEmittedLayer(parentLayer : CALayer)-> CAEmitterLayer{
        
        let flameEmitterLayer = CAEmitterLayer()
        flameEmitterLayer.emitterSize = CGSize(width: parentLayer.bounds.size.width/2, height: parentLayer.bounds.size.height/2)
        flameEmitterLayer.emitterPosition = CGPoint(x:(parentLayer.bounds.width) * 0.5 , y: (parentLayer.bounds.height)*0.25 )
        flameEmitterLayer.renderMode = CAEmitterLayerRenderMode.additive
        return flameEmitterLayer
    }

    private func setupFlameEmitterCell(parentLayer: CALayer) -> CAEmitterCell {
        let widthScale = parentLayer.frame.width / UIScreen.main.bounds.width

        let image = UIImage(named: "drop.png")

        
        let flameEmitterCell = CAEmitterCell()
        flameEmitterCell.color = UIColor(red: 1, green: 0.5, blue: 0.2, alpha: 1.0).cgColor
        flameEmitterCell.contents = image?.cgImage
        flameEmitterCell.lifetime = 5.0
        flameEmitterCell.birthRate = Float(150 * widthScale)
        flameEmitterCell.alphaSpeed = -0.4
        flameEmitterCell.velocity = 100 * widthScale
        flameEmitterCell.velocityRange = 50 * widthScale
        flameEmitterCell.emissionRange = CGFloat.pi
        return flameEmitterCell
    }
    func fireworkEmittedLayer(parentLayer : CALayer)-> CAEmitterLayer{
        
        let emitterLayer = CAEmitterLayer()
        
        emitterLayer.emitterSize = parentLayer.bounds.size
        emitterLayer.emitterPosition = CGPoint(x:(parentLayer.bounds.width) / 2, y: 0 )
        emitterLayer.renderMode = CAEmitterLayerRenderMode.additive

        
        return emitterLayer
    }

    private func setupEmitterCell(parentLayer: CALayer,isSaving : Bool) -> CAEmitterCell {
        
        let widthScale = parentLayer.frame.width / UIScreen.main.bounds.width
        let emitterCell = CAEmitterCell()
        emitterCell.color = UIColor(red: 0.5, green: 0.5, blue: 0.5, alpha: 0.5).cgColor
        emitterCell.redRange = Float(0.9 )
        emitterCell.greenRange = Float(0.9 )
        emitterCell.blueRange = Float(0.9 )
        emitterCell.lifetime = Float(2.5 )
        emitterCell.birthRate = Float(5 )
        emitterCell.velocity = 300
        emitterCell.velocityRange = 100
        emitterCell.emissionRange = CGFloat.pi / 4
        emitterCell.emissionLongitude = CGFloat.pi / 2
        emitterCell.yAcceleration = parentLayer.frame.height/50
        emitterCell.yAcceleration = 20 * widthScale
        if isSaving{
            emitterCell.yAcceleration = parentLayer.frame.height/2
        }

        return emitterCell
    }
    private func setupTrailCell(parentLayer: CALayer , isSaving : Bool ) -> CAEmitterCell {
        

        let image = UIImage(named: "drop.png")
        let trailCell = CAEmitterCell()
        trailCell.contents = image?.cgImage
        trailCell.lifetime = Float(0.5 )
        trailCell.birthRate = Float(45 )
        trailCell.velocity = 80
        trailCell.scale = 0.4
        trailCell.alphaSpeed = Float(-0.7 )
        trailCell.scaleSpeed = -0.1
        trailCell.scaleRange = 0.1
        trailCell.beginTime = CFTimeInterval(0.01 )
        trailCell.duration = CFTimeInterval(1.7 )
        trailCell.emissionRange = CGFloat.pi / 8
        trailCell.emissionLongitude = CGFloat.pi * 2
        trailCell.yAcceleration = -100
//        trailCell.yAcceleration = 100;
        if isSaving{
            trailCell.yAcceleration = -200;
        }
        return trailCell
    }
    private func setupFireworkCell(parentLayer: CALayer , isSaving : Bool) -> CAEmitterCell {
        let widthScale = parentLayer.frame.width / UIScreen.main.bounds.width

        let image = UIImage(named: "drop.png")
        let fireworkCell = CAEmitterCell()
        fireworkCell.contents = image?.cgImage
        fireworkCell.lifetime = Float(100 )
        fireworkCell.birthRate = Float(20000 )
        fireworkCell.velocity = 130
        fireworkCell.scale = 0.6
        fireworkCell.spin = 2
        fireworkCell.alphaSpeed = Float(-0.2 )
        fireworkCell.scaleSpeed = -0.1
        fireworkCell.beginTime = CFTimeInterval(1.5 )
        fireworkCell.duration = CFTimeInterval(0.1 )
        fireworkCell.emissionRange = CGFloat.pi * 2
        fireworkCell.yAcceleration = -50 * widthScale
        if isSaving{
            fireworkCell.yAcceleration = -30 * widthScale
        }
        return fireworkCell
    }


    func celticAnimation(masterlayer : CALayer, images : [UIImage] , index : Int , totalCount : Int , isSaving : Bool){
        
        let image = images[index]
        let sampleLayer = CALayer()
        sampleLayer.frame = masterlayer.frame
        sampleLayer.contents = image.cgImage
        sampleLayer.opacity = 1.0
        sampleLayer.contentsGravity = VideoEdVideoMakeManager.contentsGravity
        
        let scaleAnimation = CAKeyframeAnimation(keyPath: "transform.scale")
        scaleAnimation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
        scaleAnimation.beginTime = AVCoreAnimationBeginTimeAtZero + CFTimeInterval(CGFloat(index) * VideoEdVideoMakeManager.durationForOnePic)
        scaleAnimation.duration =  CFTimeInterval(VideoEdVideoMakeManager.durationForOnePic)*0.8
        scaleAnimation.values = [1.2,1.2,1.2]
        scaleAnimation.isRemovedOnCompletion = false
        scaleAnimation.fillMode = CAMediaTimingFillMode.both
        sampleLayer.add(scaleAnimation, forKey: "transform.scale\(index)")
        
        if index>0{
            
            let translationAnimation = CAKeyframeAnimation(keyPath: "position.x")
            translationAnimation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
            translationAnimation.beginTime = AVCoreAnimationBeginTimeAtZero + CFTimeInterval(CGFloat(index) * VideoEdVideoMakeManager.durationForOnePic)
            translationAnimation.duration =  CFTimeInterval(VideoEdVideoMakeManager.durationForOnePic)*0.8
            translationAnimation.values = [-sampleLayer.frame.width,sampleLayer.frame.width/2-sampleLayer.frame.width*0.05,sampleLayer.frame.width/2]
            translationAnimation.keyTimes = [0.0,0.05,1.0]
            translationAnimation.isRemovedOnCompletion = false
            translationAnimation.fillMode = CAMediaTimingFillMode.both
            sampleLayer.add(translationAnimation, forKey: "origin.x\(index)")
        }else{
            let translationAnimation = CAKeyframeAnimation(keyPath: "position.x")
            translationAnimation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
            translationAnimation.beginTime = AVCoreAnimationBeginTimeAtZero + CFTimeInterval(CGFloat(index) * VideoEdVideoMakeManager.durationForOnePic)
            translationAnimation.duration =  CFTimeInterval(VideoEdVideoMakeManager.durationForOnePic)*0.8
            translationAnimation.values = [sampleLayer.frame.width/2-sampleLayer.frame.width*0.05,sampleLayer.frame.width/2]
            translationAnimation.keyTimes = [0.0,1.0]
            translationAnimation.isRemovedOnCompletion = false
            translationAnimation.fillMode = CAMediaTimingFillMode.both
            sampleLayer.add(translationAnimation, forKey: "origin.x\(index)")
            
        }
        
        
        sampleLayer.masksToBounds = true
        masterlayer.addSublayer(sampleLayer)
    }
    func nightOutAnimation(masterlayer : CALayer, images : [UIImage] , index : Int , totalCount : Int , isSaving : Bool){
        
        if images.count>0{
            let image = images[index]
            let newMasterLayerForIndex = CALayer()
            newMasterLayerForIndex.frame = masterlayer.frame
            newMasterLayerForIndex.contents = image.cgImage
            newMasterLayerForIndex.opacity = 1.0
            newMasterLayerForIndex.contentsGravity = VideoEdVideoMakeManager.contentsGravity
            newMasterLayerForIndex.masksToBounds = true
            
            if index + 1 <= totalCount - 1 {
                
                nightoutLayer(duration: CFTimeInterval(VideoEdVideoMakeManager.durationForOnePic * 0.5) , filledLayer: newMasterLayerForIndex, timerFillDiameter: newMasterLayerForIndex.frame.width, begintime: AVCoreAnimationBeginTimeAtZero + CFTimeInterval( CGFloat(index) * VideoEdVideoMakeManager.durationForOnePic) , index : index)
            }
            masterlayer.insertSublayer(newMasterLayerForIndex, at: 0)
            
            if !isSaving {
                masterlayer.isGeometryFlipped = true
            }
        }
//        if images.count>0{
//            let image = images[index]
//            let newMasterLayerForIndex = CALayer()
//            newMasterLayerForIndex.frame = masterlayer.frame
//            newMasterLayerForIndex.contents = image.cgImage
//            newMasterLayerForIndex.opacity = 1.0
//            newMasterLayerForIndex.contentsGravity = VideoEdVideoMakeManager.contentsGravity
//            newMasterLayerForIndex.masksToBounds = true
//
//            if index + 1 <= totalCount - 1 {
//
//                nightoutLayer(duration: CFTimeInterval(VideoEdVideoMakeManager.durationForOnePic * 0.5) , filledLayer: newMasterLayerForIndex, timerFillDiameter: newMasterLayerForIndex.frame.width, begintime: AVCoreAnimationBeginTimeAtZero + CFTimeInterval( CGFloat(index) * VideoEdVideoMakeManager.durationForOnePic) , index : index)
//            }
//            masterlayer.insertSublayer(newMasterLayerForIndex, at: 0)
//
//            if !isSaving {
//                masterlayer.isGeometryFlipped = true
//            }
//        }
    }
    
    open func nightoutLayer(duration: CFTimeInterval, filledLayer : CALayer? , timerFillDiameter : CGFloat , begintime: CFTimeInterval , index : Int) {
        
        if let parentLayer = filledLayer {
            
            let motherlayer = CALayer()
            motherlayer.frame = parentLayer.bounds
            parentLayer.addSublayer(motherlayer)

            
            var frame1 = parentLayer.frame
            frame1.size.width = frame1.width/2
            
            let maskHelper = CALayer()
            maskHelper.frame = frame1
            motherlayer.addSublayer(maskHelper)
            
            frame1.origin.x = frame1.width/2
            let mask2Helper = CALayer()
            mask2Helper.frame = frame1
            motherlayer.addSublayer(mask2Helper)
            
            
            let maskLayer = CAShapeLayer()
            maskLayer.fillColor = UIColor.clear.cgColor
            maskLayer.strokeColor = UIColor.black.cgColor
            let circleRadius = timerFillDiameter
            
            maskLayer.path = nightoutpath(layersize: maskHelper.frame.size, index : 0)
            maskLayer.lineWidth = circleRadius*0.5
            maskLayer.strokeEnd = 0
            
            parentLayer.mask = maskLayer
            let animation = CABasicAnimation(keyPath: "strokeEnd")
            animation.duration = duration
            animation.fromValue = 0
            animation.toValue = 1
            animation.beginTime =  begintime
            animation.fillMode = CAMediaTimingFillMode.both
            animation.isRemovedOnCompletion = false
            maskLayer.add(animation, forKey: "strokeEnd0" + "\(begintime)")
            
        }
//        if let parentLayer = filledLayer {
//
//            let maskLayer = CAShapeLayer()
//            maskLayer.fillColor = UIColor.clear.cgColor
//            maskLayer.strokeColor = UIColor.black.cgColor
//            let circleRadius = timerFillDiameter
//
//            maskLayer.path = nightoutpath(layersize: parentLayer.frame.size, index : 1)
//            maskLayer.lineWidth = circleRadius*0.5
//            maskLayer.strokeEnd = 0
//
//            parentLayer.mask = maskLayer
//            let animation = CABasicAnimation(keyPath: "strokeEnd")
//            animation.duration = duration
//            animation.fromValue = 0
//            animation.toValue = 1
//            animation.beginTime =  begintime
//            animation.fillMode = CAMediaTimingFillMode.both
//            animation.isRemovedOnCompletion = false
//            maskLayer.add(animation, forKey: "strokeEnd1" + "\(begintime)")
//
//        }
    }
    func nightoutpath(layersize : CGSize , index : Int) -> CGPath {
        let mutablePath      = CGMutablePath()

        if index != -1{
            
            let scale =  layersize.width / 100
            let bezier2Path = UIBezierPath()
            bezier2Path.move(to: CGPoint(x: 25, y: 100).scalePoint(scale: scale))
            bezier2Path.addLine(to: CGPoint(x: 25, y: 0).scalePoint(scale: scale))
            UIColor.black.setStroke()
            bezier2Path.lineWidth = 50*scale
            bezier2Path.stroke()

            mutablePath.addPath(bezier2Path.cgPath)
//            return bezier2Path.cgPath
            
        }else{
            
            let scale =  layersize.width / 100
            let bezier2Path = UIBezierPath()
            bezier2Path.move(to: CGPoint(x: 75, y: 0).scalePoint(scale: scale))
            bezier2Path.addLine(to: CGPoint(x: 75, y: 100).scalePoint(scale: scale))
            UIColor.black.setStroke()
            bezier2Path.lineWidth = 50*scale
            bezier2Path.stroke()
            
//            return bezier2Path.cgPath
            mutablePath.addPath(bezier2Path.cgPath)

        }
        let scale =  layersize.width / 100
        let bezier2Path = UIBezierPath()
        bezier2Path.move(to: CGPoint(x: 75, y: 0).scalePoint(scale: scale))
        bezier2Path.addLine(to: CGPoint(x: 75, y: 100).scalePoint(scale: scale))
        UIColor.black.setStroke()
        bezier2Path.lineWidth = 50*scale
        bezier2Path.stroke()
        
        mutablePath.addPath(bezier2Path.cgPath)

        return mutablePath

    }
    func familyAnimation(masterlayer : CALayer, image : UIImage , index : Int , totalCount : Int , isSaving : Bool){
        
        let sampleLayer = CALayer()
        sampleLayer.frame = masterlayer.frame
        sampleLayer.contents = image.cgImage
        sampleLayer.opacity = 0.0
        sampleLayer.contentsGravity = VideoEdVideoMakeManager.contentsGravity
        
        addLittleFadeOpacityAnimation(sampleLayer: sampleLayer, index: index, totalCount: totalCount, isSaving: isSaving)
        
        
        let scaleAnimation = CAKeyframeAnimation(keyPath: "transform.scale")
        scaleAnimation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
        scaleAnimation.beginTime = AVCoreAnimationBeginTimeAtZero + CFTimeInterval(CGFloat(index) * VideoEdVideoMakeManager.durationForOnePic)
        scaleAnimation.duration =  CFTimeInterval(VideoEdVideoMakeManager.durationForOnePic)*0.8
        scaleAnimation.values = [5.8,1.2,1.0]
        scaleAnimation.keyTimes = [0.0,0.05,1.0]
        scaleAnimation.isRemovedOnCompletion = false
        scaleAnimation.fillMode = CAMediaTimingFillMode.both
        sampleLayer.add(scaleAnimation, forKey: "transform.scale\(index)")
        
        masterlayer.addSublayer(sampleLayer)
        
    }
    
    func babyAnimation(masterlayer : CALayer, images : [UIImage] , index : Int , totalCount : Int , isSaving : Bool){
        
        let image = images[index]
        let newMasterLayerForIndex = CALayer()
        newMasterLayerForIndex.frame = masterlayer.frame
        newMasterLayerForIndex.contents = image.cgImage
        newMasterLayerForIndex.opacity = 1.0
        newMasterLayerForIndex.contentsGravity = VideoEdVideoMakeManager.contentsGravity
        newMasterLayerForIndex.masksToBounds = true
        
        if index + 1 <= totalCount - 1 {
            
            babyLayer(duration: CFTimeInterval(VideoEdVideoMakeManager.durationForOnePic * 0.5) , filledLayer: newMasterLayerForIndex, timerFillDiameter: newMasterLayerForIndex.frame.width, begintime: AVCoreAnimationBeginTimeAtZero + CFTimeInterval( CGFloat(index) * VideoEdVideoMakeManager.durationForOnePic) , index : index)
            

        }
        masterlayer.insertSublayer(newMasterLayerForIndex, at: 0)
        
        if !isSaving {
            masterlayer.isGeometryFlipped = true
        }
    }
    open func babyLayer(duration: CFTimeInterval, filledLayer : CALayer? , timerFillDiameter : CGFloat , begintime: CFTimeInterval , index : Int) {
        
        if let parentLayer = filledLayer {
            
            let maskLayer = CAShapeLayer()
            maskLayer.fillColor = UIColor.clear.cgColor
            maskLayer.strokeColor = UIColor.black.cgColor
            let circleRadius = timerFillDiameter
            
            maskLayer.path = babyPath(layersize: parentLayer.frame.size, index : index)
            maskLayer.lineWidth = 0
            maskLayer.strokeEnd = 1.0
            
            parentLayer.mask = maskLayer
            let animation = CABasicAnimation(keyPath: "lineWidth")
            animation.duration = duration
            animation.fromValue = circleRadius*2
            animation.toValue = 0
            animation.beginTime =  begintime
            animation.fillMode = CAMediaTimingFillMode.both
            animation.isRemovedOnCompletion = false
            maskLayer.add(animation, forKey: "strokeWidth" + "\(begintime)")
            
        }
    }
    func babyPath(layersize : CGSize , index : Int) -> CGPath {
        
        if index % 2 == 0{
            
            let scale =  layersize.width / 100
            let bezierPath = UIBezierPath()
            bezierPath.move(to: CGPoint(x: 50 * scale, y: -4.5 * scale))
            bezierPath.addLine(to: CGPoint(x: 50 * scale, y: 102.5 * scale))
            UIColor.black.setStroke()
            bezierPath.stroke()
            return bezierPath.cgPath

        }else{
            
            let scale =  layersize.width / 100
            let bezierPath = UIBezierPath()
            bezierPath.move(to: CGPoint(x: 0 * scale, y: 50 * scale))
            bezierPath.addLine(to: CGPoint(x: 100 * scale, y: 50 * scale))
            UIColor.black.setStroke()
            bezierPath.stroke()
            return bezierPath.cgPath
        }
        
        
    }
    func cgColor(red: CGFloat, green: CGFloat, blue: CGFloat) -> CGColor {
        return UIColor(red: red/255.0, green: green/255.0, blue: blue/255.0, alpha: 1.0).cgColor
    }

    func rainbowAnimation(masterlayer : CALayer, images : [UIImage] , index : Int , totalCount : Int , isSaving : Bool){
        
        let image = images[index]
        let sampleLayer = CALayer()
        sampleLayer.frame = masterlayer.frame
        sampleLayer.contents = image.cgImage
        sampleLayer.opacity = 0.0
        sampleLayer.contentsGravity = VideoEdVideoMakeManager.contentsGravity
        
        let animation2 = CAKeyframeAnimation(keyPath: "opacity")
        animation2.duration = CFTimeInterval(VideoEdVideoMakeManager.durationForOnePic) + 0.19
        if index == 0 {
            
            animation2.beginTime =  AVCoreAnimationBeginTimeAtZero + CFTimeInterval( CGFloat(index) * VideoEdVideoMakeManager.durationForOnePic)
            animation2.values = [0.5,1.0,1.0,1.0]
            
            if isSaving{
                animation2.values = [1.0,1.0,1.0,1.0]
            }
            if (totalCount-1 == index){
                animation2.values = [1.0,1.0,1.0,1.0]
            }
            
        }else if (totalCount-1 == index){
            
            animation2.beginTime =  CFTimeInterval( CGFloat(index) * VideoEdVideoMakeManager.durationForOnePic) -  0.1
            animation2.values = [0,1.0,1.0,1.0]
        }
        else{
            animation2.beginTime =  CFTimeInterval( CGFloat(index) * VideoEdVideoMakeManager.durationForOnePic) -  0.1
            animation2.values = [0,1.0,1.0,1.0]
            
        }
        animation2.keyTimes = [0.0,0.1,0.9,1.0]
        animation2.isAdditive = true
        animation2.isRemovedOnCompletion = false
        animation2.fillMode = CAMediaTimingFillMode.forwards
        sampleLayer.add(animation2, forKey: "animation" + "\(index)")
        masterlayer.addSublayer(sampleLayer)
        
        
        var gradFrame = masterlayer.bounds
        gradFrame.origin.x = gradFrame.minX - gradFrame.width
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = gradFrame
        gradientLayer.colors = [cgColor(red: 209.0, green: 0.0, blue: 0.0),
                                cgColor(red: 255.0, green: 102.0, blue: 34.0),
                                cgColor(red: 255.0, green: 218.0, blue: 33.0),
                                cgColor(red: 51.0, green: 221.0, blue: 0.0),
                                cgColor(red: 17.0, green: 51.0, blue: 204.0),
                                cgColor(red: 34.0, green: 0.0, blue: 102.0),
                                cgColor(red: 51.0, green: 0.0, blue: 68.0)]
        
        gradientLayer.startPoint = CGPoint(x: 0, y: 0)
        gradientLayer.endPoint = CGPoint(x: 0, y: 1)
        masterlayer.addSublayer(gradientLayer)
        
        let posAnimationX1 = CABasicAnimation(keyPath: "position.x")
        posAnimationX1.fromValue = gradFrame.minX
        posAnimationX1.toValue = masterlayer.frame.midX
        posAnimationX1.fillMode = CAMediaTimingFillMode.forwards
        posAnimationX1.isRemovedOnCompletion = false
        posAnimationX1.beginTime =  CFTimeInterval( CGFloat(index+1) * VideoEdVideoMakeManager.durationForOnePic) -  0.2
        posAnimationX1.duration = 0.3
        if index + 1 <= totalCount - 1 {
            
            gradientLayer.add(posAnimationX1, forKey: "position\(index)")
        }
        
    }
    func crossAnimation(masterlayer : CALayer, images : [UIImage] , index : Int , totalCount : Int , isSaving : Bool){
        
        let image = images[index]
        let newMasterLayerForIndex = CALayer()
        newMasterLayerForIndex.frame = masterlayer.frame
        newMasterLayerForIndex.contents = image.cgImage
        newMasterLayerForIndex.opacity = 1.0
        newMasterLayerForIndex.contentsGravity = VideoEdVideoMakeManager.contentsGravity
        newMasterLayerForIndex.masksToBounds = true
        
        if index + 1 <= totalCount - 1 {
            
           crossLayer(duration: CFTimeInterval(VideoEdVideoMakeManager.durationForOnePic * 0.5) , filledLayer: newMasterLayerForIndex, timerFillDiameter: newMasterLayerForIndex.frame.width, begintime: AVCoreAnimationBeginTimeAtZero + CFTimeInterval( CGFloat(index) * VideoEdVideoMakeManager.durationForOnePic))
        }
        masterlayer.insertSublayer(newMasterLayerForIndex, at: 0)
        
        if !isSaving {
            masterlayer.isGeometryFlipped = true
        }
    }
    
    open func crossLayer(duration: CFTimeInterval, filledLayer : CALayer? , timerFillDiameter : CGFloat , begintime: CFTimeInterval) {
        
        if let parentLayer = filledLayer {
            
            let maskLayer = CAShapeLayer()
            maskLayer.fillColor = UIColor.clear.cgColor
            maskLayer.strokeColor = UIColor.black.cgColor
            let circleRadius = timerFillDiameter
            
            maskLayer.path = crossLayerPath(layersize: parentLayer.frame.size)
            maskLayer.lineWidth = 100
            maskLayer.strokeEnd = 1.0
            
            parentLayer.mask = maskLayer
            let animation = CABasicAnimation(keyPath: "lineWidth")
            animation.duration = duration
            animation.fromValue = circleRadius*2
            animation.toValue = 0
            animation.beginTime =  begintime
            animation.fillMode = CAMediaTimingFillMode.both
            animation.isRemovedOnCompletion = false
            maskLayer.add(animation, forKey: "strokeWidth" + "\(begintime)")
            
        }
    }
    func crossLayerPath(layersize : CGSize) -> CGPath {

        let scale =  layersize.width / 100
        let bezierPath = UIBezierPath()
        bezierPath.move(to: CGPoint(x: 0 * scale, y: 50 * scale))
//        bezierPath.addLine(to: CGPoint(x: 100 * scale, y: 100 * scale))

        bezierPath.addLine(to: CGPoint(x: 150 * scale, y: 50 * scale))
        bezierPath.addLine(to: CGPoint(x: 150 * scale, y: 150 * scale))
        bezierPath.addLine(to: CGPoint(x: 50 * scale, y: 150 * scale))
        bezierPath.addLine(to: CGPoint(x: 50 * scale, y: 0 * scale))

        UIColor.black.setStroke()
        bezierPath.stroke()
        return bezierPath.cgPath

    }

    func romanceAnimation(masterlayer : CALayer, images : [UIImage] , index : Int , totalCount : Int , isSaving : Bool){
        
        let image = images[index]
        let newMasterLayerForIndex = CALayer()
        newMasterLayerForIndex.frame = masterlayer.frame
        newMasterLayerForIndex.contents = image.cgImage
        newMasterLayerForIndex.opacity = 1.0
        newMasterLayerForIndex.contentsGravity = VideoEdVideoMakeManager.contentsGravity
        newMasterLayerForIndex.masksToBounds = true
        
        if index + 1 <= totalCount - 1 {
            
            romanceLayer(duration: CFTimeInterval(VideoEdVideoMakeManager.durationForOnePic * 0.9) , filledLayer: newMasterLayerForIndex, timerFillDiameter: newMasterLayerForIndex.frame.width, begintime: AVCoreAnimationBeginTimeAtZero + CFTimeInterval( CGFloat(index) * VideoEdVideoMakeManager.durationForOnePic))
        }
        masterlayer.insertSublayer(newMasterLayerForIndex, at: 0)
        
        if !isSaving {
            masterlayer.isGeometryFlipped = true
        }
    }
    
    open func romanceLayer(duration: CFTimeInterval, filledLayer : CALayer? , timerFillDiameter : CGFloat , begintime: CFTimeInterval) {
        
        if let parentLayer = filledLayer {
            
            let maskLayer = CAShapeLayer()
            maskLayer.fillColor = UIColor.clear.cgColor
            maskLayer.strokeColor = UIColor.black.cgColor
            let circleRadius = timerFillDiameter
            maskLayer.shouldRasterize = true
            maskLayer.path = romancePath(layersize: parentLayer.frame.size)
            maskLayer.lineWidth = 0
            maskLayer.strokeEnd = 1.0
            
            parentLayer.mask = maskLayer
            
            let animationContents = CAKeyframeAnimation(keyPath: "lineWidth")
            animationContents.duration = duration
            animationContents.beginTime =  begintime
            animationContents.fillMode = CAMediaTimingFillMode.both
            animationContents.isRemovedOnCompletion = false
            
            animationContents.values = [circleRadius*2,circleRadius,circleRadius/2,0]
            animationContents.keyTimes = [0.0,0.3,0.9,1.0]
            animationContents.timingFunctions = [
                CAMediaTimingFunction(name:CAMediaTimingFunctionName.easeOut),
                CAMediaTimingFunction(name:CAMediaTimingFunctionName.easeInEaseOut),
                CAMediaTimingFunction(name:CAMediaTimingFunctionName.easeInEaseOut)
            ]
            
            maskLayer.add(animationContents, forKey: "contentssCAKeyframes" + "\(begintime)")
            
            
            
        }
    }
    func romancePath(layersize : CGSize) -> CGPath {
        
        let scale =  layersize.width / 100
        let rectanglePath = UIBezierPath(rect: CGRect(x: 0, y: 0, width: 100 * scale, height: 100 * scale))
        UIColor.gray.setFill()
        rectanglePath.fill()
        UIColor.black.setStroke()
        rectanglePath.stroke()

        return rectanglePath.cgPath
    }

    

    func magicalGrayAnimation(masterlayer : CALayer, images : [UIImage] , index : Int , totalCount : Int , isSaving : Bool){
        
        let image = images[index]
        let newMasterLayerForIndex = CALayer()
        newMasterLayerForIndex.frame = masterlayer.frame
        newMasterLayerForIndex.contents = image.cgImage
        newMasterLayerForIndex.opacity = 1.0
        newMasterLayerForIndex.contentsGravity = VideoEdVideoMakeManager.contentsGravity
        newMasterLayerForIndex.masksToBounds = true
        
        if index + 1 <= totalCount - 1 {
            
            greyLayer(duration: CFTimeInterval(VideoEdVideoMakeManager.durationForOnePic * 0.5) , filledLayer: newMasterLayerForIndex, timerFillDiameter: newMasterLayerForIndex.frame.width, begintime: AVCoreAnimationBeginTimeAtZero + CFTimeInterval( CGFloat(index) * VideoEdVideoMakeManager.durationForOnePic))
        }
        masterlayer.insertSublayer(newMasterLayerForIndex, at: 0)
        
        if !isSaving {
            masterlayer.isGeometryFlipped = true
        }
    }
    
    open func greyLayer(duration: CFTimeInterval, filledLayer : CALayer? , timerFillDiameter : CGFloat , begintime: CFTimeInterval) {
        
        if let parentLayer = filledLayer {
            
            let maskLayer = CAShapeLayer()
            maskLayer.fillColor = UIColor.clear.cgColor
            maskLayer.strokeColor = UIColor.black.cgColor
            let circleRadius = timerFillDiameter
            maskLayer.shouldRasterize = true
            maskLayer.path = greyPath(layersize: parentLayer.frame.size)
            maskLayer.lineWidth = 0
            maskLayer.strokeEnd = 1.0
            
            parentLayer.mask = maskLayer
            
            let animationContents = CAKeyframeAnimation(keyPath: "lineWidth")
            animationContents.duration = duration
            animationContents.beginTime =  begintime
            animationContents.fillMode = CAMediaTimingFillMode.both
            animationContents.isRemovedOnCompletion = false
            
            animationContents.values = [circleRadius*2,circleRadius,circleRadius/2,0]
            animationContents.keyTimes = [0.0,0.3,0.9,1.0]
            animationContents.timingFunctions = [
                CAMediaTimingFunction(name:CAMediaTimingFunctionName.easeOut),
                CAMediaTimingFunction(name:CAMediaTimingFunctionName.easeInEaseOut),
                CAMediaTimingFunction(name:CAMediaTimingFunctionName.easeInEaseOut)
            ]

            maskLayer.add(animationContents, forKey: "contentssCAKeyframes" + "\(begintime)")

            
            
        }
    }
    func greyPath(layersize : CGSize) -> CGPath {
        
        let scale =  layersize.width / 100
        
        let bezierPath = UIBezierPath()
        bezierPath.move(to: CGPoint(x: 100 * scale, y: 0))
        bezierPath.addLine(to: CGPoint(x: 100 * scale, y: 100 * scale))
        UIColor.black.setStroke()
        bezierPath.stroke()
        return bezierPath.cgPath
        
    }

    func randomAnimation(masterlayer : CALayer, images : [UIImage] , index : Int , totalCount : Int , isSaving : Bool){
        
        let image = images[index]
        let newMasterLayerForIndex = CALayer()
        newMasterLayerForIndex.frame = masterlayer.frame
        newMasterLayerForIndex.contents = image.cgImage
        newMasterLayerForIndex.opacity = 1.0
        newMasterLayerForIndex.contentsGravity = VideoEdVideoMakeManager.contentsGravity
        newMasterLayerForIndex.masksToBounds = true
        
        if index + 1 <= totalCount - 1 {
            
            runRandom(duration: CFTimeInterval(VideoEdVideoMakeManager.durationForOnePic * 0.5) , filledLayer: newMasterLayerForIndex, timerFillDiameter: newMasterLayerForIndex.frame.width, begintime: AVCoreAnimationBeginTimeAtZero + CFTimeInterval( CGFloat(index) * VideoEdVideoMakeManager.durationForOnePic))
        }
        masterlayer.insertSublayer(newMasterLayerForIndex, at: 0)
        
        if !isSaving {
            masterlayer.isGeometryFlipped = true
        }
    }
    
    open func runRandom(duration: CFTimeInterval, filledLayer : CALayer? , timerFillDiameter : CGFloat , begintime: CFTimeInterval) {
        
        if let parentLayer = filledLayer {
            
            let maskLayer = CAShapeLayer()
            maskLayer.fillColor = UIColor.clear.cgColor
            maskLayer.strokeColor = UIColor.black.cgColor
            let circleRadius = timerFillDiameter
            
            maskLayer.path = snakeRandomPath(layersize: parentLayer.frame.size)
            maskLayer.lineWidth = 0.31 * circleRadius
            maskLayer.strokeEnd = 1.0
            
            parentLayer.mask = maskLayer
            let animation = CABasicAnimation(keyPath: "strokeEnd")
            animation.duration = duration
            animation.fromValue = 1
            animation.toValue = 0
            animation.beginTime =  begintime
            animation.fillMode = CAMediaTimingFillMode.forwards
            animation.isRemovedOnCompletion = false
            maskLayer.add(animation, forKey: "strokeEnd" + "\(begintime)")
            
        }
    }

    func snakeRandomPath(layersize : CGSize) -> CGPath {
        

        let bezierPath = UIBezierPath()
        bezierPath.move(to: CGPoint(x: -13.5 * layersize.width / 100, y: 19.89 * layersize.width / 100))
        bezierPath.addCurve(to: CGPoint(x: 14.62 * layersize.width / 100, y: 1.5 * layersize.width / 100), controlPoint1: CGPoint(x: 11.58 * layersize.width / 100, y: 1.5 * layersize.width / 100), controlPoint2: CGPoint(x: 14.62 * layersize.width / 100, y: 1.5 * layersize.width / 100))
        bezierPath.addLine(to: CGPoint(x: 28.76 * layersize.width / 100, y: 1.5 * layersize.width / 100))
        bezierPath.addCurve(to: CGPoint(x: 28.76 * layersize.width / 100, y: 91.5 * layersize.width / 100), controlPoint1: CGPoint(x: 28.76 * layersize.width / 100, y: 1.5 * layersize.width / 100), controlPoint2: CGPoint(x: 15.63 * layersize.width / 100, y: 91.5 * layersize.width / 100))
        bezierPath.addCurve(to: CGPoint(x: 64.13 * layersize.width / 100, y: 91.5 * layersize.width / 100), controlPoint1: CGPoint(x: 41.9 * layersize.width / 100, y: 91.5 * layersize.width / 100), controlPoint2: CGPoint(x: 64.13 * layersize.width / 100, y: 91.5 * layersize.width / 100))
        bezierPath.addCurve(to: CGPoint(x: 84.34 * layersize.width / 100, y: 24.73 * layersize.width / 100), controlPoint1: CGPoint(x: 64.13 * layersize.width / 100, y: 91.5 * layersize.width / 100), controlPoint2: CGPoint(x: 94.45 * layersize.width / 100, y: 29.56 * layersize.width / 100))
        bezierPath.addCurve(to: CGPoint(x: 58.5 * layersize.width / 100, y: 19.5 * layersize.width / 100), controlPoint1: CGPoint(x: 74.24 * layersize.width / 100, y: 19.89 * layersize.width / 100), controlPoint2: CGPoint(x: 58.5 * layersize.width / 100, y: 19.5 * layersize.width / 100))
        bezierPath.addLine(to: CGPoint(x: 34.5 * layersize.width / 100, y: 29.5 * layersize.width / 100))
        bezierPath.addLine(to: CGPoint(x: 8.55 * layersize.width / 100, y: 34.4 * layersize.width / 100))
        bezierPath.addLine(to: CGPoint(x: 3.5 * layersize.width / 100, y: 72.15 * layersize.width / 100))
        bezierPath.addLine(to: CGPoint(x: 58.5 * layersize.width / 100, y: 62.5 * layersize.width / 100))
        bezierPath.addLine(to: CGPoint(x: 90.5 * layersize.width / 100, y: 59.5 * layersize.width / 100))
        bezierPath.addLine(to: CGPoint(x: 94.5 * layersize.width / 100, y: 64.5 * layersize.width / 100))
        bezierPath.addLine(to: CGPoint(x: 94.5 * layersize.width / 100, y: 80.5 * layersize.width / 100))
        bezierPath.addLine(to: CGPoint(x: 94.5 * layersize.width / 100, y: 91.5 * layersize.width / 100))
        bezierPath.addLine(to: CGPoint(x: 80.5 * layersize.width / 100, y: 91.5 * layersize.width / 100))
        bezierPath.addLine(to: CGPoint(x: 38.87 * layersize.width / 100, y: 80.85 * layersize.width / 100))
        bezierPath.addLine(to: CGPoint(x: 38.87 * layersize.width / 100, y: 16.98 * layersize.width / 100))
        bezierPath.addLine(to: CGPoint(x: 90.5 * layersize.width / 100, y: 1.5 * layersize.width / 100))
        bezierPath.addLine(to: CGPoint(x: 99.5 * layersize.width / 100, y: 34.4 * layersize.width / 100))
        bezierPath.addLine(to: CGPoint(x: 68.5 * layersize.width / 100, y: 44.5 * layersize.width / 100))
        bezierPath.addLine(to: CGPoint(x: 58.07 * layersize.width / 100, y: 40.21 * layersize.width / 100))
        bezierPath.addLine(to: CGPoint(x:
            8.55 * layersize.width / 100, y: 84.73 * layersize.width / 100))
        bezierPath.addLine(to: CGPoint(x: -20 * layersize.width / 100, y: 102 * layersize.width / 100))
        UIColor.black.setStroke()
        bezierPath.stroke()
        bezierPath.close()
        return bezierPath.cgPath

        

        
    }
    
    func birthdayAnimation(masterlayer : CALayer, images : [UIImage] , index : Int , totalCount : Int , isSaving : Bool){
        
        let image = images[index]
        let newMasterLayerForIndex = CALayer()
        newMasterLayerForIndex.frame = masterlayer.frame
        newMasterLayerForIndex.contents = image.cgImage
        newMasterLayerForIndex.opacity = 1.0
        newMasterLayerForIndex.contentsGravity = VideoEdVideoMakeManager.contentsGravity
        newMasterLayerForIndex.masksToBounds = true
        
        if index + 1 <= totalCount - 1 {
            
            birthdayLayer(duration: CFTimeInterval(VideoEdVideoMakeManager.durationForOnePic * 0.5) , filledLayer: newMasterLayerForIndex, timerFillDiameter: newMasterLayerForIndex.frame.width, begintime: AVCoreAnimationBeginTimeAtZero + CFTimeInterval( CGFloat(index) * VideoEdVideoMakeManager.durationForOnePic))
        }
        masterlayer.insertSublayer(newMasterLayerForIndex, at: 0)
        
        if !isSaving {
            masterlayer.isGeometryFlipped = true
        }
    }
    
    open func birthdayLayer(duration: CFTimeInterval, filledLayer : CALayer? , timerFillDiameter : CGFloat , begintime: CFTimeInterval) {
        
        if let parentLayer = filledLayer {
            
            let maskLayer = CAShapeLayer()
            maskLayer.fillColor = UIColor.clear.cgColor
            maskLayer.strokeColor = UIColor.black.cgColor
            let circleRadius = timerFillDiameter

            maskLayer.path = birthdayPath(layersize: parentLayer.frame.size)
            maskLayer.lineWidth = 0
            maskLayer.strokeEnd = 1.0
            
            parentLayer.mask = maskLayer
            let animation = CABasicAnimation(keyPath: "lineWidth")
            animation.duration = duration
            animation.fromValue = circleRadius*2
            animation.toValue = 0
            animation.beginTime =  begintime
            animation.fillMode = CAMediaTimingFillMode.both
            animation.isRemovedOnCompletion = false
            maskLayer.add(animation, forKey: "strokeWidth" + "\(begintime)")
            
        }
    }
    func birthdayPath(layersize : CGSize) -> CGPath {
        
//        let scale =  layersize.width / 100
//        let bezierPath = UIBezierPath()
//        bezierPath.move(to: CGPoint(x: 50 * scale, y: -4.5 * scale))
//        bezierPath.addLine(to: CGPoint(x: 50 * scale, y: 102.5 * scale))
//        UIColor.black.setStroke()
//        bezierPath.stroke()
//        return bezierPath.cgPath

        let scale =  layersize.width / 100
        let bezierPath = UIBezierPath()
        bezierPath.move(to: CGPoint(x: 0 * scale, y: 50 * scale))
        bezierPath.addLine(to: CGPoint(x: 100 * scale, y: 50 * scale))
        UIColor.black.setStroke()
        bezierPath.stroke()
        return bezierPath.cgPath

    }
    func stripeVertAnimation(masterlayer : CALayer, images : [UIImage] , index : Int , totalCount : Int , isSaving : Bool){
        
        let image = images[index]
        let newMasterLayerForIndex = CALayer()
        newMasterLayerForIndex.frame = masterlayer.frame
        newMasterLayerForIndex.contents = image.cgImage
        newMasterLayerForIndex.opacity = 1.0
        newMasterLayerForIndex.contentsGravity = VideoEdVideoMakeManager.contentsGravity
        newMasterLayerForIndex.masksToBounds = true
        
        if index + 1 <= totalCount - 1 {
            
            runStripeVert(duration: CFTimeInterval(VideoEdVideoMakeManager.durationForOnePic * 0.5) , filledLayer: newMasterLayerForIndex, timerFillDiameter: newMasterLayerForIndex.frame.width, begintime: AVCoreAnimationBeginTimeAtZero + CFTimeInterval( CGFloat(index) * VideoEdVideoMakeManager.durationForOnePic))
        }
        masterlayer.insertSublayer(newMasterLayerForIndex, at: 0)
        
        if !isSaving {
            masterlayer.isGeometryFlipped = true
        }
    }
    open func runStripeVert(duration: CFTimeInterval, filledLayer : CALayer? , timerFillDiameter : CGFloat , begintime: CFTimeInterval) {
        
        if let parentLayer = filledLayer {
            
            let maskLayer = CAShapeLayer()
            //            maskLayer.frame = newfrme
            
            maskLayer.backgroundColor = UIColor.magenta.cgColor
            
            maskLayer.fillColor = UIColor.clear.cgColor
            maskLayer.strokeColor = UIColor.black.cgColor
            let circleRadius = timerFillDiameter * 0.4
            
            maskLayer.path = stripeVertPath(layersize: parentLayer.frame.size)
            maskLayer.lineWidth = circleRadius
            maskLayer.strokeEnd = 1.0
            
            parentLayer.mask = maskLayer
            //            parentLayer.addSublayer(maskLayer)
            let animation = CABasicAnimation(keyPath: "lineWidth")
            animation.duration = duration
            animation.fromValue = circleRadius
            animation.toValue = 0
            animation.beginTime =  begintime
            animation.fillMode = CAMediaTimingFillMode.forwards
            animation.isRemovedOnCompletion = false
            maskLayer.add(animation, forKey: "strokeWidth" + "\(begintime)")
            
        }
    }
    func stripeVertPath(layersize : CGSize) -> CGPath {
        
        let scale =  layersize.width / 100
        let bezierPath = UIBezierPath()
        bezierPath.move(to: CGPoint(x: 12 * scale, y: 124.81 * scale))
        bezierPath.addLine(to: CGPoint(x: 12 * scale, y: -25.81 * scale))
        bezierPath.addLine(to: CGPoint(x: 64 * scale, y: -25.81 * scale))
        bezierPath.addLine(to: CGPoint(x: 64 * scale, y: 124.81 * scale ))
        bezierPath.addLine(to: CGPoint(x: 38 * scale, y: 124.81 * scale))
        bezierPath.addLine(to: CGPoint(x: 38 * scale, y: -29.5 * scale))
        bezierPath.addLine(to: CGPoint(x: 88 * scale, y: -29.5 * scale))
        bezierPath.addLine(to: CGPoint(x: 88 * scale, y: 128 * scale))
        UIColor.lightGray.setFill()
        bezierPath.fill()
        UIColor.black.setStroke()
        bezierPath.stroke()
        return bezierPath.cgPath
    }
    
    func snakeMaskingAnimation(masterlayer : CALayer, images : [UIImage] , index : Int , totalCount : Int , isSaving : Bool){
        
        let image = images[index]
        let newMasterLayerForIndex = CALayer()
        newMasterLayerForIndex.frame = masterlayer.frame
        newMasterLayerForIndex.contents = image.cgImage
        newMasterLayerForIndex.opacity = 1.0
        newMasterLayerForIndex.contentsGravity = VideoEdVideoMakeManager.contentsGravity
        newMasterLayerForIndex.masksToBounds = true
        
        if index + 1 <= totalCount - 1 {
            
            runSnakeMasking(duration: CFTimeInterval(VideoEdVideoMakeManager.durationForOnePic * 0.5) , filledLayer: newMasterLayerForIndex, timerFillDiameter: newMasterLayerForIndex.frame.width, begintime: AVCoreAnimationBeginTimeAtZero + CFTimeInterval( CGFloat(index) * VideoEdVideoMakeManager.durationForOnePic), snakeAnimationType: .HORIZONTAL)
        }
        
        
        
        masterlayer.insertSublayer(newMasterLayerForIndex, at: 0)
        
        if !isSaving {
            masterlayer.isGeometryFlipped = true
        }
    }
    
    open func runSnakeMasking(duration: CFTimeInterval, filledLayer : CALayer? , timerFillDiameter : CGFloat , begintime: CFTimeInterval, snakeAnimationType : SNAKE_ANIMATIONS) {
        
        if let parentLayer = filledLayer {
            
            let maskLayer = CAShapeLayer()
            //            let maskLayer = CALayer()
            var newfrme = parentLayer.frame
            newfrme.size.width = newfrme.size.width*0.0
            maskLayer.frame = newfrme
            
            maskLayer.backgroundColor = UIColor.magenta.cgColor
            
            maskLayer.fillColor = UIColor.clear.cgColor
            maskLayer.strokeColor = UIColor.black.cgColor
            var circleRadius = timerFillDiameter * 0.23
            
            if snakeAnimationType == .HORIZONTAL{
                circleRadius = timerFillDiameter * 0.2
                maskLayer.path = snakeHorRollingPath(layersize: parentLayer.frame.size)
            }else if snakeAnimationType == .DIAGONAL {
                circleRadius = timerFillDiameter * 0.32
                maskLayer.path = snakeDiagRollingPath(layersize: parentLayer.frame.size)
            }else{
                circleRadius = timerFillDiameter * 0.23
                maskLayer.path = snakeVertRollingPath(layersize: parentLayer.frame.size)
                
            }
            maskLayer.lineWidth = circleRadius
            //            maskLayer.strokeEnd = 1.0
            maskLayer.strokeStart = 0.0
            
            parentLayer.mask = maskLayer
            
            let animation = CABasicAnimation(keyPath: "strokeStart")
            animation.duration = duration
            animation.fromValue = 0.0
            animation.toValue = 1.0
            animation.beginTime =  begintime
            animation.fillMode = CAMediaTimingFillMode.forwards
            animation.isRemovedOnCompletion = false
            maskLayer.add(animation, forKey: "strokeEnd")
            
        }
    }
    func snakeMasterAnimation(masterlayer : CALayer, images : [UIImage] , index : Int , totalCount : Int , isSaving : Bool , snakeAnimationType : SNAKE_ANIMATIONS){
        
        let image = images[index]
        let newMasterLayerForIndex = CALayer()
        newMasterLayerForIndex.frame = masterlayer.frame
        newMasterLayerForIndex.contents = image.cgImage
        newMasterLayerForIndex.opacity = 0.0
        newMasterLayerForIndex.contentsGravity = VideoEdVideoMakeManager.contentsGravity
        newMasterLayerForIndex.masksToBounds = true
        
        let blurImage = blurEffect(inputImage: image)

        let sampleWOblurLayer = CALayer()
        sampleWOblurLayer.frame = masterlayer.frame
        sampleWOblurLayer.contents = image.cgImage
        sampleWOblurLayer.opacity = 1.0
        sampleWOblurLayer.contentsGravity = VideoEdVideoMakeManager.contentsGravity
        
        
        let sampleblurLayer = CALayer()
        sampleblurLayer.frame = masterlayer.frame
        sampleblurLayer.contents = blurImage.cgImage
        sampleblurLayer.opacity = 1.0
        sampleblurLayer.contentsGravity = VideoEdVideoMakeManager.contentsGravity
        
        newMasterLayerForIndex.addSublayer(sampleWOblurLayer)
        newMasterLayerForIndex.addSublayer(sampleblurLayer)
        
        
        runSnakeAnimation(duration: CFTimeInterval(VideoEdVideoMakeManager.durationForOnePic * 0.5) , filledLayer: sampleblurLayer, timerFillDiameter: newMasterLayerForIndex.frame.width, begintime: AVCoreAnimationBeginTimeAtZero + CFTimeInterval( CGFloat(index) * VideoEdVideoMakeManager.durationForOnePic) , snakeAnimationType: snakeAnimationType )
        
        
        addExactOpacityAnimation(sampleLayer: newMasterLayerForIndex, index: index, totalCount: totalCount, isSaving: isSaving)
        
        
        masterlayer.insertSublayer(newMasterLayerForIndex, at: 0)
        
        if !isSaving {
            masterlayer.isGeometryFlipped = true
        }
    }
    
    open func runSnakeAnimation(duration: CFTimeInterval, filledLayer : CALayer? , timerFillDiameter : CGFloat , begintime: CFTimeInterval, snakeAnimationType : SNAKE_ANIMATIONS) {
        
        if let parentLayer = filledLayer {
            
            let maskLayer = CAShapeLayer()
            maskLayer.frame = parentLayer.frame
            
            maskLayer.fillColor = UIColor.clear.cgColor
            maskLayer.strokeColor = UIColor.black.cgColor
            var circleRadius = timerFillDiameter * 0.23
            
            if snakeAnimationType == .HORIZONTAL{
                circleRadius = timerFillDiameter * 0.2
                maskLayer.path = snakeHorRollingPath(layersize: parentLayer.frame.size)
            }else if snakeAnimationType == .DIAGONAL {
                circleRadius = timerFillDiameter * 0.32
                maskLayer.path = snakeDiagRollingPath(layersize: parentLayer.frame.size)
            }else{
                circleRadius = timerFillDiameter * 0.23
                maskLayer.path = snakeVertRollingPath(layersize: parentLayer.frame.size)

            }
            maskLayer.lineWidth = circleRadius
            maskLayer.strokeEnd = 0
            
            parentLayer.mask = maskLayer
            
            let animation = CABasicAnimation(keyPath: "strokeEnd")
            animation.duration = duration
            animation.fromValue = 1
            animation.toValue = 0
            animation.beginTime =  begintime
            animation.fillMode = CAMediaTimingFillMode.forwards
            animation.isRemovedOnCompletion = false
            maskLayer.add(animation, forKey: "strokeEnd")
            
        }
    }
    
    func snakeHorRollingPath(layersize : CGSize) -> CGPath{
        
        let bezierPath = UIBezierPath()
        bezierPath.move(to: CGPoint(x: -2.5 * layersize.width / 100, y: 8 * layersize.width / 100))
        bezierPath.addLine(to: CGPoint(x: 102.5 * layersize.width / 100, y: 8 * layersize.width / 100))
        bezierPath.addLine(to: CGPoint(x: 102.5 * layersize.width / 100, y: 23.53 * layersize.width / 100))
        bezierPath.addLine(to: CGPoint(x: -2.5 * layersize.width / 100, y: 23.53 * layersize.width / 100))
        bezierPath.addLine(to: CGPoint(x: -2.5 * layersize.width / 100, y: 40.39 * layersize.width / 100))
        bezierPath.addLine(to: CGPoint(x: 102.5 * layersize.width / 100, y: 40.39 * layersize.width / 100))
        bezierPath.addLine(to: CGPoint(x: 102.5 * layersize.width / 100, y: 57.87 * layersize.width / 100))
        bezierPath.addLine(to: CGPoint(x: -2.5 * layersize.width / 100, y: 57.87 * layersize.width / 100))
        bezierPath.addLine(to: CGPoint(x: -2.5 * layersize.width / 100, y: 75.69 * layersize.width / 100))
        bezierPath.addLine(to: CGPoint(x: 102.5 * layersize.width / 100, y: 75.69 * layersize.width / 100))
        bezierPath.addLine(to: CGPoint(x: 102.5 * layersize.width / 100, y: 92 * layersize.width / 100))
        bezierPath.addLine(to: CGPoint(x: -2.5 * layersize.width / 100, y: 92 * layersize.width / 100))
        UIColor.black.setStroke()
        bezierPath.lineWidth = 20 * layersize.width / 100
        bezierPath.stroke()
        return bezierPath.cgPath
        
    }
    func snakeDiagRollingPath(layersize : CGSize) -> CGPath{
        
        let bezierPath = UIBezierPath()
        bezierPath.move(to: CGPoint(x: 38.5 * layersize.width / 100, y: -12.5 * layersize.width / 100))
        bezierPath.addLine(to: CGPoint(x: -4.98 * layersize.width / 100, y: 26.59 * layersize.width / 100))
        bezierPath.addLine(to: CGPoint(x: -2.6 * layersize.width / 100, y: 62.28 * layersize.width / 100))
        bezierPath.addLine(to: CGPoint(x: 66.97 * layersize.width / 100, y: -0.27 * layersize.width / 100))
        bezierPath.addLine(to: CGPoint(x: 126.6 * layersize.width / 100, y: -12.5 * layersize.width / 100))
        bezierPath.addLine(to: CGPoint(x: 67.1 * layersize.width / 100, y: 41.99 * layersize.width / 100))
        bezierPath.addLine(to: CGPoint(x: -5.5 * layersize.width / 100, y: 105.38 * layersize.width / 100))
        bezierPath.addLine(to: CGPoint(x: 29.8 * layersize.width / 100, y: 116.5 * layersize.width / 100))
        bezierPath.addLine(to: CGPoint(x: 138.5 * layersize.width / 100, y: 18.77 * layersize.width / 100))
        bezierPath.addLine(to: CGPoint(x: 138.5 * layersize.width / 100, y: 56.56 * layersize.width / 100))
        bezierPath.addLine(to: CGPoint(x: 71.83 * layersize.width / 100, y: 116.5 * layersize.width / 100))
        UIColor.black.setStroke()
        bezierPath.lineWidth = 32 * layersize.width / 100
        bezierPath.stroke()
        return bezierPath.reversing().cgPath
        
    }
    
    func snakeVertRollingPath(layersize : CGSize) -> CGPath{
        
        let bezierPath = UIBezierPath()
        bezierPath.move(to: CGPoint(x: 8.5 * layersize.width / 100, y: -7.5 * layersize.width / 100))
        bezierPath.addLine(to: CGPoint(x: 8.5 * layersize.width / 100, y: 101.5 * layersize.width / 100))
        bezierPath.addLine(to: CGPoint(x: 30.06 * layersize.width / 100, y: 101.5 * layersize.width / 100))
        bezierPath.addLine(to: CGPoint(x: 30.06 * layersize.width / 100, y: -7.5 * layersize.width / 100))
        bezierPath.addLine(to: CGPoint(x: 52.53 * layersize.width / 100, y: -7.5 * layersize.width / 100))
        bezierPath.addLine(to: CGPoint(x: 52.53 * layersize.width / 100, y: 101.5 * layersize.width / 100))
        bezierPath.addLine(to: CGPoint(x: 74.94 * layersize.width / 100, y: 101.5 * layersize.width / 100))
        bezierPath.addLine(to: CGPoint(x: 74.94 * layersize.width / 100, y: -7.5 * layersize.width / 100))
        bezierPath.addLine(to: CGPoint(x: 93.5 * layersize.width / 100, y: -7.5 * layersize.width / 100))
        bezierPath.addLine(to: CGPoint(x: 93.5 * layersize.width / 100, y: 101.5 * layersize.width / 100))
        UIColor.black.setStroke()
        bezierPath.lineWidth = 23 * layersize.width / 100
        bezierPath.stroke()
        return bezierPath.reversing().cgPath
        
    }
    func birdsFlyAnimation(masterlayer : CALayer, images : [UIImage] , index : Int , totalCount : Int , isSaving : Bool){
        
        let image = images[index]
        let blurImage = blurEffect(inputImage: image)
        
        let newMasterLayerForIndex = CALayer()
        newMasterLayerForIndex.frame = masterlayer.frame
        newMasterLayerForIndex.contents = blurImage.cgImage
        newMasterLayerForIndex.opacity = 0.0
        newMasterLayerForIndex.contentsGravity = VideoEdVideoMakeManager.contentsGravity
        newMasterLayerForIndex.masksToBounds = true
        
        
        let sampleblurLayer = CALayer()
        sampleblurLayer.frame = masterlayer.frame
        sampleblurLayer.contents = blurImage.cgImage
        sampleblurLayer.opacity = 1.0
        sampleblurLayer.contentsGravity = VideoEdVideoMakeManager.contentsGravity

        
        let animationContents = CAKeyframeAnimation(keyPath: "contents")
        animationContents.duration = CFTimeInterval(VideoEdVideoMakeManager.durationForOnePic)
        animationContents.beginTime =  AVCoreAnimationBeginTimeAtZero + CFTimeInterval( CGFloat(index) * VideoEdVideoMakeManager.durationForOnePic)
        animationContents.fillMode = CAMediaTimingFillMode.both
        animationContents.isRemovedOnCompletion = false
        
        if index + 1 <= totalCount - 1 {
            animationContents.values = [blurImage.cgImage!,image.cgImage!,image.cgImage!,image.cgImage!]
            animationContents.keyTimes = [0.0,0.5,0.9,1.0]
        }else{
            animationContents.values = [blurImage.cgImage!,image.cgImage!,image.cgImage!]
            animationContents.keyTimes = [0.0,0.5,1.0]
        }
        sampleblurLayer.add(animationContents, forKey: "contentssCAKeyframes" + "\(index)")

        newMasterLayerForIndex.addSublayer(sampleblurLayer)
        
        
        let scaleAnimation = CAKeyframeAnimation(keyPath: "transform.scale")
        scaleAnimation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
        scaleAnimation.beginTime = AVCoreAnimationBeginTimeAtZero + CFTimeInterval(CGFloat(index) * VideoEdVideoMakeManager.durationForOnePic)
        scaleAnimation.duration =  CFTimeInterval(VideoEdVideoMakeManager.durationForOnePic) //+ 0.19
        if index % 2 == 0{
            scaleAnimation.values = [1.2,1.1,1.0] // zoomout
        }else{
            scaleAnimation.values = [1.0,1.1,1.2] // zoomIn
        }
        scaleAnimation.keyTimes = [0.0,0.5,1.0]
        //        scaleAnimation.isAdditive = true
        scaleAnimation.isRemovedOnCompletion = false
        scaleAnimation.fillMode = CAMediaTimingFillMode.forwards
        newMasterLayerForIndex.add(scaleAnimation, forKey: "transform.scale\(index)")
        
        
        addExactOpacityAnimation(sampleLayer: newMasterLayerForIndex, index: index, totalCount: totalCount, isSaving: isSaving)
        
        masterlayer.insertSublayer(newMasterLayerForIndex, at: 0)
        
        if !isSaving {
            masterlayer.isGeometryFlipped = true
        }
    }
 
    
    func joyfullAnimation(masterlayer : CALayer, images : [UIImage] , index : Int , totalCount : Int , isSaving : Bool){
        
        let image = images[index]
        let newMasterLayerForIndex = CALayer()
        newMasterLayerForIndex.frame = masterlayer.frame
        newMasterLayerForIndex.contents = image.cgImage
        newMasterLayerForIndex.opacity = 0.0
        newMasterLayerForIndex.contentsGravity = VideoEdVideoMakeManager.contentsGravity
        newMasterLayerForIndex.masksToBounds = true
        
        let blurImage = blurEffect(inputImage: image)
        
        let sampleWOblurLayer = CALayer()
        sampleWOblurLayer.frame = masterlayer.frame
        sampleWOblurLayer.contents = image.cgImage
        sampleWOblurLayer.opacity = 1.0
        sampleWOblurLayer.contentsGravity = VideoEdVideoMakeManager.contentsGravity
        
        
        let sampleblurLayer = CALayer()
        sampleblurLayer.frame = masterlayer.frame
        sampleblurLayer.contents = blurImage.cgImage
        sampleblurLayer.opacity = 1.0
        sampleblurLayer.contentsGravity = VideoEdVideoMakeManager.contentsGravity
        
        newMasterLayerForIndex.addSublayer(sampleWOblurLayer)
        newMasterLayerForIndex.addSublayer(sampleblurLayer)
        
        
        runMaskAnimation(duration: CFTimeInterval(VideoEdVideoMakeManager.durationForOnePic * 0.5) , filledLayer: sampleblurLayer, timerFillDiameter: newMasterLayerForIndex.frame.width * 2, begintime: AVCoreAnimationBeginTimeAtZero + CFTimeInterval( CGFloat(index) * VideoEdVideoMakeManager.durationForOnePic))
        
//        addExactOpacityAnimation(sampleLayer: newMasterLayerForIndex, index: index, totalCount: totalCount, isSaving: isSaving)
        addNormalOpacityAnimation(sampleLayer: newMasterLayerForIndex, index: index, totalCount: totalCount, isSaving: isSaving)

        masterlayer.insertSublayer(newMasterLayerForIndex, at: 0)
        
        if !isSaving {
            
            masterlayer.isGeometryFlipped = true
        }
    }
    
    open func runMaskAnimation(duration: CFTimeInterval, filledLayer : CALayer? , timerFillDiameter : CGFloat , begintime: CFTimeInterval) {
        
        if let parentLayer = filledLayer {
            
            let maskLayer = CAShapeLayer()
            maskLayer.frame = parentLayer.frame
            
            let circleRadius = timerFillDiameter * 0.5
            let circleHalfRadius = circleRadius * 0.5
            let circleBounds = CGRect(x: parentLayer.bounds.midX - circleHalfRadius, y: parentLayer.bounds.midY - circleHalfRadius, width: circleRadius, height: circleRadius)
            
            maskLayer.fillColor = UIColor.clear.cgColor
            maskLayer.strokeColor = UIColor.black.cgColor
            maskLayer.lineWidth = circleRadius
            
            let path = UIBezierPath(roundedRect: circleBounds, cornerRadius: circleBounds.size.width * 0.5)
            maskLayer.path = path.cgPath
            maskLayer.strokeEnd = 0
            
            parentLayer.mask = maskLayer
            
            let animation = CABasicAnimation(keyPath: "strokeEnd")
            animation.duration = duration
            animation.fromValue = 1.0
            animation.toValue = 0.0
            animation.beginTime =  begintime
            animation.fillMode = CAMediaTimingFillMode.forwards
            animation.isRemovedOnCompletion = false
            maskLayer.add(animation, forKey: "strokeEnd")
        }
    }
    func cheerfulAnimation(masterlayer : CALayer, images : [UIImage] , index : Int , totalCount : Int , isSaving : Bool){
        
        let image = images[index]
//        image = image.resizeImage(targetSize: CGSize(width: SCREEN_HEIGHT, height: SCREEN_HEIGHT))
        let newMasterLayerForIndex = CALayer()
        newMasterLayerForIndex.frame = masterlayer.frame
        newMasterLayerForIndex.contents = image.cgImage
        newMasterLayerForIndex.opacity = 0.0
        newMasterLayerForIndex.contentsGravity = VideoEdVideoMakeManager.contentsGravity
        newMasterLayerForIndex.masksToBounds = true
        
        
        let shapeNewLayer = CAShapeLayer()
        shapeNewLayer.opacity = 1.0
        
        
        var newFrame = masterlayer.frame
        newFrame.size.width = newFrame.width * 1.0
        newFrame.size.height = newFrame.width * 1.0
        
        shapeNewLayer.frame = newFrame
        shapeNewLayer.position = masterlayer.position
        var startShape = UIBezierPath(roundedRect: newFrame, cornerRadius: 0.1).cgPath
        startShape = cheerFulbezier1(layersize: masterlayer.frame.size)
        shapeNewLayer.path = startShape
        
        var  stopAtYLocationForAWhile = masterlayer.frame.width*0.1
        var midShape1 = UIBezierPath(roundedRect: CGRect(x: masterlayer.frame.width*0.1, y: stopAtYLocationForAWhile, width: masterlayer.frame.width*0.8, height: masterlayer.frame.width*0.8) , cornerRadius: masterlayer.frame.width*0.4).cgPath
        midShape1 = cheerFulbezier2(layersize: masterlayer.frame.size)
        
        var endShape = UIBezierPath(roundedRect: CGRect(x: masterlayer.frame.width*3, y: masterlayer.frame.width/2, width: 1, height: 1) , cornerRadius: masterlayer.frame.width/2).cgPath
        endShape = cheerFulbezier3(layersize: masterlayer.frame.size)
        
        
        let animationPath = CAKeyframeAnimation(keyPath: "path")
        animationPath.duration = CFTimeInterval(VideoEdVideoMakeManager.durationForOnePic)*0.3
        animationPath.beginTime =  AVCoreAnimationBeginTimeAtZero + CFTimeInterval( CGFloat(index) * VideoEdVideoMakeManager.durationForOnePic)
        animationPath.values = [startShape,midShape1,endShape]
        animationPath.keyTimes = [0.0,0.5,1.0]
        animationPath.fillMode = CAMediaTimingFillMode.both
        animationPath.isRemovedOnCompletion = false
        
        
        shapeNewLayer.add(animationPath, forKey: "pathAnimation" + "\(index)")
        
        stopAtYLocationForAWhile = masterlayer.frame.width*0.5
        
        
        
//        let blurImage = blurEffect(inputImage: image)
        let blurImage = image.noir

        let sampleWOblurLayer = CALayer()
        sampleWOblurLayer.frame = masterlayer.frame
        sampleWOblurLayer.contents = image.cgImage
        sampleWOblurLayer.opacity = 1.0
        sampleWOblurLayer.contentsGravity = VideoEdVideoMakeManager.contentsGravity
        
        
        let sampleblurLayer = CALayer()
        sampleblurLayer.frame = masterlayer.frame
        sampleblurLayer.contents = blurImage?.cgImage
        sampleblurLayer.opacity = 1.0
        sampleblurLayer.contentsGravity = VideoEdVideoMakeManager.contentsGravity
        
        
        
        
        if index + 1 <= totalCount - 1 {
            
            let filterAnim:CABasicAnimation = CABasicAnimation(keyPath: "contents");
            filterAnim.fromValue = image.cgImage;
            filterAnim.toValue   = images[index+1].cgImage;
            filterAnim.beginTime =  AVCoreAnimationBeginTimeAtZero + CFTimeInterval( CGFloat(index+1) * VideoEdVideoMakeManager.durationForOnePic)
            filterAnim.duration  = 0.2
            filterAnim.isRemovedOnCompletion = false
            filterAnim.fillMode = CAMediaTimingFillMode.forwards
            masterlayer.add(filterAnim, forKey: "contentsss" + "\(index)")
        }
        
        newMasterLayerForIndex.addSublayer(sampleWOblurLayer)
        newMasterLayerForIndex.addSublayer(sampleblurLayer)
        
        sampleblurLayer.mask = shapeNewLayer
        
//        addExactOpacityAnimation(sampleLayer: newMasterLayerForIndex, index: index, totalCount: totalCount, isSaving: isSaving)
        //        addExactOpacityAnimation(sampleLayer: newMasterLayerForIndex, index: index, totalCount: totalCount, isSaving: isSaving)
        addLittleFadeOpacityAnimation(sampleLayer: newMasterLayerForIndex, index: index, totalCount: totalCount, isSaving: isSaving)
        masterlayer.insertSublayer(newMasterLayerForIndex, at: 0)
        
        if !isSaving {
            
            masterlayer.isGeometryFlipped = true
        }
        //"bring"
    }
    func cheerFulbezier1(layersize: CGSize) -> CGPath{
        
        let bezierPath = UIBezierPath()
        bezierPath.move(to: CGPoint(x: 0 * layersize.width / 100, y: 100 * layersize.width / 100))
//        bezierPath.addCurve(to: CGPoint(x: 0, y: 0), controlPoint1: CGPoint(x: 0, y: 0.0), controlPoint2: CGPoint(x: 0, y: 0))
        bezierPath.addLine(to: CGPoint(x: 0 * layersize.width / 100, y: 0))

        bezierPath.addLine(to: CGPoint(x: 100 * layersize.width / 100, y: 0))
        bezierPath.addLine(to: CGPoint(x: 100 * layersize.width / 100, y: 100 * layersize.width / 100))
        bezierPath.addLine(to: CGPoint(x: 100 * layersize.width / 100, y: 100 * layersize.width / 100))
        bezierPath.close()
        UIColor.gray.setFill()
        bezierPath.fill()
        
        return bezierPath.cgPath
    }
    func cheerFulbezier2(layersize: CGSize) -> CGPath{
        
        let bezierPath = UIBezierPath()
        bezierPath.move(to: CGPoint(x: 100 * layersize.width / 100, y: 100 * layersize.width / 100))
//        bezierPath.addCurve(to: CGPoint(x: 0, y: 0), controlPoint1: CGPoint(x: 0, y: 0.0), controlPoint2: CGPoint(x: 0, y: 0))
        bezierPath.addLine(to: CGPoint(x: 0 * layersize.width / 100, y: 0))
        bezierPath.addLine(to: CGPoint(x: 100 * layersize.width / 100, y: 0))
        bezierPath.addLine(to: CGPoint(x: 100 * layersize.width / 100, y: 100 * layersize.width / 100))
        bezierPath.addLine(to: CGPoint(x: 100 * layersize.width / 100, y: 100 * layersize.width / 100))
        bezierPath.close()
        UIColor.gray.setFill()
        bezierPath.fill()
        
        return bezierPath.cgPath
    }
    func cheerFulbezier3(layersize: CGSize) -> CGPath{
        
        let bezierPath = UIBezierPath()
        bezierPath.move(to: CGPoint(x: 142 * layersize.width / 100, y: 0 * layersize.width / 100))
//        bezierPath.addCurve(to: CGPoint(x: 0, y: 0), controlPoint1: CGPoint(x: 140 * layersize.width / 100, y: 0 * layersize.width / 100), controlPoint2: CGPoint(x: 0, y: 0))
        bezierPath.addLine(to: CGPoint(x: 0 * layersize.width / 100, y: 0))

        bezierPath.addLine(to: CGPoint(x: 100 * layersize.width / 100, y: 0))
        bezierPath.addLine(to: CGPoint(x: 100 * layersize.width / 100, y: 100 * layersize.width / 100))
        bezierPath.addLine(to: CGPoint(x: 140 * layersize.width / 100, y: 0 * layersize.width))
        bezierPath.close()
        UIColor.gray.setFill()
        bezierPath.fill()
        
        return bezierPath.cgPath
    }
    func shapeBounceAnimation(masterlayer : CALayer, image : UIImage , index : Int , totalCount : Int , isSaving : Bool){
        
        
        
        let newMasterLayerForIndex = CALayer()
        newMasterLayerForIndex.frame = masterlayer.frame
        newMasterLayerForIndex.contents = image.cgImage
        newMasterLayerForIndex.opacity = 1.0
        newMasterLayerForIndex.contentsGravity = VideoEdVideoMakeManager.contentsGravity
        newMasterLayerForIndex.backgroundColor = UIColor.red.cgColor
        newMasterLayerForIndex.masksToBounds = true
        
        
        
        let shapeNewLayer = CAShapeLayer()
        shapeNewLayer.opacity = 1.0
        addNormalOpacityAnimation(sampleLayer: shapeNewLayer, index: index, totalCount: totalCount, isSaving: isSaving)
        
        
        var newFrame = masterlayer.frame
        newFrame.size.width = newFrame.width * 1.0
        newFrame.size.height = newFrame.width * 1.0
        
        shapeNewLayer.frame = newFrame
        shapeNewLayer.position = masterlayer.position
        let startShape = UIBezierPath(roundedRect: newFrame, cornerRadius: 0.1).cgPath
        
        shapeNewLayer.path = startShape
        
        var  stopAtYLocationForAWhile = masterlayer.frame.width*0.1
        let midShape1 = UIBezierPath(roundedRect: CGRect(x: masterlayer.frame.width*0.1, y: stopAtYLocationForAWhile, width: masterlayer.frame.width*0.8, height: masterlayer.frame.width*0.8) , cornerRadius: masterlayer.frame.width*0.4).cgPath
        
        let endShape = UIBezierPath(roundedRect: CGRect(x: masterlayer.frame.width*3, y: masterlayer.frame.width/2, width: 1, height: 1) , cornerRadius: masterlayer.frame.width/2).cgPath
        
        
        let animationPath = CAKeyframeAnimation(keyPath: "path")
        animationPath.duration = CFTimeInterval(VideoEdVideoMakeManager.durationForOnePic)
        animationPath.beginTime =  AVCoreAnimationBeginTimeAtZero + CFTimeInterval( CGFloat(index) * VideoEdVideoMakeManager.durationForOnePic)
        animationPath.values = [startShape,midShape1,midShape1,endShape]
        animationPath.keyTimes = [0.0,0.15,0.85,1.0]
        animationPath.fillMode = CAMediaTimingFillMode.both
        animationPath.isRemovedOnCompletion = false
        
        
        if totalCount-1 > index{
            shapeNewLayer.add(animationPath, forKey: "pathAnimation" + "\(index)")
        }
        
        stopAtYLocationForAWhile = masterlayer.frame.width*0.5

        let animation1 = CAKeyframeAnimation(keyPath: "position.y")
        animation1.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
        animation1.duration = CFTimeInterval(VideoEdVideoMakeManager.durationForOnePic) * 0.50
        animation1.beginTime = AVCoreAnimationBeginTimeAtZero + CFTimeInterval(CGFloat(index) * VideoEdVideoMakeManager.durationForOnePic) +  CFTimeInterval( VideoEdVideoMakeManager.durationForOnePic) * 0.15
        animation1.values = [-10.0 + stopAtYLocationForAWhile, 10.0 + stopAtYLocationForAWhile ,-7.0 + stopAtYLocationForAWhile,7.0 + stopAtYLocationForAWhile ,-5.0 + stopAtYLocationForAWhile, 5.0 + stopAtYLocationForAWhile ,-3.0 + stopAtYLocationForAWhile,3.0 + stopAtYLocationForAWhile,0.0 + stopAtYLocationForAWhile]
        animation1.isRemovedOnCompletion = false
        animation1.fillMode = CAMediaTimingFillMode.forwards
        if totalCount-1 > index{
            shapeNewLayer.add(animation1, forKey: "position.y" + "\(index)")
        }

        
        newMasterLayerForIndex.mask = shapeNewLayer
        
        let blurImage = blurEffect(inputImage: image)
        let sampleblurLayer = CALayer()
        sampleblurLayer.frame = masterlayer.frame
        sampleblurLayer.contents = image.cgImage
        sampleblurLayer.opacity = 1.0
        sampleblurLayer.contentsGravity = VideoEdVideoMakeManager.contentsGravity
        
        let fadeAnim:CABasicAnimation = CABasicAnimation(keyPath: "contents");
        fadeAnim.fromValue = blurImage.cgImage;
        fadeAnim.toValue   = image.cgImage;
        fadeAnim.beginTime =  AVCoreAnimationBeginTimeAtZero + CFTimeInterval( CGFloat(index) * VideoEdVideoMakeManager.durationForOnePic)
        fadeAnim.duration  = 2.0
        fadeAnim.isRemovedOnCompletion = false
        fadeAnim.fillMode = CAMediaTimingFillMode.backwards
        sampleblurLayer.add(fadeAnim, forKey: "contents" + "\(index)")
        
        newMasterLayerForIndex.addSublayer(sampleblurLayer)
        
        masterlayer.insertSublayer(newMasterLayerForIndex, at: 0)
        
    }
    func reshapeCircleAnimation(masterlayer : CALayer, image : UIImage , index : Int , totalCount : Int , isSaving : Bool){
        
        let newMasterLayerForIndex = CALayer()
        newMasterLayerForIndex.frame = masterlayer.frame
        newMasterLayerForIndex.contents = image.cgImage
        newMasterLayerForIndex.opacity = 1.0
        newMasterLayerForIndex.contentsGravity = VideoEdVideoMakeManager.contentsGravity
        newMasterLayerForIndex.masksToBounds = true

        
        let shapeNewLayer = CAShapeLayer()
        shapeNewLayer.opacity = 1.0
        addNormalOpacityAnimation(sampleLayer: shapeNewLayer, index: index, totalCount: totalCount, isSaving: isSaving)
        
        
        var newFrame = masterlayer.frame
        newFrame.size.width = newFrame.width * 1.0
        newFrame.size.height = newFrame.width * 1.0
        
        shapeNewLayer.frame = newFrame
        shapeNewLayer.position = masterlayer.position
        let startShape = UIBezierPath(roundedRect: newFrame, cornerRadius: 0.1).cgPath
        
        shapeNewLayer.path = startShape
        
        let midShape1 = UIBezierPath(roundedRect: CGRect(x: masterlayer.frame.width*0.1, y: masterlayer.frame.width*0.1, width: masterlayer.frame.width*0.8, height: masterlayer.frame.width*0.8) , cornerRadius: masterlayer.frame.width*0.4).cgPath

        
        let endShape = UIBezierPath(roundedRect: CGRect(x: masterlayer.frame.width/2, y: masterlayer.frame.width/2, width: 1, height: 1) , cornerRadius: masterlayer.frame.width/2).cgPath
        
        
        let animationPath = CAKeyframeAnimation(keyPath: "path")
        animationPath.duration = CFTimeInterval(VideoEdVideoMakeManager.durationForOnePic)
        animationPath.beginTime =  AVCoreAnimationBeginTimeAtZero + CFTimeInterval( CGFloat(index) * VideoEdVideoMakeManager.durationForOnePic)
        animationPath.values = [startShape,midShape1,midShape1,endShape]
        animationPath.keyTimes = [0.0,0.15,0.85,1.0]
        animationPath.fillMode = CAMediaTimingFillMode.both
        animationPath.isRemovedOnCompletion = false

        if totalCount-1 > index{
            shapeNewLayer.add(animationPath, forKey: "pathAnimation" + "\(index)")
        }
        
        newMasterLayerForIndex.mask = shapeNewLayer

        let blurImage = blurEffect(inputImage: image)
        let sampleblurLayer = CALayer()
        sampleblurLayer.frame = masterlayer.frame
        sampleblurLayer.contents = image.cgImage
        sampleblurLayer.opacity = 1.0
        sampleblurLayer.contentsGravity = VideoEdVideoMakeManager.contentsGravity
        
        let fadeAnim:CABasicAnimation = CABasicAnimation(keyPath: "contents");
        fadeAnim.fromValue = blurImage.cgImage;
        fadeAnim.toValue   = image.cgImage;
        fadeAnim.beginTime =  AVCoreAnimationBeginTimeAtZero + CFTimeInterval( CGFloat(index) * VideoEdVideoMakeManager.durationForOnePic)
        fadeAnim.duration  = 2.0
        fadeAnim.isRemovedOnCompletion = false
        fadeAnim.fillMode = CAMediaTimingFillMode.backwards
        sampleblurLayer.add(fadeAnim, forKey: "contents" + "\(index)")
        newMasterLayerForIndex.addSublayer(sampleblurLayer)

        masterlayer.insertSublayer(newMasterLayerForIndex, at: 0)
        
    }
    func blurEffect(inputImage : UIImage) ->  UIImage{
        
        
        let currentFilter = CIFilter(name: "CIGaussianBlur")
        let beginImage = CIImage(image: inputImage)
        currentFilter!.setValue(beginImage, forKey: kCIInputImageKey)
        currentFilter!.setValue(60, forKey: kCIInputRadiusKey)
        
        let cropFilter = CIFilter(name: "CICrop")
        cropFilter!.setValue(currentFilter!.outputImage, forKey: kCIInputImageKey)
        cropFilter!.setValue(CIVector(cgRect: beginImage!.extent), forKey: "inputRectangle")
        let context = CIContext(options: nil)
        
        let output = cropFilter!.outputImage
        let cgimg = context.createCGImage(output!, from: output!.extent)
        let processedImage = UIImage(cgImage: cgimg!)
        return processedImage
        

    }

    

    func addFadeAnimation(masterlayer : CALayer, image : UIImage , index : Int , totalCount : Int , isSaving : Bool){
        
        let sampleLayer = CALayer()
        sampleLayer.frame = masterlayer.frame
        sampleLayer.contents = image.cgImage
        sampleLayer.opacity = 0.0
        sampleLayer.contentsGravity = VideoEdVideoMakeManager.contentsGravity
        
        let animation2 = CAKeyframeAnimation(keyPath: "opacity")
        animation2.duration = CFTimeInterval(VideoEdVideoMakeManager.durationForOnePic) + 0.19
        if index == 0 {
            
            animation2.beginTime =  AVCoreAnimationBeginTimeAtZero + CFTimeInterval( CGFloat(index) * VideoEdVideoMakeManager.durationForOnePic)
            animation2.values = [0.5,1.0,1.0,0.0]
            
            if isSaving{
                animation2.values = [1.0,1.0,1.0,0.0]
            }
            if (totalCount-1 == index){
                animation2.values = [1.0,1.0,1.0,1.0]
            }
            
        }else if (totalCount-1 == index){
            
            animation2.beginTime =  CFTimeInterval( CGFloat(index) * VideoEdVideoMakeManager.durationForOnePic) -  0.1
            animation2.values = [0,1.0,1.0,1.0]
        }
        else{
            animation2.beginTime =  CFTimeInterval( CGFloat(index) * VideoEdVideoMakeManager.durationForOnePic) -  0.1
            animation2.values = [0,1.0,1.0,0.0]
            
        }
        animation2.keyTimes = [0.0,0.1,0.9,1.0]
        animation2.isAdditive = true
        animation2.isRemovedOnCompletion = false
        animation2.fillMode = CAMediaTimingFillMode.forwards
        sampleLayer.add(animation2, forKey: "animation" + "\(index)")
        masterlayer.addSublayer(sampleLayer)
        
    }
    
    func addZoomAnimation(masterlayer : CALayer, image : UIImage , index : Int , totalCount : Int , isSaving : Bool){
        
        let sampleLayer = CALayer()
        sampleLayer.frame = masterlayer.frame
        sampleLayer.contents = image.cgImage
        sampleLayer.opacity = 0.0
        sampleLayer.contentsGravity = VideoEdVideoMakeManager.contentsGravity
        
        addNormalOpacityAnimation(sampleLayer: sampleLayer, index: index, totalCount: totalCount, isSaving: isSaving)

        let boundsOvershootAnimation = CAKeyframeAnimation(keyPath: "transform")
        boundsOvershootAnimation.beginTime = AVCoreAnimationBeginTimeAtZero + CFTimeInterval(CGFloat(index) * VideoEdVideoMakeManager.durationForOnePic)
        
        let startingScale = CATransform3DScale(masterlayer.transform, 2.0, 2.0, 0.5)
        let overshootScale = CATransform3DScale(masterlayer.transform, 1.2, 1.2, 1.0)
        let undershootScale = CATransform3DScale(masterlayer.transform, 0.9, 0.9, 1.0)
        let endingScale = masterlayer.transform
        
        boundsOvershootAnimation.values = [startingScale, overshootScale, undershootScale, endingScale]
        
        boundsOvershootAnimation.keyTimes = [0.0, 0.5, 0.9, 1.0].map { NSNumber(value: $0) }
        
        boundsOvershootAnimation.timingFunctions = [
            CAMediaTimingFunction(name:CAMediaTimingFunctionName.easeOut),
            CAMediaTimingFunction(name:CAMediaTimingFunctionName.easeInEaseOut),
            CAMediaTimingFunction(name:CAMediaTimingFunctionName.easeInEaseOut)
        ]
        
        boundsOvershootAnimation.fillMode = CAMediaTimingFillMode.forwards
        boundsOvershootAnimation.isRemovedOnCompletion = false
        sampleLayer.add(boundsOvershootAnimation, forKey: "transform.scale\(index)")
        masterlayer.addSublayer(sampleLayer)
        
    }
    func addSlowZoomOutAnimation(masterlayer : CALayer, image : UIImage , index : Int , totalCount : Int , isSaving : Bool){
        
        let sampleLayer = CALayer()
        sampleLayer.frame = masterlayer.frame
        sampleLayer.contents = image.cgImage
        sampleLayer.opacity = 0.0
        sampleLayer.contentsGravity = VideoEdVideoMakeManager.contentsGravity
        
        addLittleFadeOpacityAnimation(sampleLayer: sampleLayer, index: index, totalCount: totalCount, isSaving: isSaving)
        
        
        let scaleAnimation = CAKeyframeAnimation(keyPath: "transform.scale")
        scaleAnimation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
        scaleAnimation.beginTime = AVCoreAnimationBeginTimeAtZero + CFTimeInterval(CGFloat(index) * VideoEdVideoMakeManager.durationForOnePic)
        scaleAnimation.duration =  CFTimeInterval(VideoEdVideoMakeManager.durationForOnePic) + 0.19
        scaleAnimation.values = [1.0,1.4,1.0]
        scaleAnimation.keyTimes = [0.0,0.5,1.0]
        scaleAnimation.isAdditive = true
        scaleAnimation.isRemovedOnCompletion = false
        scaleAnimation.fillMode = CAMediaTimingFillMode.forwards
        sampleLayer.add(scaleAnimation, forKey: "transform.scale\(index)")
        
        masterlayer.addSublayer(sampleLayer)
        
    }
    
    func addShakeAnimation(masterlayer : CALayer, image : UIImage , index : Int , totalCount : Int , isSaving : Bool){
        
        let sampleLayer = CALayer()
        sampleLayer.frame = masterlayer.frame
        sampleLayer.contents = image.cgImage
        sampleLayer.opacity = 0.0
        sampleLayer.contentsGravity = VideoEdVideoMakeManager.contentsGravity
        
        addLittleFadeOpacityAnimation(sampleLayer: sampleLayer, index: index, totalCount: totalCount, isSaving: isSaving)
        
        let animation1 = CAKeyframeAnimation(keyPath: "transform.translation.x")
        animation1.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
        animation1.duration = CFTimeInterval(VideoEdVideoMakeManager.durationForOnePic) + 0.19
        animation1.beginTime = AVCoreAnimationBeginTimeAtZero + CFTimeInterval(CGFloat(index) * VideoEdVideoMakeManager.durationForOnePic)
        animation1.values = [-10.0, 10.0,-7.0,7.0,-5.0, 5.0,-3.0,3.0,0.0]
        animation1.isRemovedOnCompletion = false
        animation1.fillMode = CAMediaTimingFillMode.forwards
        sampleLayer.add(animation1, forKey: "transform.scale\(index)")
        
        masterlayer.addSublayer(sampleLayer)
        
    }
    func addCinematicAnimation(masterlayer : CALayer, image : UIImage , index : Int , totalCount : Int , isSaving : Bool){
        
        let sampleLayer = CALayer()
        sampleLayer.frame = masterlayer.frame

//        if let currentFilteredImage = GPUImageSepiaFilter().image(byFilteringImage: image){
        if let currentFilteredImage = self.sepiaImage(image: image){
            sampleLayer.contents = currentFilteredImage.cgImage
        }else{
            sampleLayer.contents = image.cgImage

        }
        sampleLayer.opacity = 0.0
        sampleLayer.contentsGravity = VideoEdVideoMakeManager.contentsGravity
        
        addLittleFadeOpacityAnimation(sampleLayer: sampleLayer, index: index, totalCount: totalCount, isSaving: isSaving)
        
        let rotateAnimation = CAKeyframeAnimation(keyPath: "transform.rotation.z")
        rotateAnimation.isAdditive = true // Make the values relative to the current value
        rotateAnimation.values = [0.05 ,-0.05, 0.05,0] // Double.pi
        rotateAnimation.keyTimes = [0.0,0.33,0.667,1.0]
        rotateAnimation.beginTime = AVCoreAnimationBeginTimeAtZero + CFTimeInterval(CGFloat(index) * VideoEdVideoMakeManager.durationForOnePic)
        rotateAnimation.duration = CFTimeInterval(VideoEdVideoMakeManager.durationForOnePic) + 0.19
        rotateAnimation.isRemovedOnCompletion = false
        rotateAnimation.fillMode = CAMediaTimingFillMode.forwards
        sampleLayer.add(rotateAnimation, forKey: "rotateAnimation\(index)")
        
        
        let scaleAnimation = CAKeyframeAnimation(keyPath: "transform.scale")
        scaleAnimation.beginTime = AVCoreAnimationBeginTimeAtZero + CFTimeInterval(CGFloat(index) * VideoEdVideoMakeManager.durationForOnePic)
        scaleAnimation.duration =  CFTimeInterval(VideoEdVideoMakeManager.durationForOnePic) + 0.19
        scaleAnimation.values = [1,1.059,1]
        scaleAnimation.keyTimes = [0.0,0.5,1.0]
        scaleAnimation.isAdditive = true
        scaleAnimation.isRemovedOnCompletion = false
        scaleAnimation.fillMode = CAMediaTimingFillMode.forwards
        sampleLayer.add(scaleAnimation, forKey: "scaleAnimation\(index)")

        masterlayer.addSublayer(sampleLayer)
        
    }
    func addWhitePushAnimation(masterlayer : CALayer, image : UIImage , index : Int , totalCount : Int , isSaving : Bool){
        
        let sampleLayer = CALayer()
        let frameModify = masterlayer.frame
//        frameModify.origin.x = masterlayer.frame.size.width
        sampleLayer.frame = frameModify
        
        sampleLayer.transform = CATransform3DMakeTranslation(masterlayer.frame.size.width, 0, 0)
        
        sampleLayer.contents = image.cgImage
        sampleLayer.masksToBounds = true
        sampleLayer.opacity = 1.0
        sampleLayer.contentsGravity = VideoEdVideoMakeManager.contentsGravity
        

        /*
        let animation2 = CAKeyframeAnimation(keyPath: "opacity")
        animation2.duration = CFTimeInterval(VideoEdVideoMakeManager.durationForOnePic)
        if index == 0 {
            animation2.beginTime =  AVCoreAnimationBeginTimeAtZero + CFTimeInterval( CGFloat(index) * VideoEdVideoMakeManager.durationForOnePic)
            
        }else {
            
            animation2.beginTime = CFTimeInterval(CGFloat(index) * VideoEdVideoMakeManager.durationForOnePic)// - Double(VideoEdVideoMakeManager.durationForOnePic * 0.5)
            

        }
        animation2.values = [1,1.0]
        animation2.keyTimes = [0.0,0.10,1.0,1.0]
//        animation2.isAdditive = true
        animation2.isRemovedOnCompletion = false
        animation2.fillMode = CAMediaTimingFillMode.forwards
//        sampleLayer.add(animation2, forKey: "animation" + "\(index)")

        */
        
        let animation1 = CAKeyframeAnimation(keyPath: "transform.translation.x")
        animation1.beginTime = AVCoreAnimationBeginTimeAtZero + CFTimeInterval(CGFloat(index) * VideoEdVideoMakeManager.durationForOnePic)
        animation1.duration = CFTimeInterval(VideoEdVideoMakeManager.durationForOnePic)
        if index > 0 {
            
            animation1.beginTime =  CFTimeInterval(CGFloat(index) * VideoEdVideoMakeManager.durationForOnePic) - Double(VideoEdVideoMakeManager.durationForOnePic * CGFloat(index) * 0.25)
            
//            animation1.beginTime =  CFTimeInterval(CGFloat(index) * VideoEdVideoMakeManager.durationForOnePic) - Double(VideoEdVideoMakeManager.durationForOnePic * 0.25)
//            animation1.duration = CFTimeInterval(VideoEdVideoMakeManager.durationForOnePic) + Double(VideoEdVideoMakeManager.durationForOnePic * 0.25)


        }else{
            

        }
        animation1.values = [sampleLayer.frame.size.width, 0.0,0.0,-sampleLayer.frame.size.width]
        animation1.keyTimes = [0.0, 0.25,0.75, 1.0]
        
        
//        if totalCount - 1 == index {


        animation1.isRemovedOnCompletion = false
        animation1.fillMode = CAMediaTimingFillMode.forwards
//        animation1.isAdditive = false
        sampleLayer.add(animation1, forKey: "transform.translation.x\(index)")
        
//        let posAnimationX1 = CABasicAnimation(keyPath: "position.x")
//        posAnimationX1.fromValue = sampleLayer.frame.size.width * 0.5
//        posAnimationX1.toValue = -sampleLayer.frame.size.width * 2.0
//        posAnimationX1.fillMode = CAMediaTimingFillMode.forwards
//        posAnimationX1.isRemovedOnCompletion = false
//        posAnimationX1.beginTime = CFTimeInterval(timeRange+photoDuration-0.3)
//        posAnimationX1.duration = 0.3
//        blackLayer.add(posAnimationX1, forKey: "position\(timeRange+0.2)")

        
//        let transformAnim  = CAKeyframeAnimation(keyPath:"transform")
//        transformAnim.values  = [NSValue(caTransform3D: CATransform3DMakeRotation(0.04, 0.0, 0.0, 1.0)),NSValue(caTransform3D: CATransform3DMakeRotation(-0.04 , 0, 0, 1))]
//        transformAnim.autoreverses = true
//        animation1.beginTime = AVCoreAnimationBeginTimeAtZero + CFTimeInterval(CGFloat(index) * VideoEdVideoMakeManager.durationForOnePic)
//
//        transformAnim.duration  = CFTimeInterval(VideoEdVideoMakeManager.durationForOnePic)
////        transformAnim.repeatCount = Float.infinity
//        transformAnim.fillMode = CAMediaTimingFillMode.forwards
//        transformAnim.isRemovedOnCompletion = false

//        sampleLayer.add(transformAnim, forKey: "transform.scale\(index)")

        
        /*
         let boundsOvershootAnimation = CAKeyframeAnimation(keyPath: "transform")
         boundsOvershootAnimation.beginTime = AVCoreAnimationBeginTimeAtZero + CFTimeInterval(CGFloat(index) * VideoEdVideoMakeManager.durationForOnePic)
         
         let startingScale = CATransform3DRotate(masterlayer.transform, 1.2 * .pi , 100, 1, 100)
         let overshootScale = CATransform3DRotate(masterlayer.transform, 1 * .pi , 100, 1, 100)
         let undershootScale = CATransform3DRotate(masterlayer.transform, 1.2 * .pi , 100, 1, 100)
         
         let endingScale = masterlayer.transform
         
         boundsOvershootAnimation.values = [startingScale, overshootScale, undershootScale, endingScale]
         
         boundsOvershootAnimation.keyTimes = [0.0, 0.5, 0.9, 1.0].map { NSNumber(value: $0) }
         
         boundsOvershootAnimation.timingFunctions = [
         CAMediaTimingFunction(name:CAMediaTimingFunctionName.easeOut),
         CAMediaTimingFunction(name:CAMediaTimingFunctionName.easeInEaseOut),
         CAMediaTimingFunction(name:CAMediaTimingFunctionName.easeInEaseOut)
         ]
         
         boundsOvershootAnimation.fillMode = CAMediaTimingFillMode.forwards
         boundsOvershootAnimation.isRemovedOnCompletion = false
         sampleLayer.add(boundsOvershootAnimation, forKey: "transform.scale\(index)")
         */
        
        masterlayer.masksToBounds = true
        masterlayer.addSublayer(sampleLayer)
        
    }


}

extension AnimationsClass{
    
    
    func addExactOpacityAnimation(sampleLayer : CALayer, index : Int ,totalCount : Int ,isSaving : Bool ){
        
        let animation2 = CAKeyframeAnimation(keyPath: "opacity")
        animation2.duration = CFTimeInterval(VideoEdVideoMakeManager.durationForOnePic)
        if index == 0 {
            
            animation2.beginTime =  AVCoreAnimationBeginTimeAtZero + CFTimeInterval( CGFloat(index) * VideoEdVideoMakeManager.durationForOnePic)
            animation2.values = [0.5,1.0,1.0,0.0]
            
            if isSaving{
                animation2.values = [1.0,1.0,1.0,0.0]
            }
            if (totalCount-1 == index){
                animation2.values = [1.0,1.0,1.0,1.0]
            }
            
        }else if (totalCount-1 == index){
            
            animation2.beginTime =  CFTimeInterval( CGFloat(index) * VideoEdVideoMakeManager.durationForOnePic)
            animation2.values = [1,1.0,1.0,1.0]
        }
        else{
            animation2.beginTime =  CFTimeInterval( CGFloat(index) * VideoEdVideoMakeManager.durationForOnePic) - 0.2
            animation2.values = [1,1.0,1.0,0.0]
            
        }
        animation2.keyTimes = [0.0,0.1,0.95,1.0]
        animation2.isAdditive = true
        animation2.isRemovedOnCompletion = false
        animation2.fillMode = CAMediaTimingFillMode.forwards
        sampleLayer.add(animation2, forKey: "animation" + "\(index)")
        
    }
    func addNormalOpacityAnimation(sampleLayer : CALayer, index : Int ,totalCount : Int ,isSaving : Bool ){
        
        let animation2 = CAKeyframeAnimation(keyPath: "opacity")
        animation2.duration = CFTimeInterval(VideoEdVideoMakeManager.durationForOnePic) + 0.2
        if index == 0 {
            
            animation2.beginTime =  AVCoreAnimationBeginTimeAtZero + CFTimeInterval( CGFloat(index) * VideoEdVideoMakeManager.durationForOnePic)
            animation2.values = [0.5,1.0,1.0,0.0]
            
            if isSaving{
                animation2.values = [1.0,1.0,1.0,0.0]
            }
            if (totalCount-1 == index){
                animation2.values = [1.0,1.0,1.0,1.0]
            }
            
        }else if (totalCount-1 == index){
            
            animation2.beginTime =  CFTimeInterval( CGFloat(index) * VideoEdVideoMakeManager.durationForOnePic) -  0.1
            animation2.values = [1,1.0,1.0,1.0]
        }
        else{
            animation2.beginTime =  CFTimeInterval( CGFloat(index) * VideoEdVideoMakeManager.durationForOnePic) -  0.1
            animation2.values = [1,1.0,1.0,0.0]
            
        }
        animation2.keyTimes = [0.0,0.1,0.95,1.0]
        animation2.isAdditive = true
        animation2.isRemovedOnCompletion = false
        animation2.fillMode = CAMediaTimingFillMode.forwards
        sampleLayer.add(animation2, forKey: "animation" + "\(index)")
        
    }
    
    func addLittleFadeOpacityAnimation(sampleLayer : CALayer, index : Int ,totalCount : Int ,isSaving : Bool ){
        
        let animation2 = CAKeyframeAnimation(keyPath: "opacity")
        animation2.duration = CFTimeInterval(VideoEdVideoMakeManager.durationForOnePic) + 0.2
        if index == 0 {
            
            animation2.beginTime =  AVCoreAnimationBeginTimeAtZero + CFTimeInterval( CGFloat(index) * VideoEdVideoMakeManager.durationForOnePic)
            animation2.values = [0.5,1.0,1.0,0.0]
            
            if isSaving{
                animation2.values = [1.0,1.0,1.0,0.0]
            }
            if (totalCount-1 == index){
                animation2.values = [1.0,1.0,1.0,1.0]
            }
            
        }else if (totalCount-1 == index){
            
            animation2.beginTime =  CFTimeInterval( CGFloat(index) * VideoEdVideoMakeManager.durationForOnePic) -  0.1
            animation2.values = [0,1.0,1.0,1.0]
        }
        else{
            animation2.beginTime =  CFTimeInterval( CGFloat(index) * VideoEdVideoMakeManager.durationForOnePic) -  0.1
            animation2.values = [0,1.0,1.0,0.0]
            
        }
        animation2.keyTimes = [0.0,0.1,0.95,1.0]
        animation2.isAdditive = true
        animation2.isRemovedOnCompletion = false
        animation2.fillMode = CAMediaTimingFillMode.forwards
        sampleLayer.add(animation2, forKey: "animationLittleFade" + "\(index)")
        
    }
    
    func addLittleFadeAnimationLessTime(sampleLayer : CALayer, index : Int ,totalCount : Int ,isSaving : Bool ){
        
        let animation2 = CAKeyframeAnimation(keyPath: "opacity")
        animation2.duration = CFTimeInterval(VideoEdVideoMakeManager.durationForOnePic)
        if index == 0 {
            
            animation2.beginTime =  AVCoreAnimationBeginTimeAtZero + CFTimeInterval( CGFloat(index) * VideoEdVideoMakeManager.durationForOnePic)
            animation2.values = [0.5,1.0,1.0,0.0]
            
            if isSaving{
                animation2.values = [1.0,1.0,1.0,0.0]
            }
            if (totalCount-1 == index){
                animation2.values = [1.0,1.0,1.0,1.0]
            }
            
        }else if (totalCount-1 == index){
            
            animation2.beginTime =  CFTimeInterval( CGFloat(index) * VideoEdVideoMakeManager.durationForOnePic) -  0.1
            animation2.values = [0,1.0,1.0,1.0]
        }
        else{
            animation2.beginTime =  CFTimeInterval( CGFloat(index) * VideoEdVideoMakeManager.durationForOnePic) -  0.1
            animation2.values = [0,1.0,1.0,0.0]
            
        }
        animation2.keyTimes = [0.0,0.1,0.95,1.0]
        animation2.isAdditive = true
        animation2.isRemovedOnCompletion = false
        animation2.fillMode = CAMediaTimingFillMode.forwards
        sampleLayer.add(animation2, forKey: "animationLittleFade" + "\(index)")
        
    }
    func addReverseFadeOpacityAnimation(sampleLayer : CALayer, index : Int ,totalCount : Int ,isSaving : Bool ){
        
        let animation2 = CAKeyframeAnimation(keyPath: "opacity")
        animation2.duration = 0.2
        animation2.beginTime =  AVCoreAnimationBeginTimeAtZero
        animation2.values = [1.0,1.0,1.0,1.0]
        animation2.keyTimes = [0.0,0.1,0.95,1.0]
//        animation2.isAdditive = true
        animation2.isRemovedOnCompletion = false
        animation2.fillMode = CAMediaTimingFillMode.both
        sampleLayer.add(animation2, forKey: "animationReverseFade" + "\(index)")
        
    }
    func convertToGrayScale(image: UIImage) -> UIImage {
        
        // Create image rectangle with current image width/height
        let imageRect:CGRect = CGRect(x:0, y:0, width:image.size.width, height: image.size.height)
        
        // Grayscale color space
        let colorSpace = CGColorSpaceCreateDeviceGray()
        let width = image.size.width
        let height = image.size.height
        
        // Create bitmap content with current image size and grayscale colorspace
        let bitmapInfo = CGBitmapInfo(rawValue: CGImageAlphaInfo.none.rawValue)
        
        // Draw image into current context, with specified rectangle
        // using previously defined context (with grayscale colorspace)
        let context = CGContext(data: nil, width: Int(width), height: Int(height), bitsPerComponent: 8, bytesPerRow: 0, space: colorSpace, bitmapInfo: bitmapInfo.rawValue)
        context?.draw(image.cgImage!, in: imageRect)
        let imageRef = context!.makeImage()
        
        // Create a new UIImage object
        let newImage = UIImage(cgImage: imageRef!)
        
        return newImage
    }
    func sepiaImage(image : UIImage) -> UIImage?{
        
        let coreImage = CIImage(cgImage: image.cgImage!)
        
        let currentFilter = CIFilter(name: "CISepiaTone")
        currentFilter?.setValue(coreImage, forKey: kCIInputImageKey)
        currentFilter?.setValue(2.0, forKey: kCIInputIntensityKey)
        let output = currentFilter!.outputImage
        let context = CIContext(options: nil)

        let cgimg = context.createCGImage(output!, from: output!.extent)
        let processedImage = UIImage(cgImage: cgimg!)
        return processedImage

        
    }
}

