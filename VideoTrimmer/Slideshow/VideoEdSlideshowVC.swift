//
//  SlideshowViewController.swift
//  VideoEditorial
//
//  Created by iApp on 03/04/19.
//  Copyright © 2019 iApp. All rights reserved.
//

import UIKit


class VideoEdSlideshowVC: UIViewController {

    
    
    var galleryAssetsResults : [UIImage] = []

    var currentAnimaionIndex : Int = 0
    var currentListImageSelectedIndex : Int = -1


    @IBOutlet weak var copyPlayerView: UIView!
    @IBOutlet var playerMainView: VideoEdSlidePlayer!
    
    @IBOutlet weak var bottomMenuList: UIView!
    @IBOutlet weak var imagesListView: UIView!
    @IBOutlet weak var themesMenuView: UIView!
    @IBOutlet var durationView: UIView!
    @IBOutlet var themeCV: UICollectionView!
    
    @IBOutlet weak var musicView: UIView!
    @IBOutlet var durationLabel: UILabel!
    @IBOutlet weak var viewForWaves: UIView!
    
    @IBOutlet weak var playerSlider: UISlider!
    @IBOutlet weak var durationSlider: UISlider!
    
    @IBOutlet weak var playPauseButton: UIButton!
    @IBOutlet weak var currentTimeLbl: UILabel!
    @IBOutlet weak var totalTimeLbl: UILabel!
    
    @IBOutlet weak var listImagesCV: UICollectionView!
    
    @IBOutlet weak var multiSlider: MultiSlider!
    @IBOutlet weak var minValueLabel: UILabel!
    @IBOutlet weak var maxValueLabel: UILabel!
    
    var avAudioPlayer : AVPlayer!
    var audioTimeObserver        : (Any)? = nil
    @IBOutlet weak var deleteImageBtn: UIButton!
    @IBOutlet weak var waveImageview: UIImageView!
    @IBOutlet weak var playerviewYContraint: NSLayoutConstraint!
    @IBOutlet weak var musicRepeatSwitch: UISwitch!
    @IBOutlet var slideshowTopLabel: UILabel!
    
    @IBOutlet var aniamtionsLabel: UILabel!
    @IBOutlet var musicHeadLabel: UILabel!
    @IBOutlet var photosHeadLabel: UILabel!
    @IBOutlet var durationHeadLabel: UILabel!
    
    //MARK:-
    deinit {
        debugPrint("SlideshowVC dealloc")
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "songFromMusicVC"), object: nil)

    }
    @objc dynamic private func applicationWillResignActive() {
        // Do things here
        
        print("appWillResignActive in slideshow")

//        self.playPauseButton.setImage(#imageLiteral(resourceName: "ic_play"), for: .normal)

        
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: UIApplication.willResignActiveNotification, object: nil)

    }


    override func viewDidLoad() {
        super.viewDidLoad()

        VideoEdVideoMakeManager.durationForOnePic = 2
        VideoEdVideoMakeManager.audioUrl = Bundle.main.url(forResource: "SampleAudio", withExtension: "mp3")!

        currentTimeLbl.font = UIFont(name: FONT_REGULAR, size: 15)
        totalTimeLbl.font = UIFont(name: FONT_REGULAR, size: 15)
        durationLabel.font = UIFont(name: FONT_REGULAR, size: 15)
        
        aniamtionsLabel.font = UIFont(name: FONT_SEMIBOLD, size: 17)
        musicHeadLabel.font = UIFont(name: FONT_SEMIBOLD, size: 17)
        photosHeadLabel.font = UIFont(name: FONT_SEMIBOLD, size: 17)
        durationHeadLabel.font = UIFont(name: FONT_SEMIBOLD, size: 17)
        slideshowTopLabel.font = UIFont(name: FONT_BOLD, size: 23)
//        slideshowTopLabel.layer.shadowColor = UIColor.black.cgColor
//        slideshowTopLabel.layer.shadowOffset = .zero
        slideshowTopLabel.addAllShadow(2.0)
        VideoEdVideoMakeManager.repeatAudio = false
        musicRepeatSwitch.isOn = VideoEdVideoMakeManager.repeatAudio
    
        VideoEdVideoMakeManager.shared.cleanDocumentDirectory()
        
        debugPrint("width", UIScreen.main.bounds.width)
        debugPrint("height", UIScreen.main.bounds.height)

        listImagesCV.register(UINib.init(nibName: "ImageListCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ImageListCollectionViewCellID")
        listImagesCV.register(UICollectionReusableView.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionFooter, withReuseIdentifier: "Footer")
        
        if #available(iOS 11.0, *){
            
            self.listImagesCV.dropDelegate = self
            self.listImagesCV.dragDelegate = self
            self.listImagesCV.dragInteractionEnabled = true

        }

        NotificationCenter.default.addObserver(self, selector: #selector(self.songFromMusicVC), name: NSNotification.Name(rawValue: "songFromMusicVC"), object: nil)
        themeCV.register(UINib.init(nibName: "SlidesCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "SlidesCollectionViewCell")

        let replicatorLayer = CAReplicatorLayer()
        let imageSample = #imageLiteral(resourceName: "bars")
        
        DispatchQueue.main.async {
            
            if !IS_IPHONE_X_Series{
                self.playerviewYContraint.isActive = true
            }
            let imageViewHieght = self.viewForWaves.frame.height
            let imageViewWidth = imageSample.size.width / imageSample.size.height * imageViewHieght
            let imagesCount : Int = Int(self.viewForWaves.frame.width / imageViewWidth)
            
//            for i in 0..<4 {
//                print(i)
//            }
            var xValue : CGFloat = 0
            for _ in 0...imagesCount{
                
                let frameUpdate = CGRect(x: xValue, y: 0, width: imageViewWidth, height: imageViewHieght)
                let imageView = UIImageView()
                imageView.image = imageSample
                imageView.frame = frameUpdate
                self.viewForWaves.addSubview(imageView)
                xValue += imageViewWidth
            }
            
            self.waveImageview.backgroundColor = UIColor(patternImage: #imageLiteral(resourceName: "bars"))
            let audioAsset = AVAsset(url: VideoEdVideoMakeManager.audioUrl)
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                
                self.multiSlider.addTarget(self, action: #selector(self.sliderExitTouch(_:)), for: .touchUpInside)
                self.multiSlider.addTarget(self, action: #selector(self.sliderChanged(_:)), for: .valueChanged)

                self.multiSlider.orientation = .horizontal
                self.multiSlider.outerTrackColor = .lightGray
                self.multiSlider.trackWidth = 2
                
                self.multiSlider.value = [0, audioAsset.duration.durationInSeconds]
                self.multiSlider.maximumValue = CGFloat(audioAsset.duration.durationInSeconds)
                self.multiSlider.minimumValue = 0
                self.multiSlider.valueLabelPosition = .top // .notAnAttribute = don't show labels

            }
            
            
            VideoEdVideoMakeManager.audioStartTime = 0
            VideoEdVideoMakeManager.audioEndTime = audioAsset.duration.durationInSeconds
            
            let xFactor : CGFloat =  self.playerMainView.frame.width / self.copyPlayerView.frame.width
            let yFactor : CGFloat =  self.playerMainView.frame.height / self.copyPlayerView.frame.height

            debugPrint("xFactor",xFactor)
            debugPrint("yFactor",yFactor)
            debugPrint("self.playerMainView.frame.width",self.playerMainView.frame.width)
            debugPrint("self.copyPlayerView.frame.width",self.copyPlayerView.frame.width)
            replicatorLayer.instanceCount = 2
            replicatorLayer.frame = self.copyPlayerView.frame
            replicatorLayer.masksToBounds = true
            replicatorLayer.backgroundColor = UIColor.blue.cgColor
            replicatorLayer.instanceDelay = CFTimeInterval(1 / 30.0)
            replicatorLayer.preservesDepth = false
            replicatorLayer.instanceColor = UIColor.white.cgColor
//            replicatorLayer.instanceTransform = CATransform3DMakeTranslation(0, 50, 0)
            
            
        }
        
        DispatchQueue.main.async {
            
            self.playerSlider.tintColor = .red
            self.playerSlider.isContinuous = true
            self.playerSlider.setThumbImage(#imageLiteral(resourceName: "ic_slider_line"), for: .normal)
            self.durationSlider.setThumbImage(#imageLiteral(resourceName: "ic_slider_line"), for: .normal)
            self.durationSlider.value = Float(VideoEdVideoMakeManager.durationForOnePic)
            self.durationLabel.text = CGFloat(self.durationSlider.value).convertSecondsToDurationString()

            self.playerMainView.currentPlayingAnimaionIndex = self.currentAnimaionIndex
            self.playerMainView.imagesArray = self.galleryAssetsResults
            self.playerMainView.setupPlayer()
            
            
//            replicatorLayer.addSublayer(self.playerMainView.avSynchLayer)
//            self.copyPlayerView.layer.addSublayer(replicatorLayer)
            
            //MARK:- IMPORTANT LINE
//            self.playerMainView.removeFromSuperview()
            
            let audioAsset = AVAsset(url: VideoEdVideoMakeManager.audioUrl)
            self.maxValueLabel.text = audioAsset.duration.durationInSeconds.convertSecondsToDurationString()//junechanges19

            self.playerMainView.playerStatus = { isPlaying in
                

                if self.playerMainView.avPlayerItem != nil{
                    
                    let timeTotal = CMTimeGetSeconds(self.playerMainView.avPlayerItem!.duration)
                    self.playerSlider.maximumValue = Float(timeTotal)
                    self.totalTimeLbl.text = CGFloat(timeTotal).convertSecondsToDurationString()
                }
                if isPlaying{
                    self.playPauseButton.setImage(UIImage(named: "ic_pause"), for: .normal)

                }else{
                    self.playPauseButton.setImage(#imageLiteral(resourceName: "ic_play"), for: .normal)

                }
            }
            self.playerMainView.playerDurationClouser = {  (currentDuration)in

                self.playerSlider.setValue(Float(currentDuration), animated: true)
                self.currentTimeLbl.text = CGFloat(currentDuration).convertSecondsToDurationString()
            }
            self.playerMainView.playerUpdated = { success in
                
                if success{
                    DispatchQueue.main.async {
                        self.stopAnimating()
                    }
                }
            }
        }

    }
    @objc func sliderChanged(_ slider: MultiSlider) {
        
        print("\(slider.value)")
        minValueLabel.text = slider.value[0].convertSecondsToDurationString()
        maxValueLabel.text = slider.value[1].convertSecondsToDurationString()

        
    }
    @objc func sliderExitTouch(_ slider: MultiSlider) {
        
        debugPrint("min ",slider.value[0])
        debugPrint("max",slider.value[1])
        
        VideoEdVideoMakeManager.audioStartTime = slider.value[0]
        VideoEdVideoMakeManager.audioEndTime = slider.value[1]
        
        minValueLabel.text = slider.value[0].convertSecondsToDurationString()
        maxValueLabel.text = slider.value[1].convertSecondsToDurationString()
        self.playerMainView.updateAssetPlayer()
        
        debugPrint("print sliderExitTouch")

    }
    func didBackPressed() {

        currentListImageSelectedIndex = -1
        listImagesCV.reloadData()
        deleteImageBtn.isHidden = true
        
        if self.playerMainView != nil {
            
            self.playerMainView.imagesArray = galleryAssetsResults
            self.playerMainView.updateAssetPlayer()
        }

    }
    func releaseComponents() {
        
    }
    func themeAppearanceSetup(){
        self.view.backgroundColor =  AppThemeManager.shared.currentTheme().VCBackGroundColor
    }


    //MARK:- BUTTON ACTIONS
    @IBAction func playPauseAct(_ sender: UIButton) {
        
        if VideoEdVideoMakeManager.currentMenuMode == .MUSIC{
//            return
        }
        if self.playerMainView.player.isPlaying{
            
            self.playerMainView.player.pause()
            self.playPauseButton.setImage(#imageLiteral(resourceName: "ic_play"), for: .normal)

        }else{
            self.playerMainView.player.play()
            self.playPauseButton.setImage(UIImage(named: "ic_pause"), for: .normal)

        }
    }
    @IBAction func menuBtnAct(_ sender: UIButton) {
        
        bottomMenuList.subviews.forEach{ stackView in
            
            if stackView is UIStackView{
                
                stackView.subviews.forEach{ btn in
                    
                    if let btton = btn as? UIButton{
                        btton.tintColor = .white
                    }
                }
                
            }
        }
        
        sender.tintColor = AppThemeManager.shared.currentTheme().TintColor
        
        if (sender.tag == 99){
            self.view.bringSubviewToFront(themesMenuView)
            VideoEdVideoMakeManager.currentMenuMode = .LIST
            
        }else  if (sender.tag == 100){
            self.view.bringSubviewToFront(imagesListView)
            VideoEdVideoMakeManager.currentMenuMode = .IMAGESLIST
        }
        else if sender.tag == 101{
            self.view.bringSubviewToFront(durationView)
            VideoEdVideoMakeManager.currentMenuMode = .DURATION
        }else if sender.tag == 102{
            
            self.view.bringSubviewToFront(musicView)
            VideoEdVideoMakeManager.currentMenuMode = .MUSIC
//            self.playerMainView.player.pause()
//            self.playerMainView.manualScrolling = false
//            self.playPauseButton.setImage(#imageLiteral(resourceName: "ic_play"), for: .normal)



        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        NotificationCenter.default.addObserver(self, selector: #selector(applicationWillResignActive), name: UIApplication.willResignActiveNotification, object: nil)

//        if VideoEdVideoMakeManager.currentMenuMode == .MUSIC && !VideoEdVideoMakeManager.someAudioChanges{
//
//            VideoEdVideoMakeManager.someAudioChanges = false
//            VideoEdVideoMakeManager.currentMenuMode = .IMAGESLIST
//            self.view.bringSubviewToFront(imagesListView)
//            self.view.bringSubviewToFront(bottomMenuList)
//
//
//        }

    }
    @IBAction func dismissBtnAct(_ sender: UIButton) {
        
        debugPrint("VideoEdVideoMakeManager.audioUrl=>",VideoEdVideoMakeManager.audioUrl)

        if VideoEdVideoMakeManager.currentMenuMode == .MUSIC && VideoEdVideoMakeManager.someAudioChanges{
            self.playerMainView.updateAssetPlayer()
        }

        VideoEdVideoMakeManager.someAudioChanges = false
        VideoEdVideoMakeManager.currentMenuMode = .IMAGESLIST
        self.view.bringSubviewToFront(imagesListView)
        self.view.bringSubviewToFront(bottomMenuList)


        if avAudioPlayer != nil && self.audioTimeObserver != nil {
            self.avAudioPlayer.removeTimeObserver(self.audioTimeObserver)
        }
        avAudioPlayer?.pause()
        avAudioPlayer = nil

    }
    @IBAction func addMoreMusicBtnAction(_ sender: Any) {

        
        


    }

    @IBAction func deleteImage(_ sender: Any) {
        
        if galleryAssetsResults.count == 1{
            
            AlertManager.shared.alertWithTitle(title: "", mesage: "Are you sure to delete last picture ?", primaryActionText: "Confirm", dismissActionText: "Cancel", completionHandler: {
                self.resetPlayer()
                self.navigationController?.popToRootViewController(animated: true)

            }) {
                
            }
            
            return
        }
        if currentListImageSelectedIndex >= 0{
            
            galleryAssetsResults.remove(at: currentListImageSelectedIndex)
            currentListImageSelectedIndex = -1
            listImagesCV.reloadData()
            deleteImageBtn.isHidden = true
            
            self.playerMainView.imagesArray = galleryAssetsResults
            self.playerMainView.updateAssetPlayer()
        }
    }
    
    @IBAction func backBtnAct(_ sender: Any) {
        


        AlertManager.shared.alertWithTitle(title: "", mesage: "Are you sure to exit from Slideshow ?", primaryActionText: "Exit", dismissActionText: "Cancel", completionHandler: {
            self.resetPlayer()
            self.navigationController?.popToRootViewController(animated: true)

            
        }) {
            
        }
    }

    @IBAction func doneBtnAct(_ sender: UIButton) {
        
        if !IS_APP_FREE_TEMP && !IAPManagerLocal.sharedInstance.isInAppPurchased(){
            
            if self.playerMainView.player.isPlaying{
                self.playerMainView.player.pause()
                self.playPauseButton.setImage(#imageLiteral(resourceName: "ic_play"), for: .normal)
                
            }
            IAPManagerLocal.sharedInstance.openInapScreen()
            return
        }
        
        VideoEdVideoMakeManager.someAudioChanges = false
        VideoEdVideoMakeManager.currentMenuMode = .IMAGESLIST
        self.view.bringSubviewToFront(imagesListView)
        self.view.bringSubviewToFront(bottomMenuList)

        resetPlayer()
        
        let  tempasset : AVAsset? = AVAsset.init(url: self.backgroundVideoURL)
        
        var  mediaImagesArray : [UIImage] = []
        mediaImagesArray = self.galleryAssetsResults

        guard let asset1 = tempasset, let videoTrack = asset1.tracks(withMediaType: AVMediaType.video).first else {
            return
        }
        let composition = AVMutableComposition()
        let calcTimeForVideo = CGFloat((mediaImagesArray.count)) * VideoEdVideoMakeManager.durationForOnePic
        let endTime:CMTime = CMTime(seconds: Double(calcTimeForVideo), preferredTimescale: 600)
        
        let assetTimeRange = CMTimeRangeMake(start: .zero, duration: endTime)
        
        let sourceVideoTrack: AVAssetTrack! = videoTrack
        let compositionVideoTrack: AVMutableCompositionTrack? = composition.addMutableTrack(withMediaType: .video, preferredTrackID: kCMPersistentTrackID_Invalid)
        try? compositionVideoTrack?.insertTimeRange(assetTimeRange, of: sourceVideoTrack, at: .zero)
        self.insertAudioInSlideshow(composition: composition)

        print("durationIn ", composition.duration.durationInFloat)
        let outputSize = CGSize(width: 1080, height: 1080)
        
        let arrayLayerInstructions:[AVMutableVideoCompositionLayerInstruction] = []
        let mainInstruction = AVMutableVideoCompositionInstruction()
        mainInstruction.timeRange = CMTimeRangeMake(start: .zero, duration: composition.duration)
        mainInstruction.layerInstructions = arrayLayerInstructions
        
        let videoComposition = AVMutableVideoComposition()
        videoComposition.instructions = [mainInstruction]
        videoComposition.frameDuration = CMTimeMake(value: 1, timescale: 30)
        videoComposition.renderSize = outputSize
        
        self.addAnimationToolInComposition(composition: videoComposition)

        self.startAnimating(messageFont: UIFont(name: FONT_REGULAR, size: 18), type: NVActivityIndicatorType.circleStrokeSpin)
        
        
        
        let exporter = AVAssetExportSession.init(asset: composition, presetName: AVAssetExportPresetHighestQuality)
        exporter?.videoComposition = videoComposition
        exporter?.timeRange = assetTimeRange;
        exporter?.outputFileType = AVFileType.mp4
        exporter?.outputURL = savingVideoUrl
        exporter?.shouldOptimizeForNetworkUse = true
        
        var timerForExportStatus : Timer?
        if #available(iOS 10.0, *) {
            timerForExportStatus = Timer.scheduledTimer(withTimeInterval: 0.1, repeats: true) { timer in
                let percent : Int = (Int((exporter?.progress)!*100))
                let str = String(format: "%d%%",percent)
                NVActivityIndicatorPresenter.sharedInstance.setMessage(str)
            }

        }
        weak var weakSelf = self
        exporter?.exportAsynchronously(completionHandler: {
            DispatchQueue.main.async {
                
                timerForExportStatus?.invalidate()
                timerForExportStatus = nil
                
                if exporter?.status == AVAssetExportSession.Status.completed {
                    print("Exported file: \(String(describing: exporter?.outputURL))")
                    if let pathValid = exporter?.outputURL?.path {
                        if FileManager.default.fileExists(atPath: pathValid){
                            let urlToYourVideo = URL(fileURLWithPath: pathValid)
                            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
                                weakSelf?.saveVideoUrl(videoUrl: urlToYourVideo)
                            })
                        }
                    }
                    
                }else if exporter?.status == AVAssetExportSession.Status.failed {
                    print("exporter?.error: \(exporter?.error ?? "" as! Error)")
                    weakSelf?.stopAnimating()
                }
                
            }
        })

    }
    func insertAudioInSlideshow(composition : AVMutableComposition ){
        
        let calcTimeForVideo = CGFloat((galleryAssetsResults.count)) * VideoEdVideoMakeManager.durationForOnePic
        let endTime:CMTime = CMTime(seconds: Double(calcTimeForVideo), preferredTimescale: 100)
        
        let audioAsset = AVAsset(url: VideoEdVideoMakeManager.audioUrl)
        let minTime = CMTimeMinimum(endTime, CMTime(seconds: Double(VideoEdVideoMakeManager.audioEndTime - VideoEdVideoMakeManager.audioStartTime), preferredTimescale: 100))
        let startTime = CMTime(seconds: Double(VideoEdVideoMakeManager.audioStartTime), preferredTimescale: 100)
        let audioTimeRange = CMTimeRange(start: startTime , duration:  minTime)

        let assetAudioTrack: AVAssetTrack = audioAsset.tracks(withMediaType: AVMediaType.audio)[0]
        
        if VideoEdVideoMakeManager.repeatAudio{
            
            if minTime<endTime{
                
                let compositionAudioVideo: AVMutableCompositionTrack = composition.addMutableTrack(withMediaType: AVMediaType.audio, preferredTrackID: CMPersistentTrackID())!
                let divideTimeCount =  endTime.durationInSeconds /  minTime.durationInSeconds
                //                CMTimeMultiplyByRatio(endTime, multiplier: 1, divisor: Int32(minTime.durationInSeconds))
                debugPrint("count=",divideTimeCount)
                
                var mutCmtime : CMTime = .zero
                for _ in 0...Int(divideTimeCount) {
                    
                    do {
                        try compositionAudioVideo.insertTimeRange(audioTimeRange, of: assetAudioTrack, at: mutCmtime)
                        
                    } catch {
                        // Handle the error
                        print("Inserting time range failed. ", error)
                    }
                    
                    mutCmtime = mutCmtime + minTime
                    debugPrint("mutCmtime=",mutCmtime)
                    
                }
                
            }else{
                let compositionAudioVideo: AVMutableCompositionTrack = composition.addMutableTrack(withMediaType: AVMediaType.audio, preferredTrackID: CMPersistentTrackID())!
                
                do {
                    try compositionAudioVideo.insertTimeRange(audioTimeRange, of: assetAudioTrack, at: .zero)
                    
                } catch {
                    // Handle the error
                    print("Inserting time range failed. ", error)
                }
            }
        }else{
            
            let compositionAudioVideo: AVMutableCompositionTrack = composition.addMutableTrack(withMediaType: AVMediaType.audio, preferredTrackID: CMPersistentTrackID())!
            
            do {
                try compositionAudioVideo.insertTimeRange(audioTimeRange, of: assetAudioTrack, at: .zero)
                
            } catch {
                // Handle the error
                print("Inserting time range failed. ", error)
            }
        }

        
    }
    //MARK:- Slider Action
    @IBAction func durationUpdate(_ sender: UISlider) {
        
        durationLabel.text = CGFloat(sender.value).convertSecondsToDurationString()
//        maxValueLabel.text = durationLabel.text
        VideoEdVideoMakeManager.durationForOnePic = CGFloat(sender.value)
        debugPrint("VideoEdVideoMakeManager.durationForOnePic=>  ",VideoEdVideoMakeManager.durationForOnePic)
    }
    
    @IBAction func durationEnded(_ sender: UISlider) {
        self.playerMainView.manualScrolling = false
        self.playerMainView.imagesArray = self.galleryAssetsResults
        self.playerMainView.updateAssetPlayer()
        
        
    }
    @IBAction func playerDrag(_ sender: Any) {
        self.playerMainView.manualScrolling = false

    }
    @IBAction func playerSliderRelease(_ sender: UISlider) {
        self.playerMainView.manualScrolling = false
    }
    @IBAction func playerSliderManualyUpdate(_ sender: UISlider) {

        self.playerMainView.manualScrolling = true
        self.playerMainView.player.seek(to: CMTimeMakeWithSeconds(Float64(sender.value), preferredTimescale: 1000))
        self.currentTimeLbl.text = CGFloat(sender.value).convertSecondsToDurationString()
        
    }
    //MARK:- Switch Action
    @IBAction func switchValueChanged(_ sender: UISwitch) {
    
        if sender.isOn{
            VideoEdVideoMakeManager.repeatAudio = true
        }else{
            VideoEdVideoMakeManager.repeatAudio = false
        }
        playerMainView.updateAssetPlayer()

    }
    //MARK:-
    
    func saveVideoUrl(videoUrl: URL){
     


    }
    func addAnimationToolInComposition(composition : AVMutableVideoComposition)  {
        
        let videoSize = GlobalHelper.shared.greenLineFreeSize(composition.renderSize)
        debugPrint("videoSize ->",videoSize)
        
        let qualityScale : CGFloat = 3
        let parentLayer = CALayer()
        parentLayer.contentsScale = qualityScale
        parentLayer.rasterizationScale = qualityScale
        parentLayer.frame = CGRect(x: 0, y: 0, width: videoSize.width, height: videoSize.height)
        let videoLayer = CALayer()
        videoLayer.contentsScale = qualityScale
        videoLayer.rasterizationScale = qualityScale
        videoLayer.frame = CGRect(x: 0, y: 0, width: videoSize.width, height: videoSize.height)
        parentLayer.addSublayer(videoLayer)

        var  mediaImagesArray : [UIImage] = []
        mediaImagesArray = self.galleryAssetsResults

        let animateValue = globalMenuAnimationsArray[currentAnimaionIndex]

        for i in 0..<mediaImagesArray.count {
            
            AnimationsClass().addAnimation(masterlayer: videoLayer, images: mediaImagesArray, index: i, totalCount: mediaImagesArray.count, isSaving: true , animateValue: animateValue)

        }

//        let imageWatermark = #imageLiteral(resourceName: "theme_19")
//        let watermarkWidth = parentLayer.bounds.size.width  * 0.25
//        let height = imageWatermark.size.height / imageWatermark.size.width * watermarkWidth
//        let watermarkLayer = CALayer()
//        watermarkLayer.contents = imageWatermark.cgImage
//        watermarkLayer.contentsScale = qualityScale
//        watermarkLayer.rasterizationScale = qualityScale
//        watermarkLayer.frame = CGRect(x: parentLayer.bounds.size.width - watermarkWidth , y: 0, width: watermarkWidth, height: height)
//        parentLayer.addSublayer(watermarkLayer)

        composition.animationTool = AVVideoCompositionCoreAnimationTool(postProcessingAsVideoLayer: videoLayer, in: parentLayer)

    }
    //MARK:- Notifications
    @objc func songFromMusicVC(notification: NSNotification){
        let dict = notification.userInfo
        if let safeSongUrl = dict!["songUrl"] {
            debugPrint("safeSongUrl=>",safeSongUrl)
            VideoEdVideoMakeManager.audioUrl = safeSongUrl as? URL
            let audioAsset = AVAsset(url: VideoEdVideoMakeManager.audioUrl)
            debugPrint("audioAsset.duration=>",audioAsset.duration.durationInSeconds)
            self.multiSlider.maximumValue = CGFloat(audioAsset.duration.durationInSeconds)
            self.multiSlider.value = [0, audioAsset.duration.durationInSeconds]

            self.maxValueLabel.text = audioAsset.duration.durationInSeconds.convertSecondsToDurationString()
            minValueLabel.text = self.multiSlider.value[0].convertSecondsToDurationString()
            VideoEdVideoMakeManager.audioStartTime = 0
            VideoEdVideoMakeManager.audioEndTime = audioAsset.duration.durationInSeconds

            debugPrint(" audioUrl dration =>",audioAsset.duration.durationInSeconds)

            if playerMainView != nil{
                
                playerMainView.updateAssetPlayer()
            }
            
            
        }
    }
    @objc func itemDidFinishPlaying(_ notification: Notification) {
        DispatchQueue.main.async {

            self.avAudioPlayer?.seek(to: CMTime(seconds: Double(self.multiSlider.value[0]), preferredTimescale: 100))
        }
    }


    func resetPlayer()  {
        
        self.playerMainView.manualScrolling = false
        self.playPauseButton.setImage(#imageLiteral(resourceName: "ic_play"), for: .normal)
        if avAudioPlayer != nil && self.audioTimeObserver != nil {
            self.avAudioPlayer.removeTimeObserver(self.audioTimeObserver!)
        }
        avAudioPlayer?.pause()
        avAudioPlayer = nil
        playerMainView.player.pause()
        playerMainView.nillPlayerForcefully()
        playerMainView.removeFromSuperview()
        playerMainView = nil
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
//        var touch : UITouch! =  touches.first! as UITouch
//
//        let location = touch.location(in: self.view)
//        debugPrint("location")

        
    }
    

    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        
//        var touch : UITouch! =  touches.first! as UITouch
//
//        let location = touch.location(in: self.view)
//        debugPrint("location")
    }
    
}
//MARK:-
extension VideoEdSlideshowVC{
    var backgroundVideoURL: URL {
        get {
            return URL(fileURLWithPath: Bundle.main.path(forResource: "black", ofType: "mov")!)
        }
    }
    
    var savingVideoUrl : URL? {
        get {
            var passUrl = false
            var url: URL?
            
            while !passUrl {
                
                let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
                
                let completePath = documentsPath.appendingFormat(String(format: "/VideoDoc_%lld.mov", Int64(arc4random()) % 1000000000000000))
                //                let completePath = NSTemporaryDirectory().appendingFormat(String(format: "VideoDoc_%lld.mov", Int64(arc4random()) % 1000000000000000))
                url =  URL(fileURLWithPath: completePath)
                
                let exists: Bool = FileManager.default.fileExists(atPath: completePath)
                if !exists {
                    passUrl = true
                }
            }
            return url
        }
        
    }
}

//extension VideoEdSlideshowVC : UICollectionViewDragDelegate,UICollectionViewDropDelegate{
//    func collectionView(_ collectionView: UICollectionView, itemsForBeginning session: UIDragSession, at indexPath: IndexPath) -> [UIDragItem] {
//
//        let item = ite
//    }
//
//
//}
extension VideoEdSlideshowVC : UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout {
    
//    func collectionView(_ collectionView: UICollectionView, moveItemAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
//        print("Changing the cell order, moving: \(sourceIndexPath.row) to \(destinationIndexPath.row)")
//    }
//
//
//    func collectionView(_ collectionView: UICollectionView, targetIndexPathForMoveFromItemAt originalIndexPath: IndexPath, toProposedIndexPath proposedIndexPath: IndexPath) -> IndexPath {
//
//        return originalIndexPath
//    }
    func collectionView(_ collectionView: UICollectionView, canMoveItemAt indexPath: IndexPath) -> Bool {
        return true
    }
//    func collectionView(_ collectionView: UICollectionView, canMoveItemAt indexPath: IndexPath) -> Bool {
//        if listImagesCV == collectionView{
//            return true
//        }
//        return true
//    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if listImagesCV == collectionView{
            return  galleryAssetsResults.count
        }else{
            
            return globalMenuAnimationsArray.count
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if listImagesCV == collectionView{
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageListCollectionViewCellID", for: indexPath) as! ImageListCollectionViewCell
            
            if let image = galleryAssetsResults[indexPath.row] as? UIImage{
                cell.thumbImage.image = image
            }
            if(indexPath.row == currentListImageSelectedIndex){
                cell.layer.borderColor = AppThemeManager.shared.currentTheme().SelectedStrokeColor.cgColor
                cell.layer.borderWidth = 2.0
            }else{
                cell.layer.borderColor = AppThemeManager.shared.currentTheme().UnSelStrokeColor.cgColor
                cell.layer.borderWidth = 1
            }
            DispatchQueue.main.async {
                cell.layer.cornerRadius = cell.frame.width/2
            }

//            cell.contentView.backgroundColor = .red
            cell.backgroundColor = .clear
//            cell.clipsToBounds = true
//            cell.layer.masksToBounds = true
            return cell
            
            
        }
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SlidesCollectionViewCell", for: indexPath) as! SlidesCollectionViewCell
        cell.configure(with: globalMenuAnimationsArray[indexPath.row])
        cell.thumbnail.image = UIImage(named: "theme_\((indexPath.row % 22) + 1)")

        if(indexPath.row == currentAnimaionIndex){
            
            cell.themeName.textColor = .white
            cell.thumbnail.layer.borderColor = AppThemeManager.shared.currentTheme().SelectedStrokeColor.cgColor
            cell.thumbnail.layer.borderWidth = 2.0

        }else{
            cell.themeName.textColor = .gray
            cell.thumbnail.layer.borderColor = AppThemeManager.shared.currentTheme().UnSelStrokeColor.cgColor
            cell.thumbnail.layer.borderWidth = 1
            
        }
        cell.themeName.font = UIFont(name: FONT_REGULAR, size: 13)
        return cell
        
        
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if listImagesCV == collectionView{

            DispatchQueue.main.async {
                if self.currentListImageSelectedIndex == indexPath.row{
                    self.currentListImageSelectedIndex = -1
                }else{
                    self.currentListImageSelectedIndex = indexPath.row
                }
                if self.currentListImageSelectedIndex >= 0 {
                    self.deleteImageBtn.isHidden = false
                }else{
                    self.deleteImageBtn.isHidden = true
                }

            }
            collectionView.reloadData()
            collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
            return
        }
        if !IAPManagerLocal.sharedInstance.isInAppPurchased() && indexPath.row >= FREE_SLIDESHOWTHEMES{
            IAPManagerLocal.sharedInstance.openInapScreen()
            return
        }
        if (indexPath.row == currentAnimaionIndex){
            return;
        }

        var arrayToReload : [IndexPath] = []
        arrayToReload.append(indexPath)
        arrayToReload.append(IndexPath(row: currentAnimaionIndex, section: 0))

        currentAnimaionIndex = indexPath.row

        AppDelegate.sharedInstance.window?.isUserInteractionEnabled = false
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            AppDelegate.sharedInstance.window?.isUserInteractionEnabled = true
//            self.startAnimating( type: NVActivityIndicatorType.circleStrokeSpin)
        }
        DispatchQueue.main.async {
//            self.playerMainView.player.pause()
            self.playerMainView.currentPlayingAnimaionIndex = self.currentAnimaionIndex
            self.playerMainView.imagesArray = self.galleryAssetsResults
            self.playerMainView.updateAssetPlayer()

        }

        collectionView.performBatchUpdates({

            collectionView.reloadItems(at: arrayToReload)
            collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        })

        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if listImagesCV == collectionView{
            return CGSize(width: 60, height: 60)
        }
        return CGSize(width: 60, height: 80)
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        
        if collectionView == listImagesCV &&  galleryAssetsResults.count < IMAGES_LIMIT{
            return CGSize(width: 75, height: 60)
        }else{
            return CGSize(width: 0, height: 0)
            
        }
    }
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        switch kind {
            
        case UICollectionView.elementKindSectionFooter:
            let footerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "Footer", for: indexPath)
            
            let btnAddMoreAsset = UIButton(frame: CGRect(x: 0, y: 5, width: 60, height: 60))
            btnAddMoreAsset.setImage(UIImage(named: "ic_add_black"), for: .normal)
            btnAddMoreAsset.addTarget(self, action: #selector(addMoreAssetAction), for: .touchUpInside)
            btnAddMoreAsset.backgroundColor = .lightGray
            footerView.addSubview(btnAddMoreAsset)
            btnAddMoreAsset.layer.cornerRadius = btnAddMoreAsset.frame.width/2
            return footerView
            
        default:
            assert(false, "Unexpected element kind")
        }
        return  UICollectionReusableView()
    }
    
    @objc func addMoreAssetAction(){
        
        self.playerMainView.player.pause()
        self.playerMainView.manualScrolling = false
        self.playPauseButton.setImage(#imageLiteral(resourceName: "ic_play"), for: .normal)
        
        let pickerVC = VideoEdPickerVC()
        pickerVC.isPresented = true
        pickerVC.alreadySelectedMediaCount = galleryAssetsResults.count
        pickerVC.addNewImages = { images in
            if images.count>0{
                images.forEach{ image2Add in
                    self.galleryAssetsResults.append(image2Add)
                }
            }
            self.listImagesCV.reloadData()
            self.playerMainView.manualScrolling = false
            self.playerMainView.imagesArray = self.galleryAssetsResults
            self.playerMainView.updateAssetPlayer()
        }
        self.present(pickerVC, animated: true, completion: nil)
        
    }
}
extension VideoEdSlideshowVC : RangeSeekSliderDelegate{
    func rangeSeekSlider(_ slider: RangeSeekSlider, didChange minValue: CGFloat, maxValue: CGFloat) {
        /*
        VideoEdVideoMakeManager.audioStartTime = minValue
        VideoEdVideoMakeManager.audioEndTime = maxValue
        
        if minValue>0{
            if self.avAudioPlayer != nil{
                
                self.avAudioPlayer.seek(to: CMTime(seconds: Double(minValue), preferredTimescale: 100))
            }
        }
        maxValueLabel.text = maxValue.convertSecondsToDurationString()
        minValueLabel.text = minValue.convertSecondsToDurationString()
        VideoEdVideoMakeManager.someAudioChanges  = true
        */
        
        maxValueLabel.text = slider.selectedMaxValue.convertSecondsToDurationString()
        minValueLabel.text = slider.selectedMinValue.convertSecondsToDurationString()

        
        debugPrint("rangeSeekSlider",minValue)
        debugPrint("rangeSeekSlider",maxValue)

    }
    func didEndTouches(in slider: RangeSeekSlider) {
        
        debugPrint("didEndTouches",slider.selectedMinValue)
        debugPrint("didEndTouches",slider.selectedMaxValue)
        
        VideoEdVideoMakeManager.audioStartTime = slider.selectedMinValue
        VideoEdVideoMakeManager.audioEndTime = slider.selectedMaxValue

        maxValueLabel.text = slider.selectedMaxValue.convertSecondsToDurationString()
        minValueLabel.text = slider.selectedMinValue.convertSecondsToDurationString()
        self.playerMainView.updateAssetPlayer()
        
        /*
         if VideoEdVideoMakeManager.currentMenuMode == .MUSIC && VideoEdVideoMakeManager.someAudioChanges{
         self.playerMainView.updateAssetPlayer()
         }
         
         VideoEdVideoMakeManager.someAudioChanges = false
         VideoEdVideoMakeManager.currentMenuMode = .IMAGESLIST
         self.view.bringSubviewToFront(imagesListView)
         self.view.bringSubviewToFront(bottomMenuList)
         
         
         if avAudioPlayer != nil && self.audioTimeObserver != nil {
         self.avAudioPlayer.removeTimeObserver(self.audioTimeObserver)
         }
         avAudioPlayer?.pause()
         avAudioPlayer = nil
         */
    }
}
extension VideoEdSlideshowVC : NVActivityIndicatorViewable{
    
}
extension VideoEdSlideshowVC : UIScrollViewDelegate{
    
}
@available(iOS 11.0, *)
extension VideoEdSlideshowVC : UICollectionViewDragDelegate,UICollectionViewDropDelegate{
    
    func scaleImage (image:UIImage, width: CGFloat) -> UIImage {
        let oldWidth = image.size.width
        let scaleFactor = width / oldWidth
        
        let newHeight = image.size.height * scaleFactor
        let newWidth = oldWidth * scaleFactor
        
        UIGraphicsBeginImageContext(CGSize(width:newWidth, height:newHeight))
        image.draw(in: CGRect(x:0, y:0, width:newWidth, height:newHeight))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage!
    }

    func collectionView(_ collectionView: UICollectionView, performDropWith
        coordinator: UICollectionViewDropCoordinator) {
        
        
        let destinationIndexPath = coordinator.destinationIndexPath ?? IndexPath(item: 0, section: 0)
        
        switch coordinator.proposal.operation {
        case .forbidden :
                debugPrint("forbidden")
            break
        case .copy:
            
            
            let items = coordinator.items
            
            for item in items {
                item.dragItem.itemProvider.loadObject(ofClass: UIImage.self,
                                                      completionHandler: {(newImage, error)  -> Void in
                                                        
                                                        if var image = newImage as? UIImage {
                                                            if image.size.width > 200 {
                                                                image = self.scaleImage(image: image, width: 200)
                                                            }
                                                            
                                                            self.galleryAssetsResults.insert(image, at: destinationIndexPath.item)
                                                            
                                                            DispatchQueue.main.async {
                                                                collectionView.insertItems(
                                                                    at: [destinationIndexPath])
                                                            }
                                                        }
                })
            }
            break
            
        case .move:
            
            let items = coordinator.items
            
            for item in items {
                
                guard let sourceIndexPath = item.sourceIndexPath
                    else { return }
                
                collectionView.performBatchUpdates({
                    
                    let moveImage = self.galleryAssetsResults[sourceIndexPath.item]
                    self.galleryAssetsResults.remove(at: sourceIndexPath.item)
                    self.galleryAssetsResults.insert(moveImage, at: destinationIndexPath.item)
                    
                    collectionView.deleteItems(at: [sourceIndexPath])
                    collectionView.insertItems(at: [destinationIndexPath])
                    self.playerMainView.imagesArray = self.galleryAssetsResults
                    self.playerMainView.updateAssetPlayer()
                    self.deleteImageBtn.isHidden = true
                    self.currentListImageSelectedIndex = -1
                })
                coordinator.drop(item.dragItem,
                                 toItemAt: destinationIndexPath)
            }
        default: return
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, itemsForBeginning
        session: UIDragSession, at indexPath: IndexPath) -> [UIDragItem] {
        
        let provider = NSItemProvider(object: self.galleryAssetsResults[indexPath.row] as NSItemProviderWriting)
        let dragItem = UIDragItem(itemProvider: provider)
        return [dragItem]
    }
    func collectionView(_ collectionView: UICollectionView, itemsForAddingTo
        session: UIDragSession, at indexPath: IndexPath, point: CGPoint) ->
        [UIDragItem] {
            
            let provider = NSItemProvider(object: galleryAssetsResults[indexPath.row] as NSItemProviderWriting)
            let dragItem = UIDragItem(itemProvider: provider)
            return [dragItem]
    }
    
    func collectionView(_ collectionView: UICollectionView, canHandle
        session: UIDropSession) -> Bool {
        
        return true

    }

    func collectionView(_ collectionView: UICollectionView, dropSessionDidUpdate
        session: UIDropSession, withDestinationIndexPath destinationIndexPath:
        IndexPath?) -> UICollectionViewDropProposal {

//        let views  = collectionView.visibleSupplementaryViews(ofKind: UICollectionView.elementKindSectionFooter)
////        headerView.backgroundColor = UIColor.blue
//        if views.count > 0{
//            
//            let dragframe = CGRect(origin: (session.localDragSession?.location(in: collectionView))!, size: CGSize(width: 5, height: 30))
//            debugPrint("headerView",views[0].frame)
//            debugPrint("dragframe",dragframe)
//
//            if views[0].frame.intersects(dragframe){
//                deleteImageBtn.isHidden = false
//            }else{
//                deleteImageBtn.isHidden = true
//
//            }
//        }

        if session.localDragSession != nil {
            return UICollectionViewDropProposal(operation: .move,
                                                intent: .insertAtDestinationIndexPath)
        } else {
            return UICollectionViewDropProposal(operation: .copy,
                                                intent: .insertAtDestinationIndexPath)
        }
    }
    /*
     func collectionView(_ collectionView: UICollectionView, dropSessionDidUpdate session: UIDropSession, withDestinationIndexPath destinationIndexPath: IndexPath?) -> UICollectionViewDropProposal
     {
     return  UICollectionViewDropProposal(operation: .move, intent: .unspecified)
     
     //        if collectionView === self.collectionView1
     //        {
     //            return collectionView.hasActiveDrag ?
     //                UICollectionViewDropProposal(operation: .move, intent: .insertAtDestinationIndexPath) :
     //                UICollectionViewDropProposal(operation: .forbidden)
     //        }
     //        else
     //        {
     //            if collectionView.hasActiveDrag{
     //                return UICollectionViewDropProposal(operation: .move, intent: .insertAtDestinationIndexPath)
     //            }
     //
     //
     //            for item in session.items
     //            {
     //                guard let identifier = item.localObject as? String else {
     //                    return UICollectionViewDropProposal(operation: .forbidden)
     //                }
     //                //not every cell is allowed to be deleted
     //                if Int(identifier)! % 3 == 0{
     //                    return UICollectionViewDropProposal(operation: .forbidden)
     //                }
     //            }
     //
     //            trashImage.backgroundColor = UIColor.red.withAlphaComponent(0.4)
     //            return  UICollectionViewDropProposal(operation: .move, intent: .unspecified)
     //        }
     }
    func collectionView(_ collectionView: UICollectionView, dragSessionIsRestrictedToDraggingApplication session: UIDragSession) -> Bool {
        debugPrint("dragSessionIsRestrictedToDraggingApplication",session.items)
        return true
    }
    func dragStateDidChange(_ dragState: UICollectionViewCell.DragState) {
        
        switch dragState {
            
        case .none:
            break
        //
        case .lifting:
            debugPrint("lifting")

            break
        //
        case .dragging:
            
            debugPrint("dragging")
            break
            //
        }
    }
    func reorderItems(coordinator: UICollectionViewDropCoordinator, destinationIndexPath: IndexPath, collectionView: UICollectionView)
    {
        debugPrint("UICollectionViewDropCoordinator")

    }
*/
    func  collectionView(_ collectionView: UICollectionView, dragSessionAllowsMoveOperation session: UIDragSession) -> Bool {
        
        debugPrint("dragSessionAllowsMoveOperation",session.items)
        return true
    }

}
