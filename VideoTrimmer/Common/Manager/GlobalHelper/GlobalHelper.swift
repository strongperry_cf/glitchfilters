//
//  GlobalHelper.swift
//  VideoTrimmer
//
//  Created by strongapps on 24/07/19.
//  Copyright © 2019 strongapps. All rights reserved.
//

import UIKit

class GlobalHelper: NSObject {

    static var shared = GlobalHelper()
    
    //MARK:  dispatch

    static func dispatchDelay(deadLine: DispatchTime , execute : @escaping () -> Void) {
        DispatchQueue.main.asyncAfter(deadline: deadLine, execute: execute)
    }
    static func dispatchMain(execute : @escaping () -> Void) {
        DispatchQueue.main.async(execute: execute)
    }
    static func dispatchBackground(execute : @escaping () -> Void) {
        DispatchQueue.global().async(execute: execute)
    }
    static func dispatchMainAfter(time : DispatchTime , execute :@escaping (() -> Void)) {
        DispatchQueue.main.asyncAfter(deadline: time, execute: execute)
    }
    
    //MARK: Cgsize , VIDEO
    func calculateVideoSizeinView(_ ViewFrame: CGRect, _ oldSize: CGSize) -> CGSize {
        
        var newwidth1: Float
        var newheight1: Float
        
        if oldSize.height >= oldSize.width {
            newheight1 = Float(ViewFrame.size.height)
            newwidth1 = Float((oldSize.width / oldSize.height) * CGFloat(newheight1))
            
            if CGFloat(newwidth1) > ViewFrame.size.width {
                let diff = Float(ViewFrame.size.width - CGFloat(newwidth1))
                newheight1 = newheight1 + diff / newheight1 * newheight1
                newwidth1 = Float(ViewFrame.size.width)
            }
        } else {
            newwidth1 = Float(ViewFrame.size.width)
            newheight1 = Float((oldSize.height / oldSize.width) * CGFloat(newwidth1))
            
            if CGFloat(newheight1) > ViewFrame.size.height {
                let diff = Float(ViewFrame.size.height - CGFloat(newheight1))
                newwidth1 = newwidth1 + diff / newwidth1 * newwidth1
                newheight1 = Float(ViewFrame.size.height)
            }
        }
        
        return CGSize(width: CGFloat(newwidth1), height: CGFloat(newheight1))
        
    }
    
    func greenLineFreeSize(_ normalSize: CGSize) -> CGSize {
        
        var altered_size_of_video: CGSize = normalSize
        var height = Int(altered_size_of_video.height)
        var width = Int(altered_size_of_video.width)
        while width % 4 > 0 {
            width = width + 1
        }
        while height % 4 > 0 {
            height = height + 1
        }
        altered_size_of_video.height = CGFloat(height)
        altered_size_of_video.width = CGFloat(width)
        
        return altered_size_of_video
        
    }
    //MARK: UIImage
    func calculateRectOfImage(isAspectFit: Bool = true,parentFrame: CGRect,childSize: CGSize?) -> CGRect {
        let imageViewSize = parentFrame.size
        let imgSize = childSize
        
        guard let imageSize = imgSize, imgSize != nil else {
            return CGRect.zero
        }
        
        let scaleWidth = imageViewSize.width / imageSize.width
        let scaleHeight = imageViewSize.height / imageSize.height
        var aspect : CGFloat = 0.0
        if isAspectFit{
            aspect = fmin(scaleWidth, scaleHeight)
        }else{
            aspect  = fmax(scaleWidth, scaleHeight)
        }
        
        
        var imageRect = CGRect(x: 0, y: 0, width: imageSize.width * aspect, height: imageSize.height * aspect)
        // Center image
        imageRect.origin.x = (imageViewSize.width - imageRect.size.width) / 2
        imageRect.origin.y = (imageViewSize.height - imageRect.size.height) / 2
        
        // Add imageView offset
        imageRect.origin.x += parentFrame.origin.x
        imageRect.origin.y += parentFrame.origin.y
        
        return imageRect
    }
    func frameForImage(image: UIImage, inImageViewAspectFit imageView: UIImageView) -> CGRect
    {
        let imageRatio: CGFloat = image.size.width / image.size.height
        let viewRatio: CGFloat = imageView.frame.size.width / imageView.frame.size.height
        if imageRatio < viewRatio {
            let scale: CGFloat = imageView.frame.size.height / image.size.height
            let width: CGFloat = scale * image.size.width
            let topLeftX: CGFloat = (imageView.frame.size.width - width) * 0.5
            let heightscale: CGFloat = imageView.frame.size.width / image.size.width
            let height: CGFloat = heightscale * image.size.height
            return CGRect(x: topLeftX,y: 0,width: width, height: height)
        }
        else {
            let scale: CGFloat = imageView.frame.size.width / image.size.width
            let height: CGFloat = scale * image.size.height
            let topLeftY: CGFloat = (imageView.frame.size.height - height) * 0.5
            return CGRect(x: 0,y: topLeftY ,width: imageView.frame.size.width,height: height)
        }
    }
    
    func createImageFrom(view : UIView) -> UIImage {
        //view.layer.borderWidth = 1.0
        view.layer.borderColor = view.backgroundColor?.cgColor
        view.superview?.backgroundColor = UIColor.clear
        UIGraphicsBeginImageContextWithOptions(view.frame.size, false, UIScreen.main.scale)
        view.layer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        view.layer.borderWidth = 0.0
        view.layer.borderColor = UIColor.clear.cgColor
        return image
    }
    func imageRotatedByDegrees(oldImage: UIImage, deg degrees: CGFloat) -> UIImage {
        //Calculate the size of the rotated view's containing box for our drawing space
        let rotatedViewBox: UIView = UIView(frame: CGRect(x: 0, y: 0, width: oldImage.size.width, height: oldImage.size.height))
        let t: CGAffineTransform = CGAffineTransform(rotationAngle: degrees * CGFloat.pi / 180)
        rotatedViewBox.transform = t
        let rotatedSize: CGSize = rotatedViewBox.frame.size
        //Create the bitmap context
        UIGraphicsBeginImageContext(rotatedSize)
        let bitmap: CGContext = UIGraphicsGetCurrentContext()!
        //Move the origin to the middle of the image so we will rotate and scale around the center.
        bitmap.translateBy(x: rotatedSize.width / 2, y: rotatedSize.height / 2)
        //Rotate the image context
        bitmap.rotate(by: (degrees * CGFloat.pi / 180))
        //Now, draw the rotated/scaled image into the context
        bitmap.scaleBy(x: 1.0, y: -1.0)
        bitmap.draw(oldImage.cgImage!, in: CGRect(x: -oldImage.size.width / 2, y: -oldImage.size.height / 2, width: oldImage.size.width, height: oldImage.size.height))
        let newImage: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return newImage
    }
    
    func blurEffect(_ inputImg:UIImage) -> UIImage {
        
        let currentFilter = CIFilter(name: "CIGaussianBlur")
        let beginImage = CIImage(image:inputImg)
        currentFilter!.setValue(beginImage, forKey: kCIInputImageKey)
        currentFilter!.setValue(10, forKey: kCIInputRadiusKey)
        
        let cropFilter = CIFilter(name: "CICrop")
        cropFilter!.setValue(currentFilter!.outputImage, forKey: kCIInputImageKey)
        cropFilter!.setValue(CIVector(cgRect: beginImage!.extent), forKey: "inputRectangle")
        
        let output = cropFilter!.outputImage
        let ciContext = CIContext()
        let cgimg = ciContext.createCGImage(output!, from: (output?.extent)!)
        
        return UIImage(cgImage: cgimg!)
    }
    //MARK:-
    
    func degreeToRadian(_ number: Double) -> Double {
        return number * .pi / 180
    }

    func height(for text: String?, font: UIFont?, maxWidth: CGFloat) -> CGFloat {
        if !(text != nil) || (text?.count ?? 0) == 0 {
            // no text means no height
            return 0
        }
        
        let options: NSStringDrawingOptions = [.usesLineFragmentOrigin, .usesFontLeading]
        var attributes: [NSAttributedString.Key : UIFont?]? = nil
        if let font = font {
            attributes = [
                NSAttributedString.Key.font: font
            ]
        }
        let size = text?.boundingRect(with: CGSize(width: maxWidth, height: CGFloat.greatestFiniteMagnitude), options: options, attributes: (attributes as! [NSAttributedString.Key : Any]), context: nil).size
        let height: CGFloat = CGFloat(ceilf(Float(size!.height)) + 1) // add 1 point as padding
        
        return height
    }

}
