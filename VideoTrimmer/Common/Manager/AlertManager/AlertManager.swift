//
//  AlertManager.swift
//  VideoTrimmer
//
//  Created by strongapps on 16/07/19.
//  Copyright © 2019 strongapps. All rights reserved.
//

import UIKit

class AlertManager: NSObject {

    static var shared = AlertManager()
    
    func alertWithMessage(message: String){
        alertWithTitle(title: "", message: message)
    }
    func alertWithTitle(title : String ,message: String){
        alertWithTitle(title: title, message: message, dismiss: "Dismiss")
    }
    func alertWithTitle(title : String ,message: String,dismiss : String){
        
        let alert = UIAlertController.init(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        let alertAction = UIAlertAction(title: dismiss, style: UIAlertAction.Style.default) { (action) in
            
        }
        alert.addAction(alertAction)

        if let topController = UIApplication.topViewController() {
            topController.present(alert, animated: true, completion: nil)
        }
        
    }
    func alertWithTitle(title : String ,message: String,dismiss : String , completionHandler: @escaping()->()){
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let primaryAction = UIAlertAction(title: title, style: .cancel) { (action) in
            alertController.dismiss(animated: true, completion: nil)
            completionHandler()
        }
        alertController.addAction(primaryAction)
        
        if let topController = UIApplication.topViewController() {
            topController.present(alertController, animated: true, completion: nil)
            
        }
    }
    func alertWithTitle(title : String , mesage : String,primaryActionText: String , dismissActionText : String ,  completionHandler: @escaping()->(), dismissBlock :@escaping() -> ()) {

        
        let alertController = UIAlertController(title: title, message: mesage, preferredStyle: .alert)
        
        let primaryAction = UIAlertAction(title: primaryActionText, style: .cancel) { (action) in

            alertController.dismiss(animated: true, completion: nil)

            completionHandler()
        }
        let dismissAction = UIAlertAction(title: dismissActionText, style: .default) { (action) in
            alertController.dismiss(animated: true, completion: nil)
            
            dismissBlock()
        }

        alertController.addAction(primaryAction)
        alertController.addAction(dismissAction)

        
        if let topController = UIApplication.topViewController() {
            topController.present(alertController, animated: true, completion: nil)

        }
    }
    


    func noInternetAlert(){
        let otherAlert = UIAlertController(title: "Internet Unavailable", message: "Internet connection seems offline.", preferredStyle: UIAlertController.Style.alert)
        let cancel = UIAlertAction(title: "ok", style: UIAlertAction.Style.default) { _ in
        }
        otherAlert.addAction(cancel)
        
        if let topController = UIApplication.topViewController() {
            if let popoverPresentationController = topController.popoverPresentationController {
                popoverPresentationController.sourceView = topController.view
                popoverPresentationController.sourceRect = CGRect(x: topController.view.bounds.midX, y: topController.view.bounds.midY, width: 0, height: 0)
                popoverPresentationController.permittedArrowDirections = []
            }
            topController.present(otherAlert, animated: true, completion: nil)
        }

        
        
    }

}

