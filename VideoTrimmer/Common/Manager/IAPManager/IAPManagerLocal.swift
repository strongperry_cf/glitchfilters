//
//  IAPManagerLocal.swift
//  VideoTrimmer
//
//  Created by strongapps on 16/07/19.
//  Copyright © 2019 strongapps. All rights reserved.
//

import UIKit

let SHARED_SECRET               = "199bb1c08b434777bed57e493e5a6a0d"
let WEEKLY_SUBSCRIPTION_ID      = "com.app.weekly"
let MONTHLY_SUBSCRIPTION_ID     = "com.app.monthly"
let YEARLY_SUBSCRIPTION_ID      = "com.app.yearly"

let ALL_PRODUCTS :Set<String> = [MONTHLY_SUBSCRIPTION_ID,YEARLY_SUBSCRIPTION_ID]



class IAPManagerLocal: NSObject {
    
    static var sharedInstance = IAPManagerLocal()
    var productsIAP : [SKProduct] = []

    func isInAppPurchased() -> Bool{
        
        let status = (UserDefaults.standard.bool(forKey: WEEKLY_SUBSCRIPTION_ID)||UserDefaults.standard.bool(forKey: MONTHLY_SUBSCRIPTION_ID)||UserDefaults.standard.bool(forKey: YEARLY_SUBSCRIPTION_ID))
        print("checkInAppStatus:-",status)
        return status
        
    }
    func openInapScreen()  {
        
        if IAPManagerLocal.sharedInstance.productsIAP.count < 1 {
            AlertManager.shared.alertWithMessage(message: "Products not loaded yet ,Please try again")
            IAPManagerLocal.sharedInstance.loadProducts()
            return
        }
        
//        let subs  = SubscriptionVC()
//
//        if let topController = UIApplication.topViewController() {
//            topController.present(subs, animated: true, completion: nil)
//        }

    }
    func openInapScreen(completion: @escaping (Bool) -> Void)  {
        
        if IAPManagerLocal.sharedInstance.productsIAP.count < 1 {
            AlertManager.shared.alertWithMessage(message: "Products not loaded yet ,Please try again")
            IAPManagerLocal.sharedInstance.loadProducts()
            return
        }
        
//        let subs  = SubscriptionVC()
//        subs.inappStatusHandler = { status in
//
//            completion(status)
//        }
//        if let topController = UIApplication.topViewController() {
//            topController.present(subs, animated: true, completion: nil)
//        }
        
    }
    func loadProducts(){
        
        
        SwiftyStoreKit.retrieveProductsInfo(ALL_PRODUCTS) { result in

            self.productsIAP.removeAll()
            result.retrievedProducts.forEach{ product in
                debugPrint(product.localizedDescription)
                debugPrint(product.productIdentifier)
                debugPrint(product.localizedPrice as Any)

                self.productsIAP.append(product)
            }

            if let product = result.retrievedProducts.first {

                let priceString = product.localizedPrice!
                print("Product: \(product.localizedDescription), price: \(priceString)")
            }
            else if let invalidProductId = result.invalidProductIDs.first {
                print("Invalid product identifier: \(invalidProductId)")
            }
            else {
                print("Error: \(String(describing: result.error) )")
            }
        }
//
        
    }
    func purchaseProduct(productID : String, block : @escaping (Bool) -> Void){
        

        
        let actdata : ActivityData = ActivityData(size: CGSize(width: 50, height: 50) , type : .circleStrokeSpin)
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(actdata, nil)

        SwiftyStoreKit.purchaseProduct(productID, atomically: true) { result in
        
            DispatchQueue.main.async {
                print("SwiftyStoreKit.purchaseProduct response")
                
                
                    let appleValidator = AppleReceiptValidator(service: .production, sharedSecret: SHARED_SECRET)
                    SwiftyStoreKit.verifyReceipt(using: appleValidator) { result in
                        

                        if case .success(let receipt) = result {
                            let purchaseResult = SwiftyStoreKit.verifySubscription(
                                ofType: .autoRenewable,
                                productId: productID,
                                inReceipt: receipt)
                            
                            switch purchaseResult {
                            case .purchased(let expiryDate, let items):
                                print("Product is valid until \(expiryDate.localTime())")
                                
                                block(true)
                                // Store
                                UserDefaults.standard.set(expiryDate.localTime(), forKey: "expireDateForIAP")
                                
                                
                                for item in items {
                                    let purchasedProductID = item.productId
                                    UserDefaults.standard.set(true, forKey: purchasedProductID)
//                                    print("purchasedProductID \(purchasedProductID)")
                                    
                                }
                                NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "HandlePurchaseNotification"), object: nil)
//                                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5 , execute: {
//                                    AlertManager.shared.alertWithMessage(message: "Product purchase successfully")
//
//                                })
                                
                            case .expired(let expiryDate, let _):
                                //                            self.handleExpiredItmes(items: items, expiry: expiryDate)
                                print("Product is valid until \(expiryDate)")
                                NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
//                                AlertManager.shared.alertWithMessage(message: "Your subscription has been expired")
                                
                            case .notPurchased:
                                self.handleNotPurchased()
                            }
                            
                        } else {
                            // receipt verification error
                        }
                    }
                }
                
//            }
        
            
            
        }
        
    }
    /*
     
     func purchaseProduct(productID : String){
     
     
     //        if UserDefaults.standard.bool(forKey: productID){
     //            DispatchQueue.main.async {
     //                AlertManager.shared.alertWithMessage(message: "Already purchased")
     //            }
     //            return
     //        }
     
     let actdata : ActivityData = ActivityData(size: CGSize(width: 50, height: 50) , type : .circleStrokeSpin)
     NVActivityIndicatorPresenter.sharedInstance.startAnimating(actdata, nil)
     
     SwiftyStoreKit.purchaseProduct(productID, atomically: true) { result in
     
     DispatchQueue.main.async {
     print("SwiftyStoreKit.purchaseProduct response")
     
     if case .success(let purchase) = result{
     // Deliver content from server, then:
     if purchase.needsFinishTransaction {
     SwiftyStoreKit.finishTransaction(purchase.transaction)
     }
     
     let appleValidator = AppleReceiptValidator(service: .production, sharedSecret: SHARED_SECRET)
     SwiftyStoreKit.verifyReceipt(using: appleValidator) { result in
     
     if case .success(let receipt) = result {
     let purchaseResult = SwiftyStoreKit.verifySubscription(
     ofType: .autoRenewable,
     productId: productID,
     inReceipt: receipt)
     
     switch purchaseResult {
     case .purchased(let expiryDate, let items):
     print("Product is valid until \(expiryDate.localTime())")
     
     
     // Store
     UserDefaults.standard.set(expiryDate.localTime(), forKey: "expireDateForIAP")
     
     
     for item in items {
     let purchasedProductID = item.productId
     UserDefaults.standard.set(true, forKey: purchasedProductID)
     print("purchasedProductID \(purchasedProductID)")
     
     }
     NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
     NotificationCenter.default.post(name: NSNotification.Name(rawValue: "HandlePurchaseNotification"), object: nil)
     DispatchQueue.main.asyncAfter(deadline: .now() + 0.5 , execute: {
     AlertManager.shared.alertWithMessage(message: "Purchase Successfull")
     
     })
     
     case .expired(let expiryDate, let items):
     //                            self.handleExpiredItmes(items: items, expiry: expiryDate)
     print("Product is valid until \(expiryDate)")
     NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
     AlertManager.shared.alertWithMessage(message: "Purchase expired")
     
     case .notPurchased:
     self.handleNotPurchased()
     }
     
     } else {
     // receipt verification error
     }
     }
     } else {
     debugPrint(result)
     // purchase error
     self.handleNotPurchased()
     
     
     }
     }
     
     
     
     }
     
     }
     */
    func restoreIAP(){
        
        let actdata : ActivityData = ActivityData(size: CGSize(width: 50, height: 50) , type : .circleStrokeSpin)
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(actdata, nil)
        
        SwiftyStoreKit.restorePurchases(atomically: true) { results in
            DispatchQueue.main.async {
                
                if results.restoreFailedPurchases.count > 0 {
                    print("Restore Failed: \(results.restoreFailedPurchases)")
                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                    AlertManager.shared.alertWithMessage(message: "Restore Failed")
                    
                }
                else if results.restoredPurchases.count > 0 {
                    self.handlePurchases(purchases: results.restoredPurchases)
                    
                }
                else {
                    print("Nothing to Restore")
                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                    
                    AlertManager.shared.alertWithMessage(message: "Nothing to Restore")
                    
                }
            }
            
        }
        
    }
    fileprivate func handlePurchases(purchases : [Purchase]) {

            verifyPurchase()

    }
    
    func verifyReceipt(completion: @escaping (VerifyReceiptResult) -> Void) {
        let appleValidator = AppleReceiptValidator(service: .production, sharedSecret: SHARED_SECRET)
        SwiftyStoreKit.verifyReceipt(using: appleValidator, completion: completion)
    }

    func verifyPurchase() {
//        verifyReceipt { [weak self] result in
//            switch result {
//            case .success(let receipt):
//                let purchaseResult = SwiftyStoreKit.verifySubscriptions(productIds: ALL_PRODUCTS, inReceipt: receipt)
//                switch purchaseResult {
//                case .purchased(let expiryDate, let items):
//                    print("expiryDate: \(expiryDate.localTime())")
//
//                    self?.handlePurchasedItems(items: items, expiry: expiryDate)
//                case .expired(let expiryDate, let items):
//                    self?.handleExpiredItmes(items: items, expiry: expiryDate)
//                case .notPurchased:
//                    self?.handleNotPurchased()
//                }
//            case .error(let error):
//                print("Receipt verification failed: \(error)")
//                //                self?.refreshRecipt()
//            }
//        }
    }
    func verifyPurchaseSilently() {
        
//        verifyReceipt { [weak self] result in
//            switch result {
//            case .success(let receipt):
//                let purchaseResult = SwiftyStoreKit.verifySubscriptions(productIds: ALL_PRODUCTS, inReceipt: receipt)
//                switch purchaseResult {
//                case .purchased(let expiryDate, let items):
//
//                    print("local \(expiryDate.localTime())")
//
//                    for item in items {
//                        let purchasedProductID = item.productId
//                        UserDefaults.standard.set(true, forKey: purchasedProductID)
//                    }
//                case .expired(let expiryDate, let items):
//                    print("Product is expiry Date \(expiryDate.localTime())")
//
//                    for item in items {
//                        let purchasedProductID = item.productId
//
//                        let products = ALL_PRODUCTS.filter({$0 == purchasedProductID})
//                        for product in products {
//                            print("\(product) has been expired")
//                            UserDefaults.standard.set(false, forKey: product)
//
//                        }
//
//                    }
//                case .notPurchased:
//                    print("notPurchased")
//
//                }
//            case .error(let error):
//                print("Receipt verification failed: \(error)")
//                //                self?.refreshRecipt()
//            }
//        }
    }

    //MARK:- handle purchased items
    private func handlePurchasedItems(items:[ReceiptItem],expiry: Date) {
        for item in items {
            let purchasedProductID = item.productId
            UserDefaults.standard.set(true, forKey: purchasedProductID)
        }
        NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)

        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5 , execute: {
            
//            AlertManager.shared.alertWithMessage(message: "Restore Successfull")
            
            AlertManager.shared.alertWithTitle(title: "", message: "Restore Successfull", dismiss: "Done", completionHandler: {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "HandlePurchaseNotification"), object: nil)

            })


        })
    }
    
    //MAKR:- HANDLE EXPIRED ITEMS
    private func handleExpiredItmes(items:[ReceiptItem],expiry: Date) {
        for item in items {
            let purchasedProductID = item.productId

            let products = ALL_PRODUCTS.filter({$0 == purchasedProductID})
            for product in products {
//                print("\(product) has been expired")
                UserDefaults.standard.set(false, forKey: product)

            }
            
        }
        NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)

        AlertManager.shared.alertWithMessage(message: "Your subscription has been expired, Please renew it")
    }
    //MARK:- HANDLE NOT PURCHASED
    private func handleNotPurchased() {
        NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
      
//        AlertManager.shared.alertWithMessage(message: "Not Purchased")

    }

    func  checkExpiry(){
        
        let appleValidator = AppleReceiptValidator(service: .production, sharedSecret: SHARED_SECRET)
        SwiftyStoreKit.verifyReceipt(using: appleValidator) { result in
            switch result {
            case .success(let receipt):
                let productId = "com.musevisions.SwiftyStoreKit.Subscription"
                // Verify the purchase of a Subscription
                let purchaseResult = SwiftyStoreKit.verifySubscription(
                    ofType: .autoRenewable, // or .nonRenewing (see below)
                    productId: productId,
                    inReceipt: receipt)
                
                switch purchaseResult {
                case .purchased(let expiryDate, let items):
                    print("\(productId) is valid until \(expiryDate)\n\(items)\n")
                case .expired(let expiryDate, let items):
                    print("\(productId) is expired since \(expiryDate)\n\(items)\n")
                case .notPurchased:
                    print("The user has never purchased \(productId)")
                }
                
            case .error(let error):
                print("Receipt verification failed: \(error)")
            }
        }
        
    }
}
extension Date{
    
    func localTime() -> String{
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy hh:mm:ss"
        dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone?
        let date = self
        dateFormatter.timeZone = NSTimeZone.local
        let timeStamp = dateFormatter.string(from: date)
        return timeStamp
        
    }
}

