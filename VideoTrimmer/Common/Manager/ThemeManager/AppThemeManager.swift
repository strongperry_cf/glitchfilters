//  AppThemeManager.swift
//  VideoTrimmer
//  Created by strongapps on 18/04/19.
//  Copyright © 2019 strongapps. All rights reserved.

import UIKit

struct AppThemeModel {
    
    var VCBackGroundColor   :   UIColor
    var TextColor           :   UIColor
    var TintColor           :   UIColor
    var SelectedStrokeColor :   UIColor
    var UnSelStrokeColor    :   UIColor
}

class AppThemeManager: NSObject {

    static let shared = AppThemeManager()
    
    public var blackTheme = AppThemeModel(VCBackGroundColor: UIColor.black,
                                          TextColor: UIColor.white,
                                          TintColor: UIColor.orange,
                                          SelectedStrokeColor: .orange,
                                          UnSelStrokeColor: .white)
    
    public var whiteTheme = AppThemeModel(VCBackGroundColor: UIColor.white,
                                          TextColor: UIColor.black,
                                          TintColor: UIColor.red,
                                          SelectedStrokeColor: .orange,
                                          UnSelStrokeColor: .white )
    
    public var testTheme = AppThemeModel(VCBackGroundColor: UIColor.black,
                                         TextColor: UIColor.white,
                                         TintColor: UIColor.red,
                                         SelectedStrokeColor: .orange,
                                         UnSelStrokeColor: .white)

    
    public func currentTheme() -> AppThemeModel {

        let selectedTheme : String = UserDefaults.standard.value(forKey: "SelectedAppTheme") as? String ?? "black"
        if selectedTheme == "white"{
            
            return whiteTheme
        }else{
            return blackTheme

        }

        
    }
}
