//
//  VideoEdTrimVC.swift
//  VideoTrimmer
//
//  Created by strongapps on 03/04/19.
//  Copyright © 2019 strongapps. All rights reserved.
//

import UIKit
import AVFoundation

struct VideoCompositionModelStruct {
    var mixCompStruct: AVMutableComposition!
    var mixVideoCompStruct: AVMutableVideoComposition!
    var videoTimeRangeStruct: CMTimeRange!
    var videoAssetStruct: AVAsset!
    var videoRotationAngleStruct: CGFloat = 0.0
}

struct VideoAssetURLStruct {
    var videoAssetURL : AVURLAsset!
    var videoStartTime = 0.0
    var videoEndTime = 0.0
}

class VideoEdTrimVC: UIViewController {
    
    //MARK:- Outlets
    @IBOutlet var viewMainPlayerView: UIView!
    @IBOutlet weak var btnTrimDone: UIButton!
    @IBOutlet var trimRangeSlider: UIView!
    
    //MARK:- Variables
    var trimAvplayer : AVPlayer?
    var playerItem: AVPlayerItem?
    var playerLayer: AVPlayerLayer?
    var videoAssetFromUrl: AVAsset!
    var videoPHAsset: PHAsset?
    
    var isVideoPlaying = true
    var trimSliderTimeLine: VideoRangeSlider?
    var maxVideoDuration = 60.0
    
    var minGapVideoDuration = 2.0
    
    var videoStartTime: CGFloat = 0.0
    var videoEndTime : CGFloat = 0.0
    var videoTimeObserver: Any?
    var videoCurrentTimeScale : CMTimeScale = 100
    
    var BOTTOM_VIEWS_HEIGHT : CGFloat = 232.0
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    //MARK: LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setAVPlayer()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        themeAppearanceSetup()
        
    }
    func themeAppearanceSetup(){
        self.view.backgroundColor =  AppThemeManager().currentTheme().VCBackGroundColor
    }
    deinit {
        print("VideoEdTrimVC dealloc")
    }
    
    //MARK:- Actions
    @IBAction func ActionBackBtn(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    @IBAction func ActionNextBtn(_ sender: Any) {
        getVideoComposition()
    }
    @IBAction func ActionPlayPauseVideo(_ sender: Any) {
        
    }
    
    
    //MARK: - Methods
    //Initial Stage
    func setAVPlayer(){
        
        playerItem = AVPlayerItem.init(asset: videoAssetFromUrl)
        //        videoAssetFromUrl = AVAsset.init(url: url)
//        videoEndTime = videoAssetFromUrl.duration.durationInSeconds
        trimAvplayer  = AVPlayer(playerItem: playerItem!)
        playerLayer = AVPlayerLayer(player: trimAvplayer!)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            self.playerLayer?.frame = self.viewMainPlayerView.bounds
            self.viewMainPlayerView.layer.addSublayer(self.playerLayer!)
            do {
                try AVAudioSession.sharedInstance().setCategory(.playback, mode: .default, options: [])
            }
            catch {
                print("Setting category to AVAudioSessionCategoryPlayback failed.")
            }
            self.trimAvplayer?.volume = 1.0
            self.trimAvplayer?.play()
            self.addPeriodicTimeObserver()
            self.initTrimVideoView()
        }
    }
}

extension VideoEdTrimVC : VideoRangeSliderDelegate {
    func videoRange(_ videoRange: VideoRangeSlider, didChangeLeftPosition leftPosition: CGFloat) {    }
    func videoRange(_ videoRange: VideoRangeSlider, didChangeRightPosition rightPosition: CGFloat) {    }
    
    func videoRange(_ videoRange: VideoRangeSlider, didGestureStateEndedLeftPosition leftPosition: CGFloat, rightPosition: CGFloat) {
        videoStartTime = leftPosition
        videoEndTime  = rightPosition - 0.05
        print(videoStartTime, "<<>>>", videoEndTime)
        let time = CMTime(value: CMTimeValue(videoStartTime), timescale: CMTimeScale(1.0))
        trimAvplayer?.seek(to: time)
        trimAvplayer?.play()
        
    }
    func videoRange(didBeginMoving videoRange: VideoRangeSlider) {  }
    func videoRange(_ videoRange: VideoRangeSlider, didMovePlayerSlider value: CGFloat) {    }
    func videoRange(_ videoRange: VideoRangeSlider, leftPosition: CGFloat, rightPosition: CGFloat) {    }
    
    
    func initTrimVideoView(){
        
        trimRangeSlider.frame.size.width = self.view.frame.width
        if let asset = self.videoAssetFromUrl{
            
            self.trimSliderTimeLine = VideoRangeSlider(frame: CGRect(x: self.trimRangeSlider.frame.origin.x+10, y: 0, width: self.trimRangeSlider.frame.width-20, height: 60), avAsset: asset)
            // trimSliderTimeLine?.minGap = 5.0 // CGFloat(minGapVideoDuration)
            // trimSliderTimeLine?.maxGap = 100.0
            trimSliderTimeLine?.minSeconds = 2.0
            trimSliderTimeLine?.delegate = self
            self.trimRangeSlider.addSubview(self.trimSliderTimeLine!)
            self.trimSliderTimeLine?.bubleText.isHidden = true
            self.trimSliderTimeLine?.setPopoverBubbleSize(0, height: 0)
        }
    }
    
    func addPeriodicTimeObserver() {
        // Notify every half second
        let timeScale = CMTimeScale(NSEC_PER_SEC)
        let time = CMTime(seconds: 0.01, preferredTimescale: timeScale)
        videoTimeObserver = trimAvplayer?.addPeriodicTimeObserver(forInterval: time, queue: .main) { [weak self] time in
            
            if let player = self?.trimAvplayer {
                let currentTime = String(format: "%.2f", player.currentTime().seconds) // player.currentTime()
                let enTime = String(format: "%.2f",self!.videoEndTime)
                if currentTime ==  enTime {
                    self?.trimAvplayer?.pause()
                }
            }
        }
    }
    
    func removePeriodicTimeObserver() {
        if let timeObserverToken = videoTimeObserver {
            trimAvplayer?.removeTimeObserver(timeObserverToken)
            self.videoTimeObserver = nil
        }
    }
    
    
    func getVideoComposition(){
        
        let assetTemp:AVURLAsset  = videoAssetFromUrl as! AVURLAsset
        videoCurrentTimeScale = videoAssetFromUrl.duration.timescale
        self.trimAvplayer?.pause()

        if VideoEdVideoMakeManager.currentEditorMode == .REACTCAMERA{
            
//            let videoData = VideoData(url: assetTemp.url)
//            let startTime:CMTime = CMTime(seconds: Double(videoStartTime), preferredTimescale: 100)
//            let endTime  :CMTime = CMTime(seconds: Double(videoEndTime), preferredTimescale: 100)
//            videoData.trimStartTime =  startTime
//            videoData.trimEndTime = endTime
//            let pickerVC = ReactDashboardVC(nibName: "ReactDashboardVC", bundle: nil)
//            pickerVC.videodata = videoData
//            self.navigationController?.pushViewController(pickerVC, animated: true)
            
            return
        }
        
        
        
        let storyboard : UIStoryboard = UIStoryboard(name: "VideoEdAlbum", bundle: nil)
        let vc : VideoEdEditVC = storyboard.instantiateViewController(withIdentifier: "VideoEdEditVC") as! VideoEdEditVC
        // vc.composition = composition
        vc.videoStartTime = videoStartTime
        vc.videoEndTime = videoEndTime
        //        videoAssetFromUrl
        vc.singleVideoUrl = assetTemp.url
        vc.singleAvasset = assetTemp
        vc.singlePHAsset = videoPHAsset
        self.navigationController?.pushViewController(vc, animated: true)
        

    }
    
}
extension UIView {
    func applyGradient(colours: [UIColor]) -> Void {
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.frame = self.bounds
        gradient.colors = colours.map { $0.cgColor }
        gradient.startPoint = CGPoint(x: 0.5, y: 0.5)
        gradient.endPoint = CGPoint(x: 1.2, y: 0.5)
        self.layer.insertSublayer(gradient, at: 0)
    }
}
