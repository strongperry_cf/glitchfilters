//
//  VideoEdPickerCollectionViewCell.swift
//  VideoTrimmer
//
//  Created by strongapps on 03/04/19.
//  Copyright © 2019 strongapps. All rights reserved.
//

import UIKit

class VideoEdPickerCollectionViewCell: UICollectionViewCell {

    @IBOutlet var thumbImage: UIImageView!
    @IBOutlet var durationLabel: UILabel!
    @IBOutlet var selectedView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    @IBOutlet weak var countLabel: UILabel!
    
}
