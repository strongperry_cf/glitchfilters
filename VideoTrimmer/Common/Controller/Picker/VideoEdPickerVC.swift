//
//  VideoEdPickerVC.swift
//  VideoTrimmer
//
//  Created by strongapps on 03/04/19.
//  Copyright © 2019 strongapps. All rights reserved.


import UIKit

public protocol PickerVCDelegate: class {
    func pickerVCSelectVideos(_ phassetArray:[PHAsset])
}

class VideoEdPickerVC: UIViewController {

    @IBOutlet weak var countLabel: UILabel!
    public weak var delegate: PickerVCDelegate?
    @IBOutlet var closeBtn: UIButton!
    @IBOutlet var pickerCV: UICollectionView!
    
    @IBOutlet weak var mediaTypeLabel: UILabel!
    @IBOutlet var doneButtonBottomCons: NSLayoutConstraint!
    var galleryAssetsResults = PHFetchResult<PHAsset>()
    var selectedAssets : [PHAsset] = []
    var selectedImages : [UIImage] = []
    var arrayForSelectedIndexPaths : [IndexPath] = []
    var mediaTypePrefferred : PHAssetMediaType = .image
    var isPresented : Bool = false
    var alreadySelectedMediaCount : Int = 0
    var alreadySelectedMediaSeconds : Int = 0

    var addNewImages : (([UIImage])->())?

    override func viewDidLoad() {
        super.viewDidLoad()

        let nib = UINib(nibName: "VideoEdPickerCollectionViewCell", bundle: nil)
        self.pickerCV.register(nib, forCellWithReuseIdentifier: "VideoEdPickerCollectionViewCell")
        self.pickerCV.delegate = self
        self.pickerCV.dataSource = self
        checkAuthorizationForPhotoLibrary()
        
        mediaTypeLabel.font = UIFont(name: FONT_BOLD, size: 20)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if mediaTypePrefferred == .image{
           mediaTypeLabel.text = "Images - "
        }else{
           mediaTypeLabel.text = "Videos"
        }
        themeAppearanceSetup()
        PHPhotoLibrary.shared().register(self)


    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        PHPhotoLibrary.shared().unregisterChangeObserver(self)

    }
    func themeAppearanceSetup(){
        self.view.backgroundColor =  AppThemeManager().currentTheme().VCBackGroundColor
    }
    
    func checkAuthorizationForPhotoLibrary(){
        
        let status = PHPhotoLibrary.authorizationStatus()
        if (status == PHAuthorizationStatus.authorized) {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
                self.loadAlbums()
            }) 
        }else if(status == PHAuthorizationStatus.notDetermined) {
            PHPhotoLibrary.requestAuthorization({ (newStatus) in
                if (newStatus == PHAuthorizationStatus.authorized) {
                  
                    DispatchQueue.main.async {
                        // Access has been granted.
                        self.loadAlbums()
                    }
                }else{
                    
                }
            })
        }else if(status == PHAuthorizationStatus.denied) {

            AlertManager.shared.alertWithTitle(title: " ", message: "Permission is denied. Please Go to Settings and enable permission.")

        }
    }

    func loadAlbums() {
        

        let fetchOptions = PHFetchOptions()
        fetchOptions.sortDescriptors = [NSSortDescriptor(key: "creationDate",ascending: false)]
        if mediaTypePrefferred != .unknown{
//            fetchOptions.predicate = NSPredicate(format: "mediaType = %d" ,  mediaTypePrefferred.rawValue)
            fetchOptions.predicate = NSPredicate(format: "mediaType = %d AND !((mediaSubtype & %d) == %d)",mediaTypePrefferred.rawValue, PHAssetMediaSubtype.videoHighFrameRate.rawValue , PHAssetMediaSubtype.videoHighFrameRate.rawValue)
            //            fetchOptions.predicate = NSPredicate(format: "mediaType = %d AND !((mediaSubtype & %d) == %d) AND sourceType = %d",mediaTypePrefferred.rawValue, PHAssetMediaSubtype.videoHighFrameRate.rawValue , PHAssetMediaSubtype.videoHighFrameRate.rawValue,PHAssetSourceType.typeCloudShared.rawValue)

        }
        fetchOptions.includeAssetSourceTypes = [.typeUserLibrary]
        galleryAssetsResults = PHAsset.fetchAssets(with: fetchOptions)
        /*
        let one : PHAsset = galleryAssetsResults[0]
//        one.mediaSubtypes =
        var results = NSMutableArray()
        if true{
            var assets = PHAsset.fetchAssets(with: PHAssetMediaType.video, options: fetchOptions)
            assets.enumerateObjects { (obj, idx, bool) -> Void in
                results.add(obj)
            }
            fetchOptions.predicate = NSPredicate(format: "sourceType != %d", PHAssetSourceType.typeCloudShared.rawValue)
/*
             let cameraRollAssets = results.filtered(using: NSPredicate(format: "mediaSubType = %d", PHAssetMediaSubtype.videoTimelapse.rawValue))

             */
            let cameraRollAssets = results.filtered(using: NSPredicate(format: "mediaSubType = %d", PHAssetMediaSubtype.videoTimelapse.rawValue))
            results = NSMutableArray(array: cameraRollAssets)
            
        }
        
        debugPrint("results count",results.count)*/
        debugPrint("galleryAssetsResults count",galleryAssetsResults.count)
        DispatchQueue.main.async {
            
            self.pickerCV.reloadData()
        }
    }
    
    @IBAction func closeBtnAct(_ sender: UIButton) {
        
  
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let pushControllers =  appDelegate.window?.rootViewController?.children
        
        let vc = pushControllers?.last
        if (vc?.isKind(of: VideoEdEditVC.self))!        {
            delegate?.pickerVCSelectVideos(selectedAssets)
            self.dismiss(animated: true, completion: nil)
            return;
        }
        
        //-*-**-
        if isPresented{
            self.dismiss(animated: true, completion: nil)
            return
        }
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func doneBtnAct(_ sender: UIButton) {
        
        if mediaTypePrefferred == .video {
            
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
//            let presentedControllers = appDelegate.window?.rootViewController?.presentedViewController
            let pushControllers =  appDelegate.window?.rootViewController?.children
            
            let vc = pushControllers?.last
            if (vc?.isKind(of: VideoEdEditVC.self))!        {
                delegate?.pickerVCSelectVideos(selectedAssets)
                self.dismiss(animated: true, completion: nil)
                return;
            }

        }
        
        
        if galleryAssetsResults.count > 0 {
            
            if mediaTypePrefferred == .image{
                
                
                if selectedImages.count > 0 {
                
                    if isPresented{
                        if selectedImages != nil{
                            
                            self.addNewImages!(selectedImages)
                        }
                        self.dismiss(animated: true, completion: nil)
 
                    }else{
                        
//                        let slideVC = VideoEdSlideshowVC(nibName: "VideoEdSlideshowVC", bundle: nil)
//                        slideVC.galleryAssetsResults = selectedImages
//                        self.navigationController?.pushViewController(slideVC, animated: true)
                    }
                }
                
            }else if mediaTypePrefferred == .video {
                
                if selectedAssets.count == 1 {
                    let options: PHVideoRequestOptions = PHVideoRequestOptions()
                    options.version = .current
//                    options.deliveryMode = .fastFormat
                    options.isNetworkAccessAllowed = true
                    options.progressHandler = { (progress, err, stop,dic) in
                        print("progress for image", progress)
                        
                        //            let percent : Int = (Int((exporter?.progress)!*100))
                        let str = String(format: "%d%%", Int(progress * 100))
                        NVActivityIndicatorPresenter.sharedInstance.setMessage(str)
                        
                    }


                    
                    PHImageManager.default().requestAVAsset(forVideo: selectedAssets[0], options: options, resultHandler: {(asset: AVAsset?, audioMix: AVAudioMix?, info: [AnyHashable : Any]?) -> Void in
                        DispatchQueue.main.async {
                            debugPrint("asset class",asset?.className)
                            
                            if asset is AVComposition{
                                

                                
                                self.ConvertAvcompositionToAvasset(avComp: asset as! AVComposition, completion: { (assetNew) in
                                
                                    if let urlAsset : AVAsset = assetNew {
                                        let editVC = VideoEdTrimVC(nibName: "VideoEdTrimVC", bundle: nil)
                                        editVC.videoAssetFromUrl = urlAsset
                                        editVC.videoPHAsset = self.selectedAssets[0]
                                        self.navigationController?.pushViewController(editVC, animated: true)
                                    }else{
                                        AlertManager.shared.alertWithTitle(title: "", message: "Unable to get the video.")

                                        
                                    }
                                })
                                
                            }else{
                                if let urlAsset = asset {
                                    let editVC = VideoEdTrimVC(nibName: "VideoEdTrimVC", bundle: nil)
                                    editVC.videoAssetFromUrl = urlAsset
                                    editVC.videoPHAsset = self.selectedAssets[0]
                                    self.navigationController?.pushViewController(editVC, animated: true)
                                }else{
                                    AlertManager.shared.alertWithTitle(title: "", message: "Unable to get the video.")

                                }

                            }
                            
                        }
                    })
                }else{
                    let storyboard : UIStoryboard = UIStoryboard(name: "VideoEdAlbum", bundle: nil)
                    let vc : VideoEdEditVC = storyboard.instantiateViewController(withIdentifier: "VideoEdEditVC") as! VideoEdEditVC
                    vc.selectedAssets = selectedAssets
                    self.navigationController?.pushViewController(vc, animated: true)
                    
                }
            }
        }
    }
    
    deinit {
        debugPrint("picker dealloc")
    }
    func ConvertAvcompositionToAvasset(avComp: AVComposition, completion:@escaping (_ avasset: AVAsset) -> Void){
        
        self.startAnimating( type: NVActivityIndicatorType.circleStrokeSpin)
        
        let exporter = AVAssetExportSession(asset: avComp, presetName: AVAssetExportPreset1280x720)
        let randNum:Int = Int(arc4random())
        //Generating Export Path
        let exportPath: NSString = NSTemporaryDirectory().appendingFormat("\(randNum)"+"video.mov") as NSString
        let exportUrl: NSURL = NSURL.fileURL(withPath: exportPath as String) as NSURL
        //SettingUp Export Path as URL
        exporter?.outputURL = exportUrl as URL
        exporter?.outputFileType = AVFileType.mov
        exporter?.shouldOptimizeForNetworkUse = true
        
        var timerForExportStatus : Timer?
        if #available(iOS 10.0, *) {
            timerForExportStatus = Timer.scheduledTimer(withTimeInterval: 0.1, repeats: true) { timer in
                let percent : Int = (Int((exporter?.progress)!*100))
                let str = String(format: "%d%%",percent)
                NVActivityIndicatorPresenter.sharedInstance.setMessage(str)
            }
            
        }
        exporter?.exportAsynchronously(completionHandler: {() -> Void in
            DispatchQueue.main.async(execute: {() -> Void in
                
                timerForExportStatus?.invalidate()
                timerForExportStatus = nil

                self.stopAnimating()

                if exporter?.status == .completed {
                    let URL: URL? = exporter?.outputURL
                    let Avasset:AVAsset = AVAsset(url: URL!)
                    completion(Avasset)
                }
                else if exporter?.status == .failed{
                    print("Failed")
                    
                }
            })
        }) }

}

extension VideoEdPickerVC : UICollectionViewDelegate, UICollectionViewDataSource ,UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return galleryAssetsResults.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "VideoEdPickerCollectionViewCell", for: indexPath) as! VideoEdPickerCollectionViewCell
        let asset = galleryAssetsResults.object(at: indexPath.row)
        
        DispatchQueue.main.async {
            PHImageManager.default().requestImage(for: asset, targetSize: cell.frame.size, contentMode: PHImageContentMode.aspectFill, options: nil) { (image, userInfo) -> Void in
                DispatchQueue.main.async {
                    if let safeImage = image{
                        cell.thumbImage.image = safeImage
                    }
                }
            }
        }
        cell.durationLabel.addAllShadow(10, opacity: 1.0, color: .black, offSet: .zero)

        if asset.mediaType == .video{
            let timeString = CGFloat(asset.duration).convertSecondsToDurationString()
            cell.durationLabel.text = timeString
        }else{
            cell.durationLabel.isHidden = true
        }
        cell.durationLabel.font = UIFont(name: FONT_REGULAR, size: 16)
        if arrayForSelectedIndexPaths.contains(indexPath){
            cell.selectedView.isHidden = false
            
            var currentIndex : Int!
            currentIndex  = arrayForSelectedIndexPaths.index(of: indexPath)
            
            if let value = currentIndex{
                
                print("currentIndex->",value )
                cell.countLabel.text = "\(value+1)"
            }
            
        }else{
            cell.selectedView.isHidden = true
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        var itemInLine  : CGFloat = 3
        if(IS_IPAD){
            itemInLine = itemInLine * 2
        }
        let  gapInCells : CGFloat = 0.5
        let widthFactor = CGFloat(itemInLine)
        let screenTotalWidth = collectionView.bounds.size.width
        let width = (screenTotalWidth - widthFactor * (gapInCells+1))/itemInLine
        return CGSize.init(width: width, height: width)
    }
    func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        
        if mediaTypePrefferred == .video{
            
            if !arrayForSelectedIndexPaths.contains(indexPath){
                if arrayForSelectedIndexPaths.count + alreadySelectedMediaCount >= 10{
                    AlertManager.shared.alertWithTitle(title: "Alert", message: "Videos reached to selection limit.")
                    
                    return false
                }
            }
            
        }
        return true
    }

//    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//
//
//        if mediaTypePrefferred == .image{
//
//
//            if arrayForSelectedIndexPaths.contains(indexPath){
//
//                let tempPreviouslyArray = arrayForSelectedIndexPaths
//                if let index = arrayForSelectedIndexPaths.index(of:indexPath) {
//                    arrayForSelectedIndexPaths.remove(at: index)
//                    selectedImages.remove(at: index)
//                }
////                self.pickerCV.reloadItems(at: [indexPath])
//                self.pickerCV.reloadItems(at: tempPreviouslyArray)
//
//                if self.arrayForSelectedIndexPaths.count > 0 {
//                    self.doneButtonBottomCons.isActive = true
//                }else{
//                    self.doneButtonBottomCons.isActive = false
//                }
//                UIView.animate(withDuration: 0.5) {
//                    self.view.layoutIfNeeded()
//                }
//                countLabel.text =  "\(self.arrayForSelectedIndexPaths.count)"
//
//            }
//            else{
//
//                if self.selectedImages.count + self.alreadySelectedMediaCount >= IMAGES_LIMIT{
//
//
//                    AlertManager.shared.alertWithTitle(title: "Alert", message: "Images reached to selection limit.")
//
//                    return
//                }
//
//                let asset = galleryAssetsResults.object(at: indexPath.row)
//
////                DispatchQueue.main.async {
////                    self.startAnimating( type: NVActivityIndicatorType.circleStrokeSpin)
////                }
//                getOriginalImage(asset: asset) { (image) in
//
//                    DispatchQueue.main.async {
//
//                        if let safeImage = image{
//
////                            if let imageJpeg = safeImage.jpegData(compressionQuality: 1){
////                                if let newImage = UIImage(data: imageJpeg){
////                                    self.arrayForSelectedIndexPaths.append(indexPath)
////                                    self.selectedImages.append(newImage)
////
////                                }
////                            }
//
//                            self.arrayForSelectedIndexPaths.append(indexPath)
//                            self.selectedImages.append(safeImage)
//                            self.countLabel.text =  "\(self.arrayForSelectedIndexPaths.count)"
//                        }else{
//                            debugPrint("userInfo=> error")
//                        }
//                        self.pickerCV.reloadItems(at: [indexPath])
//
//                        if self.arrayForSelectedIndexPaths.count > 0 {
//                            self.doneButtonBottomCons.isActive = true
//                        }else{
//                            self.doneButtonBottomCons.isActive = false
//                        }
//                        UIView.animate(withDuration: 0.5) {
//                            self.view.layoutIfNeeded()
//                        }
//                    }
//
//                }
//
//
//            }
//
//
//        }else if mediaTypePrefferred == .video{
//            if arrayForSelectedIndexPaths.contains(indexPath){
//
//                let tempPreviouslyArray = arrayForSelectedIndexPaths
//                if let index = arrayForSelectedIndexPaths.index(of:indexPath) {
//                    arrayForSelectedIndexPaths.remove(at: index)
//                    selectedAssets.remove(at: index)
//                }
//                countLabel.text =  "\(self.arrayForSelectedIndexPaths.count)"
//
//
//                self.pickerCV.reloadItems(at: tempPreviouslyArray)
//
//
//            }else{
//
//                var totalSeconds : CGFloat = 0
//                for i in 0..<selectedAssets.count{
//                    let assetDuration  = selectedAssets[i].duration
//                    totalSeconds = totalSeconds + CGFloat(assetDuration)
//
//                }
//                debugPrint("totalDuration : ", totalSeconds)
//                totalSeconds = totalSeconds + CGFloat(galleryAssetsResults[indexPath.row].duration) + CGFloat(self.alreadySelectedMediaSeconds)
//                debugPrint("totalDuration : ", totalSeconds)
//
//                if totalSeconds >= VIDEO_MAXIMUM_LIMIT{
//
//
//                    AlertManager.shared.alertWithTitle(title: "Alert", message: "Adding this video will exceed maximum video limit.")
//
//                    return
//                }
//                if VideoEdVideoMakeManager.currentEditorMode == .REACTCAMERA{
//
////                    let pickerVC = ReactDashboardVC(nibName: "ReactDashboardVC", bundle: nil)
////                    pickerVC.videoAsset = galleryAssetsResults[indexPath.row]
////                    self.navigationController?.pushViewController(pickerVC, animated: true)
////
////                    let editVC = VideoEdTrimVC(nibName: "VideoEdTrimVC", bundle: nil)
////                    editVC.videoPHAsset = galleryAssetsResults[indexPath.row]
////                    self.navigationController?.pushViewController(editVC, animated: true)
//
//
//                    let options: PHVideoRequestOptions = PHVideoRequestOptions()
//                    options.version = .current
//                    //                    options.deliveryMode = .fastFormat
//                    options.isNetworkAccessAllowed = true
//                    options.progressHandler = { (progress, err, stop,dic) in
//                        print("progress for image", progress)
//
//                        let str = String(format: "%d%%", Int(progress * 100))
//                        NVActivityIndicatorPresenter.sharedInstance.setMessage(str)
//
//                    }
//
//
//                    PHImageManager.default().requestAVAsset(forVideo: galleryAssetsResults[indexPath.row], options: options, resultHandler: {(asset: AVAsset?, audioMix: AVAudioMix?, info: [AnyHashable : Any]?) -> Void in
//                        DispatchQueue.main.async {
//                            debugPrint("asset class",asset?.className)
//
//                            if asset is AVComposition{
//
//
//                                self.ConvertAvcompositionToAvasset(avComp: asset as! AVComposition, completion: { (assetNew) in
//
//                                    if let urlAsset : AVAsset = assetNew {
//                                        let editVC = VideoEdTrimVC(nibName: "VideoEdTrimVC", bundle: nil)
//                                        editVC.videoAssetFromUrl = urlAsset
//                                        editVC.videoPHAsset = self.galleryAssetsResults[indexPath.row]
//                                        self.navigationController?.pushViewController(editVC, animated: true)
//                                    }else{
//                                        AlertManager.shared.alertWithTitle(title: "", message: "Unable to get the video.")
//
//                                    }
//                                })
//
//                            }else{
//                                if let urlAsset = asset {
//                                    let editVC = VideoEdTrimVC(nibName: "VideoEdTrimVC", bundle: nil)
//                                    editVC.videoAssetFromUrl = urlAsset
//                                    editVC.videoPHAsset = self.galleryAssetsResults[indexPath.row]
//                                    self.navigationController?.pushViewController(editVC, animated: true)
//                                }else{
//                                    AlertManager.shared.alertWithTitle(title: "", message: "Unable to get the video.")
//
//                                }
//
//                            }
//
//                        }
//                    })
//                }
//
//                arrayForSelectedIndexPaths.append(indexPath)
//                selectedAssets.append(galleryAssetsResults[indexPath.row])
//                countLabel.text =  "\(self.arrayForSelectedIndexPaths.count)"
//            }
//
//            self.pickerCV.reloadItems(at: [indexPath])
//
//
//            if arrayForSelectedIndexPaths.count > 0 {
//                doneButtonBottomCons.isActive = true
//            }else{
//                doneButtonBottomCons.isActive = false
//            }
//            UIView.animate(withDuration: 0.5) {
//                self.view.layoutIfNeeded()
//            }
//            debugPrint("indexPath",indexPath.row)
//
//            collectionView.reloadData()
//
//
//        }
//
//    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
       
        
        selectedAssets.removeAll()
        selectedAssets.append(galleryAssetsResults[indexPath.row])

        let storyboard : UIStoryboard = UIStoryboard(name: "VideoEdAlbum", bundle: nil)
        let vc : VideoEdEditVC = storyboard.instantiateViewController(withIdentifier: "VideoEdEditVC") as! VideoEdEditVC
        vc.selectedAssets = selectedAssets
        self.navigationController?.pushViewController(vc, animated: true)

        
    }
    public func getOriginalImage(asset: PHAsset,clouser: @escaping((UIImage?)->())) {
        
        let options = PHImageRequestOptions()
        options.isNetworkAccessAllowed = true
        options.deliveryMode = .highQualityFormat
        
        var startedAnimation = false
        options.progressHandler = { (progress, err, stop,dic) in
            print("progress for image", progress)
            
            if !startedAnimation{
                startedAnimation = true
                DispatchQueue.main.async {
                    self.startAnimating( type: NVActivityIndicatorType.circleStrokeSpin)
                }
            }
            let str = String(format: "%d%%", Int(progress * 100))
            NVActivityIndicatorPresenter.sharedInstance.setMessage(str)
            
        }
//        var sizeForImage = PHImageManagerMaximumSize
      let   sizeForImage = UIScreen.main.bounds.size
        
        PHImageManager.default().requestImage(for: asset, targetSize: sizeForImage, contentMode: .aspectFit, options: options) { (image, info) in
            if image != nil {
                DispatchQueue.main.async {
                    self.stopAnimating()
                }
                clouser(image)
            } else {
                DispatchQueue.main.async {
                    self.stopAnimating()
                }
                clouser(nil)
            }
        }

    }
}


extension VideoEdPickerVC : NVActivityIndicatorViewable{
    
}
extension VideoEdPickerVC : PHPhotoLibraryChangeObserver{

    func photoLibraryDidChange(_ changeInstance: PHChange) {
        
        let fetchResultChangeDetails = changeInstance.changeDetails(for: galleryAssetsResults)
        guard (fetchResultChangeDetails) != nil else {
            print("No change in fetchResultChangeDetails")
            return;
        }
        print("Contains changes")
        galleryAssetsResults = (fetchResultChangeDetails?.fetchResultAfterChanges)!
//        let insertedObjects = fetchResultChangeDetails?.insertedObjects
//        let removedObjects = fetchResultChangeDetails?.removedObjects
        
        DispatchQueue.main.async {
            
            self.pickerCV.reloadData()
        }
        
        
    }

    
}
