//
//  AddStickerLayerExtenstion.swift
//  Video Editorial
//
//  Created by strongapps on 09/01/19.
//  Copyright © 2019 strongapps. All rights reserved.
//
import UIKit

let gifObject = "GIF_OBJECT"
let addOpacity = "addOpacity"
let removeOpacity = "removeOpacity"

typealias AddStickerLayerExtension = VideoEdEditVC
extension AddStickerLayerExtension //: StickerViewDelegate
{

    // MARK: - Add Gif or Image layer
    func addStickerLayer<T>(with data: T, imgUrl : String) {
        
        _ = TextData.init("String")
        /*
        model.imgUrl = imgUrl
        model.angle = 0.0
        model.startTime = 0.0
        model.duration = CGFloat(videoDuration.seconds)
        model.poistion = parentLayer.position
        var size = CGSize(width: 100.0, height: 100.0)
        let r = arc4random_uniform(1313)
         let imageLayer = CALayer()
        imageLayer.shouldRasterize = true
        imageLayer.contentsScale = 3.0
        imageLayer.rasterizationScale = 3.0
        imageLayer.opacity = 1.0
        imageLayer.masksToBounds = true
        if let image = data as? UIImage{
            let identifier = String(format: "sticker%i", r)
            model.identifier = identifier
            model.type = .photo
            model.photo = image
            imageLayer.name = identifier
            imageLayer.contents = image.cgImage
            let wtohRatio = image.size.width / image.size.height
            size.height = size.width / wtohRatio
        }else if let animation = data as? CAKeyframeAnimation{
            let identifier = String(format: "gif%i", r)
            model.identifier = identifier
            model.animations[gifObject] = animation
            model.type = .gif
            var wtohRatio : CGFloat = 1.0
            //following code is to get image of first frame from cakeyframeanimation
            if let values = animation.values?[0]{
                let image = UIImage(cgImage: (values as! CGImage))
                wtohRatio = image.size.width / image.size.height
            }//end
            size.height = size.width / wtohRatio
            imageLayer.add(animation, forKey: gifObject)
        }
        imageLayer.frame = CGRect(origin: .zero, size: size)
        imageLayer.position = parentLayer.position
        model.size = size
        model.frame = CGRect(origin: .zero, size: size)
        imageLayer.name = model.identifier
        stickerLayerArray.append(model)
        VideoEdHelper.dispatchMainAfter(time: .now() + 0.1) {[weak self] in
            self?.parentLayer.addSublayer(imageLayer)
            self?.click(on: imageLayer)
        }
        */
    }
    
    // MARK: - Add Text sticker
   
    /*public func addTextSticker() {
        let r = arc4random_uniform(1313)
        let identifier = String(format: "text%i", r)
        let model = TextData()
        model.identifier        = identifier
        model.placeHolderText   = "Double Tap Here"
        model.angle             = 0
        model.fontName          = Source.allFonts[0].fontName
        model.fontSize          = 20
        model.fontColor         = Source.colors[0]
        model.startTime         = 0.0
        model.duration          = 2.0
        model.type              = .text
        stickerLayerArray.append(model)
        addText(with: identifier, and: model)
       
    }
*/
    
    private func addText(with identifier: String, and model: TextData) {
        /*
        let textLayer = CenterTextLayer()
        textLayer.shouldRasterize = true
        textLayer.backgroundColor = UIColor.clear.cgColor
        textLayer.opacity = 1.0
        textLayer.name = identifier
        let attString = VideoEdHelper.createAttributedStringWith(model)
        textLayer.frame = CGRect(origin: CGPoint(x: 0, y: 0), size: VideoEdHelper.textSize(attString!))
        textLayer.position = parentLayer.position
        textLayer.string = attString
        textLayer.contentsScale = 3.0
        textLayer.rasterizationScale = 3.0
        textLayer.alignmentMode = .center
        textLayer.masksToBounds = false
        parentLayer.addSublayer(textLayer)
        VideoEdHelper.dispatchMainAfter(time: .now() + 0.01) {[weak self] in
            self?.click(on: textLayer)
        }
        */
    }
    
    func updateText(data: AnyObject) {
      /* guard let selectedTextLayer =  selectedStickerLayer as? CenterTextLayer else { return }
        if data is UIFont {
            selectedLayerData?.fontName = (data as? UIFont)?.fontName
        } else if data is UIColor {
            selectedLayerData?.fontColor = data as? UIColor
        }
        DispatchQueue.main.async {
            let attrString = VideoEdHelper.createAttributedStringWith(self.selectedLayerData!)
            let stringSize = VideoEdHelper.textSize(attrString!)
            selectedTextLayer.string = attrString
            CATransaction.begin()
            CATransaction.setValue(kCFBooleanTrue, forKey: kCATransactionDisableActions)
            selectedTextLayer.setAffineTransform(.identity)
            var frame = selectedTextLayer.frame
            frame.size.width = stringSize.width
            frame.size.height = stringSize.height
            selectedTextLayer.frame = frame
            selectedTextLayer.position = self.selectedLayerData!.poistion
            selectedTextLayer.setAffineTransform(CGAffineTransform(rotationAngle: (self.selectedLayerData?.angle)!))
            self.showLayerHandels(selectedTextLayer)
            self.view.setNeedsDisplay()
            CATransaction.commit()
        }
        */
    }
 
 
    
    // MARK: - Show handles of selected text layer
    func showLayerHandels_old(_ layer: CALayer?) {
        
        /*
         
        if selectedStickerView != nil {
            selectedStickerView?.removeFromSuperview()
            selectedStickerView = nil
        }
         
        selectedStickerLayer = layer
        var scale: CGFloat = 1.0
        var rotation: CGFloat = 0.0
        scale = VideoEdHelper.xscale((layer?.affineTransform())!)
        rotation = VideoEdHelper.zRotaion((layer?.affineTransform())!)
        
        if let imageLayer = layer {
         
            let layerSize = imageLayer.bounds.size
            selectedStickerView = StickerView(contentFrame: CGRect(x: 0, y: 0, width: layerSize.width, height: layerSize.height), contentImage: UIImage())
            selectedStickerView?.center = imageLayer.position
            selectedStickerView?.enabledControl = true
            selectedStickerView?.enabledBorder = true
            selectedStickerView?.delegate = self
            selectedStickerView?.fontSize = Float(selectedLayerData!.fontSize)
            selectedStickerView?.tag = 1
            selectedStickerView?.updateUI(withScale: scale, rotation: rotation)
            // videoContainer.addSubview(selectedStickerView!)
         
        }
         
        */
    }
    
    
    func updateUI_old(with itemView: StickerView?, center: CGPoint, rotation: CGFloat, size: CGSize, scale: Float) {
       /*
         var newSize = size
            print("scale ",scale)
            newSize = (itemView?.bounds.size)!
            newSize.width -= 30.0
            newSize.height -= 30.0
            newSize = newSize.applying(CGAffineTransform(scaleX: CGFloat(scale), y: CGFloat(scale)))
            CATransaction.begin()
            CATransaction.setValue(kCFBooleanTrue, forKey: kCATransactionDisableActions)
            guard let selectedLayer = selectedStickerLayer else {
                return
            }
            selectedLayer.setAffineTransform(.identity)
            selectedLayer.position = CGPoint(x: center.x, y: center.y)
            var frame = selectedStickerLayer!.bounds
            frame.size = newSize
            let newScale: CGFloat = CGFloat(scale)
            selectedLayer.setAffineTransform(CGAffineTransform(scaleX: newScale, y: newScale).concatenating(CGAffineTransform(rotationAngle: rotation)))
            CATransaction.commit()

            // **** update Data Model *****
            selectedLayerData?.angle = rotation
            selectedLayerData?.size = frame.size
            selectedLayerData?.poistion = selectedLayer.position
            selectedLayerData?.rotation = rotation
            selectedLayerData?.scale = newScale
            selectedLayerData?.frame = frame
            selectedLayerData?.fontSize = CGFloat(scale * itemView!.fontSize)
    */
    }

    
    func endScaling_old(_ itemView: StickerView!) {
    //        if selectedLayerData != nil{
    //            let rotation = selectedLayerData!.rotation
    //            selectedStickerLayer?.setAffineTransform(CGAffineTransform(rotationAngle: rotation))
    //            selectedStickerLayer?.frame = selectedLayerData!.frame
    //            selectedStickerLayer?.position = selectedLayerData!.poistion
    //
    //        }
    }

    //MARK:- Sticker view Delegates
    func stickerViewDidTapContentView_old(_ stickerView: StickerView!) {
        
    }
    
    func stickerViewDidDoubleTapContentView_old(_ stickerView: StickerView!) {
    }
    
    func stickerViewDidTapDeleteControl_old(_ stickerView: StickerView!) {
      /*
        for model in stickerLayerArray where model.identifier == selectedLayerData?.identifier {
            let index = stickerLayerArray.firstIndex(of: model)
            stickerLayerArray.remove(at: index!)
            let selectedLayer = parentLayer.sublayers?.filter { $0.name == model.identifier }
            if selectedLayer!.count > 0{
                selectedLayer![0].removeAllAnimations()
                selectedLayer![0].removeFromSuperlayer()
                selectedItemLayer = nil
                timeLineView?.hideTrimBars()
            }
        }
        if addNewTextView != nil{
            // addNewTextView.hideSelf()
        }
        if stickersView != nil{
            stickersView.hideSelf()
        }
        */
    }
    
    

    func replaceLayerToItsPositionFromStickerView(){
      /* if let sticker = selectedItemLayer{
            parentLayer.addSublayer(sticker)
        }*/
    }
    
    func editSelectedSticker_old(_ newImage: UIImage?, identifier identifierStr: String?) {
        selectedLayerData = nil
        // ****************** update TimeLine *****************
      /*
        if selectedLayerData != nil {
            showTimeLineHandels(newImage, stickerData: selectedLayerData)
            //  ***  goes to Start Time of Sticker *****
            if let durationInSeconds = videoDuration?.seconds{
                 timeLineView?.updateTimeLine(selectedLayerData!.startTime, duration: CGFloat(durationInSeconds))
            }
           
        }
    */
    }
    
    /*
    func showTimeLineHandels(_ image: UIImage?, stickerData stickerDataObj: StickerData?) {
        guard let stickerImg = image else{
            return
        }
        if timeLineView != nil {
            let startTime = stickerDataObj?.startTime ?? 0.0
            let duration = stickerDataObj?.duration ?? 0.0
            timeLineView?.showHandels(stickerImg, newStartTime: startTime, newItemDuration: duration)
        }
    }
    */
    
    // MARK: - Remove add text layer text view
    func removeAddTextView() {

    }
  
}
