//
//  MusicObjectData.swift
//  Video Editorial
//
//  Created by strongapps on 10/04/19.
//  Copyright © 2019 strongapps. All rights reserved.
//

import Foundation

class MusicObjectData: NSObject {
    
    static let MUSIC = "MUSIC"
    static let LIBRARY = "LIBRARY"
    static let NO_MUSIC = "NO_MUSIC"
    
    var title : String?
    var thumbpath : String?
    var artwork : UIImage?
    var url_path : URL?
    var volumeP  : CGFloat = 1.0
    var startD  : CGFloat = 0.0    // Audio Track start from StartD Seconds
    var duration  : CGFloat = 0.0    // duration for audio Track
    var type : String?
    var beginTime  : CGFloat = 0.0 // AT Time Track is added in Main Track(Video)

    
    override init() {
        super.init()
    }
    
    init(urlStr: String) {
        super.init()
        
        let urlEncodingStr = urlStr.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        if let urlstring = urlEncodingStr {
            self.url_path = URL(string: urlstring)
        }
        self.title = getSongName(urlString: urlStr)
    }
    
    
    func getSongName(urlString: String) -> String?{
        let components = urlString.components(separatedBy: "/")
        return components.last
    }
}

