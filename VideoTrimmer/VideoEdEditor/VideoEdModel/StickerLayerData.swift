//
//  TextLayerData.swift
//  Video Editorial
//
//  Created by strongapps on 04/01/19.
//  Copyright © 2019 strongapps. All rights reserved.
//

import UIKit

enum STICKER_TYPE{
    case gif
    case photo
    case text
}
/*
class StickerLayerData: NSObject {
    var imgUrl: String!
    var identifier: String!
    var startTime: CGFloat = 0.1
    var duration: CGFloat = 0.0
    var angle: CGFloat!
    var opacity: Float = 1.0
    var shadowColor: UIColor!
    var scale: CGFloat = 1.0
    var editable = false
    var size: CGSize! 
    var poistion: CGPoint!
    var type : STICKER_TYPE = .photo
    var animations: [String: CAAnimation] = [String: CAAnimation]()
    var photo: UIImage!
    var frame: CGRect = CGRect.zero
    var rotation: CGFloat = 0.0
    
    
    var text: String!
    var placeHolderText: String!
    var fontSize: CGFloat = 0.0 {
        didSet{
            print("fontSIZE IS",fontSize)
        }
    }
    var fontName: String!
    var fontColor: UIColor!
    var strokeWidth: Float!
    var strokeColor: UIColor!
  
}
*/
