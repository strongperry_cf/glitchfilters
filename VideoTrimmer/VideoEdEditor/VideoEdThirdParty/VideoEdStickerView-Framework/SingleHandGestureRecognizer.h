//
//  SingleHandGestureRecognizer.h
///  Video Editorial
//
//  Created by strongapps on 10/04/19.
//  Copyright © 2016年 CKJ. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SingleHandGestureRecognizer : UIGestureRecognizer

@property (assign, nonatomic) CGFloat scale;
@property (assign, nonatomic) CGFloat rotation;

- (nonnull instancetype)initWithTarget:(nullable id)target action:(nullable SEL)action anchorView:(nonnull UIView *)anchorView;

- (void)reset;

@end
