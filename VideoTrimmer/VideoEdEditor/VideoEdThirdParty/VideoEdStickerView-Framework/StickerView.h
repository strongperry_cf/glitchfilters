//  StickerView.h
//  Video Editorial
//
//  Created by strongapps on 10/04/19.
//  Copyright © 2016年 CKJ. All rights reserved.


#import <UIKit/UIKit.h>

@protocol StickerViewDelegate;

@interface StickerView : UIView

@property (assign, nonatomic) BOOL enabledControl; // determine the control view is shown or not, default is YES
@property (assign, nonatomic) BOOL enabledDeleteControl; // default is YES
@property (assign, nonatomic) BOOL enabledShakeAnimation; // default is YES
@property (assign, nonatomic) BOOL enabledBorder; // default is YES
@property (assign, nonatomic) float fontSize;

@property (strong, nonatomic) UIImage *contentImage;
@property (strong, nonatomic) UIImageView *contentView;

@property (assign, nonatomic) id<StickerViewDelegate> delegate;

- (instancetype)initWithContentFrame:(CGRect)frame contentImage:(UIImage *)contentImage;
-(void)updateUIWithScale:(CGFloat)scale rotation:(CGFloat)rotation;


- (void)performTapOperation;

@end

@protocol StickerViewDelegate <NSObject>

-(void)updateUIWithView:(StickerView *)itemView center:(CGPoint)center rotation:(CGFloat)rotation size:(CGSize)size scale:(float)scale;

-(void)endScaling:(StickerView *)itemView;

- (void)stickerViewDidTapContentView:(StickerView *)stickerView;

- (void)stickerViewDidDoubleTapContentView:(StickerView *)stickerView;

- (void)stickerViewDidTapDeleteControl:(StickerView *)stickerView;

@optional

- (UIImage *)stickerView:(StickerView *)stickerView imageForRightTopControl:(CGSize)recommendedSize;

- (void)stickerViewDidTapRightTopControl:(StickerView *)stickerView; // Effective when resource is provided.

- (UIImage *)stickerView:(StickerView *)stickerView imageForLeftBottomControl:(CGSize)recommendedSize;

- (void)stickerViewDidTapLeftBottomControl:(StickerView *)stickerView; // Effective when resource is provided.



@end
