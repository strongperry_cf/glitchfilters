//
//  CropViewCell.swift
//  Video Editorial
//
//  Created by strongapps on 13/02/19.
//  Copyright © 2019 strongapps. All rights reserved.
//

import UIKit

class CropViewCell: UICollectionViewCell {
     static let reuseIdentifier = "CropViewCell"
     var imageView: UIImageView!
    
    override func awakeFromNib() {
        
        super.awakeFromNib()

        layer.cornerRadius = 5
        layer.masksToBounds = true
    }
    
    var isSelectedCell = false {
        didSet{
           // nameLabel.backgroundColor = isSelectedCell ? #colorLiteral(red: 1, green: 0.8237235915, blue: 0, alpha: 1) : .clear
            nameLabel.textColor = isSelectedCell ? .white : .white
        }
    }
    
    
    lazy var nameLabel : UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.textAlignment = .center
        label.layer.cornerRadius = 5.0
        label.clipsToBounds = true
        return label
    }()
    

    
    
    //MARK:- For Crop View
    func setupData(with data: Crop){
        nameLabel.text = data.type?.rawValue
        nameLabel.frame = self.bounds
//        nameLabel.frame.size.height *= 0.6
        nameLabel.font = UIFont(name: FONT_REGULAR, size: 13)
        addSubview(nameLabel)
    }
    
    //MARK:- For Sticker Categories View
    func setupData(with data: String){
        nameLabel.frame = self.bounds
//        nameLabel.frame.size.height *= 0.6
        
       let formattedName = getUpdateHeader(with: data)
        nameLabel.font = UIFont(name: FONT_SEMIBOLD, size: 22) //  systemFont(ofSize: 12.0)
        nameLabel.text = formattedName
        nameLabel.addAllShadow(2)
//        let attrString = NSAttributedString(string: formattedName, attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 12.0)])
//        nameLabel.attributedText = attrString
        addSubview(nameLabel)
        bringSubviewToFront(nameLabel)
    }
    
    func getUpdateHeader(with string: String) -> String {
        var finalHeader = ""
        let subStrings = string.components(separatedBy: "_")
        for subString in subStrings{
            finalHeader += String(subString.prefix(1).uppercased() + subString.lowercased().dropFirst()) + " "
        }
      return finalHeader
    }
    
}
