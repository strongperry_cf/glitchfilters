//
//  VideoEdCropView.swift
//  VideoTrimmer
//
//  Created by strongapps on 02/05/19.
//  Copyright © 2019 strongapps. All rights reserved.

import UIKit

protocol CropViewDelegate : class{
    func cropViewChangeRatio(ratio: CGFloat)
    func cropViewChangeColor(color: UIColor?)
    func cropViewChangeGradColor(color: GradientColor?)
    func cropViewChangeBlurColor(blurType: Int)
}

struct CropModel {
    var name :String = ""
    var ratio:CGFloat = 0.0
    init(name:String,ratio:CGFloat) {
        self.name = name;
        self.ratio  = ratio;
    }
}

struct GradientColor{
    
    var colorsArray : [CGColor]!
    var startPoint : CGPoint!
    var endpoint : CGPoint!
    
}
class VideoEdCropView: UIView {
    
    weak var delegate: CropViewDelegate?
    @IBOutlet weak var upperView        :UIView!
    @IBOutlet weak var bottomView       :UIView!
    @IBOutlet weak var aspectRatioCV    :UICollectionView!
    @IBOutlet weak var colorCV          :UICollectionView!
    @IBOutlet weak var closeBtn         :UIButton!
    @IBOutlet weak var doneBtn          :UIButton!
//    var colorArr                        :NSArray = []
    var newcolorsArr                    :[UIColor]!
    
    var gradColorsArr                    :[GradientColor]!

    var thumbnailImage                  :UIImage? = nil
    
    var showColors                      : Bool = true
    

    var aspectRatioArr:[CropModel] = [CropModel.init(name: "ratios_original", ratio: 0.0),
                                      CropModel.init(name: "ratios_insta11", ratio: 1.0),
                                      CropModel.init(name: "ratios_916", ratio: 0.56),
                                      CropModel.init(name: "ratios_169", ratio: 1.77),
                                      CropModel.init(name: "ratios_insta45", ratio: 0.8),
                                      CropModel.init(name: "ratios_instastory", ratio: 0.66),
                                      CropModel.init(name: "ratios_54", ratio: 1.25),
                                      CropModel.init(name: "ratios_43", ratio: 1.3),
                                      CropModel.init(name: "ratios_34", ratio: 0.75),
                                      CropModel.init(name: "ratios_fpost", ratio: 1.4),
                                      CropModel.init(name: "ratios_23", ratio: 0.654),
                                      CropModel.init(name: "ratios_32", ratio: 1.5),
                                      CropModel.init(name: "ratios_utubecover", ratio: 1.91),
                                      CropModel.init(name: "ratios_12", ratio: 0.5)
                                ]
    var ratiosImages = ["ratios_originalsel",
                        "ratios_sel11",
                        "ratios_sel916",
                        "ratios_sel169",
                        "ratios_sel59",
                        "ratios_selinsta",
                        "ratios_sel54",
                        "ratios_sel43",
                        "ratios_sel34",
                        "ratios_selfbpost",
                        "ratios_sel23",
                        "ratios_sel32",
                        "ratios_selutube",
                        "ratios_sel12"
    ]
    var selectedColorStr               :String  = "000000"
    var selectedColorIndex           : IndexPath  = IndexPath(row: 2, section: 0)
    var selectedAspectRatio         :CropModel = CropModel.init(name: "Original", ratio: 0.0)

    override func awakeFromNib() {
        
        super.awakeFromNib()
        self.backgroundColor = UIColor.black
        self.upperView.backgroundColor = UIColor.clear
        self.colorCV.backgroundColor =  UIColor.clear
        self.aspectRatioCV.backgroundColor =  UIColor.clear

        setupCollectionView()
        newcolorsArr = generateColors()
        
        gradColorsArr = generateGradColorsArray()
        
    }
    func makeColorWithRGB(red : Int , green : Int , blue : Int) -> UIColor{
        
        return UIColor(displayP3Red: (CGFloat(red) / 255.0) , green: CGFloat(green) / 255.0 , blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    func updateFeature(){
        if showColors{
            aspectRatioCV.isHidden = true
            colorCV.isHidden = false
        }else{
            aspectRatioCV.isHidden = false
            colorCV.isHidden = true
        }

    }
    func setupCollectionView () {
        let nibname = UINib(nibName: "RatioCollectionViewCell", bundle: nil)
        aspectRatioCV.register(nibname, forCellWithReuseIdentifier: "RatioCollectionViewCell")
        aspectRatioCV.delegate = self
        aspectRatioCV.dataSource = self
        
        
        let layout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 0
        

        let nibname2 = UINib(nibName: "CropCVCell", bundle: nil)
        colorCV.register(nibname2, forCellWithReuseIdentifier: "CropCVCell")
        colorCV.delegate = self
        colorCV.dataSource = self
        
    }
    func updateDefault(){
        selectedAspectRatio        = CropModel.init(name: "Original", ratio: 0.0)
        aspectRatioCV.reloadData()
    }
    func generateColors() -> [UIColor]  {

        let colorArray = [makeColorWithRGB(red: 254, green: 254, blue: 254),
                          makeColorWithRGB(red: 255, green: 214, blue: 206),
                          makeColorWithRGB(red: 252, green: 170, blue: 159),
                          makeColorWithRGB(red: 254, green: 226, blue: 131),
                          makeColorWithRGB(red: 255, green: 125, blue: 74),
                          makeColorWithRGB(red: 255, green: 163, blue: 184),
                          makeColorWithRGB(red: 255, green: 41, blue: 118),
                          makeColorWithRGB(red: 213, green: 166, blue: 212),
                          makeColorWithRGB(red: 149, green: 210, blue: 245),
                          makeColorWithRGB(red: 126, green: 174, blue: 228),
                          makeColorWithRGB(red: 67, green: 175, blue: 157),
                          makeColorWithRGB(red: 209, green: 228, blue: 173),
                          makeColorWithRGB(red: 167, green: 206, blue: 142),
                          makeColorWithRGB(red: 115, green: 69, blue: 50),
        
        ]
        return colorArray
    }
    func generateGradColorsArray() -> [GradientColor]{
        
        var localGradColorsArr : [GradientColor] = []
        
        let color1 = GradientColor(colorsArray: [makeColorWithRGB(red: 120, green: 245, blue: 237).cgColor, makeColorWithRGB(red: 224, green: 244, blue: 10).cgColor], startPoint:CGPoint(x: 0, y: 1) , endpoint:CGPoint(x: 1, y: 0) )
        localGradColorsArr.append(color1)
        
        let color2 = GradientColor(colorsArray: [makeColorWithRGB(red: 99, green: 199, blue: 243).cgColor, makeColorWithRGB(red: 244, green: 193, blue: 239).cgColor], startPoint:CGPoint(x: 0.5, y: 0) , endpoint:CGPoint(x: 0.5, y: 1) )
        localGradColorsArr.append(color2)

        let color3 = GradientColor(colorsArray: [makeColorWithRGB(red: 102, green: 228, blue: 212).cgColor, makeColorWithRGB(red: 142, green: 138, blue: 222).cgColor], startPoint:CGPoint(x: 0.5, y: 0) , endpoint:CGPoint(x: 0.5, y: 1) )
        localGradColorsArr.append(color3)

        let color4 = GradientColor(colorsArray: [makeColorWithRGB(red: 255, green: 210, blue: 249).cgColor, makeColorWithRGB(red: 252, green: 203, blue: 192).cgColor], startPoint:CGPoint(x: 0, y: 1) , endpoint:CGPoint(x: 1, y: 0) )
        localGradColorsArr.append(color4)

        
        let color5 = GradientColor(colorsArray: [makeColorWithRGB(red: 70, green: 207, blue: 199).cgColor, makeColorWithRGB(red: 145, green: 238, blue: 233).cgColor], startPoint:CGPoint(x: 0, y: 0.5) , endpoint:CGPoint(x: 1, y: 0.5) )
        localGradColorsArr.append(color5)

        
        let color6 = GradientColor(colorsArray: [makeColorWithRGB(red: 255, green: 181, blue: 141).cgColor, makeColorWithRGB(red: 255, green: 111, blue: 219).cgColor], startPoint:CGPoint(x: 0.5, y: 0) , endpoint:CGPoint(x: 0.5, y: 1) )
        localGradColorsArr.append(color6)

        let color7 = GradientColor(colorsArray: [makeColorWithRGB(red: 204, green: 245, blue: 195).cgColor, makeColorWithRGB(red: 253, green: 176, blue: 185).cgColor], startPoint:CGPoint(x: 0, y: 0) , endpoint:CGPoint(x: 1, y: 1) )
        localGradColorsArr.append(color7)
        

        
        return localGradColorsArr
    }
   
    @IBAction func doneBtnAct(_ sender: UIButton) {
        self.isHidden = true
    }
    
}

extension VideoEdCropView:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        var side = collectionView.frame.size.height * 0.8
        if collectionView == colorCV {
            
            let width = side * 0.56

            return CGSize(width: width , height: side)
        }
        else{

            let ratioFromArray = aspectRatioArr[indexPath.row].ratio
            var widthNew  = side * ratioFromArray
            var heightNew = side
            
            if widthNew == 0{
                widthNew = side
            }
            if ratioFromArray > 1.1{
                
                widthNew = side
//                heightNew = side / ratioFromArray
                
            }else if ratioFromArray > 1{
//                heightNew  = heightNew / ratioFromArray
//                widthNew  = side * ratioFromArray
            }else if ratioFromArray == 1 || ratioFromArray == 0{
                
                widthNew =  widthNew * 0.8

            }
//            print(aspectRatioArr[indexPath.row].name,CGSize(width: widthNew , height: heightNew))
            return CGSize(width: widthNew , height: heightNew)
        }
    }
    
    /*
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return itemSpacing
    }
    
    func collectionView(_ collectionView: UICollectionView, layout
        collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return itemSpacing
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        return UIEdgeInsets(top: 0, left: itemSpacing, bottom: 0, right: itemSpacing)
    }
    */
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        if collectionView == colorCV {
            return 2
        }
        else{
            return 1;
        }

    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == colorCV  && section == 0{
            return newcolorsArr.count + 3
        }else if collectionView == colorCV  && section == 1{
            
            return gradColorsArr.count
            

        }
        else{
            return aspectRatioArr.count;
        }
    }
//
//    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
//
//        let cell:CropCVCell = collectionView.dequeueReusableCell(withReuseIdentifier: "CropCVCell", for: indexPath) as! CropCVCell
//
//        collectionView.backgroundColor = .blue
//        cell.backgroundColor = UIColor.yellow
//        cell.mainView.backgroundColor =  UIColor.clear
//
//        if(collectionView  == colorCV)      {
//
//            cell.imageView.isHidden = false
//            cell.titleLbl.isHidden = true
//            cell.imageView.layer.cornerRadius =  5.0
//            cell.titleLbl.font = UIFont(name: FONT_REGULAR, size: 15)
//            var colorStr:String =  ""
//
//            if(indexPath.item > 0){
//                cell.imageView.image = nil
//
//                colorStr = colorArr [indexPath.item - 1] as! String
//                let color =  UIColor.hexStringToUIColor(colorStr)
//                cell.imageView.backgroundColor =  color
//
//            }
//            else {
//                cell.imageView.image = thumbnailImage
//                cell.imageView.contentMode = .scaleAspectFill
//            }
//
//            // Selected String
//            if selectedColorStr == colorStr            {
////                cell.imageView.layer.borderWidth = 2.0
////                cell.imageView.layer.borderColor = AppThemeManager.shared.currentTheme().TintColor.cgColor
//                cell.topYCons.constant = 0
//
//            }else {
//                cell.imageView.layer.borderWidth = 0.0
//                cell.topYCons.constant = 20
//
//            }
//        }else if (collectionView == aspectRatioCV)   {
//
//            if cell.topYCons != nil{
//                cell.topYCons.isActive = false
//            }
//
//
//            var side = cell.imageView.frame.height
//            cell.imageView.removeConstraints(cell.imageView.constraints)
//
//            let ratioFromArray = aspectRatioArr[indexPath.row].ratio
//            var widthNew  = side * ratioFromArray
//            var heightNew = side
//
//            if widthNew == 0{
//                widthNew = side
//            }
//            if ratioFromArray > 1{
//                //                heightNew  = heightNew / ratioFromArray
//                //                widthNew  = side * ratioFromArray
//            }
//            print(aspectRatioArr[indexPath.row].name,CGSize(width: widthNew , height: heightNew))
//            cell.imageView.frame = CGRect(origin: CGPoint.zero, size: CGSize(width: widthNew , height: heightNew))
//
//
//            /*
//            cell.imageView!.translatesAutoresizingMaskIntoConstraints = false
//            cell.imageView!.addConstraint(NSLayoutConstraint(item: cell.imageView!,
//                                                             attribute: NSLayoutConstraint.Attribute.height,
//                                                             relatedBy: NSLayoutConstraint.Relation.equal,
//                                                            toItem: cell.imageView!,
//                                                            attribute: NSLayoutConstraint.Attribute.width,
//                                                            multiplier: aspectRatioArr[indexPath.row].ratio,
//                                                            constant: 0))
//*/
//
//            cell.imageView.contentMode = .scaleToFill
//            cell.imageView.backgroundColor = .red
//            let ratioName =  aspectRatioArr[indexPath.item].name
//            cell.titleLbl.text = ratioName
//            cell.titleLbl.layer.borderColor = UIColor.clear.cgColor
//            cell.titleLbl.font = UIFont(name: FONT_REGULAR, size: 15)
////            cell.imageView.image = UIImage(named: ratioName)
////            cell.titleLbl.isHidden = true
//            cell.imageView.isHidden = false
//            if selectedAspectRatio.name == ratioName          {
//                cell.titleLbl.textColor =  AppThemeManager.shared.currentTheme().TintColor
//            }else {
//                cell.titleLbl.textColor =  UIColor.gray
//            }
//        }
//        return cell
//    }


    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        if(collectionView  == colorCV)      {
            
            let cell:CropCVCell = collectionView.dequeueReusableCell(withReuseIdentifier: "CropCVCell", for: indexPath) as! CropCVCell
            
            cell.mainView.backgroundColor =  UIColor.clear
            cell.imageView.isHidden = false
            cell.titleLbl.isHidden = true
            cell.imageView.layer.cornerRadius =  5.0
            cell.titleLbl.font = UIFont(name: FONT_REGULAR, size: 15)
            
            if  cell.imageView.layer.sublayers != nil{
                
                cell.imageView.layer.sublayers!.forEach{ layer in
                    if let newSafelayer = layer as? CAGradientLayer{
                        newSafelayer.removeFromSuperlayer()
                    }
                }
            }

            if indexPath.section == 1{
                
                
                let gradColorGlobal = gradColorsArr[indexPath.row]
                let gradient: CAGradientLayer = CAGradientLayer()
                gradient.colors = gradColorGlobal.colorsArray
                gradient.startPoint = gradColorGlobal.startPoint
                gradient.endPoint = gradColorGlobal.endpoint
                gradient.frame = CGRect(x: 0.0, y: 0.0, width: cell.frame.size.width, height: cell.frame.size.height)
                cell.imageView.layer.insertSublayer(gradient, at: 0)

                

            }else{
                if(indexPath.item > 2){
                    cell.imageView.image = nil
                    
                    let color =  newcolorsArr[indexPath.row - 3] //UIColor.hexStringToUIColor(colorStr)
                    cell.imageView.backgroundColor =  color
                    
                }
                else {
                    cell.imageView.image = thumbnailImage
                    cell.imageView.contentMode = .scaleAspectFill
                }
            }
            

            
            if selectedColorIndex == indexPath {
                cell.topYCons.constant = 0
            }else {
                
                cell.imageView.layer.borderWidth = 0.0
                cell.topYCons.constant = 20
                
            }
            return cell
            
        }else {
            
            
            let cell:RatioCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "RatioCollectionViewCell", for: indexPath) as! RatioCollectionViewCell
            
            
            let side = cell.frame.height
            let ratioFromArray = aspectRatioArr[indexPath.row].ratio
            var widthNew  = side * ratioFromArray
            var heightNew = side
            
            if widthNew == 0{
                widthNew = side
            }
            if ratioFromArray > 1.2{
                
                widthNew = side
                heightNew = side / ratioFromArray
                
            }else if ratioFromArray > 1{
                //                heightNew  = heightNew / ratioFromArray
                //                widthNew  = side * ratioFromArray
            }else if ratioFromArray == 1 || ratioFromArray == 0{
                
                widthNew = heightNew * 0.8
                heightNew = widthNew

            }
//            if ratioFromArray > 1{
//                heightNew  = heightNew / ratioFromArray
//                widthNew  = side * ratioFromArray
//            }
//            print(aspectRatioArr[indexPath.row].name,CGSize(width: widthNew , height: heightNew))
//            cell.imageView.frame = CGRect(origin: CGPoint.zero, size: CGSize(width: widthNew , height: heightNew))
            cell.imageHeightCons.constant = heightNew
            
            
            cell.imageView.contentMode = .scaleAspectFit
//            cell.imageView.backgroundColor = UIColor(displayP3Red: 51/255.0, green: 51/255.0, blue: 51/255.0, alpha: 1)
            let ratioName =  aspectRatioArr[indexPath.item].name
            cell.titleLabel.font = UIFont(name: FONT_REGULAR, size: 15)
            
            var imageNew = UIImage(named: ratioName)
//            imageNew = imageNew?.withRenderingMode(.alwaysTemplate)

            cell.imageView.image = imageNew
            cell.imageView.isHidden = false
//            cell.titleLabel.text = ratioName
            cell.titleLabel.isHidden = true
            cell.imageView.layer.cornerRadius = 5.0
            if selectedAspectRatio.name == ratioName {
//                cell.titleLabel.textColor =  AppThemeManager.shared.currentTheme().TintColor
                
                imageNew = UIImage(named: ratiosImages[indexPath.row])
                cell.imageView.image = imageNew

//                cell.imageView.tintColor = AppThemeManager.shared.currentTheme().TintColor
            }else {
//                cell.imageView.tintColor =  UIColor.gray
            }
            
            
            return cell

        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout
        collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if collectionView == colorCV {
            selectedColorIndex = indexPath
            
            if indexPath.section == 1{
                
                let grad = gradColorsArr[indexPath.row]
                delegate?.cropViewChangeGradColor(color: grad)

            }else{
                
                if(indexPath.item > 2){
                    //                selectedColorStr = colorArr [indexPath.item - 1] as! String
                    //                let color =  UIColor.hexStringToUIColor(selectedColorStr)
                    let color =  newcolorsArr[indexPath.row-3]
                    delegate?.cropViewChangeColor(color: color)
                }
                else{
                    selectedColorStr =  "\(indexPath.row)"
                    delegate?.cropViewChangeBlurColor(blurType: indexPath.row)
                    //                delegate?.cropViewChangeColor(color: nil)
                }
            }
        } 
        else if(collectionView == aspectRatioCV)        {
            selectedAspectRatio = aspectRatioArr[indexPath.item]
            let ratio:CGFloat = selectedAspectRatio.ratio
            delegate?.cropViewChangeRatio(ratio: ratio)
        }
        collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        collectionView.reloadData()
    }
    
}

extension VideoEdCropView {
    class func fromNib<T: VideoEdCropView>() -> T {
        return Bundle.main.loadNibNamed(String(describing: T.self), owner: nil, options: nil)![0] as! T
    }
}
