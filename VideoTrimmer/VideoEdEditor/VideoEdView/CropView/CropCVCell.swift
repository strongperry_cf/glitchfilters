//  CropCVCell.swift
//  VideoTrimmer
//
//  Created by strongapps on 02/05/19.
//  Copyright © 2019 strongapps. All rights reserved.


import UIKit

class CropCVCell: UICollectionViewCell {

    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLbl: UILabel!
    
    @IBOutlet weak var topYCons: NSLayoutConstraint!
    override func awakeFromNib() {
        super.awakeFromNib()

    }

}
