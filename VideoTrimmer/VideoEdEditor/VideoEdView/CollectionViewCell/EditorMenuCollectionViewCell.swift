//
//  EditorMenuCollectionViewCell.swift
//  
//
//  Created by strongapps on 18/06/19.
//

import UIKit

class EditorMenuCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var thumbnail: UIImageView!
    @IBOutlet var textLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
