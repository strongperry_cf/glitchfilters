
//  VideoEdAdjustCVCell.swift
//  VideoTrimmer
//  Created by strongapps on 25/04/19.
//  Copyright © 2019 strongapps. All rights reserved.

import UIKit

class VideoEdAdjustCVCell: UICollectionViewCell {

    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var titleLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()

    }

}
