//
//  GlitchFilterView.swift
//  VideoTrimmer
//
//  Created by strongapps on 10/07/19.
//  Copyright © 2019 strongapps. All rights reserved.
//

import UIKit

protocol GlitchFilterViewDelegate: class {
    func didSelectGlitchFilter(index : Int)
    func dismissGlitchView()
}
class GlitchFilterView: UIView {

    var indexSelected = 0
    @IBOutlet var filterCV: UICollectionView!
    weak var delegate               :GlitchFilterViewDelegate?

    class func initWithFrame(frame:CGRect) -> GlitchFilterView {
        let view = self.fromNib()
        view.frame = frame
        return view
    }
    class func fromNib<T: GlitchFilterView>() -> T {
        return Bundle.main.loadNibNamed(String(describing: T.self), owner: nil, options: nil)![0] as! T
    }
    
    func setupUI() {

        // --- set Up Filter Collection View ----
        filterCV.delegate = self
        filterCV.dataSource =  self
        filterCV.register(UINib(nibName: "FilterCV_Cell", bundle: nil), forCellWithReuseIdentifier: "FilterCV_Cell")

    }
    
    @IBAction func cancelBtnAct(_ sender: UIButton) {
        self.isHidden = true
        if delegate != nil {
            delegate?.dismissGlitchView()
        }
    }
    
    @IBAction func doneBtnAct(_ sender: UIButton) {
        self.isHidden = true
        if delegate != nil {
            delegate?.dismissGlitchView()
        }
        
        
        
    }

}
//MARK:- Collection View Methods
extension GlitchFilterView :  UICollectionViewDelegate,UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 15
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 16.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 16.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let height = collectionView.frame.size.height * 0.85
        let width = height / 1.3
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 16.0, bottom: 0, right: 16.0)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell:FilterCV_Cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FilterCV_Cell", for: indexPath) as! FilterCV_Cell
        cell.layer.masksToBounds        = true
        cell.backView.backgroundColor   = UIColor.clear
        cell.backgroundColor            = UIColor.clear
        cell.categoryImageView.backgroundColor = UIColor.clear
        cell.categorytitleLabel.isHidden = true
        cell.effectNameLbl.isHidden     = true
//        cell.thumbnailView.layoutIfNeeded()
        cell.thumbnailView.layer.cornerRadius = 30
        if indexSelected == indexPath.row{

            cell.thumbnailView.layer.borderWidth = 1.5
            cell.thumbnailView.layer.borderColor = AppThemeManager.shared.currentTheme().TintColor.cgColor
        }else{
            cell.thumbnailView.layer.borderWidth = 0
        }
        
        var image = UIImage(named: "girl.jpg")
        if (indexPath.item>0){
            image = UIImage(named: "glitchThumbnail\(Int(indexPath.item))")
        }
        
        cell.thumbnailView.image = image
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    
        if !IAPManagerLocal.sharedInstance.isInAppPurchased() && indexPath.row >= FREE_GLITCH_EFFCTS{
            IAPManagerLocal.sharedInstance.openInapScreen()
            return
        }

        indexSelected = indexPath.row
        
        print(String(format: "Glitch Filter index Path item is :%i", indexPath.item))
        filterCV.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        filterCV.reloadData()
        
        if delegate != nil {
            delegate?.didSelectGlitchFilter(index: indexPath.row)
            
        }
    }
    

}

