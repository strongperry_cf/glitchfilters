//  VideoEdTimelineView.swift
//  VideoTrimmer
//  Created by strongapps on 29/04/19.
//  Copyright © 2019 strongapps. All rights reserved.

import UIKit


@objc protocol TimelineDelegate : class  {
 
    func timelineMusicDidChange();
    func timelineDoneBtnClicked();
    func timelineMusicTrimChange(startTime:Double,endTime:Double);
    @objc optional func timeLineMusicTrimBeganChange();
  
}

class VideoEdTimelineView: UIView {

    weak var delegate                       :TimelineDelegate?
    @IBOutlet weak var mainView             :UIView!
    @IBOutlet weak var bottomView           :UIView!
    @IBOutlet weak var timeLineWaveView     :UIView!
    
    @IBOutlet weak var lblRangeEndTime      :UILabel!
    @IBOutlet weak var lblRangeStartTime    :UILabel!
    @IBOutlet weak var btnDoneMusicTrim     :UIButton!
    @IBOutlet weak var btnSelectMusic       :UIButton!
    @IBOutlet weak var multiSlider          : MultiSlider!
    @IBOutlet var startTimeSlider: UISlider!
    
    @IBOutlet var musicHeadlineLabel: UILabel!
    
    override func awakeFromNib() {
        
        self.startTimeSlider.setThumbImage(#imageLiteral(resourceName: "musicObject"), for: .normal)
        self.startTimeSlider.addTarget(self, action: #selector(onSliderValChanged(slider:event:)), for: .valueChanged)

//        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
        self.startTimeSlider.isContinuous = false
        self.multiSlider.addTarget(self, action: #selector(self.sliderExitTouch(_:)), for: .touchUpInside)
        self.multiSlider.addTarget(self, action: #selector(self.sliderChanged(_:)), for: .valueChanged)
        
        self.multiSlider.orientation = .horizontal
        self.multiSlider.outerTrackColor = .lightGray
        self.multiSlider.trackWidth = 2
        
        self.multiSlider.value = [0, 27.4]
        self.multiSlider.maximumValue = 27.4
        self.multiSlider.minimumValue = 0
        
//        }
        self.multiSlider.valueLabelPosition = .top // .notAnAttribute = don't show labels


        musicHeadlineLabel.font = UIFont(name: FONT_BOLD, size: 23)
        createWaves()
    }
    
    func createWaves() {
        
        let waveWidth =  SCREEN_WIDTH - 82
     
        let image:UIImage = UIImage(named: "bars")!
        let imageWidth = image.size.width
        
        let count:Int = Int(waveWidth / imageWidth) + 1
        
        var x:CGFloat = 0.0
        
        for _ in 0...count  {

            let imageView  = UIImageView(image: image)
            
            imageView.frame = CGRect(x: x, y: 0.0, width: image.size.width, height: image.size.height)
            timeLineWaveView.addSubview(imageView)
            
            x += image.size.width
            x += 3.0
        }
        

    }
    func playSelectedSong(){
        
        if multiSlider != nil{
            
            delegate?.timelineMusicTrimChange(startTime: Double(multiSlider.value[0]), endTime: Double(multiSlider.value[1]))
        }
        
    }
    
    func  updateTimeLineDuration(duration:CGFloat)  {
        
        
        self.multiSlider.value = [0, duration]
        self.multiSlider.maximumValue = CGFloat(duration)

        lblRangeStartTime.text  = "00:00"
        lblRangeEndTime.text  = VideoEdVideoManager().stringFromat(seconds: TimeInterval(duration))
        


    }
    @objc func onSliderValChanged(slider: UISlider, event: UIEvent) {
        if let touchEvent = event.allTouches?.first {
            switch touchEvent.phase {
            case .began:
                debugPrint("began ")
            case .moved:
                // handle drag moved
                debugPrint("moved ")
                
            case .ended:
                // handle drag ended
//                debugPrint("ended ",slider.value)
                delegate?.timelineMusicTrimChange(startTime: Double(multiSlider.value[0]), endTime: Double(multiSlider.value[1]))
                
            default:
                break
            }
        }
    }
    @objc func sliderChanged(_ slider: MultiSlider) {
        
        lblRangeStartTime.text  = VideoEdVideoManager().stringFromat(seconds: TimeInterval(slider.value[0]))
        lblRangeEndTime.text  = VideoEdVideoManager().stringFromat(seconds: TimeInterval(slider.value[1]))


    }

    @objc func sliderExitTouch(_ slider: MultiSlider) {
        
//        debugPrint("min ",slider.value[0])
//        debugPrint("max",slider.value[1])
        
        VideoEdVideoMakeManager.audioStartTime = slider.value[0]
        VideoEdVideoMakeManager.audioEndTime = slider.value[1]
        
        lblRangeStartTime.text  = VideoEdVideoManager().stringFromat(seconds: TimeInterval(slider.value[0]))
        lblRangeEndTime.text  = VideoEdVideoManager().stringFromat(seconds: TimeInterval(slider.value[1]))
        delegate?.timelineMusicTrimChange(startTime: Double(slider.value[0]), endTime: Double(slider.value[1]))

        
    }
    
    @IBAction func ActionSelectMusic(_ sender: Any) {
        
        if(delegate != nil){
            delegate?.timelineMusicDidChange()
        }
    }
    
    @IBAction func ActionDoneMusicTrim(_ sender: Any) {
        
        if(delegate != nil){
            delegate?.timelineDoneBtnClicked()
        }
    }
    
}
extension VideoEdTimelineView {
    class func fromNib<T: VideoEdTimelineView>() -> T {
        return Bundle.main.loadNibNamed(String(describing: T.self), owner: nil, options: nil)![0] as! T
    }
}

