//
//  CategoriesCell.swift
//  Video Editorial
//
//  Created by strongapps on 18/02/19.
//  Copyright © 2019 strongapps. All rights reserved.
//

import UIKit

class CategoriesCell: UICollectionViewCell {
    
    @IBOutlet weak var lblName: UILabel!
    
    var isSelectedCell = false {
        didSet{
            lblName.textColor = isSelectedCell ? UIColor.red : UIColor.black
        }
    }
    
    func setupData(with name: String){
        lblName.text = getUpdateHeader(with: name)
    }
    
    func getUpdateHeader(with string: String) -> String {
        var finalHeader = ""
        let subStrings = string.components(separatedBy: "_")
        for subString in subStrings{
            finalHeader += String(subString.prefix(1).uppercased() + subString.lowercased().dropFirst()) + " "
        }
        return finalHeader
    }
    
}
