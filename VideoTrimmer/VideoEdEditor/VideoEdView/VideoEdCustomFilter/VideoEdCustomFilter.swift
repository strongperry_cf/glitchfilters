//
//  VideoEdCustomFilter.swift
//  Video Editorial
//
//  Created by strongapps on 18/01/19.
//  Copyright © 2019 strongapps. All rights reserved.
//

import UIKit

enum CustomFilterType: Int {
    case None = 00, A1 = 01, A2 = 02, A3 = 03, A4 = 04, A5 = 05, A6 = 06, A7 = 07, A8 = 08, A9 = 09, A10 = 10, A11 = 11, A12 = 12, A13 = 13, A14 = 14, A15 = 15, A16 = 16, A17 = 17, A18 = 18, A19 = 19, A20 = 20, A21 = 21, A22 = 22, A23 = 23, A24 = 24, A25 = 25, A26 = 26, A27 = 27
    
    static let count: Int = {
        var max: Int = 0
        while let _ = CustomFilterType(rawValue: max) { max += 1 }
        return max
    }()
}

class VideoEdCustomFilter {

    static func getACVFilterWith(_ type: CustomFilterType) -> GPUImageFilterGroup {
        var filter: GPUImageFilter?
        let groupFilter = GPUImageFilterGroup()
        if type == .None {
            filter = GPUImageFilter()
        } else {
            filter = GPUImageToneCurveFilter(acv: "VideoEd\(type.rawValue)")
        }
        groupFilter.addFilter(filter)
        groupFilter.initialFilters = [filter!]
        groupFilter.terminalFilter = filter
        return groupFilter
    }
    
    static func getFilteredImageWithACVFilter(_ type: CustomFilterType,to image: UIImage) -> UIImage? {
        var newImage : UIImage? = image
        var filter: GPUImageFilter?
        if type == .None {
            filter = GPUImageFilter()
        } else {
            filter = GPUImageToneCurveFilter(acv: "VideoEd\(type.rawValue)")
            newImage = filter?.image(byFilteringImage: newImage)
        }
        return newImage
    }
    
    static func getShaderFilter() -> GPUImageFilter {
        let filter = GPUImageFilter(fragmentShaderFromFile: "VintageBigFilter")
        filter?.setInteger(0, forUniformName: "type")
        filter?.setFloat(GLfloat(SCREEN_WIDTH), forUniformName: "frameWidth")
        filter?.setFloat(GLfloat(SCREEN_WIDTH), forUniformName: "frameHeight")
        filter?.setPoint(CGPoint(x: 0.5, y: 0.5), forUniformName: "iMouse")
        filter?.setFloat(0.5, forUniformName: "sliderValue")
        filter?.setFloat(0.5, forUniformName: "sliderValue2")
        filter?.setFloat(0.5, forUniformName: "sliderValue3")
        filter?.setFloat(3.33, forUniformName: "iGlobalTime")
        filter?.setFloat(1.0, forUniformName: "aspectRatio")
        return filter!
    }
}
