//
//  SpeedView.swift
//  VideoTrimmer
//
//  Created by strongapps on 20/06/19.
//  Copyright © 2019 strongapps. All rights reserved.
//

import UIKit

protocol SpeedViewDelegate : class{
    
    func speedDidChange(speed:CGFloat)
    func speedDoneAction(speed:CGFloat)
    func speedCancelAction()

}
class SpeedView: UIView {

    public var videoData:VideoData?
    weak var delegate                       :SpeedViewDelegate?
    public var videoSeletedIndex             =    -1

    @IBOutlet weak var speedSlider: UISlider!
    @IBOutlet weak var speedValueLabel: UILabel!
    @IBOutlet weak var newJMSlider: JMMarkSlider!
    @IBAction func sliderChanged(_ sender: UISlider) {

        
    }
    @IBAction func newSliderTouchupInside(_ sender: JMMarkSlider) {
        
        debugPrint("newSliderTouchupInside value=> ",sender.value)
        
        let intValue = Int(sender.value * 10)
        let floaVal = Float(Double(intValue) * 0.1)
        
        speedValueLabel.text = "\(floaVal)x"

        
        var newSpeed = 1.0
        
        if sender.value < 0.8{
            newSpeed = 0.5
        }else if sender.value  < 1.3{
            newSpeed = 1.0
        }else if sender.value  < 1.7{
            newSpeed = 1.5
            
        }else {
            newSpeed = 2.0
        }
        if delegate != nil {
            delegate?.speedDidChange(speed: CGFloat(newSpeed))
        }
    }
    @IBAction func sliderUpdation(_ sender: UISlider) {
        
        debugPrint("sliderUpdation value=> ",sender.value)

        
        var newSpeed = 1.0

        if sender.value < 0.8{
            newSpeed = 0.5
        }else if sender.value  < 1.3{
            newSpeed = 1.0
        }else if sender.value  < 1.7{
            newSpeed = 1.5
            
        }else {
            newSpeed = 2.0
        }
        speedValueLabel.text = "\(newSpeed)x"
        if delegate != nil {
            delegate?.speedDidChange(speed: CGFloat(newSpeed))
        }
    }
    class func initWithFrame(frame:CGRect) -> SpeedView {
        let view = self.fromNib()
        view.frame = frame
        return view
    }
    class func fromNib<T: SpeedView>() -> T {
        return Bundle.main.loadNibNamed(String(describing: T.self), owner: nil, options: nil)![0] as! T
    }
    func setupUI() {
        speedSlider.setMaximumTrackImage(#imageLiteral(resourceName: "minTrack.png"), for: .normal)
        speedSlider.setMinimumTrackImage(#imageLiteral(resourceName: "ratios_sel34"), for: .normal)
        

        newJMSlider.markWidth = 0.0
        newJMSlider.unselectedBarColor = .gray
        newJMSlider.selectedBarColor = UIColor(displayP3Red: 229/255.0, green: 53/255.0, blue: 108/255.0, alpha: 1.0)
//        newJMSlider.markPositions = [0.5,1.0,0.5,2.0]

        
//        speedSlider.setThumbImage(UIImage(named: "ic_slider_line"), for: .normal)
//        speedSlider.tintColor = AppThemeManager.shared.currentTheme().TintColor
//        speedSlider.minimumTrackTintColor = AppThemeManager.shared.currentTheme().TintColor
        if videoData != nil{
            speedSlider.value = videoData!.videoSpeed
            
        }
    }
    @IBAction func cancelBtnAct(_ sender: UIButton) {
        self.isHidden = true
        if delegate != nil {
            delegate?.speedCancelAction()
        }
    }
    
    @IBAction func doneBtnAct(_ sender: UIButton) {
        self.isHidden = true
        if delegate != nil {
            

        }
    }
    @IBAction func speedAction(_ sender: UIButton) {
        
        if !sender.isSelected{
            

            sender.isSelected = true
            
            var speedValue = 0.5
            
            if sender.tag == 101{
                speedValue = 0.5

            }else if sender.tag == 102{
                speedValue = 1.0

            }else if sender.tag == 103{
                speedValue = 1.5

            }else if sender.tag == 104{
                speedValue = 2.0

            }
            if delegate != nil {
                delegate?.speedDidChange(speed: CGFloat(speedValue))
            }
        }
        
    }
    
}
