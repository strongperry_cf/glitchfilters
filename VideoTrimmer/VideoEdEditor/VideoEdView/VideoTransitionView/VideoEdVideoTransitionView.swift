
import UIKit

public protocol VideoTransitionViewDelegate: class {
    func transViewVideoDidSelect(index:Int);
    func transViewTransitionDidSelect(index:Int)
    func transViewAddNewVideo()
    func transViewDeleteVideo(index:Int)
    func transViewCopyVideo(index:Int)
}


class VideoEdVideoTransitionView: UIView {

    @IBOutlet weak var topTitleLbl      : UILabel!
    @IBOutlet weak var videosCV         : UICollectionView!
    public var videoDataArr             : [VideoData] = []
    @IBOutlet weak var deleteBtn        : UIButton!
    @IBOutlet weak var copyBtn        : UIButton!
    
    let optionBackColor                 : UIColor   = UIColor(red: 255.0/255.0, green: 210.0/255.0, blue: 0.0, alpha: 1.0)
    
    public weak var delegate: VideoTransitionViewDelegate?

    var thumbnailArr                :[UIImage] = []
    var itemSpacing :CGFloat = 10
    public var videoSeletedIndex               :IndexPath = IndexPath(item: 0, section: 00)


    public init() {
        super.init(frame: .zero)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    private func nibSetup() {
        debugPrint(" nibSetup() ")
        
        backgroundColor = .clear
        let view = loadViewFromNib()
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        view.translatesAutoresizingMaskIntoConstraints = true
        addSubview(view)
    }
    private func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        let nibView = nib.instantiate(withOwner: self, options: nil).first as! UIView
        return nibView
    }
    
    override func awakeFromNib() {
        
        super.awakeFromNib()
        debugPrint(" awakeFromNib ")
        nibSetup()
        self.backgroundColor = UIColor.black

    }
    
    
    
    func createSquareFrame(size :CGSize ) -> CGRect {
        let length  = min(size.width,size.height)
        let frame = CGRect(x: (size.width / 2.0) - (length / 2.0), y: (size.height / 2.0) - (length / 2.0), width: length, height: length)
        return frame
    }
    
    
    @IBAction func copyBtnAct(_ sender: UIButton) {
        

    }
    
    @IBAction func deleteBtnAct(_ sender: UIButton) {
        
       
    }
    
    @objc func deleteSingleVideo(sender:UIButton)  {
        
       
    }
    
}

extension VideoEdVideoTransitionView: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return videoDataArr.count + (videoDataArr.count - 1) + 1; // VideosCell + Transition Cell + Plus Btn cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if ((videoDataArr.count + (videoDataArr.count - 1)) == indexPath.item)        {
            return CGSize(width: 80.0 , height: 80.0 )
        }
        else  if (indexPath.item % 2 == 0)        {
            // return CGSize(width: collectionView.frame.size.height - 8.0 , height: collectionView.frame.size.height - 8.0 )
            return CGSize(width: 80.0 , height: 80.0 )
        }   else{
            return CGSize(width: 46, height: 46)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return itemSpacing
    }
    
    func collectionView(_ collectionView: UICollectionView, layout
        collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return itemSpacing
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        return UIEdgeInsets(top: 0, left: itemSpacing, bottom: 0, right: itemSpacing)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoryCVCell", for: indexPath as IndexPath) as! CategoryCVCell

        cell.thumbnailView.contentMode =  .scaleAspectFit
        cell.thumbnailView.layer.cornerRadius =  (cell.frame.size.height - 20.0) / 2.0
        cell.deleteBtn.isHidden = true
        cell.thumbnailView.backgroundColor = UIColor.clear

        
        if ((videoDataArr.count + (videoDataArr.count - 1)) == indexPath.item)        {
            // Add Video Cell
            // let plusImage = #imageLiteral(resourceName: "ic_add_black")
            let plusImage = UIImage(named: "ic_add_black")
            cell.thumbnailView.image = plusImage
            cell.thumbnailView.layer.borderWidth = 0.0
            cell.thumbnailView.layer.borderColor = UIColor.clear.cgColor
            cell.thumbnailView.backgroundColor = UIColor.darkGray
            cell.thumbnailView.contentMode =  .center

        }
        else if (indexPath.item % 2 == 0)        {
            //  Video Cell
            let currentIndex = indexPath.item / 2
            cell.thumbnailView.image = thumbnailArr[currentIndex]
            cell.thumbnailView.layer.borderWidth = 2.0
            cell.thumbnailView.layer.borderColor = UIColor.white.cgColor
            //cell.deleteBtn.tag = indexPath.item / 2
            //cell.deleteBtn.addTarget(self, action: #selector(deleteSingleVideo(sender:)), for: .touchUpInside)
            

            
            // Selected Color
            if (videoSeletedIndex.item == currentIndex)      {
                cell.thumbnailView.layer.borderColor = AppThemeManager.shared.currentTheme().TintColor.cgColor
            }
                
                
        }
        else {
            
            let currentIndex = (indexPath.item-1)/2
            let videoData = videoDataArr[currentIndex]
            
            if (videoData.transitionType == 0)            {
                cell.thumbnailView.image = UIImage(named:"smallPlus")
            }else {
                cell.thumbnailView.image = UIImage(named:"fade")
            }
            // TransitionCell
            // cell.thumbnailView.backgroundColor = UIColor.yellow
            cell.thumbnailView.backgroundColor = optionBackColor
            cell.thumbnailView.contentMode =  .center
            cell.thumbnailView.layer.borderColor = UIColor.clear.cgColor
            cell.thumbnailView.layer.borderWidth = 0.0

        }
      
        return cell;
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if ((videoDataArr.count + (videoDataArr.count - 1)) == indexPath.item)        {
            // Add Video Cell
            delegate?.transViewAddNewVideo()
        }
        else if (indexPath.item % 2 == 0)        {
            //  Video Cell
            let currentIndex = indexPath.item / 2
            
            if videoSeletedIndex.item != currentIndex            {
                videoSeletedIndex.item = currentIndex
                deleteBtn.isHidden = false
                copyBtn.isHidden = false
            }
            else {
                videoSeletedIndex.item = -1
                deleteBtn.isHidden = true
                copyBtn.isHidden = true
            }
            
            // Dont Show Delete Button When one Video is Remaining
            if videoDataArr.count < 2      {
                deleteBtn.isHidden = true            }
            
            delegate?.transViewVideoDidSelect(index:videoSeletedIndex.item )
            videosCV.reloadData()
            videosCV.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        }
        else {
            // TransitionCell
            delegate?.transViewTransitionDidSelect(index:(indexPath.item - 1) / 2)
        }
    }
    
}


extension PHAsset {
    
    var thumbnailImage : UIImage {
        get {
            var finalImage = UIImage()
            let option = PHImageRequestOptions()
            option.isSynchronous = true
            option.resizeMode = PHImageRequestOptionsResizeMode.exact
            PHImageManager.default().requestImage(for: self, targetSize: CGSize(width: 350, height: 350), contentMode: .aspectFill, options: option, resultHandler: {(result, info)->Void in
                finalImage = result!
            })
            return finalImage
        }
    }
}
