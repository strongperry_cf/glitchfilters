//
//  FilterViewCell.swift
//  Video Editorial
//
//  Created by strongapps on 12/02/19.
//  Copyright © 2019 strongapps. All rights reserved.
//

import UIKit

class FilterViewCell: UICollectionViewCell {
    
    static let reuseIdentifier = "FilterViewCell"
    
    @IBOutlet weak var imgView: UIImageView!
    
    @IBOutlet weak var lblName: UILabel!
    var isSelectedCell = false {
        didSet{
                if isSelectedCell{
                    lblName.backgroundColor = AppThemeManager.shared.currentTheme().TintColor
                }else{
                    lblName.backgroundColor = UIColor.black
                }
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()

    }
    
    func updateData(image: UIImage,name: String,isSelected: Bool = false){
        imgView.image = image
        imgView.contentMode = .scaleAspectFill
        lblName.text = name
        isSelectedCell = isSelected
        lblName.frame = CGRect(x: 0, y: self.height * 0.8, width: self.width, height: self.height * 0.2)
        lblName.minimumScaleFactor = 0.3
        lblName.adjustsFontSizeToFitWidth = true
        imgView.frame = CGRect(x: 0, y: 0, width: self.width, height: self.height)
        self.setNeedsLayout()
        
    }
    
    
}

extension FilterViewCell {
    class func fromNib<T: FilterViewCell>() -> T {
        return Bundle.main.loadNibNamed(String(describing: T.self), owner: nil, options: nil)![0] as! T
    }
}
