//
//  VideoEdFilterView.swift
//  VideoTrimmer
//
//  Created by strongapps on 08/04/19.
//  Copyright © 2019 strongapps. All rights reserved.
//

import UIKit


protocol FilterViewDelegate: class {
    func didSelectFilter()
    func didSelectACVFilter(filter:GPUImageFilterGroup,alpha:CGFloat)
    func updateACVFilter(alpha:CGFloat)
}

class VideoEdFilterView: UIView , UICollectionViewDelegate ,UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    @IBOutlet weak var sliderView       :UIView!
    @IBOutlet weak var slider           :UISlider!
    @IBOutlet weak var cv_filters       :UICollectionView!
    
    // GPU Objects
    //var gpuImageView                :GPUImageView?
    //var gpuMovie                    :GPUImageMovie?
    //var alphaBlendFilter            :GPUImageAlphaBlendFilter?
    var filters                     :VideoEdFilters?
    weak var delegate               :FilterViewDelegate?
    var selectedFilterIndex         :IndexPath? = IndexPath(row: 0, section: 0) 
    
    var previousFilter              :VideoEdFilters?
    
    @IBOutlet weak var btnCancel    :UIButton!
    @IBOutlet weak var btnDone      :UIButton!
    
    let filterNames = ["NONE","VIVID","WINDY","CHILL","JOKER","RAINDAY","ELMWOOD","PEACE","NOSTALGIA","X-PRO","ENCEIL","GORGEOUS","GLEAM","BROOKLN","VINTAGE","ART","ELVIC","DELMA","OLD SKOOL","GANGAM","JESSIE","KONICA","CHILDHOOD","FIRST LOVE","HERO","DAYLIGHT","COMMON","TIMBER","PASTEL","SWEET"]
    
    
    var filterImagesArr = [UIImage]()
    var firstFrame : UIImage! {
        didSet{
            if firstFrame != nil{
                cv_filters.reloadData()
            }
        }
    }
    
    override func removeFromSuperview() {
        super.removeFromSuperview()
        closeSliderView(self)
    }
    
    override func awakeFromNib() {
        
        super.awakeFromNib()

        cv_filters.register(UINib.init(nibName: String(describing: FilterViewCell.reuseIdentifier), bundle: nil), forCellWithReuseIdentifier: FilterViewCell.reuseIdentifier)
        cv_filters.dataSource = self
        cv_filters.delegate = self
        cv_filters.backgroundColor = .clear
        slider.addTarget(self, action: #selector(touchEvent(slider:for:)), for: .valueChanged)
        slider.minimumTrackTintColor = AppThemeManager.shared.currentTheme().TintColor
        slider.setThumbImage(#imageLiteral(resourceName: "ic_slider_line"), for: .normal)
    }
    
    @objc func touchEvent(slider: UISlider,for event: UIEvent){
        if let touchEvent = event.allTouches?.first {
            switch touchEvent.phase {
            case .began:
                print("slider began editing")
            case .moved:
                filters?.value = slider.value
                delegate?.updateACVFilter(alpha: CGFloat(slider.value))
               // alphaBlendFilter?.mix = CGFloat(slider.value)
            // handle drag began
            default:
                break
            }
        }
    }
    
//    func setupData(with imageView: GPUImageView){
//        self.gpuImageView = imageView
//        cv_filters.reloadData()
//    }
    
//    class func initWith(andImageView: GPUImageView,gpuMovie: GPUImageMovie) -> VideoEdFilterView {
//        let view = self.fromNib()
//        view.gpuImageView = andImageView
//        view.gpuMovie = gpuMovie
//        view.initView()
//        return view
//    }
    
    
    class func initWithFrame(frame:CGRect) -> VideoEdFilterView {
        let view = self.fromNib()
        view.frame = frame
        return view
    }
    
    
    func initView() {
        cv_filters.reloadData()
    }
    
    func updateWithModel(_ model: VideoEdFilters) {
        
        // alphaBlendFilter = GPUImageAlphaBlendFilter()
        let filter = VideoEdCustomFilter.getACVFilterWith(CustomFilterType(rawValue: model.type!.rawValue)!)
        // gpuMovie?.removeAllTargets()
        // gpuMovie?.addTarget(filter)
        // gpuMovie?.addTarget(alphaBlendFilter)
        // filter.addTarget(alphaBlendFilter)
        // alphaBlendFilter?.mix = 1.0
        // alphaBlendFilter?.addTarget(gpuImageView)
        
        /*
        if let controller = self.parentViewController as? VideoEdEditVC{
            // controller.alphaBlend = alphaBlendFilter
            controller.firstFilter = filter
        }*/


        delegate?.didSelectACVFilter(filter: filter, alpha: 1.0)
        
        //update slider
        updateSliderWithType(model)
        filters = model
    }
    
    func updateSliderWithType(_ model: VideoEdFilters) {
        slider.minimumValue = 0.0
        slider.maximumValue = 1.0
        slider.value = 1.0
        guard let newValue = model.value else { return }
        slider.value = newValue
        //alphaBlendFilter?.mix = CGFloat(newValue)
    }
    
    @IBAction func closeBtnAct(_ sender: UIButton) {
        
        if sliderView.isHidden == false{
            sliderView.isHidden = true
        }else{
            self.isHidden = true
        }
        
    }
    
    
    @IBAction func closeSliderView(_ sender: Any) {
        sliderView.isHidden = true
    }
    
    @IBAction func sliderValueEvents(_ sender: UISlider) {
        filters?.value = sender.value
        delegate?.updateACVFilter(alpha: CGFloat(sender.value))
        // alphaBlendFilter?.mix = CGFloat(sender.value)
        
    }
    
    
    //MARK:- Collection View Methods
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        print("CustomFilterType.count >>> ", CustomFilterType.count)
        return CustomFilterType.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 16.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 16.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let height = collectionView.frame.size.height * 0.85
        let width = height / 1.3
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 16.0, bottom: 0, right: 16.0)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: FilterViewCell.reuseIdentifier, for: indexPath) as! FilterViewCell
        var image = UIImage()
//        if firstFrame != nil{
            image = firstFrame
//            if filterImagesArr.count > indexPath.row{
//                image = filterImagesArr[indexPath.row]
//            }else{
//                if let filteredImage = VideoEdCustomFilter.getFilteredImageWithACVFilter(CustomFilterType(rawValue: indexPath.row) ?? .None, to: indexPath.row == 0 ? UIImage(named: "ic_none")! : image){
//                    image = filteredImage
//                    filterImagesArr.append(image)
//                }
//            }
//        }
        
        if let filteredImage = VideoEdCustomFilter.getFilteredImageWithACVFilter(CustomFilterType(rawValue: indexPath.row) ?? .None, to: indexPath.row == 0 ? UIImage(named: "ic_none")! : image){
            image = filteredImage
//            filterImagesArr.append(image)
            cell.updateData(image: image, name: filterNames[indexPath.row])
        }

        cell.lblName.font = UIFont(name: FONT_REGULAR, size: 13)
        cell.isSelectedCell = (selectedFilterIndex != nil && indexPath.row == selectedFilterIndex?.row) ? true: false
        cell.layer.cornerRadius = 5.0
        cell.clipsToBounds = true
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let previousIndex = selectedFilterIndex
        
        if !IAPManagerLocal.sharedInstance.isInAppPurchased() && indexPath.row >= FREE_FILTERS{
            IAPManagerLocal.sharedInstance.openInapScreen()
            return
        }

        
        if selectedFilterIndex?.item == indexPath.item{
            sliderView.isHidden = false
            updateSliderWithType(filters!)
            selectedFilterIndex = indexPath
            return
        }
        
        let filterModel = VideoEdFilters(type: CustomFilterType(rawValue: indexPath.row) ?? .None)
        filterModel.value = 1.0
        delegate?.didSelectFilter()
        updateWithModel(filterModel)
        selectedFilterIndex = indexPath
        
        
        if previousIndex != nil{
            collectionView.reloadItems(at: [previousIndex!,selectedFilterIndex!])
        }else{
            collectionView.reloadItems(at: [selectedFilterIndex!])
        }
        collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
    
    }
    
    
    //MARK:- Actions
    @IBAction func btnCancelClicked(_ sender: UIButton) {
        selectedFilterIndex = nil
        if previousFilter != nil{
            delegate?.didSelectFilter()
            updateWithModel(previousFilter!)
        }else{
            //gpuMovie?.removeAllTargets()
            //alphaBlendFilter?.removeAllTargets()
            //gpuMovie?.addTarget(gpuImageView!)
            if let controller = self.parentViewController as? VideoEdEditVC {
                controller.alphaBlend = nil
                controller.firstFilter = nil
            }
        }
        UIView.animate(withDuration: 0.5, animations: {
            self.frame.origin.y += 160
        }) { (true) in
            self.removeFromSuperview()
        }
    }
    
    @IBAction func btnDoneClicked(_ sender: UIButton) {
        previousFilter = filters
        UIView.animate(withDuration: 0.5, animations: {
            self.frame.origin.y += 160
        }) { (true) in
            self.removeFromSuperview()
        }
    }
    
    func reset(){
        filterImagesArr.removeAll()
        // gpuMovie?.removeAllTargets()
        // alphaBlendFilter?.removeAllTargets()
    }
    
    
}

extension VideoEdFilterView {
    class func fromNib<T: VideoEdFilterView>() -> T {
        return Bundle.main.loadNibNamed(String(describing: T.self), owner: nil, options: nil)![0] as! T
    }
}
