//
//  FrontScreenVC.swift
//  camerafilter
//
//

import UIKit
import GoogleMobileAds

class FrontScreenVC: UIViewController {
    var interstitial: GADInterstitial!

    override func viewDidLoad() {
        super.viewDidLoad()

        interstitial = createAndLoadInterstitial()
        
    }


    @IBAction func settingButtonAction(_ sender: UIButton) {
        

        let picker = SettingsViewController(nibName: "SettingsViewController", bundle: nil)
        self.navigationController?.pushViewController(picker, animated: true)

    }
    func createAndLoadInterstitial() -> GADInterstitial {
      let interstitiallocal = GADInterstitial(adUnitID: GOOGLE_UNIT_AD)
      interstitiallocal.delegate = self
      interstitiallocal.load(GADRequest())
      return interstitiallocal
    }

    @IBAction func proButtonAction(_ sender: Any) {
        
    }
    @IBAction func adsButtonAction(_ sender: UIButton) {
        if interstitial.isReady {
          interstitial.present(fromRootViewController: self)
        } else {
          print("Ad wasn't ready")
        }

        
    }
    
    @IBAction func editButtonAction(_ sender: UIButton) {
        
        let picker = VideoEdPickerVC(nibName: "VideoEdPickerVC", bundle: nil)
        picker.mediaTypePrefferred = .video
        self.navigationController?.pushViewController(picker, animated: true)
        
        

//        let storyboard = UIStoryboard(name: "Main", bundle: nil)
//        let vc = storyboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
////        vc.videoUrl = videoUrl
//        //        vc.delegate =  self
//        GlobalHelper.dispatchMainAfter(time: .now() + 0.2, execute: {[weak self] in
//            self?.navigationController?.pushViewController(vc, animated: true)
//        })

        
    }
    @IBAction func recordButtonAction(_ sender: UIButton) {
    }
}
extension FrontScreenVC : GADInterstitialDelegate{
    func interstitialDidDismissScreen(_ ad: GADInterstitial) {
      interstitial = createAndLoadInterstitial()
    }

}
