//
//  TermsPolicyVC.swift
//  VideoTrimmer
//
//  Created by strongapps on 02/08/19.
//  Copyright © 2019 strongapps. All rights reserved.
//

import UIKit
import WebKit
class TermsPolicyVC: UIViewController {

    public var isPrivacyPolicy = true
    @IBOutlet weak var termspolicyLabel: UILabel!
    @IBOutlet weak var webview: WKWebView!
    @IBAction func bactButtonAction(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if isPrivacyPolicy{
            termspolicyLabel.text = "Privacy Policy"
            let myURLString = "https://drive.google.com/open?id=1T1g3jsJyIVwUUK4qVEwinFkMe6CtHMlg"
            let url = URL(string: myURLString)
            let request = URLRequest(url: url!)
            webview.navigationDelegate = self
            webview.load(request)
        }else{
            termspolicyLabel.text = "End-User License Agreement"
            let myURLString = "https://drive.google.com/open?id=1I5NCbAR3_isJA4et2pFBwg_4IfYONLCM"
            let url = URL(string: myURLString)
            let request = URLRequest(url: url!)
            webview.navigationDelegate = self
            webview.load(request)

        }
    }
}
extension TermsPolicyVC: WKNavigationDelegate {
    func webView(_ webView: WKWebView, didCommit navigation: WKNavigation!) {
        debugPrint("didCommit")
        let actdata : ActivityData = ActivityData(size: CGSize(width: 50, height: 50) , type : .circleStrokeSpin)
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(actdata, nil)
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        debugPrint("didFinish")
         NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        debugPrint("didFail")
         NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
    }
}
