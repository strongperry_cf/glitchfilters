//
//  DirectoryManager.swift
//  Video Editorial
//
//  Created by strongapps on 10/04/19.
//  Copyright © 2018 strongapps. All rights reserved.
//

import UIKit

enum FileName: String{
    case finalVideo = "/finalComposition.mov"
}

class DirectoryManager: NSObject {
    
    static var shared = DirectoryManager()
    static var saveURL: URL {
        let path = NSTemporaryDirectory().appendingFormat("SlideVideo.mov")
        let videoURL = URL(fileURLWithPath: path)
        try? FileManager.default.removeItem(atPath: path)
        return videoURL
    }
    
    var directoryURL: URL {
        return FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
    }
    var searchPathForDirectories:String{
        return NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!
    }
    
    func isFileExist(filePath:String) -> Bool{
        if FileManager.default.fileExists(atPath: filePath) {
            print("The file already exists at path")
           return true
        }
        return false
    }
    
    func createFileWith(fileName:FileName) -> String {
        let videoPath = searchPathForDirectories + fileName.rawValue
        
        if isFileExist(filePath: videoPath){
            try! FileManager.default.removeItem(atPath: videoPath)
        }
        return videoPath
    }
    
    
    //in the below func count is used to append and int value to the name if the file already exists
    static func getFilePath(with string: String) -> URL{
        let pdfName = string
        
        let folderName = "MyVideos"
        let destinationURL = getDownloadsPath(folder: folderName).appendingPathComponent(pdfName).appendingPathExtension("mp4")
        if FileManager.default.fileExists(atPath: destinationURL.path) {
            try? FileManager.default.removeItem(atPath: destinationURL.path)
        }
        return destinationURL
    }
    
    static func getDownloadsPath(folder: String) -> URL{
        let documentsUrl:URL =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        let downloadsPath = documentsUrl.appendingPathComponent(folder)
        let exists = FileManager.default.fileExists(atPath: downloadsPath.path, isDirectory: nil)
        
        if !exists{
            do{
                try FileManager.default.createDirectory(atPath: downloadsPath.path, withIntermediateDirectories: true, attributes: nil)
            }catch{
                print("Error creating path")
            }
        }
        return downloadsPath
    }
    
    static func getDownloadsPath() -> URL{
        let documentsUrl:URL =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        let downloadsPath = documentsUrl.appendingPathComponent("DownloadedMusics")
        let exists = FileManager.default.fileExists(atPath: downloadsPath.path, isDirectory: nil)
        if !exists{
            do{
                try FileManager.default.createDirectory(atPath: downloadsPath.path, withIntermediateDirectories: true, attributes: nil)
            }catch{
                print("Error creating path")
            }
        }
        return downloadsPath
    }
    
    
    func clearTmpDirectory() {
        do {
            let tempDir = try FileManager.default.contentsOfDirectory(atPath: NSTemporaryDirectory())
            try  tempDir.forEach({ (file) in
                let path = String(format: "%@%@", NSTemporaryDirectory(),file)
                if let url = URL(string: path){
                    try FileManager.default.removeItem(at: url)
                }
                
            })
        } catch {
            print(error)
        }
    }
    
    
    static func clearVideosFoler() {
        let fileManager = FileManager.default
        do {
            let downloadsPath = DirectoryManager.getDownloadsPath(folder: "MyVideos")
            let filePaths = try fileManager.contentsOfDirectory(atPath: downloadsPath.path)
            for filePath in filePaths {
                try fileManager.removeItem(atPath: downloadsPath.appendingPathComponent(filePath).path)
            }
        } catch {
            print("Could not clear temp folder: \(error)")
        }
    }
    
    
    static func isFileExist(with url: String) -> (Bool,URL){
        var name = "abc"
        let components = url.components(separatedBy: "/")
        if components.count > 1{
            name = components.last!
        }
        print("name",name)
        let destinationUrl = DirectoryManager.getDownloadsPath().appendingPathComponent( name)
        
        if FileManager.default.fileExists(atPath: destinationUrl.path){
            return (true,destinationUrl)
        }
        return (false,destinationUrl)
    }
    func cleanDocumentVideos(){
        let fileManager = FileManager.default

        // Get the document directory url
        let documentsUrl =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        
        do {
            // Get the directory contents urls (including subfolders urls)
            let directoryContents = try FileManager.default.contentsOfDirectory(at: documentsUrl, includingPropertiesForKeys: nil)
//            print(directoryContents)
            
            // if you want to filter the directory contents you can do like this:
            let movFiles = directoryContents.filter{ $0.pathExtension == "mov" }
            print("movFiles urls:",movFiles)
            movFiles.forEach{ path in
                do {
                    try fileManager.removeItem(atPath: path.path)
                } catch {
                    print("Could not clear temp folder: \(error)")
                }
            }
            
            let mp4Files = directoryContents.filter{ $0.pathExtension == "mp4" }
            print("mp4Files urls:",mp4Files)
            mp4Files.forEach{ path in
                do {
                    try fileManager.removeItem(atPath: path.path)
                } catch {
                    print("Could not clear temp folder: \(error)")
                }
            }

            
//            let movFilesNames = movFiles.map{ $0.deletingPathExtension().lastPathComponent }
//            print("movFilesNames list:", movFilesNames)
            
        } catch {
            print(error)
        }

    }
}
