//
//  CustomLayer.swift
//  Video Editorial
//
//  Created by strongapps on 04/01/19.
//  Copyright © 2019 strongapps. All rights reserved.
//

import UIKit

class CustomLayer: CALayer {

    required override init(layer: Any) {
        super.init(layer: layer)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    init(layerName: String, gravity: CALayerContentsGravity? = .resizeAspect) {
        super.init()
        self.name = layerName
        self.contentsGravity = gravity!
    }
}
