//
//  VideoEdHelper.swift
//  Video Editorial
//
//  Created by strongapps on 08/04/19.
//  Copyright © 2019 strongapps. All rights reserved.


import UIKit
import Foundation
import Photos


enum alertType : Int {
    case setting
    case ok
    case onlyok
}
class VideoEdHelper: NSObject {
    var myCompletion : ((_ image : UIImage) -> Void)!
    var mediaCompltion : ((_ media : Any)-> Void)!
    
    static let sharedInstance : VideoEdHelper = {
        let instance = VideoEdHelper()
        return instance
    }()
    
    class func isIPhoneX() -> Bool  {
        var found : Bool = false
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 2436:
                found = true
                break
            default:
                break
            }
        }
        return found
    }

    
    static func xscale(_ transform: CGAffineTransform) -> CGFloat {
        let t: CGAffineTransform = transform
        return sqrt(t.a * t.a + t.c * t.c)
    }
    
    static func yscale(_ transform: CGAffineTransform) -> CGFloat {
        let t: CGAffineTransform = transform
        return sqrt(t.b * t.b + t.d * t.d)
    }
    
    static func zRotaion(_ transform: CGAffineTransform) -> CGFloat {
        let radians = atan2f(Float(transform.b), Float(transform.a))
        return CGFloat(radians)
    }
    
    
    static func getDownloadsPath() -> URL{
        let documentsUrl:URL =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        let downloadsPath = documentsUrl.appendingPathComponent("downloads")
        let exists = FileManager.default.fileExists(atPath: downloadsPath.path, isDirectory: nil)
        
        if !exists{
            do{
                try FileManager.default.createDirectory(atPath: downloadsPath.path, withIntermediateDirectories: true, attributes: nil)
            }catch{
                print("Error creating path")
            }
        }
        return downloadsPath
        
    }
    
    func secondsToHoursMinutesSeconds(seconds : Int) -> (Int, Int, Int) {
        return (seconds / 3600, (seconds % 3600) / 60, (seconds % 3600) % 60)
    }
    
    
    static func isGalleryAuthorized(completion:@escaping (Bool) -> ()) {
        var isPermitted = false
        switch PHPhotoLibrary.authorizationStatus() {
        case .authorized:
            isPermitted = true
            completion(isPermitted)
        case .denied:
            isPermitted = false
            completion(isPermitted)
        case .notDetermined:
            PHPhotoLibrary.requestAuthorization({ (status) in
                isPermitted = ((status == PHAuthorizationStatus.authorized) ? true : false)
                DispatchQueue.main.async {
                    completion(isPermitted)
                }
            })
        default:
            break
        }
    }
    
    static func isCameraAuthorized(completion:@escaping (Bool) -> ()) {
        var isPermitted = false
        switch AVCaptureDevice.authorizationStatus(for: .video) {
        case .authorized:
            isPermitted = true
            completion(isPermitted)
        case .denied:
            isPermitted = false
            completion(isPermitted)
        case .notDetermined:
            AVCaptureDevice.requestAccess(for: .video) { (isGiven) in
                isPermitted = isGiven
                DispatchQueue.main.async {
                    completion(isPermitted)
                }
            }
            
        default:
            break
        }
    }
    

    
    static func textSize(_ str: NSAttributedString) -> CGSize {
        var newSize = str.size()
        newSize.width += 10
        newSize.height += 10
        return newSize
    }
    

    
    static func isInternetAvailable() -> Bool {
        
       return NetworkManager.isNetworkReachable()
//        if reachability.connection == .none{
//            return false
//        }else if reachability.connection == .wifi{
//            return isDataConnection() ? true : false
//        }else if reachability.connection == .cellular{
//            return isDataConnection() ? true : false
//        }
//        return false
    }
    
    static func isDataConnection() -> Bool {
        let scriptUrl = URL(string: "http://www.google.com")
        var data: Data? = nil
        if let anUrl = scriptUrl {
            do{
                try data = Data(contentsOf: anUrl)
            }catch{
                return false
            }
            
        }
        if data != nil {
            debugPrint("Device is connected to the internet")
            return true
        } else {
            debugPrint("Device is not connected to the internet")
            return false
        }
    }
    
    
    /*
    func getTextLayer(from model: StickerData, scaleFactor: CGFloat = 1.0, layerOpacity: Float = 0.0) -> CALayer {
        
        let myModel = StickerData()
        myModel.identifier = model.identifier
        myModel.fontName = model.fontName
        myModel.fontSize = model.fontSize
        myModel.fontColor = model.fontColor
        myModel.text = model.text
        myModel.placeHolderText = model.placeHolderText
        myModel.strokeColor = model.strokeColor
        myModel.strokeWidth = model.strokeWidth
        myModel.fontSize = myModel.fontSize * scaleFactor
        myModel.size = model.size
        myModel.poistion = model.poistion
        myModel.startTime = model.startTime
        myModel.duration = model.duration
        myModel.angle = model.angle
        myModel.opacity = model.opacity
        myModel.scale = model.scale
        myModel.photo = model.photo
        
        if myModel.identifier.lowercased().contains("sticker") {
            return getStickerLayer(myModel: myModel, scaleFactor: scaleFactor, layerOpacity: layerOpacity)
        }
        if myModel.identifier.lowercased().contains("text") {
            return getTextLayers(myModel: myModel, scaleFactor: scaleFactor, layerOpacity: layerOpacity)
        }
        return CALayer()
    }
    
    func getTextLayers(myModel: StickerLayerData, scaleFactor: CGFloat = 1.0, layerOpacity: Float = 0.0) -> CenterTextLayer {
        let textLayer = CenterTextLayer()
        textLayer.shouldRasterize = true
        textLayer.backgroundColor = UIColor.clear.cgColor
        textLayer.opacity = 1.0
        textLayer.name = myModel.identifier
        let attString = VideoEdHelper.createAttributedStringWith(myModel)
        textLayer.string = attString
        textLayer.contentsScale = 3.0
        textLayer.rasterizationScale = 3.0
        textLayer.alignmentMode = .center
        textLayer.masksToBounds = false
        
        let frame = CGRect(x: 0, y: 0, width: myModel.size.width * scaleFactor, height: myModel.size.height * scaleFactor)
        let position = CGPoint(x: myModel.poistion.x * scaleFactor, y: myModel.poistion.y * scaleFactor)
        textLayer.frame = frame
        textLayer.setAffineTransform(CGAffineTransform(scaleX: CGFloat(myModel.scale), y: CGFloat(myModel.scale)).concatenating(CGAffineTransform(rotationAngle: myModel.angle)))
        textLayer.position = position
        return textLayer
    }
    
    func getStickerLayer(myModel: StickerLayerData, scaleFactor: CGFloat = 1.0, layerOpacity: Float = 0.0) -> CALayer {
        let stickerLayer = CALayer()
        stickerLayer.shouldRasterize = true
        stickerLayer.backgroundColor = UIColor.clear.cgColor
        stickerLayer.opacity = 1.0
        stickerLayer.name = myModel.identifier
        stickerLayer.contents = myModel.photo.cgImage
        stickerLayer.contentsScale = 3.0
        stickerLayer.rasterizationScale = 3.0
        stickerLayer.masksToBounds = false
        let frame = CGRect(x: 0, y: 0, width: myModel.size.width * scaleFactor, height: myModel.size.height * scaleFactor)
        let position = CGPoint(x: myModel.poistion.x * scaleFactor, y: myModel.poistion.y * scaleFactor)
        stickerLayer.frame = frame
        stickerLayer.setAffineTransform(CGAffineTransform(rotationAngle: myModel.angle))
        stickerLayer.position = position
        return stickerLayer
    }
    */
    
    static func indexOf(category: String,array: [String]) -> Int{
        for (index,cat) in array.enumerated(){
            if category == cat{
                return index
            }
        }
        return 0
    }
    
    
    
}
