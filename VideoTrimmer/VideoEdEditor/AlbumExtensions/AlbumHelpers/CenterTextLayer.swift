//
//  CenterTextLayer.swift
//  Video Editorial
//
//  Created by strongapps on 08/03/19.
//  Copyright © 2019 strongapps. All rights reserved.
//

import UIKit

public class CenterTextLayer: CATextLayer {
    
    public override init() {
        super.init()
    }
    
    public override init(layer: Any) {
        super.init(layer: layer)
    }
    
    public required init(coder aDecoder: NSCoder) {
        super.init(layer: aDecoder)
    }
    
    override open func draw(in ctx: CGContext) {
        let yDiff: CGFloat
        let fontSize: CGFloat
        let height = self.bounds.height
        CATransaction.begin()
        CATransaction.setValue(kCFBooleanTrue, forKey: kCATransactionAnimationDuration)
        
        if let attributedString = self.string as? NSAttributedString {
            let newSize = attributedString.size()
            let largestSize = CGSize(width: newSize.width, height: .greatestFiniteMagnitude)
            let framesetter = CTFramesetterCreateWithAttributedString(attributedString)
            let textSize = CTFramesetterSuggestFrameSizeWithConstraints(framesetter, CFRange(), nil, largestSize, nil)
            yDiff = (height-textSize.height)/2
        } else {
            fontSize = self.fontSize
            yDiff = (height-fontSize)/2 - fontSize/10
        }
        ctx.saveGState()
        ctx.translateBy(x: 0.0, y: yDiff)
        super.draw(in: ctx)
        ctx.restoreGState()
        CATransaction.commit()
    }
}


extension CALayer{
    
    func bringSubLayerToFront(layer: CALayer){
        layer.removeFromSuperlayer()
        self.insertSublayer(layer, at: UInt32(self.sublayers?.count ?? 0))
    }
}
