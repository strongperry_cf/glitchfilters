

import UIKit
import AVFoundation

class ViewController: UIViewController   {

    let options = ["Glitch","Filter","Music"]
    var BOTTOM_VIEWS_HEIGHT : CGFloat = 160.0
    
    @IBOutlet weak var btnPlay: UIButton!
    
    @IBOutlet weak var filterAndMusicContainer: UIView!
    lazy var videoContainer: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.layer.masksToBounds = true
        return view
    }()
    lazy var gpuImageView: GPUImageView = {
        return GPUImageView()
    }()
    var gpuMovie: GPUImageMovie?
    var gpuWriter: GPUImageMovieWriter?
    var synclayer: AVSynchronizedLayer!
    
    @IBOutlet weak var mainContainer: UIView!
    
    //MARK: Filters
    var lastFilter: GPUImageFilter!
    var firstFilter: GPUImageFilterGroup!
    var alphaBlend: GPUImageAlphaBlendFilter!
    var liveFilter : GPUImageFilter!
//    var bulgeDistortion : GPUImageBulgeDistortionFilter?
    
    
    var player : AVPlayer?
    var playerItem: AVPlayerItem?
    var timeObserverToken: Any?
    var isSeeking = false
    var isPlaying = true
    var isRepeatVideo = true
    var isSaving = false
    
    var audioPlayer: AVPlayer?
    var audioPlayerItem: AVPlayerItem?
    
    @IBOutlet weak var btnMusic: UIButton!
    @IBOutlet weak var btnGlitch: UIButton!
    @IBOutlet weak var btnFilter: UIButton!
    weak var videoContainerWidth: NSLayoutConstraint?
    weak var videoContainerHeight: NSLayoutConstraint?
    weak var gpuImageWidth: NSLayoutConstraint?
    weak var gpuImageHeight: NSLayoutConstraint?
    var videoSize: CGSize = .zero
    
    @IBOutlet weak var btnSave: UIButton!
    var videoModel: VideoModel!
    var videoDuration : CMTime!
    var currentTimeScale :CMTimeScale = 100
    var bottomSafeArea: CGFloat = 0
    
    @IBOutlet weak var btnPlayPause: UIButton!
    @IBOutlet weak var seekBar: UISlider!
    @IBOutlet weak var liveFilterSlider: UIView!
    var effectFilterView: EffectFilters?
    
    weak var videoTimer: Timer?
    var arrayAudioModel = [AudioModel]()
    var filtersView: FilterView?
    var musicView: MusicView?
    
    var audioModel: AudioModel?
    var videoUrl : URL!

    @IBAction func backAction(_ sender: UIButton) {
        
        if self.videoTimer != nil{
            self.videoTimer!.invalidate()
            self.videoTimer = nil
            
        }
        audioPlayer?.pause()
        player?.pause()

        self.navigationController?.popToRootViewController(animated: true)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        //videoUrl = Bundle.main.url(forResource: "sample", withExtension: "mp4")
        btnPlayPause.setImage(UIImage(named: ""), for: .normal)

        if #available(iOS 11.0, *) {
            let window = UIApplication.shared.keyWindow
            if let bottomPadding = window?.safeAreaInsets.bottom {
              bottomSafeArea = bottomPadding
            }
        }
        
//        guard let videoUrl = Bundle.main.url(forResource: "Video1", withExtension: "mp4") else {
//            return
//        }
        let avAsset = AVURLAsset(url: videoUrl)
        videoDuration = avAsset.duration
        videoModel = VideoUtility.shared.getComposition(videoAsset: avAsset, from: 0.0, to:CGFloat(videoDuration.seconds) )
        videoModel.asset = avAsset
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            self.btnSave.layer.cornerRadius =  self.btnSave.frame.height / 2
            self.btnSave.clipsToBounds = true
            self.btnSave.backgroundColor = UIColor(red: 252/255, green: 50/255, blue: 117/255, alpha: 1.0)

        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
            self.setupPlayer()
            self.selectGlitchWhenLoad()
        }
        btnPlayPause.addTarget(self, action: #selector(playClicked(sender:)), for: .touchUpInside)
        // Do any additional setup after loading the view.
        
    }
    
    
    func setupPlayer() {
        playerItem = AVPlayerItem(asset: videoModel.composition)
        playerItem?.videoComposition = videoModel.videoComposition
        synclayer = AVSynchronizedLayer(playerItem: playerItem!)
        player  = AVPlayer(playerItem: playerItem!)
        player?.play()
        isRepeatVideo = true
        initGPUWith(item: playerItem!) //vikas testing
        NotificationCenter.default.addObserver(self, selector: #selector(playerDidEndPlaying(notification:)), name: Notification.Name.AVPlayerItemDidPlayToEndTime, object: playerItem!)
        addPeriodicTimeObserver()
        setupSeekBar()
        currentTimeScale = videoModel.composition.duration.timescale
        
    }
    
    func setupSeekBar() {
        seekBar.minimumValue = 0.0
        seekBar.maximumValue = Float(videoDuration.seconds)
        seekBar.value = 0.0
        seekBar.addTarget(self, action: #selector(touchEvent(slider:for:)), for: .valueChanged)
        seekBar.setThumbImage(UIImage(named: "SliderThumb"), for: .normal)
        seekBar.setThumbImage(UIImage(named: "SliderThumb"), for: .highlighted)
    }
    
    
    func initGPUWith(item: AVPlayerItem) {
        videoContainer.removeFromSuperview()
        gpuImageView.removeFromSuperview()
        self.mainContainer.addSubview(self.videoContainer)
        self.videoContainer.addSubview(self.gpuImageView)
        // Initialize layers
        videoContainer.layer.addSublayer(synclayer)
        // Provide auto layout for video container
        defaultVideoPosition()
        
        lastFilter = GPUImageFilter()
        gpuMovie = GPUImageMovie(playerItem: item)
        gpuMovie?.playAtActualSpeed = true
        liveFilter = CustomEffects.getShaderFilter()
        gpuMovie?.addTarget(liveFilter)
        
        lastFilter = liveFilter
        lastFilter.addTarget(gpuImageView)
        gpuMovie?.startProcessing()
        mainContainer.bringSubviewToFront(btnPlayPause)
    }
    
    
    
    // MARK: - Add constraints for video container
    func defaultVideoPosition() {
        
        view.layoutIfNeeded() // need to get exact position
        videoSize = videoModel.composition.naturalSize
        let playerFrame = AVMakeRect(aspectRatio: videoSize, insideRect: mainContainer.bounds)
        view.addConstraintSameCenterXY(mainContainer, and: videoContainer)
        if let videoContainerWidth = videoContainerWidth {
            videoContainer.removeConstraint(videoContainerWidth)
        }
        if let videoContainerHeight = videoContainerHeight {
            videoContainer.removeConstraint(videoContainerHeight)
        }
        videoContainerWidth = videoContainer.addConstraintForWidth(playerFrame.width)
        videoContainerHeight = videoContainer.addConstraintForHeight(playerFrame.height)
        videoContainer.layoutIfNeeded()
        resetGpuImagePosition()
    }
    
    func resetGpuImagePosition() {
        
        view.layoutIfNeeded()
        guard let parent = gpuImageView.superview else { return }
        let playerFrame = AVMakeRect(aspectRatio: videoSize, insideRect: parent.bounds)
        if let gpuImageWidth = gpuImageWidth {
            gpuImageView.removeConstraint(gpuImageWidth)
        }
        if let gpuImageHeight = gpuImageHeight {
            gpuImageView.removeConstraint(gpuImageHeight)
        }
        
        gpuImageWidth = gpuImageView.addConstraintForWidth(playerFrame.width)
        gpuImageHeight = gpuImageView.addConstraintForHeight(playerFrame.height)
        view.addConstraintSameCenterXY(parent, and: gpuImageView)
    }
    
    //MARK:- Seek Player
    func seekPlayer(to seconds: Double){
        let time = CMTime(seconds: seconds, preferredTimescale: currentTimeScale)
        if !self.isSeeking{
            self.isSeeking = true
            player?.seek(to: time, toleranceBefore: .zero, toleranceAfter: .zero, completionHandler: { [weak self]_ in
                self?.isSeeking = false
            })
        }
    }
    
    @objc func playerDidEndPlaying(notification: Notification){
        if let item = notification.object as? AVPlayerItem, item == playerItem{
            seekBar.value = 0.0
            player?.pause()
            audioPlayer?.pause()
            player?.seek(to: .zero)
            audioPlayer?.seek(to: .zero)
            if isRepeatVideo {
                player?.play()
                audioPlayer?.play()
            }
        }
    }
    
    
    //MARK:- Periodic time observers
    func addPeriodicTimeObserver() {
        
        // Notify every half second
        let timeScale = CMTimeScale(NSEC_PER_SEC)
        let time = CMTime(seconds: 0.03, preferredTimescale: timeScale)
        timeObserverToken = player?.addPeriodicTimeObserver(forInterval: time, queue: .main) { [weak self] time in
            if self?.player != nil{
                let currentTime = self!.player!.currentTime()
//                self?.seekBar.value = Float(currentTime.seconds)
                self?.filtersView?.updateCurrentTime(time: CGFloat(currentTime.seconds))
                self?.effectFilterView?.updateCurrentTime(time: CGFloat(currentTime.seconds))
            }
        }
    }
    
    func removePeriodicTimeObserver() {
        if let timeObserverToken = timeObserverToken {
            player?.removeTimeObserver(timeObserverToken)
            self.timeObserverToken = nil
        }
    }
    
    
    func addEffectsView(_ index:NSInteger) {
        if effectFilterView == nil {
            effectFilterView = EffectFilters.initWith(delegate: self, imageView: gpuImageView, gpuMovie: gpuMovie!, asset: (player?.currentItem!.asset)!, player: player!,liveEffectFilter: liveFilter)
            if let thumb = VideoManager.shared.generateThumbnail(asset: videoModel.composition,at: .zero, of : CGSize(width: 150, height: 150)){
                effectFilterView!.firstFrame = thumb
            }
            effectFilterView?.asset = videoModel.composition
            self.filterAndMusicContainer.addSubview(effectFilterView!)
        }
        isPlaying = false
        player?.pause()
        player?.seek(to: .zero)
        audioPlayer?.pause()
        audioPlayer?.seek(to: .zero)
        btnPlayPause.setImage(UIImage(named: "play"), for: .normal)
        liveFilterSlider.isHidden = false
//        effectFilterView?.filterSlider = liveFilterSlider
        seekBar.minimumTrackTintColor = seekBar.maximumTrackTintColor
        effectFilterView?.filterCategory = index
        effectFilterView?.cv_EffectFilters.reloadData()
        effectFilterView?.frame = filterAndMusicContainer.bounds
        effectFilterView?.isHidden = false
    }
    
    
    func addFiltersView(){
        if filtersView == nil{
            filtersView = FilterView.initWith(andImageView: gpuImageView, gpuMovie: gpuMovie!)
            filtersView?.delegate = self
            if let thumb = VideoManager.shared.generateThumbnail(asset: videoModel.composition,at: .zero, of : CGSize(width: 150, height: 150)){
                filtersView?.firstFrame = thumb
            }
            filtersView?.asset = self.videoModel.composition
            filterAndMusicContainer.addSubview(filtersView!)
//            self.view.addSubview(filtersView!)
        }
//        filtersView?.frame = CGRect(x: 0, y: SCREEN_HEIGHT - BOTTOM_VIEWS_HEIGHT - bottomSafeArea - 70, width: SCREEN_WIDTH, height: BOTTOM_VIEWS_HEIGHT)
        filtersView?.frame = filterAndMusicContainer.bounds
        filtersView?.isHidden = false
        
    }
    
    func addMusicView(){
        if musicView == nil{
            let frame = filterAndMusicContainer.bounds
            musicView = MusicView.initWith(with: frame)
            filterAndMusicContainer.addSubview(musicView!)
            musicView?.delegate = self
        }
        musicView?.isHidden = false
       
    }
    
    
    @objc func playClicked(sender: UIButton){
        isRepeatVideo = true
        isPlaying = !isPlaying
        if isPlaying {
            self.player?.play()
            self.audioPlayer?.play()
//            sender.setImage(UIImage(named: "pause"), for: .normal)
            sender.setImage(UIImage(named: ""), for: .normal)
        }else{
            player?.pause()
            audioPlayer?.pause()
            sender.setImage(UIImage(named: "play"), for: .normal)
        }
    }
    
    
    //MARK:- Actions
    @objc func touchEvent(slider: UISlider,for event: UIEvent) {
        if let touchEvent = event.allTouches?.first {
            switch touchEvent.phase {
            case .began:
                player?.pause()
                btnPlayPause.setImage(UIImage(named: "play"), for: .normal)
                isPlaying = false
            case .moved:
                seekPlayer(to: Double(slider.value))
                player?.seek(to: CMTime(seconds: Double(slider.value), preferredTimescale: 6000))
            default:
                break
            }
        }
    }

    
    
    
    @IBAction func saveAction(_ sender: UIButton) {
        
        isRepeatVideo = true
        sender.isUserInteractionEnabled = false
        self.player?.pause()
        self.audioPlayer?.pause()
        isPlaying = false
        
        // mute player volume
        self.player?.volume = 0.0
        btnPlayPause.setImage(nil, for: .normal)
//        btnPlayPause.setImage(UIImage(named: "pause"), for: .normal)
        isPlaying = true
        let startPoint: CGFloat = CGFloat(videoModel.timeRange.start.seconds)
        let endPoint: CGFloat = CGFloat(videoModel.composition.duration.seconds) + startPoint
        let containerBgColor = videoContainer.backgroundColor ?? UIColor.white
        sender.isUserInteractionEnabled = true
        isSaving = true
        if audioModel != nil {
            arrayAudioModel.append(audioModel!)
        }

        guard let (mixComp,videoComp) = VideoManager.getCompositionsFrom(asset: videoModel.asset, isNewAudio: false, audioAssetModel: arrayAudioModel) else {
            return
        }
        let gpuComposition = GPUImageMovieComposition(composition: mixComp, andVideoComposition: videoComp, andAudioMix: nil)
        gpuComposition?.runBenchmark = true
//        let playerItem = AVPlayerItem(asset: gpuComposition!.compositon)
//        playerItem.videoComposition = gpuComposition!.videoComposition
//
//        gpuMovie?.playerItem = playerItem
//        player?.replaceCurrentItem(with: playerItem)
//        gpuMovie?.startProcessing()
//
//        player?.play()
        
        
        saveGPUMovie(comp: gpuComposition!)
    }
    
    
    
    
        
  
    func saveGPUMovie(comp: GPUImageMovieComposition) {
        // re-arrange video container frame
        gpuWriter = nil
        let isAudio = comp.compositon.tracks(withMediaType: .audio).count > 0
        // Save path
        let saveURL = DirectoryManager.saveURL
        // Last filter
        gpuMovie?.cancelProcessing()
        gpuMovie?.playAtActualSpeed = true
        // Get rect for video and video container
        
        let videoRect = AVMakeRect(aspectRatio: comp.videoComposition.renderSize, insideRect: CGRect(origin: .zero, size: CGSize(width: 1280, height: 1280)))
        var finalWidth = Int(videoRect.width)
        var finalHeight = Int(videoRect.height)
        while finalWidth % 4 > 0 {
            finalWidth = finalWidth + 1
        }
        while finalHeight % 4 > 0 {
            finalHeight = finalHeight + 1
        }
        let writeSize = CGSize(width: finalWidth, height: finalHeight)
        // Layer to contain ui elements
        
        gpuWriter = GPUImageMovieWriter(movieURL: saveURL, size: writeSize)
        comp.enableSynchronizedEncoding(using: gpuWriter)
        gpuWriter?.shouldPassthroughAudio = isAudio
        gpuWriter?.encodingLiveVideo = false
        comp.audioEncodingTarget = isAudio ? gpuWriter : nil
        comp.playAtActualSpeed = true
        lastFilter.removeAllTargets()
        
        comp.addTarget(liveFilter)

        if firstFilter != nil {
            firstFilter.removeAllTargets();
            alphaBlend.removeAllTargets()
            liveFilter.addTarget(firstFilter)
            liveFilter.addTarget(alphaBlend, atTextureLocation: 0);

            firstFilter.addTarget(alphaBlend, atTextureLocation: 1);
            alphaBlend.addTarget(gpuWriter)

        } else {
            liveFilter.addTarget(gpuWriter)

        }
        gpuWriter?.failureBlock = { error in
            
            
            print("gpuWriter gpuWriter?.failureBlock =>",error.debugDescription)

        }
        
        // writer block
        gpuWriter?.completionBlock = {[weak self] in
            self?.videoTimer?.invalidate()
            self?.videoTimer = nil
            comp.endProcessing()
            self?.gpuWriter?.finishRecording()
            self?.gpuWriter?.endProcessing()
            self?.gpuWriter = nil
            self?.player?.pause()
            self?.audioPlayer?.pause()

            DispatchQueue.main.async(execute: {
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)

                self?.openShareView(videoUrl: saveURL)
                
            })
        }
        comp.startProcessing()
        gpuWriter?.startRecording()

        GlobalHelper.dispatchMain { [weak self] in
            let actdata : ActivityData = ActivityData(size: CGSize(width: 50, height: 50) , type : .circleStrokeSpin)
            NVActivityIndicatorPresenter.sharedInstance.startAnimating(actdata, nil)

        }
        videoTimer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(retrievingProgress(timer:)), userInfo: comp, repeats: true)
    }
    
    
    func openShareView(videoUrl:URL)  {
        
        let shareVC = ShareViewController(nibName: "ShareViewController", bundle: nil)
        shareVC.videoUrl = videoUrl

        GlobalHelper.dispatchMainAfter(time: .now() + 0.2, execute: {[weak self] in
            self?.navigationController?.pushViewController(shareVC, animated: true)
        })

        
        return
            
//        let storyboard = UIStoryboard(name: "VideoEdAlbum", bundle: nil)
//        let vc = storyboard.instantiateViewController(withIdentifier: "VideoEdShareVC") as! VideoEdShareVC
//        vc.videoUrl = videoUrl
        
            PHPhotoLibrary.shared().performChanges({
                PHAssetChangeRequest.creationRequestForAssetFromVideo(atFileURL: videoUrl)
                
            }) { (isSuccess, error) in
                if isSuccess {
                    
                    

//                    AlertManager.shared.alertWithTitle(title: "", message: "Successfully saved to gallery.")
                    
                }else{
                    AlertManager.shared.alertWithTitle(title: "", message: "Error saving to gallery.")
                    
                }
            }
        

        
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        if segue.identifier == "ShareVCSegue"{
//            let shareVC = segue.destination as! ShareVC
//            shareVC.finalUrl = sender as? URL
//        }
    }
    
    
    // MARK: - Get progress of saving
    @objc func retrievingProgress(timer: Timer) {
        guard let gpuComp = timer.userInfo as? GPUImageMovieComposition else{
            return
        }
        let total = gpuComp.compositon.duration.seconds
        let compProgress = gpuComp.compositionProgress
        print("compProgress is",compProgress)

        if !compProgress.isNaN{
            let progress = compProgress / Float(total)
            print("progress is",progress)
            
            DispatchQueue.main.async(execute: {
                NVActivityIndicatorPresenter.sharedInstance.setMessage(String(Int(progress*100)) + "%")
            })
//             if self.hud != nil{
//                self.hud.progress = progress
//                self.hud.label.text = String(Int(progress * 100)) + "%"
//            }
        }
    }
    //MARK: Actions
    @IBAction func actionBottomButtons(_ sender: UIButton) {
        let UNSELECTED_TINT = UIColor(red: 133/255, green: 133/255, blue: 133/255, alpha: 1.0)
        btnGlitch.tintColor = UNSELECTED_TINT
        btnFilter.tintColor = UNSELECTED_TINT
        btnMusic.tintColor = UNSELECTED_TINT
        sender.tintColor = .white
        effectFilterView?.isHidden = true
        musicView?.isHidden = true
        filtersView?.isHidden = true
        switch sender.tag {
        case 11: //glitch
            addEffectsView(1)
        case 12:
            addFiltersView()
        case 13:
            addMusicView()
            break
        default:
            break
        }
    }
    func selectGlitchWhenLoad(){
        let UNSELECTED_TINT = UIColor(red: 133/255, green: 133/255, blue: 133/255, alpha: 1.0)
        btnGlitch.tintColor = .white
        btnFilter.tintColor = UNSELECTED_TINT
        btnMusic.tintColor = UNSELECTED_TINT
        effectFilterView?.isHidden = false
        musicView?.isHidden = true
        filtersView?.isHidden = true

        addEffectsView(1)

    }
    
    
    @objc func appWillResignActive(notification: Notification){
        if isSaving{
            gpuWriter?.cancelRecording()
            gpuWriter = nil
            videoTimer?.invalidate()
            videoTimer = nil
            VideoManager.shared.cancelExport()
        }
    }
    
    @objc func appDidBecomeActive(notification: Notification){
        if isSaving{
//            didBackPressed()
            isSaving = false
        }
    }
 
}

extension ViewController: EffectFiltersDelegate , Filter2ViewDelegate {
    
    func sliderDidBeginMoving() {
        isRepeatVideo = true
        isPlaying = false
        player?.pause()
        audioPlayer?.pause()
        btnPlayPause.setImage(UIImage(named: "play"), for: .normal)
    }
    
    func sliderMoved(value: Float) {
        seekPlayer(to: Double(value))
        effectFilterView?.updateCurrentTime(time: CGFloat(value))
        filtersView?.updateCurrentTime(time: CGFloat(value))
    }
    
  
    func effectEndEditing() {
        isRepeatVideo = true
        isPlaying = false
        player?.pause()
        audioPlayer?.pause()
        btnPlayPause.setImage(UIImage(named: "play"), for: .normal)
    }
    
    //MARK:- Filter view delegate
    func didSelectFilter() {
        if !isPlaying{
            isPlaying = true
            player?.play()
            audioPlayer?.pause()
            btnPlayPause.setImage(nil, for: .normal)
//            btnPlayPause.setImage(UIImage(named: "pause"), for: .normal)
        }
    }
    
    func filterEndEditing() {
    }
 
}

extension ViewController: MusicViewDelegate {
    func pausePlayer(){
        player?.pause()
        audioPlayer?.pause()
    }
    
    func didSelectMusic(audioModel: AudioModel) {
        audioPlayer?.pause()
        self.player?.volume = 0.0
        player?.seek(to: .zero)
        player?.pause()
        guard let composition = AudioManager().getAudioComposition(with: audioModel) else {return}
        self.audioModel = audioModel
        self.audioPlayerItem = AVPlayerItem(asset: composition)
//        self.audioPlayerItem = AVPlayerItem(url: audioModel.audioUrl)
        self.audioPlayer = AVPlayer(playerItem: self.audioPlayerItem!)
        self.audioPlayer?.play()
        self.player?.play()
        btnPlayPause.setImage(nil, for: .normal)

    }
    
    func didChangeVolume(volumeLevel: Float) {
        self.audioModel?.volume = CGFloat(volumeLevel)
        self.audioPlayer?.volume = volumeLevel
    }
}



