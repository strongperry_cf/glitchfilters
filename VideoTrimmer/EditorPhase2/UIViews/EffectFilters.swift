
import UIKit
import Photos

protocol EffectFiltersDelegate : class{
    func effectEndEditing()
    func sliderDidBeginMoving()
    func sliderMoved(value: Float)
}

class EffectFilters: UIView , UICollectionViewDelegate ,UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UIGestureRecognizerDelegate {

    @IBOutlet weak var filterSlider: UIView!
    @IBOutlet weak var lblEndTime: UILabel!
    @IBOutlet weak var lblStartTime: UILabel!
//    @IBOutlet weak var sliderThumb: UIView!
    @IBOutlet weak var timeLineBgView: UIView!
//    @IBOutlet weak var sliderThumbLeading: NSLayoutConstraint!
    
    @IBOutlet weak var slider: UISlider!
    
    @IBOutlet weak var lblCurrentTime: UILabel!
    var oneSecondWidth : CGFloat = 0.0
    private var imageGenerator: AVAssetImageGenerator!
    
    //MARK: Previos properties
    @IBOutlet weak var cv_EffectFilters: UICollectionView!
    
//    var filterSlider: UIView!
    var gpuImageView: GPUImageView?
    var gpuMovie: GPUImageMovie?
    var effectLiveFilter: GPUImageFilter?
    
    var effectsVHSNames = ["ERASE","Chromatic","Old TV","Scavenger","Desi Blur","VCR","Film","Grains","Burst","Celtic","Swicth Noise","Chromatic Pulse","Simon","Salem"]
    var effectsGlitchNames = ["ERASE","Shake","Doom","Ghost","Raven","Virus","Bram","Spunky","Preta","Mogwai","Jack","CHILL","JOKER","RAINDAY"]
    
    var effectsVintageNames = ["ERASE","Shake","Doom","Ghost","Raven","Virus","Bram","Spunky","Preta","Mogwai","Jack","CHILL","JOKER","RAINDAY"]
    var effectsBWNames = ["ERASE","Shake","Doom","Ghost","Raven","Virus","Bram","Spunky","Preta","Mogwai","Jack","CHILL","JOKER","RAINDAY"]
    
    var effectsMirorNames = ["ERASE","Shake","Doom","Ghost","Raven","Virus","Bram","Spunky","Preta","Mogwai","Jack","CHILL","JOKER","RAINDAY"]
    
    var effectsVHSColours = [UIColor]()
    var effectsGlitchColours = [UIColor]()
    var effectsVintageColours = [UIColor]()
    var effectsBWColours = [UIColor]()
    var effectsMirrorColours = [UIColor]()
    
    var currentFilterView: UIView?
    var currentFilterWidth: NSLayoutConstraint?
    var isLongpressGesture = false
    var selectedFilterIndex: IndexPath? = nil
    var reachEndPoint = false
    var startTime = CMTime.zero
    var endTime = CMTime.zero
    var appliedLiveFilterEffect: [LiveEffectModel] = []
    
    var player: AVPlayer?
    var timeObserverLiveEffect: Any?
    var filterCategory : NSInteger = 0
    
    weak var delegate: EffectFiltersDelegate?
    
    var asset: AVAsset? = nil {
        didSet {
            initializeTrimmerView()
        }
    }
    
    var effectImagesVHSArr = [UIImage]()
    var effectImagesGlitchArr = [UIImage]()
    var effectImagesVINArr = [UIImage]()
    var effectImagesBWArr = [UIImage]()
    var effectImagesMirrorArr = [UIImage]()
    
    var firstFrame : UIImage! {
        didSet{
            if firstFrame != nil{
                cv_EffectFilters.reloadData()
            }
        }
    }
    
    class func initWith(delegate: EffectFiltersDelegate,imageView: GPUImageView,gpuMovie: GPUImageMovie, asset:AVAsset, player:AVPlayer, liveEffectFilter:GPUImageFilter) -> EffectFilters {
        
        let view = self.fromEffectNib()
        view.delegate = delegate
        view.gpuImageView = imageView
        view.gpuMovie = gpuMovie
        view.initView()
        view.player = player
        view.player?.pause()
        view.player?.seek(to: CMTime.zero)
        view.effectLiveFilter = liveEffectFilter
        
//        view.playerTimeSlider.minimumValue = 0.0
//        view.playerTimeSlider.maximumValue = Float(CMTimeGetSeconds(asset.duration))
//        view.playerTimeSlider.value = 0.0
        
        view.addPeriodicTimeObserver()
        
//        view.btnDone.layer.cornerRadius = 5.0
//        view.btnDone.clipsToBounds = true
//        view.btnDone.isHidden = true
        
        return view
    }
    
    func addPeriodicTimeObserver() {
        
        let duration = Float64(0.01)
        let timescale = Float64(1 / duration)
        
        self.timeObserverLiveEffect = self.player?.addPeriodicTimeObserver(forInterval: CMTimeMakeWithSeconds(duration, preferredTimescale: Int32(timescale)), queue: DispatchQueue.main, using: { (timerInterval) in
           
            self.moveSlider(time:Float(timerInterval.seconds))
            self.applyLiveFilter(timerInterval)
            if (self.effectLiveFilter != nil) {
                self.effectLiveFilter!.setFloat(GLfloat(timerInterval.seconds), forUniformName: "MainTime")
            }

        })
        
    }
    
    func initializeTrimmerView() {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            self.setMovieFrame()
            self.slider.isHidden = false
        }
        
    }
    
    func updateCurrentTime(time: CGFloat) {
//        UIView.animate(withDuration: 0.2) {
////            self.sliderThumbLeading.constant = self.oneSecondWidth * time
////            self.sliderThumb.setNeedsLayout()
//        }
        slider.value = Float(time)
        lblCurrentTime.text = CGFloat(time).convertSecondsToDurationString2()
//        lblCurrentTime.text = getTimeLabel(seconds: Int(time))
    }
    
    func setMovieFrame(){
        guard let myAsset = asset else {
            return
        }
        oneSecondWidth = timeLineBgView.frame.width / CGFloat(myAsset.duration.seconds)
        imageGenerator = AVAssetImageGenerator(asset: myAsset)
        imageGenerator.maximumSize =  CGSize(width: 60, height: 60)
        let interval = myAsset.duration.seconds / 10.0 //only 10 images will be generated
        var imageArray = [UIImage]()
        for i in 0..<10 {
            print("current ",i)
            do {
                let time = CMTime(seconds: Double(Double(i) * interval), preferredTimescale: myAsset.duration.timescale)
                let img = try imageGenerator.copyCGImage(at: time, actualTime: nil)
                let image = UIImage(cgImage: img)
                imageArray.append(image)
            } catch {
                print(error.localizedDescription)
            }
        }
        let picWidth: CGFloat = timeLineBgView.frame.width / 10.0
        var xPos : CGFloat = 0
        for image in imageArray{
            let imgLayer = CALayer()
            imgLayer.frame = CGRect(x: xPos, y: 0, width: picWidth, height: timeLineBgView.frame.size.height)
            xPos += picWidth
            imgLayer.contents = image.cgImage
            self.timeLineBgView.layer.addSublayer(imgLayer)
        }
        imageArray.removeAll()
        timeLineBgView.bringSubviewToFront(filterSlider)
        timeLineBgView.bringSubviewToFront(slider)
        timeLineBgView.backgroundColor = .yellow
        lblStartTime.text = "00:00.0"
        lblEndTime.text = getTimeLabel(seconds: Int(myAsset.duration.seconds))
        lblEndTime.text = CGFloat(myAsset.duration.seconds).convertSecondsToDurationString2()
        setupSlider(asset: myAsset)
    }
    
    func setupSlider(asset: AVAsset){
        self.slider.setThumbImage(UIImage(named: "SliderThumb"), for: .normal)
        slider.setThumbImage(UIImage(named: "SliderThumb"), for: .highlighted)
        self.slider.minimumValue = 0.0
        self.slider.maximumValue = Float(asset.duration.seconds)
        self.slider.value = 0.0
        self.slider.addTarget(self, action: #selector(sliderAction(slider:for:)), for: .allEvents)
    }
    
    
    @objc func sliderAction(slider: UISlider,for event: UIEvent){
        if let touchEvent = event.allTouches?.first {
            switch touchEvent.phase {
            case .began:
                print("slider began editing")
                delegate?.sliderDidBeginMoving()
            case .moved:
                delegate?.sliderMoved(value: slider.value)
            // handle drag began
            default:
                break
            }
        }
    }
    
    
    func getTimeLabel(seconds: Int) -> String{
        var timeStr = ""
        let time = secondsToHoursMinutesSeconds(seconds:seconds)
        let hour = time.0 < 10 ? "0" + String(time.0) : String(time.0)
        let minute = time.1 < 10 ? "0" + String(time.1) : String(time.1)
        let seconds = time.2 < 10 ? "0" + String(time.2) : String(time.2)
        timeStr = minute + ":" + seconds
        if hour != "00"{
            timeStr = hour + ":" + timeStr
        }
        return timeStr
    }
    
    func secondsToHoursMinutesSeconds(seconds : Int) -> (Int, Int, Int) {
        return (seconds / 3600, (seconds % 3600) / 60, (seconds % 3600) % 60)
    }
    
    func initView() {
        
        cv_EffectFilters.reloadData()
        effectsVHSColours.append(UIColor.init(red: 236/255.0, green: 236/255.0, blue: 236/255.0, alpha: 1.0))
        effectsVHSColours.append(UIColor.indigoColor())
        effectsVHSColours.append(UIColor.babyBlueColor())
        effectsVHSColours.append(UIColor.salmonColor())
        effectsVHSColours.append(UIColor.limeColor())
        effectsVHSColours.append(UIColor.eggplantColor())
        effectsVHSColours.append(UIColor.hollyGreenColor())
        effectsVHSColours.append(UIColor.pinkLipstickColor())
        effectsVHSColours.append(UIColor.raspberryColor())
        effectsVHSColours.append(UIColor.almondColor)
        effectsVHSColours.append(UIColor.lavenderColor())
        effectsVHSColours.append(UIColor.periwinkleColor())
        effectsVHSColours.append(UIColor.goldenrodColor())
        effectsVHSColours.append(UIColor.palePurpleColor())

        effectsGlitchColours.append(UIColor.init(red: 236/255.0, green: 236/255.0, blue: 236/255.0, alpha: 1.0))
        effectsGlitchColours.append(UIColor.mudColor())
        effectsGlitchColours.append(UIColor.bananaColor())
        effectsGlitchColours.append(UIColor.cardTableColor())
        effectsGlitchColours.append(UIColor.watermelonColor())
        effectsGlitchColours.append(UIColor.robinEggColor())
        effectsGlitchColours.append(UIColor.coralColor())
        effectsGlitchColours.append(UIColor.steelBlueColor())
        effectsGlitchColours.append(UIColor.oliveColor())
        effectsGlitchColours.append(UIColor.cantaloupeColor())
        effectsGlitchColours.append(UIColor.linenColor())
        effectsGlitchColours.append(UIColor.periwinkleColor())
        effectsGlitchColours.append(UIColor.goldenrodColor())
        effectsGlitchColours.append(UIColor.palePurpleColor())
        
        effectsVintageColours.append(UIColor.init(red: 236/255.0, green: 236/255.0, blue: 236/255.0, alpha: 1.0))
        effectsVintageColours.append(UIColor.black50PercentColor())
        effectsVintageColours.append(UIColor.grassColor())
        effectsVintageColours.append(UIColor.cornflowerColor())
        effectsVintageColours.append(UIColor.seafoamColor())
        effectsVintageColours.append(UIColor.cactusGreenColor())
        effectsVintageColours.append(UIColor.grapefruitColor())
        effectsVintageColours.append(UIColor.palePurpleColor())
        effectsVintageColours.append(UIColor.periwinkleColor())
        effectsVintageColours.append(UIColor.goldenrodColor())
        effectsVintageColours.append(UIColor.chiliPowderColor())
        effectsVintageColours.append(UIColor.periwinkleColor())
        effectsVintageColours.append(UIColor.goldenrodColor())
        effectsVintageColours.append(UIColor.palePurpleColor())
        
        effectsBWColours.append(UIColor.init(red: 236/255.0, green: 236/255.0, blue: 236/255.0, alpha: 1.0))
        effectsBWColours.append(UIColor.siennaColor())
        effectsBWColours.append(UIColor.peachColor())
        effectsBWColours.append(UIColor.goldColor())
        effectsBWColours.append(UIColor.fuschiaColor())
        effectsBWColours.append(UIColor.orchidColor())
        effectsBWColours.append(UIColor.maroonColor())
        effectsBWColours.append(UIColor.easterPinkColor())
        effectsBWColours.append(UIColor.honeydewColor())
        effectsBWColours.append(UIColor.icebergColor())
        effectsBWColours.append(UIColor.tealColor())
        effectsBWColours.append(UIColor.periwinkleColor())
        effectsBWColours.append(UIColor.goldenrodColor())
        effectsBWColours.append(UIColor.palePurpleColor())

        effectsMirrorColours.append(UIColor.init(red: 236/255.0, green: 236/255.0, blue: 236/255.0, alpha: 1.0))
        effectsMirrorColours.append(UIColor.siennaColor())
        effectsMirrorColours.append(UIColor.peachColor())
        effectsMirrorColours.append(UIColor.goldColor())
        effectsMirrorColours.append(UIColor.fuschiaColor())
        effectsMirrorColours.append(UIColor.orchidColor())
        effectsMirrorColours.append(UIColor.maroonColor())
        effectsMirrorColours.append(UIColor.easterPinkColor())
        effectsMirrorColours.append(UIColor.honeydewColor())
        effectsMirrorColours.append(UIColor.icebergColor())
        effectsMirrorColours.append(UIColor.tealColor())
        effectsMirrorColours.append(UIColor.periwinkleColor())
        effectsMirrorColours.append(UIColor.goldenrodColor())
        effectsMirrorColours.append(UIColor.palePurpleColor())
        
        let longpgr = UILongPressGestureRecognizer(target: self, action: #selector(self.handleLongPress(gesture:)))
        longpgr.delegate = self
        longpgr.delaysTouchesBegan = true
        cv_EffectFilters.addGestureRecognizer(longpgr)
        
    }
    
    override func awakeFromNib() {
        cv_EffectFilters.register(UINib.init(nibName: String(describing: FilterViewCell.reuseIdentifier), bundle: nil), forCellWithReuseIdentifier: FilterViewCell.reuseIdentifier)
        cv_EffectFilters.dataSource = self
        cv_EffectFilters.delegate = self
        cv_EffectFilters.allowsSelection = false
    }

    @objc func handleLongPress(gesture : UILongPressGestureRecognizer!) {
        
        let p = gesture.location(in: self.cv_EffectFilters)
        if let indexPath = self.cv_EffectFilters.indexPathForItem(at: p) {
            let filterModel = LiveEffectFilter.init(index: indexPath.item, state: gesture.state)
            self.getLongGestureState(live: filterModel)
        } else {
            print("couldn't find index path")
        }
    }
    
    var itemCurrentTime: Float {
        guard let player = player, let item = player.currentItem else { return 0.0 }
        return Float(CMTimeGetSeconds(item.currentTime()))
    }
    
    var itemDuration: Float {
        guard let player = player, let item = player.currentItem else { return 0.0 }
        return Float(CMTimeGetSeconds(item.duration))
    }
    
    func moveSlider(time: Float) {
        
//        playerTimeSlider.value = time
        if time == 0 { return }
        if isLongpressGesture {
//            provideWidth(playerTimeSlider.value)
        }
        if (player?.rate)! < 0 { return }
        weak var weakSelf = self
        UIView.animate(withDuration: 0.01) {
            weakSelf?.layoutIfNeeded()
        }
    }
    
    func getLongGestureState(live: LiveEffectFilter) {
        
        if let controller = self.parentViewController as? ViewController {
            controller.isRepeatVideo = false
        }
        if live.state == .began {
            
            isLongpressGesture = true
            player?.play()
            player?.rate = 1.0
            if let controller = self.parentViewController as? ViewController {
                controller.isPlaying = true
                controller.btnPlay.setImage(UIImage(named: "pause"), for: .normal)
            }
            
            let startPoint: CGFloat = CGFloat(itemCurrentTime / itemDuration)
            let x = startPoint * filterSlider.bounds.width
            createLiveFilterView(x: x, index: live.index!)
            startTime = (player?.currentItem?.currentTime())!
            
            let indexPath = IndexPath.init(row: live.index!, section: 0)
            let cell = cv_EffectFilters.cellForItem(at: indexPath) as! FilterViewCell
            cell.transform = CGAffineTransform(scaleX: 1.2, y: 1.2)
            cell.isSelectedCell = true
        }
        
        if live.state == .ended {
            player?.pause()
            
            if let controller = self.parentViewController as? ViewController{
                controller.isPlaying = false
                controller.btnPlay.setImage(UIImage(named: "play"), for: .normal)
            }
            
            isLongpressGesture = false
            if reachEndPoint {
                reachEndPoint = false
                return
            }
            endTime = (player?.currentItem?.currentTime())!
            appliedLiveFilterEffect.append(LiveEffectModel(startTime: startTime, endTime: endTime, currentFilter: live.index!, currentFilterCategory: filterCategory, filterColor: UIColor.red))
            provideWidth(Float(CMTimeGetSeconds(endTime))) /********* THIS MIGHT BE FIXED ********/
            
            let indexPath = IndexPath.init(row: live.index!, section: 0)
            let cell = cv_EffectFilters.cellForItem(at: indexPath) as! FilterViewCell
            cell.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            cell.isSelectedCell = false
        }
        
        if itemCurrentTime == itemDuration {
            reachEndPoint = true
            isLongpressGesture = false
            player?.pause()
            if let controller = self.parentViewController as? ViewController{
                controller.isPlaying = false
                controller.btnPlay.setImage(UIImage(named: "play"), for: .normal)
            }
            
            endTime = (player?.currentItem?.currentTime())!
            appliedLiveFilterEffect.append(LiveEffectModel(startTime: startTime, endTime: endTime, currentFilter: live.index!, currentFilterCategory: filterCategory, filterColor: UIColor.red))
            player?.seek(to: CMTime.zero)
            provideWidth(Float(CMTimeGetSeconds(endTime))) /********* THIS MIGHTBE FIXED ********/
            
        }
        selectedLiveFilterCategory = filterCategory
        selectedLiveFilter = live.index!
        
    }

    func createLiveFilterView(x: CGFloat, index:NSInteger) {
        
        currentFilterView = UIView()
        switch filterCategory {
        case 0:
            currentFilterView?.backgroundColor = effectsVHSColours[index].withAlphaComponent(0.3)
        case 1:
            currentFilterView?.backgroundColor = effectsGlitchColours[index].withAlphaComponent(0.3)
        default:
            currentFilterView?.backgroundColor = effectsVHSColours[index].withAlphaComponent(0.3)
        }
        
        filterSlider.addSubview(currentFilterView!)
        
        currentFilterView?.translatesAutoresizingMaskIntoConstraints = false
        
        let horizontalConstraint = NSLayoutConstraint(item: currentFilterView!, attribute: .leading, relatedBy: .equal, toItem: filterSlider, attribute: .leading, multiplier: 1, constant: x)
        
        let verticalConstraint = NSLayoutConstraint(item: currentFilterView!, attribute: .centerY, relatedBy: .equal, toItem: filterSlider, attribute: .centerY, multiplier: 1, constant: 0)
        
        let heightConstraint = NSLayoutConstraint(item: currentFilterView!, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: filterSlider.frame.height)
        
        filterSlider.addConstraints([horizontalConstraint, verticalConstraint, heightConstraint])
        currentFilterWidth = currentFilterView?.addConstraintForWidth(0.0)
    }
    
    func provideWidth(_ time: Float) {
        let interval = Float(filterSlider.width) / itemDuration
        let x: CGFloat = CGFloat(time * interval)
        guard let currentFilterWidth = currentFilterWidth else { return }
        let width = x - (currentFilterView?.frame.origin.x)!
        if width >= 0 {
            currentFilterWidth.constant = width
        }
    }
    
    var selectedLiveFilter: Int = 0 {
        didSet {
            
            guard let filter = effectLiveFilter else { return }
            
            filter.setInteger(GLint(selectedLiveFilter), forUniformName: "type")
            
            if selectedLiveFilterCategory == 6 {
                
                if selectedLiveFilter == 1 {
                    filter.setFloat(0.01, forUniformName: "amplitude")
                    filter.setFloat(1.0, forUniformName: "speed")
                }
                
                if selectedLiveFilter == 2 {
                    filter.setFloat(0.01, forUniformName: "amplitude")
                    filter.setFloat(1.0, forUniformName: "speed")
                }
                
                if selectedLiveFilter == 3 {
                    filter.setFloat(0.01, forUniformName: "amplitude")
                    filter.setFloat(0.8, forUniformName: "speed")
                }
                
            }
        }
    } 
    
    var selectedLiveFilterCategory: Int = 0 {
        didSet {
            
            guard let filter = effectLiveFilter else { return }
            
            if selectedLiveFilterCategory == 0 {
                filter.setInteger(GLint(1), forUniformName: "category")
            }
            
            if selectedLiveFilterCategory == 1 {
                filter.setInteger(GLint(2), forUniformName: "category")
            }
            
            if selectedLiveFilterCategory == 2 {
                filter.setInteger(GLint(3), forUniformName: "category")
            }
            
            if selectedLiveFilterCategory == 3 {
                filter.setInteger(GLint(4), forUniformName: "category")
            }
            
            if selectedLiveFilterCategory == 4 {
                filter.setInteger(GLint(5), forUniformName: "category")
            }
        }
    }
    
    func applyLiveFilter(_ time: CMTime) {
        if isLongpressGesture { return }
        let reverseFilters = (player?.rate)! > 0 ? appliedLiveFilterEffect.reversed() : appliedLiveFilterEffect
        for filter in reverseFilters {
            let start = Double(CMTimeGetSeconds(filter.startTime!)).rounded(toPlaces: 2)
            let end = Double(CMTimeGetSeconds(filter.endTime!)).rounded(toPlaces: 2)
            let timer = Double(CMTimeGetSeconds(time)).rounded(toPlaces: 2)
//            debugPrint("end>>", end, "rimer", timer)
            if start <= timer && end >= timer {
                selectedLiveFilter = filter.currentFilter!
                selectedLiveFilterCategory = filter.currentFilterCategory!
                break
            } else {
                selectedLiveFilter = 0
            }
        }
    }
    
    //MARK:- Collection View Methods
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if filterCategory == 0 {
            return CustomEffectsVHSType.count
        }
        if filterCategory == 1 {
            return CustomEffectsGlitchType.count
        }
        if filterCategory == 2 {
            return CustomEffectsVINType.count
        }
        if filterCategory == 3{
            return CustomEffectsMirrorType.count
        }
        
        
        return CustomEffectsBWType.count
        
    
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: FilterViewCell.reuseIdentifier, for: indexPath) as! FilterViewCell
        var image = UIImage()
        
        if firstFrame != nil {
            
            image = firstFrame
            
            if filterCategory == 0 {
                if effectImagesVHSArr.count > indexPath.row {
                    image = effectImagesVHSArr[indexPath.row]
                } else {
                    if let filteredImage = CustomEffects.getFilteredImageWithVHSFilter(CustomEffectsVHSType(rawValue: indexPath.row) ?? .A1, to: image) {
                        image = filteredImage
                        effectImagesVHSArr.append(image)
                    }
                }
                cell.updateData(image: image, name: effectsVHSNames[indexPath.row].uppercased())
            }
            else if filterCategory == 1 {
                if effectImagesGlitchArr.count > indexPath.row {
                    image = effectImagesGlitchArr[indexPath.row]
                } else {
                    if let filteredImage = CustomEffects.getFilteredImageWithGLITFilter(CustomEffectsGlitchType(rawValue: indexPath.row) ?? .A1, to: image){
                        image = filteredImage
                        effectImagesGlitchArr.append(image)
                    }
                }
                cell.updateData(image: image, name: effectsGlitchNames[indexPath.row].uppercased())
            } else if filterCategory == 2 {
                if effectImagesVINArr.count > indexPath.row {
                    image = effectImagesVINArr[indexPath.row]
                } else {
                    if let filteredImage = CustomEffects.getFilteredImageWithVINFilter(CustomEffectsVINType(rawValue: indexPath.row) ?? .A1, to: image){
                        image = filteredImage
                        effectImagesVINArr.append(image)
                    }
                }
                cell.updateData(image: image, name: effectsVintageNames[indexPath.row].uppercased())
            } else if filterCategory == 3 {
                if effectImagesBWArr.count > indexPath.row {
                    image = effectImagesBWArr[indexPath.row]
                } else {
                    if let filteredImage = CustomEffects.getFilteredImageWithBAWFilter(CustomEffectsBWType(rawValue: indexPath.row) ?? .A1, to: image){
                        image = filteredImage
                        effectImagesBWArr.append(image)
                    }
                }
                cell.updateData(image: image, name: effectsBWNames[indexPath.row].uppercased())
            } else {
                if effectImagesMirrorArr.count > indexPath.row {
                    image = effectImagesMirrorArr[indexPath.row]
                } else {
                    if let filteredImage = CustomEffects.getFilteredImageWithMirrorFilter(CustomEffectsMirrorType(rawValue: indexPath.row) ?? .A1, to: image){
                        image = filteredImage
                        effectImagesMirrorArr.append(image)
                    }
                }
                cell.updateData(image: image, name: effectsMirorNames[indexPath.row].uppercased())
            }
        }
        
        cell.isSelectedCell = (selectedFilterIndex != nil && indexPath.row == selectedFilterIndex?.row) ? true: false
        cell.layer.cornerRadius = 5.0
        cell.clipsToBounds = true
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 16.0
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 16.0
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
            return CGSize(width: 60, height: 70)
//        let height = collectionView.frame.size.height * 0.85
//        let width = height / 1.3
//        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 16.0, bottom: 0, right: 16.0)
    }
    
    func removePeriodicTimeObserver() {
        if let timeObserverToken = timeObserverLiveEffect {
            player?.removeTimeObserver(timeObserverToken)
            self.timeObserverLiveEffect = nil
        }
    }
    
    func resetLiveFilters(){
        // remove observer
        self.removePeriodicTimeObserver()
        self.appliedLiveFilterEffect.removeAll() // Empty array
    }
    
    @IBAction func btnDoneClicked(_ sender: UIButton) {
        
        filterSlider.isHidden = true
        
        if let controller = self.parentViewController as? ViewController{
            controller.seekBar.minimumTrackTintColor = UIColor.init(red: 242/255.0, green: 103/255.0, blue: 115/255.0, alpha: 1.0)
        }
        
        UIView.animate(withDuration: 0.5, animations: {
            var bottomSafeArea : CGFloat = 0
            if #available(iOS 11.0, *) {
                let window = UIApplication.shared.keyWindow
                let bottomPadding = window?.safeAreaInsets.bottom
                bottomSafeArea = bottomPadding!
            }
            
            self.frame.origin.y += (160+bottomSafeArea)
        }) { (true) in
            self.removeFromSuperview()
            self.delegate?.effectEndEditing()
        }
    
    }
}

extension EffectFilters {
    class func fromEffectNib<T: EffectFilters>() -> T {
        return Bundle.main.loadNibNamed(String(describing: T.self), owner: nil, options: nil)![0] as! T
    }
}

extension Double {
    /// Rounds the double to decimal places value
    func rounded(toPlaces places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}
