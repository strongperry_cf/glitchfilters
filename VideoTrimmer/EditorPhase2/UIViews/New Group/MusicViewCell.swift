

import UIKit

class MusicViewCell: UICollectionViewCell {
    
    static let reuseIdentifier = "MusicViewCell"
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    
    var audioModel: AudioModel!
    var isSelectedCell: Bool = false {
        didSet {
            if isSelectedCell {
                self.imgView.image = UIImage(named: "MusicSelected")
            } else {
                if audioModel.type == "AddMusic" {
                    self.imgView.image = UIImage(named: "AddMusic")
                }else {
                    self.imgView.image = UIImage(named: audioModel.title ?? "") ?? UIImage(named: "Music")
                }
            }
        }
    }
    
    override func awakeFromNib() {
       print("awake from nib")
    }
    
    func setupData(with model: AudioModel) {
        self.audioModel = model
        self.lblTitle.text = model.title
        
    }   
    
}
