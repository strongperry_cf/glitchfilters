// Ref URL  : https://www.shadertoy.com/view/4s2GRR

varying highp vec2 textureCoordinate;
uniform sampler2D inputImageTexture;

uniform highp float fractionalWidthOfPixel;
uniform highp float aspectRatio;
uniform highp float iGlobalTime ;
uniform highp float frameWidth ;
uniform highp float frameHeight ;
uniform highp float sliderValue ;
uniform highp float sliderValue2 ;
uniform highp float sliderValue3 ;
uniform highp vec2  iMouse;
uniform highp int   type;
uniform highp int   updownEffect;


#define M_PI (3.1415926535897932384626433832795)

#define posterSteps 4.0
#define lumaMult 0.5
//#define timeMult 0.15
#define timeMult 5.0
#define BW 0
#define scanline true


///******************************   Completed Functions *************************////

highp float snoise(highp vec2 v) ;

highp float rand(highp vec2 co){
    
    //return fract(sin(dot(co.xy ,vec2(12.9898,78.233))) * 43758.5453);
    
    return fract(cos(dot(co.xy ,vec2(12.9898,78.233))) * 43758.5453);
    
    
    //return sin(dot(co.xy ,vec2(12.9898,78.233))) * 43758.5453;
    
    //    return fract(cos(dot(co.xy ,vec2(12.9898,78.233))) * 43758.5453);
    //return dot(co.xy ,vec2(12.9898,78.233));
    // return dot(co.xy ,vec2(1.29898,7.8233));
    //    return dot(co.xy ,vec2(2.0,5.0));
    //    return dot (co.xy,vec2(1.0,1.0));
    
}

highp float rand1(in highp float a, in highp float b)
{
    return fract((cos(dot(vec2(a,b) , vec2(12.9898,78.233))) * 43758.5453));
    //return 1.0;
}
highp float rand2(highp float frag_x,highp float frag_y)
{
    return fract(sin(frag_y+frag_x)*iGlobalTime+sin(frag_y-frag_x)*iGlobalTime);
    //return 1.0;
}

highp float onOff(highp float a, highp float b,highp float c)
{
    return step(c, sin(iGlobalTime + a*cos(iGlobalTime*b)));
    // return 1.0;
}
highp float fastLightFlash(highp vec2 p)  // Done
{
    highp float s = texture2D(inputImageTexture,vec2(1.,2.*cos(iGlobalTime))*iGlobalTime*8. + p*1.).x;
    s *= s;
    return s;
}

highp float rgbToGray(highp vec4 rgba)  // Done
{
    const highp vec3 W = vec3(0.2125, 0.7154, 0.0721);
    return dot(rgba.xyz, W);
}

highp vec3 hsv2rgb(highp vec3 c)    // Done
{
    highp vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
    highp vec3 p = abs(fract(c.xxx + K.xyz) * 6.0 - K.www);
    return c.z * mix(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);
}


// Smooth HSV to RGB conversion
highp vec3 hsv2rgb_smooth( highp vec3 c )    // Done
{
    highp vec3 rgb = clamp( abs(mod(c.x*6.0+vec3(0.0,4.0,2.0),6.0)-3.0)-1.0, 0.0, 1.0 );
    rgb = rgb*rgb*(3.0-2.0*rgb); // cubic smoothing
    return c.z * mix( vec3(1.0), rgb, c.y);
}
highp float brightness (highp vec3 color)
{
    return (0.2126*color.r + 0.7152*color.g + 0.0722*color.b);
}

///********* Sepia Effects *******//
highp vec4 sepia_shadowbox(highp vec4 color,highp vec2 uv) //Done
{
    //https://www.shadertoy.com/view/4lVXRw
    highp float boxradius = 0.9;
    highp float boxdarkness= 0.75;
    highp float desat =0.85;
    highp vec3  black = vec3(0.0,0.0,0.0);
    highp vec3  sepia =vec3(1.0,0.95,0.9);
    color = color * color * 1.4;// add some grit, overexposure
    highp float lum = (color.r + color.g + color.b) /3.0;
    color.rgb = color.rgb * (1.0 - desat) + sepia * vec3(lum,lum,lum) * desat;
    
    // shadowbox it
    highp float r = distance(vec2(0.5,0.5),uv)*2.0;
    // r is 0 to 1 for the radius from center
    
    if (r > boxradius){
        color.rgb = mix( color.rgb, black, (r - boxradius) * boxdarkness);
    }
    return color;
}

highp vec4 SepiaWithTime(highp vec4 color,highp float iTime)  //Done
{
    //    iTime = 1.0 ;
    // https://www.shadertoy.com/view/ldtSRr
    highp float timeFactor = ( 1.0 + sin( iTime ) ) * 0.5;
    highp mat4 rgba2sepia = mat4( 0.393, 0.349, 0.272, 0,
                                 0.769,0.686,0.534,0,
                                 0.189,0.168,0.131,0,
                                 0,0,0,1);
    
    highp mat4 rgba2sepiaDiff = mat4( 1.0 ) + timeFactor * ( rgba2sepia - mat4( 1.0 ) );
    return rgba2sepiaDiff * color ;
}

highp vec4 SepiaEffectwithAdjust( highp vec4 color,highp float adjust ) {
    // Ref :-  https://www.shadertoy.com/view/MdVGRt
    color.r = min(1.0, (color.r * (1.0 - (0.607 * adjust))) + (color.g * (0.769 * adjust)) + (color.b * (0.189 * adjust)));
    color.g = min(1.0, (color.r * (0.349 * adjust)) + (color.g * (1.0 - (0.314 * adjust))) + (color.b * (0.168 * adjust)));
    color.b = min(1.0, (color.r * (0.272 * adjust)) + (color.g * (0.534 * adjust)) + (color.b * (1.0 - (0.869 * adjust))));
    return color;
}

highp vec4 GrayImage(highp vec4  clr)  //Done
{
    highp vec3 luminanceWeighting = vec3(0.2125, 0.7154, 0.0721);
    
    highp float luminance = dot(clr.rgb, luminanceWeighting);
    highp vec3 greyScaleColor = vec3(luminance);
    return vec4( greyScaleColor, clr.a);
}
highp vec4 darkBlueColorFilter(highp vec4 c) //Done
{
    highp float g = (c.x + c.y + c.z) / 3.0;
    c = vec4(g,g,g,1.0);
    
    c.x *= 0.3;
    c.y *= 0.5;
    c.z *= 0.7;
    return c;
}

//******** Blend Filters Effect **********//
highp vec4 NormalBlend (highp vec4 c1,highp vec4 c2)  // Done
{
    highp vec4 clr;
    //     outputColor.r = c1.r + c2.r * c2.a * (1.0 - c1.a);
    //     outputColor.g = c1.g + c2.g * c2.a * (1.0 - c1.a);
    //     outputColor.b = c1.b + c2.b * c2.a * (1.0 - c1.a);
    //     outputColor.a = c1.a + c2.a * (1.0 - c1.a);
    highp float a = c1.a + c2.a * (1.0 - c1.a);
    highp float alphaDivisor = a + step(a, 0.0); // Protect against a divide-by-zero blacking out things in the output
    
    clr.r = (c1.r * c1.a + c2.r * c2.a * (1.0 - c1.a))/alphaDivisor;
    clr.g = (c1.g * c1.a + c2.g * c2.a * (1.0 - c1.a))/alphaDivisor;
    clr.b = (c1.b * c1.a + c2.b * c2.a * (1.0 - c1.a))/alphaDivisor;
    clr.a = a;
    
    return clr;
}

//******** VignetteEffect **********//

highp vec4 vignetteEffect(highp vec4 clr,highp float time, highp float strong)  // Done
{
    //highp float vigAmt = 3.+.3*sin(time + 5.*cos(time*5.));
    //highp float vignette = (1.0-vigAmt*(textureCoordinate.y-.5)*(textureCoordinate.y-.5))*(1.-vigAmt*(textureCoordinate.x-.5)*(textureCoordinate.x-.5));
    //return vignette;
    
    //highp float vigAmt = strong+.3*sin(time + 5.*cos(time*5.));
    
    highp float vigAmt = strong+.3*sin(time + 5.*cos(time*5.));
    highp float vignetteHorizontal = -vigAmt*(textureCoordinate.x-.5)*(textureCoordinate.x-.5);
    highp float vignetteVertical =  -vigAmt*(textureCoordinate.y-.5)*(textureCoordinate.y-.5);
    
//    vignetteHorizontal= 0.0;
    
    return  clr +(vignetteHorizontal+vignetteVertical);
    
    //highp vec4  color2 =   clr +vignetteHorizontal;
    //highp vec4  color3 =   clr+vignetteVertical;
    
   // return  color3 ;//* color2;
    
    /*void vignette( inout vec3 color, vec2 uv, float adjust ) {
        color.rgb -= max((distance(uv, vec2(0.5, 0.5)) - 0.25) * 1.25 * adjust, 0.0);
    }*/
    
     // GPUImage
    /*highp float d = distance(textureCoordinate, vec2(vignetteCenter.x, vignetteCenter.y));
     highp float percent = smoothstep(vignetteStart, vignetteEnd, d);
     gl_FragColor = vec4(mix(clr, vignetteColor, percent), clr);*/
    
}

highp vec4 vignetteSquareEffect(highp vec4 color,highp vec2 uv,highp float time,highp float strong)
{
     // highp float vigAmt = 3.+.3*sin(time + 5.*cos(time*5.));
    time = sin(time)*0.005;
    strong = strong-1.0;
    strong *= 0.02;
    
    if (strong<=0.0)
        strong= 0.0;
    
    highp float dx = distance(uv.x,0.5);
    highp float dy = distance(uv.y,0.5);
    highp float dxPercent = smoothstep(0.42+time-strong, 0.48+time-strong, dx);
    highp float dyPercent = smoothstep(0.42+time-strong, 0.48+time-strong, dy);
    
    color =  vec4(mix(color.rgb, vec3(0.0,0.0,0.0), dxPercent), color.a);
    color =  vec4(mix(color.rgb, vec3(0.0,0.0,0.0), dyPercent), color.a);
    return color;
}


highp vec4 vignetteEffect2(highp vec4 sourceImageColor,highp vec2 center, highp float start,highp float end,highp float time)
{
    highp float d = distance(textureCoordinate, vec2(center.x, center.y));
    highp float percent = smoothstep(start, end, d);
   // gl_FragColor = vec4(mix(sourceImageColor.rgb, vignetteColor, percent), sourceImageColor.a);
    return vec4(mix(sourceImageColor.rgb, vec3(0.0,0.0,0.0), percent), sourceImageColor.a);
}


highp vec4 SingleColorEffect (highp vec4 color ,highp int type )
{
    highp float lum;
    
    lum = dot(color.rgb, vec3( 0.85, 0.30, 0.10));
    
    if (type == 1){ // Red
        color.rgb = vec3(lum,0.0, 0.0);
    }else if(type == 2){  // Green
        color.rgb = vec3(0.0,lum, 0.0);
    }else if(type == 3){ // Blue
        
       // lum = (color.r+color.g+color.b)/3.0;
        color.rgb = vec3(0.0,0.0, lum);
    }
    
    return color;
}

//**************** Stripes and Line Effect **********//

highp vec4 stripes (highp vec4 clr,highp float time)  // Done
{
    clr *= (12.+mod(textureCoordinate.y*30.+time,1.))/13.;
    
    // clr += 0.42 * sin(textureCoordinate.y * 2.0);
    return  clr ;
}


highp float qScanLine (highp vec2 uv, highp float n) {
    return abs (sin (uv.y*M_PI*n)) ;
}

highp vec4 StaticStripes (highp vec4 clr)  // Done
{
    clr = clr * qScanLine (textureCoordinate.xy, 12000.0);                 // add scanlines
    return clr;
}
highp vec4 verticalStrips(highp vec4 color,highp vec2 uv,highp float time)  // Done
{
    // apply vertical scanlines
    highp float v = abs(sin(uv.x * 270.0 + time * 3.0));
    v += abs(sin(uv.x * 380.0 + time * 1.1));
    v *= abs(sin(uv.x * 300.0 + time * 1.8));
    v = mix(v, 0.5, 0.9) - 0.1;
    
    // overlay
    if(v > 0.5)
        color = 1.0 - (1.0 - 2.0 * (v - 0.5)) * (1.0 - color);
    else
        color = (2.0 * v) * color;
    return color;
}
highp vec4 verticalLine(highp vec4 color,highp vec2 uv,highp float time,highp float yaxis)  // Done
{
    time += 20.2356;
    
    /*//time = time *100.0;
     if (mod(uv.x + time*0.0005 + rand(uv)*0.000851 ,1.0) > 0.5 && mod(uv.x + time*0.0005 + rand(uv)*0.00085 ,1.0) < 0.53)
     {
     //color.xyz *= vec3(0.57,0.54,0.56);
     color.xyz *= vec3(0.36,0.38,0.39);
     }*/
    
    if (mod(uv.x + time*0.02 + rand(uv)*0.000851 ,1.0) > 0.5 && mod(uv.x + time*0.02 + rand(uv)*0.00085 ,1.0) < 0.519)
    {
        //color.xyz *= vec3(0.57,0.54,0.56);
        color.xyz *= vec3(0.6,0.6,0.6);
    }
    
    if (mod(uv.x - time*0.036 + rand(uv)*0.000851 ,1.0) > 0.5 && mod(uv.x - time*0.036 + rand(uv)*0.00085 ,1.0) < 0.51)
        color.xyz *= vec3(0.76,0.78,0.79);
    
    if (mod(uv.x + time*0.0009 + rand(uv)*0.000851 ,1.0) > 0.5 && mod(uv.x + time*0.0009 + rand(uv)*0.00085 ,1.0) < 0.505)
        color.xyz *= vec3(0.86,0.84,0.86);
    
    if (mod(uv.x + time*0.002 + rand(uv)*0.000851 ,1.0) > 0.5 && mod(uv.x + time*0.002 + rand(uv)*0.00085 ,1.0) < 0.506)
        color.xyz *= vec3(0.78,0.8,0.74);
    
    if (mod(uv.x - time*0.002 + rand(uv)*0.000851 ,1.0) > 0.5 && mod(uv.x - time*0.002 + rand(uv)*0.00085 ,1.0) < 0.506)
        color.xyz *= vec3(0.6,0.6,0.6);
    
    return color;
}

highp vec4 singleScanlineEffect(highp vec4 color,highp vec2 uv,highp float time)    //Done
{
    // https://www.shadertoy.com/view/MlX3Rs
    highp float y = mod(-time / 10.0,1.1);
    highp float d = sqrt(abs(uv.y - y));
    highp float a = 1.0 - smoothstep(0.001,0.2,d);
    // highp vec4 white = vec4(1.0,1.0,1.0,1.0);
    highp vec4 white = vec4(0.0,0.0,0.0,0.0);
    color = white * (a*0.5) + (color * (1.0-a));
    return color;
}
highp vec4 doubleScanLineEffect(highp vec4 color,highp vec2 uv,highp float time)  //Done
{
    /// thicker
    if (scanline && mod(uv.y + time*0.0005 + rand(uv)*0.000851 ,1.5) > 0.5 && mod(uv.y + time*0.0005 + rand(uv)*0.00085 ,1.5) < 0.6)
    {
        color.xyz *= vec3(0.57,0.54,0.56);
    }
    
    //thiner
    if (scanline && mod(uv.y + time*0.005 + rand(uv)*0.000851 ,1.5) > 0.7 && mod(uv.y + time*0.005 + rand(uv)*0.00085 ,1.5) < 0.726)
    {
        color.xyz *= vec3(0.57,0.54,0.56);
    }
    return color;
}

highp vec2 Tracking(highp vec2 uv,highp float time)
{
    highp float TRACKING_HEIGHT  = 0.15;
    highp float TRACKING_SEVERITY = 0.025;
    highp float TRACKING_SPEED = 0.058;
    
    // Tracking
    highp float t = time * TRACKING_SPEED;
    highp float fractionalTime = (t - floor(t)) * 1.3 - TRACKING_HEIGHT;
    if(fractionalTime + TRACKING_HEIGHT >= uv.y && fractionalTime <= uv.y)
    {
        uv.x -= fractionalTime * TRACKING_SEVERITY;
    }
    return uv;
}

highp vec4 vibrationEffect(sampler2D sampler,highp vec2 uv,highp float time )  // Done
{
    
    highp vec2 look = uv;
    highp float window = 1./(1.+20.*(look.y-mod(time/4.,1.))*(look.y-mod(time/4.,1.)));
    window = 1.0 ;
    look.x = look.x + sin(look.y*10. + time)/50.*onOff(4.,4.,.3)*(1.+cos(time*80.))*window;
    
    highp vec4 video = texture2D(sampler,look);
    return video;
}

highp vec4 GrainEffect(highp vec4 clr, highp float strength) // Done
{
    //https://www.shadertoy.com/view/4sXSWs
    highp vec2 iResolution = vec2(1.0,1.0);
    highp vec2 uv = textureCoordinate.xy / iResolution.xy;
    //highp float strength = 16.0;
    
    highp float x = (uv.x + 4.0 ) * (uv.y + 4.0 ) * (iGlobalTime * 10.0);
    highp vec4 grain = vec4(mod((mod(x, 13.0) + 1.0) * (mod(x, 123.0) + 1.0), 0.01)-0.00005) * strength;
    clr = clr + grain;
    return clr;
}

//******** move Video Up Down *****///

highp vec2 upDownEffect(sampler2D sampler,highp vec2 uv,highp float time)  //Done
{
    /*
    // https://www.shadertoy.com/view/ldjGzV
    time *= 0.02;
    
    highp vec2 look = uv;
    // highp float vShift = 0.4 * onOff(2.,3.,.9)*(sin(time)*sin(time*20.) +                                           (0.5 + 0.1*sin(time*200.)*cos(time)));
    
     highp float vShift = 0.4*onOff(2.,3.,.9)*(sin(time)*sin(time*20.) +(0.5 + 0.1*sin(time*200.)*cos(time)));
//    highp float vShift = 0.4*(sin(time)*sin(time) +(0.5 + 0.1*sin(time)*cos(time)));
    
    //    highp float vShift = 0.4 * onOff(1.0,2.0,0.9) * (sin(time)*sin(time) + ( 0.5 + 0.1 * sin(time)*cos(time)));
    //    highp float vShift = 0.4 * onOff(4.,4.,0.3) * (sin(time)*sin(time) + ( 0.5 + 0.1 * sin(time)*cos(time)));
    look.y = mod(look.y + vShift, 1.);
    highp vec4 video = texture2D(sampler,look);
    */
    
    lowp float vertJerkOpt = 0.5;
    lowp float vertMovementOpt = 0.4;

    time = time / 5.0;
//    time=mod(time,5.0);
    
   // snoise(vec2(time, uv.y * 0.3))
    
   /* lowp float vertMovementOn = (step(snoise(vec2(time*0.2,8.0)),0.4))*vertMovementOpt;
    lowp float vertJerk = (1.0-step(snoise(vec2(time*1.5,8.0)),0.6))*vertJerkOpt;
    lowp float vertJerk2 = (1.0-step(snoise(vec2(time*5.5,8.0)),0.2))*vertJerkOpt;
    lowp float yOffset = abs(sin(time)*4.0)*vertMovementOn+vertJerk*vertJerk*0.3;
    lowp float y = mod(uv.y+yOffset,1.0);
    */
   
    //lowp float vertMovementOn = step((time*0.2),0.4)*vertMovementOpt;// vertMovementOpt;
    
    lowp float vertMovementOn = (step(snoise(vec2(time*0.2,8.0)),0.4))*vertMovementOpt;
    lowp float vertJerk = (1.0-step(snoise(vec2(time*1.5,8.0)),0.6))*vertJerkOpt;
    lowp float yOffset = abs(sin(time)*4.0)*vertMovementOn+vertJerk*vertJerk*0.3;

    //lowp float yOffset = abs(sin(time)*4.0)*vertMovementOn;
    lowp float y = mod(uv.y+yOffset,1.0);

    highp vec4 video = texture2D(sampler,vec2(uv.x,y));

    // return video;
    return vec2(uv.x,y);

}

//**************************  Noise Effects ***********************//
highp vec2 sc(in highp float i)       // DOne
{
    //    return vec2(sin(i),cos(i));
    return vec2(sin(i),sin(i));
}

highp float snoise(in highp vec2 i, in highp float seed){  // // DOne
    // i*=sc(seed);
    i*=sc(iGlobalTime*textureCoordinate.y);
    return fract(sin(dot(i,vec2(12.9898,78.233))) * 43758.5453);
}
//#define RAND(p, seed) poltergeist((p), (seed))
#define RAND(p, seed) snoise((p), (seed))

highp vec4 RingNoise(highp vec4 color,highp vec2 uv,highp float time )    // // DOne
{
    highp float r = RAND(uv.xy, 0.3);
    highp vec4 noiseColor = vec4(r, r, r, -r);
    color =  NormalBlend(noiseColor,color);
    return color ;
}


#define SEED 1.123456789
#define HASHM mat3(40.15384, 31.973157,31.179219,10.72341,13.123009,41.441023,-311.61923,10.41234,178.127121)

highp float smallPixelNoise_Hash(highp vec3 p) {     //Done
    p = fract((vec3(p.x, p.y, p.z) + SEED * 1e-3) * HASHM);
    p += dot(p, p.yzx + 41.19);
    return fract((p.x + p.y) * p.z);
}
highp float smallPixelNoise_Hash(highp vec2 p) {     ////Done
    highp vec3 p3 = fract(vec3(p.x, p.y, (p.x + p.y + SEED * 1e-7)) * HASHM);
    p3 += dot(p3, p3.yzx + 41.19);
    return fract((p3.x + p3.y) * p3.z);
}

highp vec4 smallPixelNoiseEffect(highp vec4 color ,highp vec2 uv,highp float time,highp int type)    //Done
{
    highp float value;
    if (type == 1)
        value = smallPixelNoise_Hash(vec3(uv, (uv.x + uv.y) * time * 0.001));  // small
    else
        value = smallPixelNoise_Hash(uv + time * 0.001);  // more Small
    
    //    highp vec4 noiseClr = vec4(vec3(value < 1. ? iGlobalTime : 0.), 1.0);
    //highp vec4 noiseClr = vec4(vec3(value < 0.1 ? value : 0.0), value);  // org
    
    /* highp vec4 noiseClr = vec4(vec3(value < 0.56 ? value : 0.0),value);
    return  NormalBlend(noiseClr,color); */

    color.rgb += vec3(value < 0.42 ? value : 0.0);
   
    return color;
}

highp vec4 cornerWaveNoise(highp vec4 color,highp vec2 uv,highp float time)  // Done
{
    //  https://www.shadertoy.com/view/4lSSRw
    
    //highp vec2 x = uv.xy;
    highp vec2 i = uv.xy;
    //vec3 a=vec3(max((fract(dot(sin(x),x))-.99)*89.,.0));
    
    highp vec3 a = vec3(max((fract(dot(sin(i),i+.0)+time*0.36)-.98)*89.,.0));
    highp vec4  noiseColor ;//= vec4(1.0,0.1,0.2,1.0);
    noiseColor = vec4(-a,a);
    color =  NormalBlend(noiseColor,color);
    
    //-**-*
    //noiseColor = 9.*fract(dot(sin(x),x)+iDate.wwww*.1)-8.;
    //highp float o = 9.*fract(dot(sin(x),x)+time*.1)-8.;
    //noiseColor = 90.*fract(dot(sin(i),i) + vec4(time)*.1) - 8.;
    //    noiseColor = vec4(onOff(60.0,40.0,0.9));
    return color;
    //    return noiseColor;
}




//highp vec4 TVTubeEffect (highp vec4 color,highp vec2 uv,highp float time )
highp vec4 TVTubeEffect (sampler2D sampler,highp vec2 uv,highp float time ) // Done
{
    // https://www.shadertoy.com/view/Xtf3zX
    // center
    uv -= 0.5;
    highp vec2 uv2 = uv;
    
    // correct aspect ratio
    // highp float ar = iResolution.x / iResolution.y;
    highp float ar = 1.0 / 1.0;
    uv.x *= ar;
    
    // measure distance from center
    highp float dd = dot(uv, uv);
    highp float dd2 = dot(uv2, uv2);
    
    // warp
    uv = (uv * dd) * 0.4 + uv * 0.6;
    uv2 = (uv2 * dd2) * 0.4 + uv2 * 0.6;
    
    //compute vignette
    highp float vignette = (1.0 - abs(uv2.x)) * (1.0 - abs(uv2.y)) / (1.0 + dd2);
    vignette *= vignette * 2.0;
    vignette *= max(0.0, 1.0 - 2.75 * max(abs(uv2.x), abs(uv2.y)));
    vignette = pow(vignette, 0.25);
    
    // restore
    uv += 0.5;
    uv2 += 0.5;
    
    // sample texture
    highp vec4 color = texture2D(sampler, uv2);
    // debug checker with aspect ratio correction
    highp float a = mod(uv.x * 20.0, 2.0);
    highp float b = mod(uv.y * 20.0, 2.0);
    highp float c = 0.3;
    if(int(a) != int(b))
        c = 0.7;
    
    // apply vertical scanlines
    highp float v = abs(sin(uv.x * 270.0 + time * 3.0));
    v += abs(sin(uv.x * 380.0 + time * 1.1));
    v *= abs(sin(uv.x * 300.0 + time * 1.8));
    v = mix(v, 0.5, 0.9) - 0.1;
    // overlay
    if(v > 0.5)
        color = 1.0 - (1.0 - 2.0 * (v - 0.5)) * (1.0 - color);
    else
        color = (2.0 * v) * color;
    
    // apply vignette
    color *= vignette;
    return color;
}


highp vec4 badTVEffect (highp vec4 color,sampler2D sampler,highp vec2 uv, highp float time)     //Done
{
    //   https://www.shadertoy.com/view/lsf3z4
    
    //vec2 pos = ( fragCoord.xy / resolution.xy );
    //pos += .01 * vec2(1. * sin(time), 1. * cos(time));
    //pos *= 3.;
    highp vec2 pos =  uv.xy/vec2(1.0,1.0);
    
    //highp vec3 oricol = texture2D(iChannel0, vec2(pos.x,pos.y)).xyz;
    highp vec3 oricol = color.xyz;
    highp vec3 col;
    
    col.r = texture2D(sampler, vec2(pos.x+0.035*sin(0.02*time),pos.y)).x;
    col.g = texture2D(sampler, vec2(pos.x+0.000                ,pos.y)).y;
    col.b = texture2D(sampler, vec2(pos.x-0.035*sin(0.02*time),pos.y)).z;
    
    highp float c = 1.;
    
    //c += sin(pos.x * 20.01);
    
    c += 2. * sin(time * 4. + pos.y * 1000.);
    c += 1. * sin(time * 1. + pos.y * 800.);
    //     c += 20. * sin(time * 1. + (pos.y - 0.0) * 9000.);   // not Working properly
    c += 1. * cos(time * 1. + pos.x * 1.);
    
    //vignetting
    c *= sin(pos.x*3.15);
    c *= sin(pos.y*3.);
    c *= .9;
    pos += time;
    
    //color = vec4(col.x * r*c*.35, col.y * b*c*.95, col.z * g*c*.35, 1);
    //c = 1.0;
    color = vec4(col.x *c, col.y *c, col.z *c, 1);
    
    return color ;
}


///////******************* TVDamage Glitch Effect  *************//// Done
//   https://www.shadertoy.com/view/MlfBRS

highp vec3 mod289(highp vec3 x) {  // tvDamage Done
    return x - floor(x * (1.0 / 289.0)) * 289.0;
}
highp vec2 mod289(highp vec2 x) {  // tvDamage Done
    return x - floor(x * (1.0 / 289.0)) * 289.0;
}
highp vec3 permute(highp vec3 x) {  // tvDamage Done
    return mod289(((x*34.0)+1.0)*x);
}

highp float snoise(highp vec2 v)   // tvDamage Done
{
    const highp vec4 C = vec4(0.211324865405187,  // (3.0-sqrt(3.0))/6.0
                              0.366025403784439,  // 0.5*(sqrt(3.0)-1.0)
                              -0.577350269189626,  // -1.0 + 2.0 * C.x
                              0.024390243902439); // 1.0 / 41.0
    
    // First corner
    highp vec2 i  = floor(v + dot(v, C.yy) );
    highp vec2 x0 = v -   i + dot(i, C.xx);
    
    // Other corners
    highp vec2 i1;
    //i1.x = step( x0.y, x0.x ); // x0.x > x0.y ? 1.0 : 0.0
    //i1.y = 1.0 - i1.x;
    i1 = (x0.x > x0.y) ? vec2(1.0, 0.0) : vec2(0.0, 1.0);
    // x0 = x0 - 0.0 + 0.0 * C.xx ;
    // x1 = x0 - i1 + 1.0 * C.xx ;
    // x2 = x0 - 1.0 + 2.0 * C.xx ;
    highp vec4 x12 = x0.xyxy + C.xxzz;
    x12.xy -= i1;
    
    // Permutations
    i = mod289(i); // Avoid truncation effects in permutation
    highp vec3 p = permute( permute( i.y + vec3(0.0, i1.y, 1.0 ))
                           + i.x + vec3(0.0, i1.x, 1.0 ));
    
    highp vec3 m = max(0.5 - vec3(dot(x0,x0), dot(x12.xy,x12.xy), dot(x12.zw,x12.zw)), 0.0);
    m = m*m ;
    m = m*m ;
    
    // Gradients: 41 points uniformly over a line, mapped onto a diamond.
    // The ring size 17*17 = 289 is close to a multiple of 41 (41*7 = 287)
    
    highp vec3 x = 2.0 * fract(p * C.www) - 1.0;
    highp vec3 h = abs(x) - 0.5;
    highp vec3 ox = floor(x + 0.5);
    highp vec3 a0 = x - ox;
    
    // Normalise gradients implicitly by scaling m
    // Approximation of: m *= inversesqrt( a0*a0 + h*h );
    m *= 1.79284291400159 - 0.85373472095314 * ( a0*a0 + h*h );
    
    // Compute final noise value at P
    highp vec3 g;
    g.x  = a0.x  * x0.x  + h.x  * x0.y;
    g.yz = a0.yz * x12.xz + h.yz * x12.yw;
    return 130.0 * dot(m, g);
}

highp float rand55(highp vec2 co){  // tvDamage Done
    return fract(sin(dot(co.xy ,vec2(12.9898,78.233))) * 43758.5453) * 2.0 - 1.0;
}

highp float offset(highp float blocks,highp vec2 uv) {  // tvDamage Done
    return rand55(vec2(iGlobalTime, floor(uv.y)));
}

highp vec4 tvDamage(highp vec4 col, sampler2D sampler,highp vec2 uv , highp float time) // tvDamage Done
{
    //   https://www.shadertoy.com/view/MlfBRS
    
    // Amount of effect
    highp float amount = 0.1;
    
    time = time * 2.0;
    
    // Create large, incidental noise waves
    highp float noise = max(0.0, snoise(vec2(time, uv.y * 0.3)) - 0.3) * (1.0 / 0.7);
    
    // Offset by smaller, constant noise waves
    noise = noise + (snoise(vec2(time*10.0, uv.y * 2.4)) - 0.5) * 0.15;
    
    // Apply the noise as x displacement for every line
    highp float xpos = uv.x - noise * noise * (amount * 8.0);
    
    // Noise shift to red channel
    col.r = texture2D(sampler, uv + vec2(offset(16.0, uv) * 0.03, 0.0)).r;
    
    // Shift green/blue channels (using the red channel)
    col.g = mix(col.g, texture2D(sampler, vec2(xpos + noise * 0.05, uv.y)).g, 1.0);
    col.b = mix(col.b, texture2D(sampler, vec2(xpos - noise * 0.05, uv.y)).b, 1.0);
    return col;
}



//***********  Drop Effects ***********////   // Done

// https://www.shadertoy.com/view/ltffzl

#define S(a, b, t) smoothstep(a, b, t)
#define HAS_HEART
#define USE_POST_PROCESSING

highp vec3 N13(highp float p) {
    //  from DAVE HOSKINS
    highp vec3 p3 = fract( vec3(p) * vec3(.1031,.11369,.13787));
    p3 += dot(p3, p3.yzx + 19.19);
    return fract( vec3((p3.x + p3.y)*p3.z, (p3.x+p3.z)*p3.y, (p3.y+p3.z)*p3.x));
}

highp vec4 N14(highp float t) {
    return fract(sin(t*vec4(123., 1024., 1456., 264.))*vec4(6547., 345., 8799., 1564.));
}
highp float N(highp float t) {
    return fract(sin(t*12345.564)*7658.76);
}

highp float Saw(highp float b, highp float t) {
    return S(0., b, t)*S(1., b, t);
}

highp vec2 DropLayer2(highp vec2 uv,highp float t) {
    
    highp vec2 UV = uv;
    
    uv.y += t*0.75;
    highp vec2 a = vec2(6., 1.);
    highp vec2 grid = a*2.;
    highp vec2 id = floor(uv*grid);
    
    highp float colShift = N(id.x);
    uv.y += colShift;
    
    id = floor(uv*grid);
    highp vec3 n = N13(id.x*35.2+id.y*2376.1);
    highp vec2 st = fract(uv*grid)-vec2(.5, 0);
    
    highp float x = n.x-.5;
    
    highp float y = UV.y*20.;
    highp float wiggle = sin(y+sin(y));
    x += wiggle*(.5-abs(x))*(n.z-.5);
    x *= .7;
    highp float ti = fract(t+n.z);
    y = (Saw(.85, ti)-.5)*.9+.5;
    highp vec2 p = vec2(x, y);
    
    highp float d = length((st-p)*a.yx);
    
    highp float mainDrop = S(.4, .0, d);
    
    highp float r = sqrt(S(1., y, st.y));
    highp float cd = abs(st.x-x);
    highp float trail = S(.23*r, .15*r*r, cd);
    highp float trailFront = S(-.02, .02, st.y-y);
    trail *= trailFront*r*r;
    
    y = UV.y;
    highp float trail2 = S(.2*r, .0, cd);
    highp float droplets = max(0., (sin(y*(1.-y)*120.)-st.y))*trail2*trailFront*n.z;
    y = fract(y*10.)+(st.y-.5);
    highp float dd = length(st-vec2(x, y));
    droplets = S(.3, 0., dd);
    highp float m = mainDrop+droplets*r*trailFront;
    
    //m += st.x>a.y*.45 || st.y>a.x*.165 ? 1.2 : 0.;
    return vec2(m, trail);
}

highp float StaticDrops(highp vec2 uv, highp float t) {
    
    uv *= 40.;
    highp vec2 id = floor(uv);
    uv = fract(uv)-.5;
    highp vec3 n = N13(id.x*107.45+id.y*3543.654);
    highp vec2 p = (n.xy-.5)*.7;
    highp float d = length(uv-p);
    
    highp float fade = Saw(.025, fract(t+n.z));
    highp float c = S(.3, 0., d)*fract(n.z*10.)*fade;
    return c;
}

highp vec2 Drops(highp vec2 uv,highp float t,highp float l0,highp float l1,highp float l2) {
    
    highp float s = StaticDrops(uv, t)*l0;
    highp vec2 m1 = DropLayer2(uv, t)*l1;
    highp vec2 m2 = DropLayer2(uv*1.85, t)*l2;
    
    highp float c = s+m1.x+m2.x;
    c = S(.3, 1., c);
    
    return vec2(c, max(m1.y*l0, m2.y*l1));
}


highp vec4 DropEffects (sampler2D sampler,highp vec2 fragCoord , highp float iTime)
{
    highp vec3  iResolution = vec3(0.7,1.0,.0);
    highp vec3  iMouse = vec3(0.5,0.5,.0);
    
    //      highp vec2 uv = (fragCoord.xy-.5*iResolution.xy) / iResolution.y;
    //      highp vec2 UV = fragCoord.xy/iResolution.xy;
    
    highp vec2 uv = fragCoord.xy-.5;
    highp vec2 UV = fragCoord.xy ;
    
    
    uv.y = -uv.y;
    
    
    //highp vec3 M = iMouse.xyz/iResolution.xyz;
    highp vec3 M = iMouse.xyz;
    // M = vec3(0.2);
    highp float T = iTime+M.x*2.;
    
#ifdef HAS_HEART
    T = mod(iTime, 102.);
    T = mix(T, M.x*102., M.z>0.?1.:0.);
#endif
    
    
    highp float t = T*.2;
    
    highp float rainAmount = iMouse.z>0. ? M.y : sin(T*.05)*.3+.7;
    rainAmount =  M.y ;// sin(T*.05)*.3+.7;
    
    
    highp float maxBlur = mix(3., 6., rainAmount);
    highp float minBlur = 2.;
    
    highp float story = 0.;
    highp float heart = 0.;
    
#ifdef HAS_HEART
    story = S(0., 70., T);
    
    t = min(1., T/70.);                        // remap drop time so it goes slower when it freezes
    t = 1.-t;
    t = (1.-t*t)*70.;
    
    highp float zoom= mix(.3, 1.2, story);        // slowly zoom out
    uv *=zoom;
    minBlur = 4.+S(.5, 1., story)*3.;        // more opaque glass towards the end
    maxBlur = 6.+S(.5, 1., story)*1.5;
    
    highp vec2 hv = uv-vec2(.0, -.1);                // build heart
    hv.x *= .5;
    highp float s = S(110., 70., T);                // heart gets smaller and fades towards the end
    hv.y-=sqrt(abs(hv.x))*.5*s;
    heart = length(hv);
    heart = S(.4*s, .2*s, heart)*s;
    rainAmount = heart;                        // the rain is where the heart is
    
    maxBlur-=heart;                            // inside the heart slighly less foggy
    uv *= 1.5;                                // zoom out a bit more
    t *= .25;
#else
    highp float zoom = -cos(T*.2);
    uv *= .7+zoom*.3;
#endif
    UV = (UV-.5)*(.9+zoom*.1)+.5;
    
    highp float staticDrops = S(-.5, 1., rainAmount)*2.;
    highp float layer1 = S(.25, .75, rainAmount);
    highp float layer2 = S(.0, .5, rainAmount);
    
    
    highp vec2 c = Drops(uv, t, staticDrops, layer1, layer2);
#ifdef CHEAP_NORMALS
    highp vec2 n = vec2(dFdx(c.x), dFdy(c.x));// cheap normals (3x cheaper, but 2 times shittier ;))
#else
    highp vec2 e = vec2(.001, 0.);
    highp float cx = Drops(uv+e, t, staticDrops, layer1, layer2).x;
    highp float cy = Drops(uv+e.yx, t, staticDrops, layer1, layer2).x;
    highp vec2 n = vec2(cx-c.x, cy-c.x);        // expensive normals
#endif
    
    
#ifdef HAS_HEART
    n *= 1.-S(60., 85., T);
    c.y *= 1.-S(80., 100., T)*.8;
#endif
    
    highp float focus = mix(maxBlur-c.y, minBlur, S(.1, .2, c.x));
    //highp vec3 col = textureLod(iChannel0, UV+n, focus).rgb;
    highp vec3 col = texture2D(sampler, UV+n, focus).rgb;
    
    
#ifdef USE_POST_PROCESSING
    t = (T+3.)*.5;                                        // make time sync with first lightnoing
    highp float colFade = sin(t*.2)*.5+.5+story;
    col *= mix(vec3(1.), vec3(.8, .9, 1.3), colFade);    // subtle color shift
    highp float fade = S(0., 10., T);                            // fade in at the start
    highp float lightning = sin(t*sin(t*10.));                // lighting flicker
    lightning *= pow(max(0., sin(t+sin(t))), 10.);        // lightning flash
    col *= 1.+lightning*fade*mix(1., .1, story*story);    // composite lightning
    col *= 1.-dot(UV-=.5, UV);                            // vignette
    
#ifdef HAS_HEART
    col = mix(pow(col, vec3(1.2)), col, heart);
    fade *= S(102., 97., T);
#endif
    
    col *= fade;                                        // composite start and end fade
#endif
    
    //col = vec3(heart);
    highp vec4 fragColor = vec4(col, 1.);
    
    return fragColor;
}



///*******************************  Under Construcion Function *********************** ///



/*highp vec4 get_lum(sampler2D sampler , highp vec2 uv)
{
    highp vec4 col = vec4(1.);
    highp vec4 sampled = texture2D(sampler,uv);
    
    highp float lumiosity = brightness(sampled.xyz);
    
    col.a = sampled.a;
    col.xyz = vec3(lumiosity);
    
    return col;
}*/

/*highp vec4 VHS_bleed( sampler2D sampler, highp vec2 uv ,highp int blurness)
{
    highp vec4 col = vec4(1.);
    highp vec4 sampled = texture2D(sampler,uv);
    
    col = get_lum(sampler,uv);
    highp float lum = brightness(col.xyz);
    
    highp float multiplier = lum+1.*15.;
    highp float j=0.;
    
    for (int i = 0;i<4;i++)
    {
        col += (get_lum(sampler,uv + vec2( float(i) * 0.0001 * multiplier * (col.x + 1.2), 0. ) ) / (vec4(blurness * 4)/(multiplier*0.11))) * vec4(j / float(blurness)/2. * 4. );
        col += (get_lum(sampler,uv + vec2( float(i) * -0.0001 * multiplier * (col.x + 1.2), 0. ) ) / (vec4(blurness * 4)/(multiplier*0.11))) * vec4(j / float(blurness)/2. * 4. );
        
        j+=1.;
    }
    return col;
}*/



highp vec4 whiteLineEffect(highp vec4 color,highp vec2 uv,highp float time)
{
    // Random white line
    //if (uv.y < 0.25 && mod(uv.y+time * 0.1,0.01)*9.-uv.y*0.3 > 0.02 && rand(uv+time * 0.1) >= 0.5)
    if (uv.y > 0.75 && mod(uv.y+time * 0.1,0.1)*9.-uv.y*0.3 > 0.02 && rand(uv+time*0.1) >= 0.5)
    {
        //color.xyz = vec3(1.);
        color.xyz += vec3( 0.76);
    }
    
    if((mod(time,20.) > 3.0 )&& (mod(time,20.) < 5.) &&  (mod(uv.y,0.5) > 0.02+rand(vec2(time))*0.5) &&  (mod(uv.y + time*0.005 + rand(uv)*0.00085 ,1.) < 0.03 +  rand(vec2(time))*2.0 ))
    {
        // col.xyz = vec3(1.);
        // col.xyz = vec3(0.8,0.8,0.8);
        //color.xyz = vec3(0.8);
        color.xyz = vec3(0.76);

    }
    return color;
}



highp vec4 linesEffect(highp vec4 col, sampler2D sampler,highp vec2 uv , highp float time2)
{
    // highp float global_time = time * 1.2 + sin(time*0.2)*10.;
    highp float time = time2 * 1.2 + sin(time2*0.2)*10.;
    
    highp vec2 uv2 = uv;
    uv2.x += sin(uv.y*20.+time)*0.01 * (1. - uv.y)*1.2;
    
    //highp vec4 col = vec4(0.);
    highp vec4 sample_tex = texture2D(sampler,uv2);
    
    highp float lum = brightness(col.xyz);
    // col = VHS_bleed(sampler,uv,64);
   // col = texture2D(sampler,uv);

    //col.xyz *= texture(sampler,uv2).xyz;
    
    // Chromatic abberation base from
    // http://gamedev.stackexchange.com/questions/58408/how-would-you-implement-chromatic-aberration
    
    highp vec2 rOffset = vec2(-0.005,0.005);
    highp vec2 gOffset = vec2(0.,-0.005);
    highp vec2 bOffset = vec2(0.005,0.005);
     
    highp vec4 rValue = texture2D(sampler, uv2 - rOffset*rand(uv/2.+time)*1.2*(lum+1.6)*1.2);
    highp vec4 gValue = texture2D(sampler, uv2 - gOffset*rand(uv/2.+time)*1.3*(lum+1.5)*1.2);
    highp vec4 bValue = texture2D(sampler, uv2 - bOffset*rand(uv/2.+time)*1.2*(lum+1.7)*1.2);
    
    col.x *= rValue.r;
    col.y *= gValue.g;
    col.z *= bValue.b;
    
    
 /* col.x += (col.x*0.28 + rand(uv+time)*0.05 + sin(uv.y*2.2+1.7)*1.32) * ((lum+1.)*0.1);
    col.y += (col.y*0.15 + rand(uv+time)*0.05) * (lum*0.01);
    col.z += (col.z*0.28 + rand(uv+time)*0.05 + sin(uv.y*2.2+1.85)*1.65) * ((lum+1.)*0.1);
    */

    //Scan line thing
    // already done in other
    /*if (scanline && mod(uv.y + time*0.0005 + rand(uv)*0.000851 ,1.5) > 0.5 && mod(uv.y + time*0.0005 + rand(uv)*0.00085 ,1.5) < 0.6)
    {
          col.xyz *= vec3(0.57,0.54,0.56);
    }
    if (scanline && mod(uv.y + time*0.0005 + rand(uv)*0.000851 ,1.5) > 0.7 && mod(uv.y + time*0.0005 + rand(uv)*0.00085 ,1.5) < 0.726)
    {
        col.xyz *= vec3(0.57,0.54,0.56);
    }*/

     // Square Noise
    if (rand(mod(uv*0.1+time,1.)) >= 0.7)
    {
//        col.xyz *= vec3(0.57,0.54,0.56) + sin(rand(mod(uv*0.1+time,1.))*2.)*0.5;
//        col.xyz *= vec3(0.01,0.01,0.01) + sin(rand(mod(uv*0.1+time,1.))*2.)*0.5;
    }
    
    
    // Random white line
    //if (uv.y < 0.25 && mod(uv.y+time * 0.1,0.01)*9.-uv.y*0.3 > 0.02 && rand(uv+time * 0.1) >= 0.5)
    if (uv.y > 0.75 && mod(uv.y+time * 0.1,0.1)*9.-uv.y*0.3 > 0.02 && rand(uv+time*0.1) >= 0.5)
    {
          col.xyz = vec3(1.);
    }
    
   /* if ((mod(time,20.) > 3. && mod(time,20.) < 5.) && mod(uv.y,0.5) > 0.02+rand(vec2(time))*0.5 && mod(uv.y,0.5) < 0.03+rand(vec2(time))*0.5 )
    {
        col.xyz = vec3(0.8);
    }*/
    
//    if ((mod(time,20.) > 3.0 )&& (mod(time,20.) < 5.) &&  (mod(uv.y,0.5) > 0.02+rand(vec2(time))*0.5) &&  (mod(uv.y,0.3) < 0.03 +// rand(vec2(time))*0.5 ))
    
//    if ((mod(time,20.) > 3. && mod(time,20.) < 5.) && mod(uv.y,0.5) > 0.02+rand(vec2(time))*0.5 && mod(uv.y,0.5) < 0.03+rand(vec2(time))*0.5 )
    
   // mod(uv.y + time*0.0005 + rand(uv)*0.00085 ,1.5)
    
    if((mod(time,20.) > 3.0 )&& (mod(time,20.) < 5.) &&  (mod(uv.y,0.5) > 0.02+rand(vec2(time))*0.5) &&  (mod(uv.y + time*0.005 + rand(uv)*0.00085 ,1.) < 0.03 +  rand(vec2(time))*2.0 ))
    {
       // col.xyz = vec3(1.);
        // col.xyz = vec3(0.8,0.8,0.8);
         col.xyz = vec3(0.8);
    }
    
    
    return col;
    //  return vec4(1.0,0.0,1.0,1.0);
}



highp vec2 hash( highp vec2 x )  // replace this by something better
{
    highp vec2 k = vec2( 0.3183099, 0.3678794 );
     x = x*k + k.yx ;
    
//    x = x*k + k.yx + rand1(iGlobalTime, x.x) ;
    // sin(textureCoordinate.y * 2.0)
    
    return -1.0 + 2.0*fract( 16.0 * k*fract( x.x*x.y*(x.x+x.y)) );
}

highp float noise( in highp vec2 p )
{
    highp vec2 i = floor( p );
    highp vec2 f = fract( p );
    
    highp vec2 u = f*f*(3.0-2.0*f) ;//*sin(iGlobalTime * 2.0);
    
    return mix( mix( dot( hash( i + vec2(0.0,0.0) ), f - vec2(0.0,0.0) ),
                    dot( hash( i + vec2(1.0,0.0) ), f - vec2(1.0,0.0) ), u.x),
               mix( dot( hash( i + vec2(0.0,1.0) ), f - vec2(0.0,1.0) ),
                   dot( hash( i + vec2(1.0,1.0) ), f - vec2(1.0,1.0) ), u.x), u.y);
}

// Gold Noise function

highp vec4 gold_noise(highp vec4 color,highp vec2 coordinate, highp float seed)
{
   // https://www.shadertoy.com/view/ltB3zD
    
    highp float PHI = 1.61803398874989484820459 * 00000.1; // Golden Ratio
    highp float PI  = 3.14159265358979323846264 * 00000.1; // PI
    highp float SRT = 1.41421356237309504880169 * 10000.0; // Square Root of Two

    highp float noise  = fract(sin(dot(coordinate*seed, vec2(PHI, PI)))*SRT);
    // return vec4(vec3(noise),-noise);
    
    highp vec4 noiseClr =  vec4(vec3(noise),-noise);

   // return color*noiseClr;
    //return NormalBlend(noise,color);
    return NormalBlend(noiseClr,color);
}

highp vec4 goldWhite_noise(highp vec4 color,highp vec2 coordinate, highp float seed)
{
    highp float PHI = 1.61803398874989484820459 * 00000.1; // Golden Ratio
    highp float PI  = 3.14159265358979323846264 * 00000.1; // PI
    highp float SRT = 1.41421356237309504880169 * 10000.0; // Square Root of Two

    //PI *= 00.001;
    SRT *= 10.0;
    
    // seed +=PHI;
    //PHI *= .09;          // correct the Angle of Lines ; horizontal
    
    highp float noise  = fract(sin(dot(coordinate*seed, vec2(PHI, PI)))*SRT);
    //return vec4(noise); //    White line Noise
    
    return NormalBlend(vec4(noise),color);

//    return vec4(-noise,-noise,-noise,noise); // black Line
}


/*highp float hash2(highp vec2 x) {
    highp vec2 k = vec2( 0.3183099, 0.3678794 );
    x = x*k + k.yx ;
    //    x = x*k + k.yx + rand1(iGlobalTime, x.x) ;
    // sin(textureCoordinate.y * 2.0)
    //return -1.0 + 2.0*fract( 16.0 * k*fract( x.x*x.y*(x.x+x.y)) );
    return -1.0 + 2.0*fract( 16.0 * k.x*fract( x.x*x.y*(x.x+x.y)) );
}
*/

/*highp float hash2(highp vec2 uv)
{
    highp float r;
    uv = abs(mod(10.*fract((uv+1.1312)*31.),uv+2.));
    uv = abs(mod(uv.x*fract((uv+1.721711)*17.),uv));
    return r = fract(10.* (7.*uv.y + 31.*uv.x));
}*/

/*highp vec4 sideColorEffect(highp vec4 color , highp float time )
{
    // side Color
//    highp vec2 uv2 = .5*(abs(sin(time/2.))+.5)*(2.0*textureCoordinate.xy-iResolution.xy)/iResolution.y;
//     orgColor = vec4(uv2.x,uv2.x,uv2.x,1.);
    highp vec2 uv2 = .8*(abs(sin(time/2.))+.8)*(2.0*textureCoordinate.xy);
    //highp vec4 effectColor = vec4(uv2.x,uv2.x,uv2.x,-uv2.x);
    highp vec4 effectColor = vec4(uv2.x,uv2.x,uv2.x,-uv2.x);
    color = effectColor*color;
//    color = NormalBlend(effectColor,color);
//    color = effectColor;
    return color;
}
*/


/*const highp int   c_samplesX    = 15;  // must be odd
const highp int   c_samplesY    = 15;  // must be odd
 const highp float c_textureSize = 512.0;
*/

const highp int   c_samplesX    = 3 ;  // must be odd
const highp int   c_samplesY    = 3 ;  // must be odd
const highp float c_textureSize = 512.0;

const highp int   c_halfSamplesX = c_samplesX / 2;
const highp int   c_halfSamplesY = c_samplesY / 2;
const highp float c_pixelSize = (1.0 / c_textureSize);

highp float Gaussian (highp float sigma, highp float x)
{
    return exp(-(x*x) / (2.0 * sigma*sigma));
}

highp vec4 blurEffect(sampler2D sampler,highp vec2 uv,highp float time )
{
    //highp float c_sigmaX      = (sin(iGlobalTime*2.0)*0.5 + 0.5) * 5.0;
    highp float c_sigmaX      = (sin(time*2.0)*0.5 + 0.5) * 2.0;
    highp float c_sigmaY      =  c_sigmaX;
    
    highp float total = 0.;
    highp vec3 ret = vec3(0.);
    
//    highp vec3 ret = color.xyg;

    for (highp int iy = 0; iy < c_samplesY; ++iy)
    {
        highp float fy = Gaussian (c_sigmaY, float(iy) - float(c_halfSamplesY));
        highp float offsety = float(iy-c_halfSamplesY) * c_pixelSize;
        
        for (highp int ix = 0; ix < c_samplesX; ++ix)
        {
            highp float fx = Gaussian (c_sigmaX, float(ix) - float(c_halfSamplesX));
            highp float offsetx = float(ix-c_halfSamplesX) * c_pixelSize;
            total += fx * fy;
            
           ret += texture2D(sampler, uv + vec2(offsetx, offsety)).rgb * fx*fy;
           
            //color = uv + vec2(offsetx, offsety);
//            color = texture2D(color.sampler, uv + vec2(offsetx, offsety));
           // ret += (color ).rgb * fx*fy;
        }
    }
    return  vec4(ret / total, 1.0);
}

highp vec4 TVNoise(highp vec4 col, sampler2D sampler,highp vec2 uv , highp float time)  //Done
{
    /*highp float noiseSpeed = 30.0;
    highp float noiseClampNum = 1.0;
    highp float noiseWaveNum = 32.0;
    highp float noisePower = 0.05;*/
    
    highp float noiseSpeed = 3.0;
    highp float noiseClampNum = 2.0;
    highp float noiseWaveNum = 12.0;
    highp float noisePower = 0.05;
    
    highp float noise = fract( time * noiseSpeed ) - (1.0-uv.y);
    uv.x    += sin( radians( clamp( noise * 360.0*noiseWaveNum,0.0,360.0 * noiseClampNum ) ) ) * noisePower;
    highp vec4 color = texture2D(sampler, uv );
    
//    fragColor = col;
    return color;
}

highp vec2 screenDistort(highp vec2 uv)   // under Construction
{
    uv -= vec2(.5,.5);
    uv = uv*1.2*(1./1.2+2.*uv.x*uv.x*uv.y*uv.y);
    uv += vec2(.5,.5);
    return uv;
}


/*highp vec4 vignetteEffect(highp vec4 clr,highp float time, highp float strong)  // Done

highp vec4 vignetteEffect2(highp vec4 sourceImageColor,highp vec2 center, highp float start,highp float end,highp float time)
*/

#define FREQUENCY 15.0

highp float randBlotch(highp vec2 co){
    return fract(sin(dot(co.xy ,vec2(12.9898,78.233))) * 43758.5453);
}

highp float randBlotch(highp float c){
    return rand(vec2(c,1.0));
}

// Generate some blotches.
highp float randomBlotch(highp float seed)
{
    highp float x = randBlotch(seed);
    highp float y = randBlotch(seed+1.0);
    highp float s = 0.01 * randBlotch(seed+2.0);
    
    highp vec2 iResolution = vec2(1.0,1.0);
    highp vec2 uv = textureCoordinate.xy;
    
    highp vec2 p = vec2(x,y) - uv;
    //p.x *= iResolution.x / iResolution.y;
    highp float a = atan(p.y,p.x);
    highp float v = 1.0;
    highp float ss = s*s * (sin(6.2831*a*x)*0.1 + 1.0);
    
    if ( dot(p,p) < ss ) v = 0.2;
    else
        v = pow(dot(p,p) - ss, 1.0/16.0);
    
    return mix(0.3 + 0.2 * (1.0 - (s / 0.02)), 1.0, v);
}

highp vec4 BLOTCHES(highp vec4 col,highp vec2 uv , highp float time)  //Done
{
    highp float t = float(int(time * FREQUENCY));
//    highp float t = float(int(time * .2));

    highp int s = int( max(8.0 * randomBlotch(t+18.0) -2.0, 0.0 ));
    highp float vI = 16.0 * (uv.x * (1.0-uv.x) * uv.y * (1.0-uv.y));
    vI = 1.0 ;
    
    if ( 0 < s ) vI *= randomBlotch( t+6.0+19.* float(0));
    if ( 1 < s ) vI *= randomBlotch( t+6.0+19.* float(1));
    if ( 2 < s ) vI *= randomBlotch( t+6.0+19.* float(2));
    if ( 3 < s ) vI *= randomBlotch( t+6.0+19.* float(3));
    //if ( 4 < s ) vI *= randomBlotch( t+6.0+19.* float(4));// error of white cirlcle.
    if ( 5 < s ) vI *= randomBlotch( t+6.0+19.* float(5));
    
    // Show the image modulated by the defects
    col.xyz = col.xyz * vI;
    
    
    return col;
}

void main()
{
    //float aze = (sin(2.*iTime)*iResolution.x/4.+iResolution.x/2.);
    highp vec2 uv = textureCoordinate.xy ;
    
    if(updownEffect==1)
        uv = upDownEffect(inputImageTexture,textureCoordinate,iGlobalTime);

    
    highp vec4 orgColor = texture2D(inputImageTexture,uv);
    highp vec2 iResolution = vec2(1.0,1.0);

    
    if (type == 111) {
        
        //orgColor = smallPixelNoiseEffect(orgColor,uv,iGlobalTime,0);
        orgColor = BLOTCHES(orgColor,uv, iGlobalTime);
        
        //orgColor = vignetteSquareEffect(orgColor,uv,iGlobalTime,1.0);
        //orgColor = GrayImage(orgColor);

        //uv = Tracking(uv,iGlobalTime);
        // orgColor = texture2D(inputImageTexture,uv);
        

        // orgColor = vignetteSquareEffect(orgColor,uv,iGlobalTime,1.0);

    }
    else if (type==1) {
        orgColor= TVNoise(orgColor,inputImageTexture,textureCoordinate,iGlobalTime);
        //orgColor = vignetteEffect(orgColor,iGlobalTime,1.0);
        orgColor = vignetteSquareEffect(orgColor,uv,iGlobalTime,1.0);

    }else if(type ==2)
    {
        orgColor = smallPixelNoiseEffect(orgColor,uv,iGlobalTime,0);
        orgColor = BLOTCHES(orgColor,uv, iGlobalTime);
        orgColor = vignetteSquareEffect(orgColor,uv,iGlobalTime,1.0);
        //orgColor = GrayImage(orgColor);
    }
    else if(type ==3)
    {
        orgColor = smallPixelNoiseEffect(orgColor,uv,iGlobalTime,0);
        orgColor = BLOTCHES(orgColor,uv, iGlobalTime);
        orgColor = vignetteSquareEffect(orgColor,uv,iGlobalTime,1.0);
        orgColor = SepiaEffectwithAdjust(orgColor, 0.75);

        
    }else if(type ==4)
    {
        orgColor= badTVEffect(orgColor,inputImageTexture,textureCoordinate,iGlobalTime);
        //orgColor = vignetteEffect(orgColor,iGlobalTime,1.2);
        orgColor = vignetteSquareEffect(orgColor,uv,iGlobalTime,1.0);

    }else if(type ==5)
    {
        orgColor = TVTubeEffect(inputImageTexture,textureCoordinate,iGlobalTime);
    }
    else if(type ==6)
    {
        orgColor = TVTubeEffect(inputImageTexture,textureCoordinate,iGlobalTime);
        orgColor = darkBlueColorFilter(orgColor);
    }
    else if(type ==7)
    {
        orgColor = TVTubeEffect(inputImageTexture,textureCoordinate,iGlobalTime);
        orgColor = doubleScanLineEffect(orgColor,textureCoordinate,iGlobalTime*100.);
    }
    else if(type ==8)
    {
        orgColor = stripes(orgColor,iGlobalTime);
        //orgColor = vignetteEffect(orgColor,iGlobalTime,1.0);
        orgColor = vignetteSquareEffect(orgColor,uv,iGlobalTime,1.0);
        orgColor = doubleScanLineEffect(orgColor,textureCoordinate,iGlobalTime*100.);
    }
    else if(type ==9)
    {
        //orgColor = GrayImage(orgColor);
        // orgColor = vignetteEffect(orgColor,iGlobalTime,1.0);
        orgColor = vignetteSquareEffect(orgColor,uv,iGlobalTime,1.0);
        orgColor = doubleScanLineEffect(orgColor,textureCoordinate,iGlobalTime*100.);
    }
    else if(type ==10)
    {
        orgColor = whiteLineEffect(orgColor,textureCoordinate,iGlobalTime*100.);
        //orgColor = vignetteEffect(orgColor,iGlobalTime,1.2);
        orgColor = vignetteSquareEffect(orgColor,uv,iGlobalTime,1.0);

    }else if(type ==11)
    {
        orgColor = verticalStrips(orgColor,textureCoordinate,iGlobalTime);
        //orgColor = vignetteEffect(orgColor,iGlobalTime,1.2);
        orgColor = vignetteSquareEffect(orgColor,uv,iGlobalTime,1.0);

    }else if(type == 12)
    {
        orgColor = cornerWaveNoise(orgColor,textureCoordinate,iGlobalTime);
        //orgColor = vignetteEffect(orgColor,iGlobalTime,1.2);
        orgColor = vignetteSquareEffect(orgColor,uv,iGlobalTime,1.0);

    }else if(type == 13)
    {
        orgColor = linesEffect(orgColor,inputImageTexture,textureCoordinate,iGlobalTime*100.);

    }
    else if(type == 14)
    {
        orgColor = GrainEffect(orgColor,36.0);
        orgColor = verticalLine(orgColor,textureCoordinate,iGlobalTime,1.0);
        //orgColor = vignetteEffect(orgColor,iGlobalTime,1.2);
        orgColor = vignetteSquareEffect(orgColor,uv,iGlobalTime,1.0);


    }else if(type == 15)
    {
        //orgColor = GrayImage(orgColor);
        orgColor = verticalLine(orgColor,textureCoordinate,iGlobalTime,1.0);
        
    }else if(type == 16)
    {
        //orgColor = GrayImage(orgColor);
        orgColor = singleScanlineEffect(orgColor,textureCoordinate,iGlobalTime);
        //orgColor = vignetteEffect(orgColor,iGlobalTime,1.2);
        orgColor = vignetteSquareEffect(orgColor,uv,iGlobalTime,1.0);


    }else if(type == 17)
    {
//        orgColor = GrayImage(orgColor);
        orgColor = verticalStrips(orgColor,textureCoordinate,iGlobalTime);
        orgColor = doubleScanLineEffect(orgColor,textureCoordinate,iGlobalTime*100.);

    }else if(type == 18)
    {
        orgColor = smallPixelNoiseEffect(orgColor,uv,iGlobalTime,0);
        orgColor = stripes(orgColor,iGlobalTime);
        orgColor = whiteLineEffect(orgColor,textureCoordinate,iGlobalTime*100.);
        orgColor = doubleScanLineEffect(orgColor,textureCoordinate,iGlobalTime*100.);
        // orgColor = GrayImage(orgColor);
        
        // orgColor = vignetteEffect(orgColor,iGlobalTime,1.2);
        orgColor = vignetteSquareEffect(orgColor,uv,iGlobalTime,1.0);


    }
    else if(type ==19)
    {
        orgColor= TVNoise(orgColor,inputImageTexture,textureCoordinate,iGlobalTime);
        //orgColor = vignetteEffect(orgColor,iGlobalTime,1.2);
        orgColor = vignetteSquareEffect(orgColor,uv,iGlobalTime,1.0);

    }
    else if(type ==20)
    {
        orgColor = smallPixelNoiseEffect(orgColor,uv,iGlobalTime,0);
        orgColor = singleScanlineEffect(orgColor,textureCoordinate,iGlobalTime);
        orgColor = doubleScanLineEffect(orgColor,textureCoordinate,iGlobalTime*100.);

    }
    else if(type ==21)
    {
        orgColor = vibrationEffect(inputImageTexture,textureCoordinate,iGlobalTime);
        orgColor = verticalLine(orgColor,textureCoordinate,iGlobalTime,1.0);
        orgColor = smallPixelNoiseEffect(orgColor,uv,iGlobalTime,0);
        
    }
    else if(type ==22)
    {
        orgColor = verticalLine(orgColor,textureCoordinate,iGlobalTime,1.0);
        orgColor = smallPixelNoiseEffect(orgColor,uv,iGlobalTime,0);
        //orgColor =  vignetteEffect(orgColor,iGlobalTime,1.2);
        orgColor = vignetteSquareEffect(orgColor,uv,iGlobalTime,1.0);

        orgColor =  SepiaWithTime(orgColor,iGlobalTime);
    }else if (type ==23)
    {
        orgColor = vibrationEffect(inputImageTexture,textureCoordinate,iGlobalTime);
        orgColor = verticalLine(orgColor,textureCoordinate,iGlobalTime,1.0);
        orgColor = smallPixelNoiseEffect(orgColor,uv,iGlobalTime,0);
        //orgColor = vignetteEffect(orgColor,iGlobalTime,1.0);
        orgColor = vignetteSquareEffect(orgColor,uv,iGlobalTime,1.0);

    }
    else if (type == 24)
    {
        orgColor= TVNoise(orgColor,inputImageTexture,textureCoordinate,iGlobalTime);
        orgColor = smallPixelNoiseEffect(orgColor,uv,iGlobalTime,0);
        //orgColor = vignetteEffect(orgColor,iGlobalTime,0.8);
        orgColor = vignetteSquareEffect(orgColor,uv,iGlobalTime,1.0);

    }
    else if (type == 25)
    {
        orgColor = tvDamage(orgColor,inputImageTexture,textureCoordinate,iGlobalTime);
        orgColor = smallPixelNoiseEffect(orgColor,uv,iGlobalTime,0);
        //orgColor = vignetteEffect(orgColor,iGlobalTime,0.8);
        orgColor = vignetteSquareEffect(orgColor,uv,iGlobalTime,1.0);

    }
    else if (type == 26)
    {
        orgColor= badTVEffect(orgColor,inputImageTexture,textureCoordinate,iGlobalTime);
        orgColor = smallPixelNoiseEffect(orgColor,uv,iGlobalTime,0);
        //orgColor = vignetteEffect(orgColor,iGlobalTime,.8);
        orgColor = vignetteSquareEffect(orgColor,uv,iGlobalTime,1.0);

    }
    else if (type ==27)
    {
        orgColor = smallPixelNoiseEffect(orgColor,uv,iGlobalTime,0);
        //orgColor = vignetteEffect(orgColor,iGlobalTime,.8);
        orgColor = vignetteSquareEffect(orgColor,uv,iGlobalTime,1.0);

    }
    else if (type ==28)
    {
        orgColor = verticalLine(orgColor,textureCoordinate,iGlobalTime,1.0);
        orgColor = smallPixelNoiseEffect(orgColor,uv,iGlobalTime,0);
        //orgColor = vignetteEffect(orgColor,iGlobalTime,.8);
        orgColor = vignetteSquareEffect(orgColor,uv,iGlobalTime,1.0);

    }
    else if (type ==29)
    {
        orgColor = smallPixelNoiseEffect(orgColor,uv,iGlobalTime,0);

        //orgColor = vignetteEffect(orgColor,iGlobalTime,.8);
        orgColor = vignetteSquareEffect(orgColor,uv,iGlobalTime,1.0);

    }
    else if (type ==30)
    {
        orgColor = smallPixelNoiseEffect(orgColor,uv,iGlobalTime,0);
        orgColor = singleScanlineEffect(orgColor,textureCoordinate,iGlobalTime);
        orgColor = doubleScanLineEffect(orgColor,textureCoordinate,iGlobalTime*100.);
        //orgColor = vignetteEffect(orgColor,iGlobalTime,.8);
        orgColor = vignetteSquareEffect(orgColor,uv,iGlobalTime,1.0);
    }
    else if (type ==31)
    {
        //orgColor = smallPixelNoiseEffect(orgColor,uv,iGlobalTime,0);
        orgColor = gold_noise(orgColor,uv,iGlobalTime);
        orgColor = verticalLine(orgColor,textureCoordinate,iGlobalTime,1.0);
        orgColor = verticalStrips(orgColor,textureCoordinate,iGlobalTime);
        //orgColor = vignetteEffect(orgColor,iGlobalTime,.8);
        orgColor = vignetteSquareEffect(orgColor,uv,iGlobalTime,1.0);
    }
    else if (type ==32)
    {
        orgColor = TVTubeEffect(inputImageTexture,textureCoordinate,iGlobalTime);
        orgColor = verticalLine(orgColor,textureCoordinate,iGlobalTime,1.0);
    }
    else if (type ==33)
    {
        orgColor = TVTubeEffect(inputImageTexture,textureCoordinate,iGlobalTime);
        orgColor = singleScanlineEffect(orgColor,textureCoordinate,iGlobalTime);
        orgColor = doubleScanLineEffect(orgColor,textureCoordinate,iGlobalTime*100.);
    }
    else if (type ==34)
    {
        orgColor = GrainEffect(orgColor,36.0);
        orgColor = verticalLine(orgColor,textureCoordinate,iGlobalTime,1.0);
        orgColor = vignetteSquareEffect(orgColor,uv,iGlobalTime,1.0);
        
    }
    else if (type ==35)
    {
        orgColor = tvDamage(orgColor,inputImageTexture,textureCoordinate,iGlobalTime);
        // orgColor = vignetteEffect(orgColor,iGlobalTime,1.0);
        orgColor = vignetteSquareEffect(orgColor,uv,iGlobalTime,1.0);
    }else if (type ==36)
    {
       // orgColor =  DropEffects(inputImageTexture,textureCoordinate,iGlobalTime*0.46 + 1.5);
    }
    
    /*
     
    //        orgColor = whiteLineEffect(orgColor,textureCoordinate,iGlobalTime*100.);
    
    else if (type==1) {
//          orgColor = vignetteEffect(orgColor,iGlobalTime,1.0);
          orgColor = smallPixelNoiseEffect(orgColor,uv,iGlobalTime,0);
    }
    else if(type == 2) {
        
        orgColor = GrayImage(orgColor);
        orgColor = smallPixelNoiseEffect(orgColor,uv,iGlobalTime,0);
    }
    else if(type == 3) {
        orgColor = vignetteEffect(orgColor,iGlobalTime,1.0);
        orgColor = smallPixelNoiseEffect(orgColor,uv,iGlobalTime,0);
    }
    else if(type == 4) {
        orgColor = vibrationEffect(inputImageTexture,textureCoordinate,iGlobalTime);
        orgColor = vignetteEffect(orgColor,iGlobalTime,1.0);
//        orgColor = smallPixelNoiseEffect(orgColor,uv,iGlobalTime,0);
    }
    else if(type == 5) {
        orgColor = vibrationEffect(inputImageTexture,textureCoordinate,iGlobalTime);
        orgColor = vignetteEffect(orgColor,iGlobalTime,1.0);
        orgColor = smallPixelNoiseEffect(orgColor,uv,iGlobalTime,0);
    }
    else if(type == 6) {
        
        orgColor = vibrationEffect(inputImageTexture,textureCoordinate,iGlobalTime);
        orgColor = vignetteEffect(orgColor,iGlobalTime,1.0);
    }
    else if(type == 6) {
        orgColor = stripes(orgColor,iGlobalTime);
        orgColor = vignetteEffect(orgColor,iGlobalTime,1.0);
    }
    else if(type == 7) {
        //orgColor = GrayImage(orgColor);
        //orgColor = GrainEffect(orgColor,16.0);
        //orgColor = vignetteEffect(orgColor,iGlobalTime,1.0);
    }
    else if(type == 8) {
        
        //orgColor = linesEffect(orgColor,inputImageTexture,textureCoordinate,iGlobalTime*100.);
        orgColor = smallPixelNoiseEffect(orgColor,uv,iGlobalTime,0);
        orgColor = stripes(orgColor,iGlobalTime);
        orgColor = whiteLineEffect(orgColor,textureCoordinate,iGlobalTime*100.);
        orgColor = doubleScanLineEffect(orgColor,textureCoordinate,iGlobalTime*100.);
        orgColor = GrayImage(orgColor);
        orgColor = vignetteEffect(orgColor,iGlobalTime,1.2);
    }
    else if(type == 9) {
        
//        orgColor = upDownEffect(inputImageTexture,textureCoordinate,iGlobalTime*1.0);
        orgColor = GrayImage(orgColor);
        orgColor = vignetteEffect(orgColor,iGlobalTime,1.0);
    }
    else if(type == 10) {
        
        orgColor = RingNoise(orgColor,uv,iGlobalTime);
        orgColor =  vignetteEffect(orgColor,iGlobalTime,1.0);
        
//        orgColor = GrayImage(orgColor);
//        orgColor = vignetteEffect(orgColor,iGlobalTime,1.0);
    }
    else if (type == 11)
    {
        orgColor= TVNoise(orgColor,inputImageTexture,textureCoordinate,iGlobalTime);
        orgColor = GrayImage(orgColor);
        orgColor = smallPixelNoiseEffect(orgColor,uv,iGlobalTime,0);
        orgColor = vignetteEffect(orgColor,iGlobalTime,1.0);
        
    }else if (type == 12)
    {
        //orgColor = verticalLine(orgColor,textureCoordinate,iGlobalTime,1.0);
        
        orgColor = whiteLineEffect(orgColor,textureCoordinate,iGlobalTime*100.);
        orgColor = darkBlueColorFilter(orgColor);
        orgColor = vignetteEffect(orgColor,iGlobalTime,1.2);
        
    }else if (type == 13)
    {
//        orgColor= badTVEffect(orgColor,inputImageTexture,textureCoordinate,iGlobalTime);
//        orgColor = vignetteEffect(orgColor,iGlobalTime,1.2);

        
    }else if (type == 14)
    {
//        orgColor = TVTubeEffect(inputImageTexture,textureCoordinate,iGlobalTime);
//        orgColor = darkBlueColorFilter(orgColor);

    }
    else if (type == 15)
    {
        //orgColor = TVTubeEffect(inputImageTexture,textureCoordinate,iGlobalTime);
        orgColor = cornerWaveNoise(orgColor,textureCoordinate,iGlobalTime);
        orgColor = darkBlueColorFilter(orgColor);
        orgColor = vignetteEffect(orgColor,iGlobalTime,1.2);
    }
    else if (type == 16)
    {
        orgColor = verticalStrips(orgColor,textureCoordinate,iGlobalTime);
        orgColor = vignetteEffect(orgColor,iGlobalTime,1.2);


    }else if (type == 17)
    {
        orgColor = singleScanlineEffect(orgColor,textureCoordinate,iGlobalTime);
        orgColor = vignetteEffect(orgColor,iGlobalTime,1.2);

    }else if (type == 18)
    {
        orgColor = verticalStrips(orgColor,textureCoordinate,iGlobalTime);
        orgColor = singleScanlineEffect(orgColor,textureCoordinate,iGlobalTime);
        orgColor = GrayImage(orgColor);
        orgColor = vignetteEffect(orgColor,iGlobalTime,1.2);
    }
    
 */
    else if(type == 111) {
        
        
//        orgColor = whiteLineEffect(orgColor,textureCoordinate,iGlobalTime*100.);
        
        

        //orgColor = whiteLineEffect(orgColor,textureCoordinate,iGlobalTime*100.);
        
//        orgColor = linesEffect(orgColor,inputImageTexture,textureCoordinate,iGlobalTime*100.);

        
       // orgColor = upDownEffect(inputImageTexture,textureCoordinate,iGlobalTime*1.0);

       // orgColor = gold_noise(orgColor,uv,iGlobalTime);
        
       //  orgColor= TVNoise(orgColor,inputImageTexture,textureCoordinate,iGlobalTime);

        
      //  orgColor = goldWhite_noise(orgColor,textureCoordinate, iGlobalTime);
        
//        orgColor = tvDamage(orgColor,inputImageTexture,textureCoordinate,iGlobalTime);
        
        // orgColor = RingNoise(orgColor,uv,iGlobalTime);
        
        //orgColor = verticalStrips(orgColor,textureCoordinate,iGlobalTime);
       
        //orgColor = verticalLine(orgColor,textureCoordinate,iGlobalTime,1.0);
        
        
//        orgColor =  DropEffects(inputImageTexture,textureCoordinate,iGlobalTime*0.46 + 1.5);
        
//        orgColor = doubleScanLineEffect(orgColor,textureCoordinate,iGlobalTime*100.);


        //orgColor = whiteLineEffect(orgColor,textureCoordinate,iGlobalTime);
        
        
        //orgColor = GrainEffect(orgColor,36.0);

//        stripes
        
        
          //orgColor = stripes(orgColor,iGlobalTime);

//        orgColor = StaticStripes(orgColor);
        
        
        //orgColor =  sepia_shadowbox(orgColor,uv);
        
       // orgColor = SepiaEffectwithAdjust(orgColor, 0.75);
       // orgColor =  SepiaWithTime(orgColor,iGlobalTime);
        
        
        //highp vec4 noiseClr =  vec4(noiseLineEffect(textureCoordinate, iGlobalTime));
        //        highp vec4 noiseClr =  vec4(gold_noise(textureCoordinate, iGlobalTime));

       // orgColor = orgColor * fastLightFlash(textureCoordinate) ;
        
        //orgColor = verticalLine(orgColor,textureCoordinate,iGlobalTime,1.0);
        
        //orgColor = darkBlueColorFilter(orgColor);
        
        
        //orgColor= badTVEffect(orgColor,inputImageTexture,textureCoordinate,iGlobalTime);
        
        //orgColor= TVNoise(orgColor,inputImageTexture,textureCoordinate,iGlobalTime);
        
        
       // orgColor = cornerWaveNoise(orgColor,textureCoordinate,iGlobalTime);

       // orgColor = TVTubeEffect(inputImageTexture,textureCoordinate,iGlobalTime);
        
        // orgColor = verticalStrips(orgColor,textureCoordinate,iGlobalTime);
        
        // orgColor = singleScanlineEffect(orgColor,textureCoordinate,iGlobalTime);

        
        // orgColor = blurEffect(inputImageTexture,textureCoordinate,iGlobalTime * 0.1);
        //highp vec4 noiseClr =  vec4(noiseLineEffect(textureCoordinate, iGlobalTime));
        
        /*highp vec4 noiseClr =  vec4(gold_noise(textureCoordinate, iGlobalTime));
        orgColor =  NormalBlend(noiseClr,orgColor);
        orgColor =  vignetteEffect(orgColor,iGlobalTime,1.0);*/
        
        
        //        orgColor = GrayImage(orgColor);
        //        orgColor = vignetteEffect(orgColor,iGlobalTime,1.0);
    }
    else{
    
    
//    orgColor = GrayImage(orgColor);
    
//     orgColor =  vignetteEffect(orgColor,iGlobalTime,1.0);
//    orgColor = stripes(orgColor,iGlobalTime);
    //orgColor = GrainEffect(orgColor);
        
//    orgColor = linesEffect(inputImageTexture,textureCoordinate,iGlobalTime*100.);
//    orgColor = vibrationEffect(inputImageTexture,textureCoordinate,iGlobalTime);
    //orgColor = upDownEffect(inputImageTexture,textureCoordinate,iGlobalTime*1.0);
    //orgColor = smallPixelNoiseEffect(orgColor,uv,iGlobalTime,2);
   // orgColor = blurEffect(inputImageTexture,textureCoordinate,iGlobalTime * 0.1);
    //    orgColor =  vignetteEffect2(orgColor,vec2(0.5,0.5),0.43,0.48,iGlobalTime);

    //    orgColor = SingleColorEffect(orgColor,1);
     //   highp vec4 noiseClr = RingNoise(inputImageTexture,uv,iGlobalTime/10.0);
     //   orgColor =  NormalBlend(noiseClr,orgColor);
       // orgColor =  vignetteEffect(orgColor,iGlobalTime,1.0);

    //-**- under Construction
    
//        orgColor = linesEffect(inputImageTexture,textureCoordinate,iGlobalTime*100.);

//    orgColor = GrainEffect(orgColor);
    
    
//        orgColor = blurEffect(inputImageTexture,textureCoordinate,iGlobalTime * 0.6);
//        orgColor = GrayImage(orgColor);

//    orgColor = vibrationEffect(inputImageTexture,textureCoordinate,iGlobalTime);

    //orgColor = smallPixelNoiseEffect(orgColor,uv,iGlobalTime,1);
    
   // orgColor =  vignetteEffect(orgColor,iGlobalTime,1.0);
    


    
    /*
     highp float f = 0.0;
    f = noise( 42.0*uv );

    f = 0.5 + 0.5*f;
    
    //f *= smoothstep( 0.0, 0.005, abs(uv.x-0.4) );
    //highp vec4 noiseClr = vec4( f, f, f, 1.0 );
    highp vec4 noiseClr = vec4( f, f, f, f );
    
    if (noiseClr.r >0.3 && noiseClr.g > 0.3 && noiseClr.b > 0.3) {
        noiseClr.a = 0.0;
    }
   */
    
    
  //  orgColor = sideColorEffect(orgColor,iGlobalTime);
    
  /*  highp vec2 uv2 = 1.0*(abs(sin(iGlobalTime/20.))+.5)*(2.0*textureCoordinate.xy);
    
    // uv2.x = hash2(vec2(hash2(uv2),1.0));
    
    uv2.x = hash2(uv2) ; // hash2(vec2(hash2(uv2),1.0));
    // highp vec4 noiseClr = vec4(uv2.x,uv2.x,uv2.x,1.);
    highp vec4 noiseClr = vec4(uv2.x,uv2.x,uv2.x,-uv2.x);
*/
    
//highp vec4 noiseClr =  vec4(noiseLineEffect(textureCoordinate, iGlobalTime));
//    highp vec4 noiseClr =  vec4(gold_noise(textureCoordinate, iGlobalTime));
    
   // orgColor = noiseClr;
    //orgColor = noiseClr+ orgColor;
    //orgColor = noiseClr* orgColor.w;
    //orgColor =  NormalBlend(orgColor,noiseClr);
    
    

    
//     orgColor =  vignetteEffect(orgColor,iGlobalTime,1.0);

//    orgColor =  noiseClr + orgColor;
    
//    orgColor =  mix(orgColor,noiseClr,0.5);
    
    
    }
    
    
    ///- * - * other Practice ////-- * - * - ** - * - * - * -- * - * - * - *
    
    //uv.x = 2.*uv.x-1.;
    
    //highp float k = floor((59.8*(1.-uv.x*uv.x)-1.)/10.)*10.+1.;
    
    highp float k = floor((59.8*(1.-uv.x*uv.x)-1.)/10.)*10.+1.;

     //highp float k = 1.0 ;
    
    highp vec4 lineClr = texture2D(inputImageTexture,uv);

    highp float iTime = iGlobalTime;
    // iTime = floor(mod(k*iTime,2.));
   
    iTime = floor(mod(k*iTime,2.0));

    lineClr = sqrt(2.*uv.y)*( (iTime>0.) ? vec4(1.0,1.0,1.0,1.0): vec4(0.,0.0,0.,0.) );
    
    //lineClr = vec4 (1.0,0.0,1.0,1.0);
    
//    highp vec4  fragColor = NormalBlend(lineClr,orgColor);

    highp vec4  fragColor = orgColor;
    
    /*
    //---*-*-**
    
    highp vec4 t1 = texture2D(inputImageTexture,uv);
    
//    highp float shake = sin(rand2(t1.x, t1.x) * 0.5) * .001 * fract(t1.y * iResolution.y/(1.) / rand1(iGlobalTime, t1.x));
    
    //highp float shake = sin(t1.x * 0.5) * fract(t1.y / rand1(iTime *0.001 , t1.x));
    
    highp float shake = sin(t1.x * 0.5) * fract(t1.y / rand1(iTime *0.001 , t1.x));

    

    //shake += sin(shake - t1.r * t1.g) * t1.g * 0.14 * fract(uv.y * iResolution.y/(1.) / 2.0);
    
    
    highp vec4 fragColorTemp = texture2D(inputImageTexture,uv);
    
    highp float grey = (fragColorTemp.r + fragColorTemp.g + fragColorTemp.b) / 2.1;

    
//    highp vec4  fragColor = mix(fragColorTemp, texture2D(inputImageTexture, 0.9 * uv * -150.0), -shake * .09 * iGlobalTime / grey * 0.008);

    highp vec4  fragColor = mix(fragColorTemp, vec4(1.0,1.0,1.0,1.0),shake );//* .09 * iGlobalTime/grey * 0.008 );//* .09 * iGlobalTime / grey * 0.008);


    
    //highp vec4  fragColor = mix(lineClr,orgColor,);

    */
    
    
    gl_FragColor = fragColor ;//fragColor;

}


/*
void main()
{

    highp vec2 uv = textureCoordinate.xy ;
    highp vec4 orgColor = texture2D(inputImageTexture,textureCoordinate);
    highp vec2 iResolution = vec2(1, 1);  // temp need to change

    
    highp vec2 p = textureCoordinate.xy / iResolution.x;//normalized coords with some cheat
    //(assume 1:1 prop)
    highp float prop = iResolution.x / iResolution.y;//screen proroption
    highp vec2 m = vec2(0.5, 0.5 / prop);//center coords
    highp vec2 d = p - m;//vector from center to current fragment
    highp float r = sqrt(dot(d, d)); // distance of pixel from center
    
    //highp float power = ( 2.0 * 3.141592 / (2.0 * sqrt(dot(m, m))) ) * (iMouse.x / iResolution.x - 0.5);//amount of effect
    highp float power = ( 2.0 * 3.141592 / (2.0 * sqrt(dot(m, m))) ) * (sliderValue / iResolution.x - 0.5);//amount of effect

    highp float bind;//radius of 1:1 effect
    if (power > 0.0)
        bind = sqrt(dot(m, m));//stick to corners
    else {
        if (prop < 1.0)
            bind = m.x;
        else
            bind = m.y;
        
    }//stick to borders
    
   
    //Weird formulas
//    highp vec2 uv;
    if (power > 0.0)//fisheye
        uv = m + normalize(d) * tan(r * power) * bind / tan( bind * power);
    else if (power < 0.0)//antifisheye
        uv = m + normalize(d) * atan(r * -power * 10.0) * bind / atan(-power * bind * 10.0);
    else uv = p;//no effect for power = 1.0
    
    //gl_FragColor = orgColor;

    
    //highp vec3 col = texture2D(inputImageTexture, vec2(uv.x, -uv.y * prop)).xyz;//Second part of cheat
    highp vec3 col = texture2D(inputImageTexture, vec2(uv.x, uv.y * prop)).xyz;//Second part of cheat
    //for round effect, not elliptical
    
    //fragColor = vec4(col, 1.0);
    
     gl_FragColor = vec4(col, 1.0);

}
*/
