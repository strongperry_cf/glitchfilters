varying highp vec2 textureCoordinate;
uniform sampler2D inputImageTexture;

uniform highp float aspectRatio;
uniform highp float MainTime;

uniform highp float width;
uniform highp float height;

uniform highp int   type;
uniform highp int   category;

uniform highp float power;
uniform highp int Zoom;
uniform highp float zoompower;

//** Ramdom Zoom
//** random RGB Shift
//** Random Blur
//** Collage 9 , 16, 25

#define saturate(i) clamp(i,0.,1.)
#define PI 3.14159

precision highp float;

float sinf(float x)
{
    x*=0.159155;
    x-=floor(x);
    float xx=x*x;
    float y=-6.87897;
    y=y*xx+33.7755;
    y=y*xx-72.5257;
    y=y*xx+80.5874;
    y=y*xx-41.2408;
    y=y*xx+6.28077;
    return x*y;
}

float cosf(float x){    return sinf(x+1.5708);}
vec3 z(){    return vec3(0.);}
vec2 sinf(vec2 x){    return vec2(sinf(x.x),sinf(x.y));}
vec2 cosf(vec2 x){    return vec2(cosf(x.x),cosf(x.y));}
vec3 sinf(vec3 x){    return vec3(sinf(x.x),sinf(x.y),sinf(x.z));}
vec3 cosf(vec3 x){    return vec3(cosf(x.x),cosf(x.y),cosf(x.z));}
vec4 sinf(vec4 x){    return vec4(sinf(x.x),sinf(x.y),sinf(x.z),sinf(x.w));}
vec4 cosf(vec4 x){    return vec4(cosf(x.x),cosf(x.y),cosf(x.z),cosf(x.w));}

highp float rand(highp vec2 co){
    return fract(sin(dot(co.xy ,vec2(12.9898,78.233))) * 43758.5453) * 2.0 - 1.0;
}

highp float rand (highp float localTime) {
    return fract(sin(localTime)*1e4);
}

highp float randNormal(highp vec2 co){
    return fract(sin(dot(co.xy ,vec2(12.9898,78.233))) * 43758.5453);
}
highp float randNormal(highp float c){
    return rand(vec2(c,1.0));
}

// ******************************* BlackWhiteEffect *******************************
highp vec4 BlackWhiteEffect(highp vec4 color,highp vec4 ratio)
{
    highp float average = ((color.r * ratio.r) + (color.g * ratio.g)+ (color.b * ratio.b))/3.0;
    
    highp vec3 finalColor = vec3(average);
    return vec4(finalColor,color.a * ratio.a);
}

highp float brightness (highp vec3 color)
{
    return (0.2126*color.r + 0.7152*color.g + 0.0722*color.b);
}

highp vec4 brightness (highp vec4 color)
{
    return vec4(color.rgb * (0.2126*color.r + 0.7152*color.g+ 0.0722*color.b),1.0);
}

// ******************************* RandomZoom Blur *******************************
highp vec4 RandomZoomBlur(sampler2D sampler ,highp vec2 uv , highp float time ,highp int type,highp float tPower )
{
    highp vec2 blurCenter = vec2(0.5,0.5);
    highp float blurSize = 0.0;
    highp float Distance = 0.6;

    blurSize = 8.0 ;
    highp float amount = 2.0 ;
    highp float iTime = time;
    
    amount = (1.0 + sin(iTime*6.0)) * Distance;
    amount *= 1.0 + sin(iTime*16.0) * Distance;
    amount *= 1.0 + sin(iTime*19.0) * Distance;
    amount *= 1.0 + sin(iTime*27.0) * Distance;

    highp vec2 samplingOffset = 1.0/100.0 * (blurCenter - uv) * blurSize*amount;
    highp vec4 fragmentColor = texture2D(sampler, uv) * 0.2;
   
    if (type == 1) {
        fragmentColor += texture2D(sampler, uv + (3.0 * samplingOffset)) * 0.8;
    }
    
    return fragmentColor;

}

// ******************************* Music Distortion *******************************
highp vec3 rgb2yiq(highp vec3 c){
    return vec3(
                (0.2989*c.x + 0.5959*c.y + 0.2115*c.z),
                (0.5870*c.x - 0.2744*c.y - 0.5229*c.z),
                (0.1140*c.x - 0.3216*c.y + 0.3114*c.z)
                );
}

highp vec3 yiq2rgb(highp vec3 c){
    return vec3(
                (1.0 * c.x + 1.0 * c.y + 1.0 * c.z),
                (0.956*c.x - 0.2720*c.y - 1.1060*c.z),
                (0.6210*c.x - 0.6474*c.y + 1.7046*c.z)
                );
}

highp vec2 Circle(highp float Start,highp float Points,highp float Point)
{
    highp float Rad = (3.141592 * 2.0 * (1.0 / Points)) * (Point + Start);
    return vec2(-(.3+Rad), cos(Rad));
}

highp vec3 Blur(sampler2D sampler,highp vec2 uv,highp float f,highp float d,highp float n,highp float localTime){
    
    uv.y*=1.0-.01*floor(n*2.50);
    highp float t = (sin(localTime*5.0+uv.y*5.0))/10.0;
    highp float b = 1.0;
    t=sin(localTime*5.0+f)/10.0;
    t=0.0;
    highp vec2 PixelOffset=vec2(d+.0005*t,0);
    
    highp float Start = 2.0 / 14.0;
    highp vec2 Scale = 0.66 * 4.0 * 2.0 * PixelOffset.xy;
    
    highp vec3 N0 = texture2D(sampler, uv + Circle(Start, 14.0, 0.0) * Scale).rgb;
    highp vec3 N1 = texture2D(sampler, uv + Circle(Start, 14.0, 1.0) * Scale).rgb;
    highp vec3 N2 = texture2D(sampler, uv + Circle(Start, 14.0, 2.0) * Scale).rgb;
    highp vec3 N3 = texture2D(sampler, uv + Circle(Start, 14.0, 3.0) * Scale).rgb;
    highp vec3 N4 = texture2D(sampler, uv + Circle(Start, 14.0, 4.0) * Scale).rgb;
    highp vec3 N5 = texture2D(sampler, uv + Circle(Start, 14.0, 5.0) * Scale).rgb;
    highp vec3 N6 = texture2D(sampler, uv + Circle(Start, 14.0, 6.0) * Scale).rgb;
    highp vec3 N7 = texture2D(sampler, uv + Circle(Start, 14.0, 7.0) * Scale).rgb;
    highp vec3 N8 = texture2D(sampler, uv + Circle(Start, 14.0, 8.0) * Scale).rgb;
    highp vec3 N9 = texture2D(sampler, uv + Circle(Start, 14.0, 9.0) * Scale).rgb;
    highp vec3 N10 = texture2D(sampler, uv + Circle(Start, 14.0, 10.0) * Scale).rgb;
    highp vec3 N11 = texture2D(sampler, uv + Circle(Start, 14.0, 11.0) * Scale).rgb;
    highp vec3 N12 = texture2D(sampler, uv + Circle(Start, 14.0, 12.0) * Scale).rgb;
    highp vec3 N13 = texture2D(sampler, uv + Circle(Start, 14.0, 13.0) * Scale).rgb;
    highp vec3 N14 = texture2D(sampler, uv).rgb;
    
    highp vec4 clr = texture2D(sampler, uv);
    highp float W = 1.0 / 15.0;
    
    clr.rgb=
    (N0 * W) +
    (N1 * W) +
    (N2 * W) +
    (N3 * W) +
    (N4 * W) +
    (N5 * W) +
    (N6 * W) +
    (N7 * W) +
    (N8 * W) +
    (N9 * W) +
    (N10 * W) +
    (N11 * W) +
    (N12 * W) +
    (N13 * W) +
    (N14 * W);
    
    return  vec3(clr.xyz)*b;
}

highp vec4 MusicDistortion(sampler2D sampler,highp vec2 uv,highp float localTime,highp float power )
{
    power = power * 0.9;
    highp float d = .1*0.5/50.0;
    power = rand(localTime);
    highp float s = power;
    highp float ff = power+0.1;
    
    highp float e = min(.30,pow(max(0.0,cos(uv.y*4.0+.3)-.75)*(s+0.5)*1.0,3.0))*25.0;
    s -= pow(power+0.2,1.0);
    uv.x += e*abs(s*3.0);
    highp float r = texture2D(sampler,vec2(mod(localTime*10.0,mod(localTime*10.0,256.0)*(1.0/256.0)),0.0)).r*(2.0*s); // Third Input
    uv.x-=r*pow(min(.003,(uv.y-.15))*6.0,2.0);
    d=.1+sin(s);
    highp float c = .002*d;
    highp vec2 uvo = uv;
    highp vec4 fragColor;
    fragColor = texture2D(sampler,uv);
    fragColor.xyz = Blur(sampler,uv,0.0,c,ff,localTime);
    highp float y = rgb2yiq(fragColor.xyz).r;
    uv.x+=.01*d;
    c*=2.0;
    fragColor.xyz =Blur(sampler,uv,.333,c,ff,localTime);
    highp float i = rgb2yiq(fragColor.xyz).g;
    uv.x+=.005*d;
    c*=1.50;
    fragColor.xyz =Blur(sampler,uv,.666,c,ff,localTime);
    highp float q = rgb2yiq(fragColor.xyz).b;
    fragColor.xyz=yiq2rgb(vec3(y,i,q))-pow(s+e*2.0,3.0);
    fragColor.xyz*=smoothstep(1.0,.999,uv.x-.1);
    return fragColor;
}

// ******************************* Glitch Pixel Effect *******************************
highp float hash(highp vec2 p) {
    highp float h = dot(p,vec2(127.1,311.7));
    return -1.0 + 2.0*fract(sin(h)*43758.5453123);
}

highp float noise(highp vec2 p) {
    
    highp vec2 i = floor(p);
    highp vec2 f = fract(p);
    
    highp vec2 u = f*f*(3.0-2.0*f);
    
    return mix(mix(hash( i + vec2(0.0,0.0) ),
                   hash( i + vec2(1.0,0.0) ), u.x),
               mix( hash( i + vec2(0.0,1.0) ),
                   hash( i + vec2(1.0,1.0) ), u.x), u.y);
}

highp float noise(highp vec2 p,highp int oct) {
    
    mat2 m = mat2( 1.6,  1.2, -1.2,  1.6 );
    highp float f  = 0.0;
    
    for(highp int i = 1; i < 3; i++){
        highp float mul = 1.0/pow(2.0, float(i));
        f += mul*noise(p);
        p = m*p;
    }
    
    return f;
}

highp vec4 GlitchPixel(sampler2D sampler,highp vec2 uv,highp float iTime,highp float localPower)
{
    iTime = mod(iTime,4.0);
    highp float glitch = pow(cos(iTime*0.5)*1.5+1.0, 1.2);
    
    if(noise(iTime+vec2(0, 0))*glitch > 0.6){
        uv.y = mod(uv.y+noise(vec2(iTime*4.0, 0)), 1.0);
    }
    
    highp vec2 hp = vec2(0.0, uv.y);
    highp float nh = noise( hp * 7.0 + iTime * 10.0, 3) * (noise( hp + iTime * 0.3 ) * 0.8);
    nh += noise(hp * 100.0 + iTime * 10.0, 3) * 0.02;
    highp float rnd = 0.0;
    if(glitch > 0.0){
        rnd = hash(uv);
        if(glitch < 1.0){
            rnd *= glitch;
        }
    }
    nh *= glitch + rnd;
    highp float r = texture2D(sampler, uv+vec2(nh, 0.08)*nh).r;
    highp float g = texture2D(sampler, uv+vec2(nh-0.07, 0.0)*nh).g;
    highp float b = texture2D(sampler, uv+vec2(nh, 0.0)*nh).b;
    
    highp vec3 col = vec3(r, g, b);
    return vec4(col.rgb, 1.0);
}

// ******************************* Grain Effect *******************************
highp vec4 GrainEffect(highp vec4 clr, highp float strength,highp float localTime ,highp int type)
{
    highp vec2 iResolution = vec2(1.0,1.0);
    highp vec2 uv = textureCoordinate.xy / iResolution.xy;
    highp float x = (uv.x + 4.0 ) * (uv.y + 4.0 ) * (localTime * 10.0);
    highp vec4 grain = vec4(mod((mod(x, 13.0) + 1.0) * (mod(x, 123.0) + 1.0), 0.01)-0.00005) * strength;

    if (type == 0) {
            clr = clr + grain;
    }
    else if (type == 1)
    {
        grain = 1.0 - grain;
        clr = clr * grain;
    }
    
    return clr;
}

// ******************************* SmallPixelNoise Effect *******************************
#define SEED 1.123456789
#define HASHM mat3(40.15384, 31.973157,31.179219,10.72341,13.123009,41.441023,-311.61923,10.41234,178.127121)

highp float smallPixelNoise_Hash(highp vec3 p) {
    p = fract((vec3(p.x, p.y, p.z) + SEED * 1e-3) * HASHM);
    p += dot(p, p.yzx + 41.19);
    return fract((p.x + p.y) * p.z);
}

highp float smallPixelNoise_Hash(highp vec2 p) {
    highp vec3 p3 = fract(vec3(p.x, p.y, (p.x + p.y + SEED * 1e-7)) * HASHM);
    p3 += dot(p3, p3.yzx + 41.19);
    return fract((p3.x + p3.y) * p3.z);
}

highp vec4 SmallPixelNoiseEffect(highp vec4 color ,highp vec2 uv,highp float localTime,highp int type)
{
    highp float value;
    
    if (type == 0) {
        value = smallPixelNoise_Hash(uv + localTime * 0.001);
    }
    
    if (type == 1) {
        value = smallPixelNoise_Hash(vec3(uv, (uv.x + uv.y) * localTime * 0.001));
    }
    
    if (type == 2) {
        value = smallPixelNoise_Hash(uv + localTime * 0.02);
    }
    
    color.rgb += vec3(value < 0.42 ? -value : 0.0);

    return color;
}

// ******************************* VHS BigEffect *******************************
highp float hash_VHS_Big( highp vec2 _v ){
    return fract( sin( dot( _v, vec2( 89.44, 19.36 ) ) ) * 22189.22 );
}

highp float iHash( highp vec2 _v, highp vec2 _r ){
    highp float h00 = hash_VHS_Big( vec2( floor( _v * _r + vec2( 0.0, 0.0 ) ) / _r ) );
    highp float h10 = hash_VHS_Big( vec2( floor( _v * _r + vec2( 1.0, 0.0 ) ) / _r ) );
    highp float h01 = hash_VHS_Big( vec2( floor( _v * _r + vec2( 0.0, 1.0 ) ) / _r ) );
    highp float h11 = hash_VHS_Big( vec2( floor( _v * _r + vec2( 1.0, 1.0 ) ) / _r ) );
    highp vec2 ip = vec2( smoothstep( vec2( 0.0, 0.0 ), vec2( 1.0, 1.0 ), mod( _v*_r, 1. ) ) );
    return ( h00 * ( 1. - ip.x ) + h10 * ip.x ) * ( 1. - ip.y ) + ( h01 * ( 1. - ip.x ) + h11 * ip.x ) * ip.y;
}

highp float noiseVHSBig( highp vec2 _v ){
    highp float sum = 0.;
    for(highp  int i=1; i<5; i++ )
    {
        sum += iHash( _v + vec2( i ), vec2( 2. * pow( 2., float( i ) ) ) ) / pow( 2., float( i ) );
    }
    return sum;
}

// ******************************* VHSChromatic *******************************
highp vec4 VHSChromatic(sampler2D sampler,highp vec2 uv ,highp float localTime)
{
    highp float amount = sin(localTime) * 0.1;
    amount *= 0.1;
    highp float split = fract(localTime / 2.);
    highp float split2 = fract(localTime / 2.5);
    highp float scanOffset = 0.01;
    highp vec2 uv1 = vec2(uv.x + amount, uv.y);
    highp vec2 uv2 = vec2(uv.x, uv.y + amount);
    if (uv.y > split) {
        uv.x += scanOffset;
        uv1.x += scanOffset;
        uv2.x += scanOffset;
    }
    if (uv.y > split2) {
        uv.x += scanOffset;
        uv1.x += scanOffset;
        uv2.x += scanOffset;
    }
    highp float r = texture2D(sampler, uv1).r;
    highp float g = texture2D(sampler, uv).g;
    highp float b = texture2D(sampler, uv2).b;
    
    return vec4(r, g, b, 1.0);
}

// ******************************* SIMPLEVHS*******************************
highp float randVHS(vec2 co){
    return fract(sin(dot(co.xy ,vec2(12.9898,78.233))) * 43758.5453);
}

highp float sample_noise_vhs(sampler2D sampler,vec2 fragCoord,highp float iTime,highp vec2 uv)
{
    highp vec2 iResolution = vec2(width,height);
    highp vec2 uvX = mod(fragCoord + vec2(0, 100. * iTime), iResolution.xy);
    highp float value = texture2D(sampler, uvX / iResolution.xy).r;
    return pow(value, 7.);
}

highp vec4 VHSSimpleEffect(sampler2D sampler,highp vec2 uv ,highp float wobbValue,highp float iTime)
{
    highp vec2 wobbl = vec2(wobbValue * randVHS(vec2(iTime, uv.y)), 0.);

    //  band distortion
    highp float t_val = tan(0.5 * iTime + uv.y * PI * .8);
    highp vec2 tan_off = vec2(wobbl.x * min(0., t_val), 0.);

    //  chromab
    highp vec4 color1 = texture2D(sampler, uv + wobbl + tan_off);
    highp vec4 color2 = texture2D(sampler, (uv + (wobbl * 1.5) + (tan_off * 1.3)) * 1.005);
    //  combine + grade
    highp vec4 color = vec4(color2.rg, pow(color1.b, .67), 1.);
    color.rgb = mix(texture2D(sampler, uv + tan_off).rgb, color.rgb, 0.5);

    //  scanline sim
    highp float s_val = ((sin(2. * PI * uv.y + iTime * 20.) + sin(2. * PI * uv.y)) / 2.) * .015 * sin(iTime);
    color += s_val;

    //  noise lines
    highp float ival = uv.y / 4.;
    highp float r = randVHS(vec2(iTime, uv.y));
    //  dirty hack to avoid conditional
    int a =  int(uv.y + (iTime * r * 1000.));
    int b =  int(ival + 1.);
    float c =  mod(float(a) ,float(b));
    float on = floor(c / ival);

    highp float wh = sample_noise_vhs(sampler,uv,iTime,uv) * on;
    color = vec4(min(1., color.r + wh), min(1., color.g + wh), min(1., color.b + wh), 1.);

    highp float vig = 1. - sin(PI * uv.x) * sin(PI * uv.y);

    return color - (vig * 0.15);

}

//// ******************************* VHSTVNoise *******************************
highp float randNoise(vec2 co){
    return fract(sin(dot(co.xy ,vec2(12.9898,78.233))) * 43758.5453);
}

highp vec4 VHSTVNoise(highp vec4 fragColor,sampler2D sampler,highp vec2 uv ,highp float iTime)
{
    highp vec2 uv_org = vec2(uv);
    
    highp float t = mod(iTime, 360.0);
    highp float t2 = floor(t*0.6);
    
    highp float x,y,yt,xt;
    
    yt= abs(cos(t)) * rand(vec2(t,t)) * 100.0;
    xt= sin(360.0*rand(vec2(t,t)))*0.25;
    if (xt < 0.0) {
        xt=0.25;
    }
    x=uv.x-xt*exp(-pow(uv.y*100.0-yt,2.0)/24.0);
    y=uv.y;
    uv.x=x;
    uv.y=y;
    
    yt=0.5*cos((yt/100.0)/100.0*360.0);
    highp float yr=0.1*cos((yt/100.0)/100.0*360.0);
    if (uv_org.y > yt && uv_org.y < yt+rand(vec2(t2,t))*0.25) {
        highp float md = mod(x*100.0,10.0);
        if (md*sin(t) > sin(yr*360.0) || rand(vec2(md,md))>0.4) {
            highp vec4 org_c = texture2D(sampler, uv);
            highp float colx = rand(vec2(t2,t2)) * 0.75;
            highp float coly = rand(vec2(uv.x+t,t));
            highp float colz = rand(vec2(t2,t2));
            fragColor = vec4(org_c.x+colx,org_c.y+colx,org_c.z+colx,0.0);
        }
    }
    else if (y<cos(t) && mod(x*40.0,2.0)>rand(vec2(y*t,t*t))*1.0 ||  mod(y*12.0,2.0)<rand(vec2(x,t))*1.0) {
        if (rand(vec2(x+t,y+t))>0.8) {
            fragColor = vec4(rand(vec2(x*t,y*t)),rand(vec2(x*t,y*t)),rand(vec2(x*t,y*t)),0.0);
            
        }
        else
        {
            fragColor = texture2D(sampler, uv);
        }
    }
    else {
        uv.x = uv.x + rand(vec2(t,uv.y)) * 0.0087 * sin(y*2.0);
        fragColor = texture2D(sampler, uv);
    }
    
    return fragColor;
}

// ******************************* VHSTVSignal *******************************
#define MAGIC_FORMULA pow(abs(sin(iTime+0.1*randSignal(uv + iTime))*1.002), 200.)

float randSignal(vec2 co){
    return fract(sin(dot(co.xy ,vec2(12.98,78.233))) * 3758.5453);
}

highp vec4 VHSTVSignal(highp vec4 fragColor,sampler2D sampler,highp vec2 localuv,highp float iTime)
{
    highp vec2 uv = localuv;
    
    uv -= vec2(0.5);
    uv *= pow(length(uv), 0.5);
    uv += vec2(0.5);
    
    uv.x += sin(uv.y*20.+iTime*10.)*0.01*MAGIC_FORMULA;
    uv.x += cos(uv.y*30.+iTime*10.)*0.01*MAGIC_FORMULA;
    uv.y += 0.2*MAGIC_FORMULA*sin(randSignal(vec2(iTime)));
    uv.y = mod(uv.y, 1.);
    
    highp vec4 video_col = mix(fragColor, vec4(0.7), 0.2*MAGIC_FORMULA);
    
    video_col -= abs(sin(uv.y*100. + iTime*5.))*0.08;
    video_col -= abs(sin(uv.y*100. - iTime*2.))*0.05;
    video_col -= vec4(pow(length(uv - vec2(0.5)), 0.5))*0.5;
    video_col *= randSignal(uv + vec2(cos(iTime), sin(iTime)))*3.;
    video_col -= 0.1;
    video_col += uv.y*vec4(0.5, 0.5, 1., 1.)/4.;
    video_col -= (1. - uv.y)*vec4(0.5, 1., 0.5, 1.)/8.;
    return video_col;
}

// ******************************* VHSTVBUG *******************************
highp float randBug(float n) {
    return fract(sin(n) * 43758.5453123);
}

highp float noiseBug(float p) {
    highp float fl = floor(p);
    highp float fc = fract(p);
    return mix(randBug(fl), randBug(fl + 1.0), fc);
}

highp float mapBug(float val, float amin, float amax, float bmin, float bmax) {
    highp float n = (val - amin) / (amax-amin);
    highp float m = bmin + n * (bmax-bmin);
    return m;
}

highp float snoiseBug(float p){
    return mapBug(noiseBug(p),0.0,1.0,-1.0,1.0);
}

highp float threshold(float val,float cut){
    highp float v = clamp(abs(val)-cut,0.0,1.0);
    v = sign(val) * v;
    highp float scale = 1.0 / (1.0 - cut);
    return v * scale;
}

highp vec2 uv_curve(vec2 uv) {
    uv = (uv - 0.5) * 2.0;
    uv *= 1.15;
    uv.x *= 1.0 + pow((abs(uv.y) / 5.0), 2.0);
    uv.y *= 1.0 + pow((abs(uv.x) / 5.0), 2.0);
    uv /= 1.15;
    uv  = (uv / 2.0) + 0.5;
    return uv;
}

highp vec3 ghost(sampler2D tex, vec2 uv){
    return texture2D(tex,uv).rgb;
}

highp vec2 uv_ybug(vec2 uv,highp float iTime){
    float n4 = clamp(noiseBug(200.0+iTime*2.)*14.,0.,2.);
    uv.y += n4;
    uv.y = mod(uv.y,1.);
    return uv;
}

highp vec2 uv_hstrip(vec2 uv,highp float iTime){
    highp float vnoise = snoiseBug(iTime*6.);
    highp float hnoise = threshold(snoiseBug(iTime*10.),.5);
    
    highp float line = (sin(uv.y*10.+vnoise)+1.)/2.;
    line = (clamp(line,.9,1.)-.9)*10.;
    
    uv.x += line * 0.03 * hnoise;
    uv.x = mod(uv.x,1.);
    return uv;
}

highp vec4 VHSTVBugEffect(sampler2D sampler,highp vec2 localuv ,highp float localTime)
{
    highp float t = float(int(localTime * 13.0));
    
    highp vec2 uv = localuv;
    
    uv = uv_curve(uv);
    highp vec2 ouv = uv;
    
    uv = uv_ybug(uv,localTime);
    uv = uv_hstrip(uv,localTime);
    
    highp vec2 onePixel = vec2(0.0, 1.0) / localuv.xy * 3.;
    highp vec3 color = ghost(sampler,uv);
    
    highp float scanA = (sin(uv.y*3.1415*localuv.y/2.7)+1.)/2.;
    highp float scanB = (sin(uv.y*3.1415*1.)+1.)/2.;
    color *= .75 + scanA * .02;
    
    color *= 1.0 + rand(uv+t*.01) * 0.2;
    
    highp vec3 backColor = vec3(.1,.1,.1);
    if (ouv.x < 0.0 || ouv.x > 1.0)
        color = backColor;
    if (ouv.y < 0.0 || ouv.y > 1.0)
        color = backColor;
    
    return vec4(color,1.0);
}

// ******************************* VHSScreenshake *******************************
const float amountShake = 0.8;
const float speedShake = 8.;

highp vec3 random3Shake(vec3 c) {
    highp float j = 4096.0*sin(dot(c,vec3(17.0, 59.4, 15.0)));
    highp vec3 r;
    r.z = fract(512.0*j);
    j *= .125;
    r.x = fract(512.0*j);
    j *= .125;
    r.y = fract(512.0*j);
    return r;
}

const float F3 =  0.3333333;
const float G3 =  0.1666667;

highp float simplex3d(vec3 p) {
    highp vec3 s = floor(p + dot(p, vec3(F3)));
    highp vec3 x = p - s + dot(s, vec3(G3));
    
    highp vec3 e = step(vec3(0.0), x - x.yzx);
    highp vec3 i1 = e*(1.0 - e.zxy);
    highp vec3 i2 = 1.0 - e.zxy*(1.0 - e);
    
    highp vec3 x1 = x - i1 + G3;
    highp vec3 x2 = x - i2 + 2.0*G3;
    highp vec3 x3 = x - 1.0 + 3.0*G3;
    
    highp vec4 w, d;
    
    w.x = dot(x, x);
    w.y = dot(x1, x1);
    w.z = dot(x2, x2);
    w.w = dot(x3, x3);
    
    w = max(0.6 - w, 0.0);
    
    d.x = dot(random3Shake(s)-.5, x);
    d.y = dot(random3Shake(s + i1)-.5, x1);
    d.z = dot(random3Shake(s + i2)-.5, x2);
    d.w = dot(random3Shake(s + 1.0)-.5, x3);
    
    w *= w;
    w *= w;
    d *= w;
    
    return dot(d, vec4(52.0));
}

highp vec4 VHSScreenShake(sampler2D sampler,highp vec2 uv ,highp float localTime)
{
    highp vec3 p3 = vec3(0,0, localTime*speedShake)*8.0+8.0;
    highp vec2 noise = vec2(simplex3d(p3),simplex3d(p3+10.));
    highp vec4 fragColor = vec4(texture2D(sampler, uv+noise*amountShake*0.1 ).rgb, 1.0);
    return fragColor;
}

// ******************************* VHSOldTVShakyEffect *******************************
highp vec4 VHSOldTVShakyEffect(sampler2D sampler,highp vec2 uv ,highp float iTime)
{
    highp float d = length(uv - vec2(0.5,0.5));
    
    // blur amount
    highp float blur = 0.02;
    blur = (1.0 + sin(iTime*6.0)) * 0.5;
    blur *= 1.0 + sin(iTime*16.0) * 0.5;
    blur = pow(blur, 3.0);
    blur *= 0.05;
    // reduce blur towards center
    blur *= d;
    
    highp float myTime = iTime * 1.0;
    
    highp vec4 fragColor = texture2D( sampler, vec2(uv.x + sin( (uv.y + sin(myTime)) * abs(sin(myTime) + sin(2.0 * myTime) + sin(0.3 * myTime) + sin(1.4 * myTime) + cos(0.7 * myTime) + cos(1.3 * myTime)) * 4.0 ) * 0.02,uv.y) );
    
    highp vec2 myuv = vec2(uv.x + sin( (uv.y + sin(myTime)) * abs(sin(myTime) + sin(2.0 * myTime) + sin(0.3 * myTime) + sin(1.4 * myTime) + cos(0.7 * myTime) + cos(1.3 * myTime)) * 4.0 ) * 0.02,uv.y) ;
    
    // final color
    highp vec3 col;
    col.r = texture2D( sampler, vec2(myuv.x+blur,myuv.y) ).r;
    col.g = texture2D( sampler, myuv ).g;
    col.b = texture2D( sampler, vec2(myuv.x-blur,myuv.y) ).b;
    
    // scanline
    highp float scanline = sin(uv.y*400.0)*0.08;
    col -= scanline;
    
    // vignette
    col *= 1.0 - d * 0.5;
    fragColor = vec4(col,1.0);
    return fragColor;
}

// ******************************* VHSOldReelEffect *******************************
highp float rand_Old(highp vec2 co)
{
    return fract(sin(dot(co.xy, vec2(12.9898,78.233))) * 43758.5453);
}
highp vec2 hash22Old(vec2 p)
{
    p = vec2(dot(p,vec2(127.1,311.7)), dot(p,vec2(269.5,183.3)));
    return -1.0 + 2.0 * fract(sin(p)*43758.5453123);
}
highp float nosOld(vec2 p)
{
    const float K1 = 0.366025404; // (sqrt(3)-1)/2;
    const float K2 = 0.211324865; // (3-sqrt(3))/6;
    highp vec2 i = floor(p + (p.x + p.y) * K1);
    highp vec2 a = p - (i - (i.x + i.y) * K2);
    highp vec2 o = (a.x < a.y) ? vec2(0.0, 1.0) : vec2(1.0, 0.0);
    highp vec2 b = a - o + K2;
    highp vec2 c = a - 1.0 + 2.0 * K2;
    highp vec3 h = max(0.5 - vec3(dot(a, a), dot(b, b), dot(c, c)), 0.0);
    highp vec3 n = h * h * h * h * vec3(dot(a, hash22Old(i)), dot(b, hash22Old(i + o)), dot(c, hash22Old(i + 1.0)));
    return dot(vec3(70.0, 70.0, 70.0), n);
}

highp vec4 VHSOldReelEffect(sampler2D sampler,highp vec2 uv ,highp float iTime)
{
    highp vec2 iResolution = vec2(1.0,1.0);
    highp vec2 pos = uv/iResolution.xy;
    //screen dither x：
    //screen dither x：
    vec2 nos_pos = pos - 0.5 + vec2(min(fract(iTime) - 0.02, 0.0), 0.0);
    vec2 abs_n_p = vec2(abs(nos_pos));
    //screen dither y：
    vec2 P = pos + vec2(0.0, max(min(40.0 * fract(iTime) - 38.0, -40.0 * fract(iTime) + 40.0), 0.0));
    vec3 Col = texture2D(sampler, P).rgb;
    //border x：
    if(abs_n_p.x > 0.40)
    {
        if(abs_n_p.x > 0.42 && abs_n_p.x < 0.48)
            Col = vec3(0.7) * step(mod(10.0 * (nos_pos.y + fract(iTime)), 1.0), 0.8);
        else
            Col = vec3(0.0);
    }
    //border y：
    if(abs_n_p.y > 0.40)
        Col *= mix(0.0, 1.0, 5.0 - 10.0 * abs_n_p.y);
    //dark spots：
    Col *= 4.0 * min(0.5 * nosOld(8.0 * nos_pos) + 0.5, 0.25);
    //screen scratch：
    float coord = rand_Old(vec2(floor(4.0 * iTime), pos.x));
    if(abs(pos.x - coord) <= 0.001)
        Col *= 0.5 + 0.5 * nosOld(vec2(20.0, 2.0) * pos);
    //light spots：
    if(fract(0.1 * iTime) > 0.9)
    {
        Col += step(nosOld(6.0 * (pos + vec2(floor(4.0 * fract(iTime)), 0.0))), -0.8);
        if(fract(0.1 * iTime) > 0.98)
            Col *= 1.0 + 0.25 * abs(sin(20.0 * fract(2.0 * iTime)));
    }
    
    return vec4(clamp(Col, 0.0, 0.8),1.0);
}

// ******************************* VHSCameraEffect *******************************
highp vec4 VHSCameraEffect(sampler2D sampler,highp vec2 uv ,highp float localTime)
{
    highp vec2 pos=vec2(0.5+0.5*sin(localTime),uv.x);
    highp vec2 pos1=vec2(0.5+0.5*sin(localTime),uv.y);
    highp vec3 col=vec3(texture2D(sampler,uv));
    highp vec3 col2=vec3(texture2D(sampler,pos))*0.1;
    highp vec3 col3=vec3(texture2D(sampler,pos1))*0.1;
    col+=col2;
    col+=col3;
    return vec4(col,1.0);
}

// ******************************* BlackWhiteRGBShift *******************************
highp vec4 BlackWhiteRGBShift(sampler2D sampler,highp vec2 localUV,highp int type)
{
    highp vec4 texColor;
    
    if (type == 1) {
        texColor.r = BlackWhiteEffect(texture2D(sampler,vec2(localUV.x+0.006,localUV.y+0.009)),vec4(1.0,1.0,1.0,1.0)).r;
        texColor.g = BlackWhiteEffect(texture2D(sampler,vec2(localUV.x,localUV.y)),vec4(1.0,1.0,1.0,1.0)).g;
        texColor.b = BlackWhiteEffect(texture2D(sampler,vec2(localUV.x-0.006,localUV.y-0.009)),vec4(1.0,1.0,1.0,1.0)).b;
    }
    else if(type ==2)
    {
        highp vec2 tempUV = localUV;
        texColor.r = BlackWhiteEffect(texture2D(sampler,vec2(tempUV.x+0.006,tempUV.y+0.009)),vec4(1.0,1.0,1.0,1.0)).r;
        texColor.g = BlackWhiteEffect(texture2D(sampler,vec2(localUV.x,localUV.y)),vec4(1.0,1.0,1.0,1.0)).g;
        texColor.b = BlackWhiteEffect(texture2D(sampler,vec2(tempUV.x-0.006,tempUV.y-0.009)),vec4(1.0,1.0,1.0,1.0)).b;
    }
    else if(type == 3)
    {
        highp float maxDistance =  1.0/60.0;
        highp float xPosR = 0.0;
        highp float xPosB = 0.0;

        if (localUV.x<0.4) {
            highp float ratio = localUV.x / 0.4;
            ratio = 0.4 - ratio;
            xPosR = maxDistance * ratio;
        } else if(localUV.x>0.6) {
            highp float ratio = localUV.x - 0.6;
            ratio = ratio / 0.4;
            xPosB = maxDistance * ratio;
        }
        
        texColor.r = BlackWhiteEffect(texture2D(sampler,vec2(localUV.x-xPosR,localUV.y)),vec4(1.0,1.0,1.0,1.0)).r;
        texColor.g = BlackWhiteEffect(texture2D(sampler,vec2(localUV.x,localUV.y)),vec4(1.0,1.0,1.0,1.0)).g;
        texColor.b = BlackWhiteEffect(texture2D(sampler,vec2(localUV.x-0.0,localUV.y)),vec4(1.0,1.0,1.0,1.0)).b;
    }
    return texColor;
}

// ******************************* BottomPixelNoise *******************************
highp float scale = 0.03;
highp float b_threshold = 0.4;
highp float l_threshold = 0.5;
highp float rgb_offset = 5.0;
highp float bias = 0.3;
highp float opacity = 1.0;

highp float rand2(highp vec2 co)
{
    return fract(sin(dot(co.xy,vec2(12.9898,78.233))) * 43758.5453);
}

highp vec3 rand1(highp vec2 uv) {

    highp vec2 iResolution = vec2(1.0,1.0);
    highp vec2 c = ((.01)*iResolution .x)*vec2(1.,(iResolution .y/iResolution .x));
    highp vec3 col = vec3(0.0);
    highp float r = rand2(vec2((2.) * floor(uv.x*c.x)/c.x, (2.) * floor(uv.y*c.y)/c.y ));
    highp float g = rand2(vec2((5.) * floor(uv.x*c.x)/c.x, (5.) * floor(uv.y*c.y)/c.y ));
    highp float b = rand2(vec2((9.) * floor(uv.x*c.x)/c.x, (9.) * floor(uv.y*c.y)/c.y ));
    
    col = vec3(r,g,b);
    return col;
}

float n_scale = 1.0 ;
float n_g_noise = 1.0 ;

highp vec4 ColorNoise(highp vec4 finalColor,sampler2D sampler,highp vec2 localUV,highp float iLocalTime)
{
    // color noise
    finalColor.rgb *= 0.9 + 0.1 * texture2D( sampler, mod( localUV * vec2( 1.0, 1.0 ) + iLocalTime * vec2( 5.97, 4.45 ), vec2( 1.0 ) ) ).xyz;
    finalColor = saturate(finalColor);
    return finalColor;
}

highp vec2 SwitchingNoise(highp vec2 localUV,highp float iLocalTime)
{
    highp float snPhase = smoothstep( 0.8, 1.0, localUV.y );
    localUV.x += snPhase * ( ( noiseVHSBig( vec2( localUV.y * 100.0, iLocalTime * 10.0 ) ) - 0.5 ) * 0.2 );
    return localUV;
}

highp vec4 BottomPixelNoise(highp vec4 inputColor,sampler2D sampler,highp vec2 uv,highp float localTime,highp float localPower,int type)
{
    highp vec4 texColor = vec4(0);
    highp vec2 samplePosition = uv ;
    highp float whiteNoise = 1.0;
    highp vec2 fragCoord = vec2(uv.x * width,uv.y * height);
    
    samplePosition.x = samplePosition.x+(randNormal(vec2(localTime,fragCoord.y))-0.5)/64.0;
    samplePosition.y = samplePosition.y+(randNormal(vec2(localTime))-0.5)/32.0;
    texColor = texColor + (vec4(-0.5)+vec4(randNormal(vec2(fragCoord.y,localTime)),randNormal(vec2(fragCoord.y,localTime+1.0)),randNormal(vec2(fragCoord.y,localTime+2.0)),0))*0.1;
    
    whiteNoise = randNormal(vec2(floor(samplePosition.y*200.0),floor(samplePosition.x*160.0))+vec2(localTime,0));

    if(type == 0)
    {
        if (whiteNoise < 27.0-30.0*samplePosition.y || whiteNoise > 5.0 -5.0*samplePosition.y)
        {
            texColor = texture2D(sampler,uv);
        } else {
            texColor = vec4(0.8,0.8,0.8,1.0);
        }
    } else if(type == 1) {
        if (whiteNoise < 27.0-30.0*samplePosition.y || whiteNoise > 5.0 -5.0*samplePosition.y)
        {
            texColor.r = texture2D(sampler,vec2(uv.x+0.006,uv.y+0.006)).r;
            texColor.g = texture2D(sampler,uv).g;
            texColor.b = texture2D(sampler,vec2(uv.x-0.006,uv.y-0.006)).b;
            texColor = GrainEffect(texColor,36.0,3.43,0);
            texColor = mix(inputColor,texColor,0.5);
        } else {
            texColor = vec4(0.8,0.8,0.8,1.0);
        }
    } else {
        texColor = inputColor;
    }
    
    return texColor;
}

// ******************************* Brannan Effect *******************************
highp float overlay(highp float s, highp float d )
{
    return (d < 0.5) ? 2.0 * s * d : 1.0 - 2.0 * (1.0 - s) * (1.0 - d);
}

highp vec3 overlay(highp vec3 s, highp vec3 d )
{
    highp vec3 c;
    c.x = overlay(s.x,d.x);
    c.y = overlay(s.y,d.y);
    c.z = overlay(s.z,d.z);
    return c;
}

highp float greyScale(highp vec3 col)
{
    return dot(col, vec3(0.3, 0.59, 0.11));
}

mat3 saturationMatrix(highp float saturation ) {
    
    highp vec3 luminance = vec3( 0.3086, 0.6094, 0.0820 );
    highp float oneMinusSat = 1.0 - saturation;
    highp vec3 red = vec3( luminance.x * oneMinusSat );
    red.r += saturation;
    
    highp vec3 green = vec3( luminance.y * oneMinusSat );
    green.g += saturation;
    
    highp vec3 blue = vec3( luminance.z * oneMinusSat );
    blue.b += saturation;
    
    return mat3(red, green, blue);
}


void levels(inout highp vec3 col, in highp vec3 inleft, in highp vec3 inright, in highp vec3 outleft, in highp vec3 outright) {
    col = clamp(col, inleft, inright);
    col = (col - inleft) / (inright - inleft);
    col = outleft + col * (outright - outleft);
}

void brightnessAdjust( inout highp vec3 color, in highp float b) {
    color += b;
}

void contrastAdjust( inout highp vec3 color, in highp float c) {
    float t = 0.5 - c * 0.5;
    color = color * c + t;
}

vec3 colorBurn(in vec3 s, in vec3 d )
{
    return 1.0 - (1.0 - d) / s;
}

highp vec4 BrannanEffect (highp vec4 inputColor,highp vec2 uv,highp float localTime,highp float localPower,int type)
{
    highp vec3 col = inputColor.rgb;
    highp vec3 grey = vec3(greyScale(col));
    col = saturationMatrix(0.7) * col;
    grey = overlay(grey, col);
    col = mix(grey, col, 0.63);
    levels(col, vec3(0., 0., 0.) / 255., vec3(228., 255., 239.) / 255.,
           vec3(23., 3., 12.) / 255., vec3(255.) / 255.);
    
    brightnessAdjust(col, -0.1);
    contrastAdjust(col, 1.05);
    
    highp vec3 tint = vec3(255., 248., 242.) / 255.;
    
    levels(col, vec3(0., 0., 0.) / 255., vec3(255., 224., 255.) / 255.,
           vec3(9., 20., 18.) / 255., vec3(255.) / 255.);
    col = pow(col, vec3(0.91, 0.91, 0.91*0.94));
    brightnessAdjust(col, -0.04);
    contrastAdjust(col, 1.14);
    col = tint * col;
    
    inputColor = vec4(col, 1.0);

    return inputColor;
}


// ******************************* EarlyBirdEffect *******************************
highp vec4 EarlyBirdEffect (highp vec4 inputColor,highp vec2 uv,highp float localTime,highp float localPower,int type)
{
    highp vec3 col = inputColor.rgb;
    
    highp vec2 iResolution = vec2(720.0,1280.0);
    
    highp vec2 coord = ( uv.xy + uv.xy -  iResolution.xy ) / iResolution.y;
    
    highp vec3 gradient = vec3(pow(1.0 - length(coord * 0.4), 0.6) * 1.2);
    highp vec3 grey = vec3(184./255.);
    highp vec3 tint = vec3(252., 243., 213.) / 255.;
    col = saturationMatrix(0.68) * col;
    levels(col, vec3(0.), vec3(1.0),
           vec3(27.,0.,0.) / 255., vec3(255.) / 255.);
    col = pow(col, vec3(1.19));
    brightnessAdjust(col, 0.13);
    contrastAdjust(col, 1.05);
    col = saturationMatrix(0.85) * col;
    levels(col, vec3(0.), vec3(235./255.),
           vec3(0.,0.,0.) / 255., vec3(255.) / 255.);
    
    col = mix(tint * col, col, gradient);
    col = colorBurn(grey, col);
    //col *= 0.8;
    inputColor = vec4(col, 1.0);
    
    return inputColor;
}

// ******************************* GlitchEffect *******************************
highp vec4 GlitchEffect( highp vec4 finalColor,sampler2D sampler, highp vec2 localUV,highp float power, highp float iTime,int type)
{
    highp vec2 uv = localUV ;
    highp vec4 texColor = finalColor;
    
    highp float gt = 30.0;
    highp float m = mod(iTime, 1.0);
    bool glitch = m < 0.5;
    highp float t = floor(iTime * gt) / gt;
    highp float r = randNormal(vec2(t, t));
    
    if(glitch) {
        
        highp vec2 uvGlitch = uv;
        uvGlitch.x -= r / 5.0;
        if(uv.y > r && uv.y < r + 0.01) {
            texColor = texture2D(sampler, uvGlitch);
        }
    }
    
    finalColor = texColor;
    
    return finalColor;
}

// ******************************* VHSEffects *******************************
highp vec4 VHSEffects(sampler2D sampler,highp vec2 localUV,highp float iLocalTime,highp float localPower , int type)
{
    highp vec4 finalColor = texture2D(inputImageTexture, localUV);
    
    if(type == 0)
    {
        finalColor = texture2D(inputImageTexture, localUV);
    }
    else if(type == 1)
    {
        finalColor = VHSChromatic(sampler,localUV,iLocalTime);
        finalColor = GlitchEffect(finalColor,sampler,localUV,localPower,iLocalTime,1);
        finalColor.r *= finalColor.r;
        finalColor.g *= finalColor.g;
        finalColor.b *= finalColor.b;
    }
    else if(type == 2)
    {
        finalColor = VHSCameraEffect(sampler,localUV,iLocalTime);
        finalColor.r *= finalColor.r;
        finalColor.g *= finalColor.g;
        finalColor.b *= finalColor.b;
    }
    else if(type == 3)
    {
        finalColor = VHSSimpleEffect(sampler,localUV,0.01,iLocalTime);
        finalColor = brightness(finalColor);
    }
    else if(type == 4)
    {
        highp float tcPhase = clamp( ( sin( localUV.y * 8.0 - iLocalTime * PI * 1.2 ) - 0.92 ) * noise( vec2( iLocalTime ) ), 0.0, 0.01 ) * 10.0;
        highp float tcNoise = max( noise( vec2( localUV.y * 100.0, iLocalTime * 10.0 ) ) - 0.5, 0.0 );
        localUV.x = localUV.x + (tcNoise * tcPhase);
        localUV.y = localUV.y + (tcNoise * tcPhase);
        finalColor = MusicDistortion(sampler,localUV,iLocalTime,localPower);
        finalColor.r *= finalColor.r;
        finalColor.g *= finalColor.g;
        finalColor.b *= finalColor.b;
        finalColor = SmallPixelNoiseEffect(finalColor,localUV,iLocalTime,0);
    }
    else if(type == 5)
    {
        finalColor = VHSTVNoise(finalColor,sampler,localUV, iLocalTime);
        finalColor = brightness(finalColor);
    }
    else if(type == 6)
    {
        localUV = SwitchingNoise(localUV,iLocalTime);
        finalColor = texture2D(sampler,localUV);
        finalColor = VHSTVSignal(finalColor,sampler,localUV,iLocalTime);
    }
    else if(type == 7)
    {
        localUV = SwitchingNoise(localUV,iLocalTime);
        finalColor = VHSTVBugEffect(sampler,localUV,iLocalTime);
    }
    else if(type == 8)
    {
        finalColor = VHSScreenShake(sampler,localUV,iLocalTime);
        finalColor = SmallPixelNoiseEffect(finalColor,localUV,iLocalTime,0);
    }
    else if(type == 9)
    {
        finalColor = VHSSimpleEffect(sampler,localUV,0.05,iLocalTime);
        finalColor.r *= finalColor.r;
        finalColor.g *= finalColor.g;
        finalColor.b *= finalColor.b;
    }
    else if(type == 10)
    {
        finalColor = VHSOldTVShakyEffect(sampler,localUV,iLocalTime);
        finalColor = SmallPixelNoiseEffect(finalColor,localUV,iLocalTime,0);
    }
    else if(type == 11)
    {
        highp float tcPhase = clamp( ( sin( localUV.y * 8.0 - iLocalTime * PI * 1.2 ) - 0.92 ) * noise( vec2( iLocalTime ) ), 0.0, 0.01 ) * 10.0;
        highp float tcNoise = max( noise( vec2( localUV.y * 100.0, iLocalTime * 10.0 ) ) - 0.5, 0.0 );
        localUV.x = localUV.x + (tcNoise * tcPhase);
        finalColor = VHSTVNoise(finalColor,sampler,localUV, iLocalTime);
    }
    else if(type == 12)
    {
        finalColor = VHSOldReelEffect(sampler,localUV,iLocalTime);
    }
    else if(type == 13)
    {
        highp float tcPhase = clamp( ( sin( localUV.y * 8.0 - iLocalTime * PI * 1.2 ) - 0.92 ) * noise( vec2( iLocalTime ) ), 0.0, 0.01 ) * 10.0;
        highp float tcNoise = max( noise( vec2( localUV.y * 100.0, iLocalTime * 10.0 ) ) - 0.5, 0.0 );
        localUV.x = localUV.x + (tcNoise * tcPhase);
        finalColor = BottomPixelNoise(finalColor,sampler,localUV,iLocalTime,localPower,0);
        finalColor = SmallPixelNoiseEffect(finalColor,localUV,iLocalTime,0);
        finalColor = EarlyBirdEffect(finalColor,localUV,iLocalTime,localPower,type);
    }
    
    return finalColor;
}

// ******************************* GlitchRGBEffects *******************************
uniform highp float amplitude;
uniform highp float speed;

highp vec4 rgbShift(sampler2D sampler,highp vec2 p ,highp vec4 shift,int type) {
    
    shift *= 4.0*shift.w - 1.0;
    highp vec2 rs = vec2(shift.x,-shift.y);
    highp vec2 gs = vec2(shift.y,-shift.z);
    highp vec2 bs = vec2(shift.z,-shift.x);
    
    if (type==2) {
        rs = vec2(shift.x,+shift.y);
        gs = vec2(shift.y,+shift.z);
        bs = vec2(shift.z,+shift.x);
    }
    
    if (type==3) {
        rs = vec2(shift.x);
        gs = vec2(shift.y);
        bs = vec2(shift.z);
    }
    
    highp float r = texture2D(sampler, p+rs, 1.0).x;
    highp float g = texture2D(sampler, p+gs, 1.0).y;
    highp float b = texture2D(sampler, p+bs, 1.0).z;
    
    return vec4(r,g,b,1.0);
}

highp vec4 noise(sampler2D sampler, highp vec2 p ) {
    return texture2D(sampler, p, 0.0);
}

highp vec4 vec4pow( highp vec4 v, highp float p ) {
    // Don't touch alpha (w), we use it to choose the direction of the shift
    // and we don't want it to go in one direction more often than the other
    return vec4(pow(v.x,p),pow(v.y,p),pow(v.z,p),v.w);
}

highp vec4 GlitchRGBEffects (highp vec4 color,sampler2D sampler,highp vec2 uv,highp float localTime,highp float localPower,int type)
{
    highp vec2 p = uv.xy;
    highp vec4 c = vec4(0.0,0.0,0.0,1.0);
    highp float iTime = localTime;
    
    highp vec4 shift = vec4pow(noise(sampler,vec2(speed*iTime,4.0*speed*iTime/40.0 )),15.0) * vec4(amplitude,amplitude,amplitude,1.3);
    
    if (type==1) {
        shift = vec4pow(noise(sampler,vec2(speed*iTime,4.0*speed*iTime/40.0 )),15.0) * vec4(amplitude,amplitude,amplitude,1.3);
    }
    
    if (type==2) {
        shift = vec4pow(noise(sampler,vec2(speed*iTime,2.0*speed*iTime/40.0 )),20.0) * vec4(amplitude,amplitude,amplitude,3.0);
    }
    
    if (type==3) {
        shift = vec4pow(noise(sampler,vec2(speed*iTime,4.0*speed*iTime/40.0 )),10.0) * vec4(amplitude,amplitude,amplitude,4.0);
    }
    
    c += rgbShift(sampler,p, shift,type);
    
    return c;
}

// ******************************* GlitchChicco : SAHIL *******************************
highp float randomChoco (vec2 st) {
    return fract(sin(dot(st.xy, vec2(12.9898,78.233))) * 43758.5453123);
}

highp float noiseChoco (vec2 st) {
    
    highp vec2 i = floor(st);
    highp vec2 f = fract(st);

    highp float a = randomChoco(i);
    highp float b = randomChoco(i + vec2(1.0, 0.0));
    highp float c = randomChoco(i + vec2(0.0, 1.0));
    highp float d = randomChoco(i + vec2(1.0, 1.0));

    highp vec2 u = f*f*(3.0-2.0*f);
    return mix(a, b, u.x) + (c - a)* u.y * (1.0 - u.x) + (d - b) * u.x * u.y;
}

highp float noiseChicco (float st) {
    return noiseChoco(vec2(st));
}

highp vec4 ChiccoGlitch(highp vec4 color,sampler2D sampler,highp vec2 localUV,highp float iTime,highp float localPower , int type)
{
    highp vec2 uv = localUV.xy;
    highp float time = iTime;
    highp float y = 1.0 - uv.y;
    highp float f = y * y * y * abs(sin(y * 3. + noiseChicco(time) * 3.) * cos(1. / y* 200. + time * 20.) / sin(y * 10. + noiseChicco(time * time) * 0.3));
    highp vec2 textCoords = vec2(uv.x - y * f, uv.y);

    return texture2D(sampler, textCoords);

}

// ******************************* GlitchImage *******************************
highp float random2d(vec2 n) {
    return fract(sin(dot(n, vec2(12.9898, 4.1414))) * 43758.5453);
}

highp float randomRange (vec2 seed,float min,float max) {
    return min + random2d(seed) * (max - min);
}

highp float insideRange(float v, float bottom, float top) {
    return step(bottom, v) - step(top, v);
}

highp float AMT = 0.2;
highp float SPEED = 0.6;

highp vec4 GlitchImage(highp vec4 color,sampler2D sampler,highp vec2 localUV,highp float iTime,highp float localPower , int type)
{
    if (type==1) {
        AMT = 0.1;
        SPEED = 0.2;
    }
    
    if (type==2) {
        AMT = 0.25;
        SPEED = 1.0;
    }
    
    if (type==3) {
        AMT = 0.35;
        SPEED = 1.5;
    }
    
    highp float time = floor(iTime * SPEED * 60.0);
    highp vec2 uv = localUV.xy;
    highp vec3 outCol = texture2D(sampler, uv).rgb;
  
    highp float maxOffset = AMT/2.0;
    
    for (float i = 0.0; i < 10.0 * AMT; i += 1.0) {
        highp float sliceY = random2d(vec2(time , 2345.0 + float(i)));
        highp float sliceH = random2d(vec2(time , 9035.0 + float(i))) * 0.25;
        highp float hOffset = randomRange(vec2(time , 9625.0 + float(i)), -maxOffset, maxOffset);
        highp vec2 uvOff = uv;
        uvOff.x += hOffset;
        if (insideRange(uv.y, sliceY, fract(sliceY+sliceH)) == 1.0 ){
            outCol = texture2D(sampler, uvOff).rgb;
        }
    }
    
    //do slight offset on one entire channel
    highp float maxColOffset = AMT/6.0;
    highp float rnd = random2d(vec2(time , 9545.0));
    highp vec2 colOffset = vec2(randomRange(vec2(time , 9545.0),-maxColOffset,maxColOffset), randomRange(vec2(time , 7205.0),-maxColOffset,maxColOffset));
    
    if (rnd < 0.33){
        outCol.r = texture2D(sampler, uv + colOffset).r;
        
    }else if (rnd < 0.66){
        outCol.g = texture2D(sampler, uv + colOffset).g;
        
    } else{
        outCol.b = texture2D(sampler, uv + colOffset).b;
    }
    
    return vec4(outCol,1.0);
    
}

// ******************************* GlitchNoise *******************************
highp float renderRandom(vec2 st)
{
    const vec2 d = vec2(12.9898,78.233);
    return fract(sin(dot(st.xy, d)) * 43758.5453123);
}

highp float renderNoise(vec2 st)
{
    highp vec2 i = floor(st);
    highp vec2 f = fract(st);
    
    highp vec2 dv = vec2(1.0, 0.0);
    
    highp float a = renderRandom(i);
    highp float b = renderRandom(i + dv.xy);
    highp float c = renderRandom(i + dv.yx);
    highp float d = renderRandom(i + dv.xx);
    
    highp vec2 u = f * f * (3.0 - 2.0 * f);
    
    return mix(a, b, u.x) + (c - a) * u.y * (1.0 - u.x) + (d - b) * u.x * u.y;
}

highp vec3 renderMosaic(vec2 uv, float scale)
{
    highp float rand = renderRandom(floor(uv * scale));
    
    return vec3(rand);
}

highp vec3 renderValueNoise(vec2 uv, float scale)
{
    highp float n = renderNoise(uv * 15.0);
    return vec3(n);
}

highp vec4 NoiseGlitch(highp vec4 color,sampler2D sampler,highp vec2 localUV,highp float iTime,highp float localPower , int type)
{
    highp vec2 iResolution = vec2(1.0,1.0);
    
    highp vec2 uv = localUV / iResolution.xy;

    highp vec2 d = 1.0 / iResolution.xy;

    highp float mask = renderMosaic(uv * vec2(0.3, 1.0), 20.0).r;

    highp vec4 tex = texture2D(sampler, uv);
    highp float r = texture2D(sampler, uv + d * 5.0).r;
    highp float g = texture2D(sampler, uv + d * 8.0).g;
    highp float b = texture2D(sampler, uv + d * 10.0).b;
    highp vec3 tex2 = vec3(r, g, b);
    //tex2 = vec3(0.0);

    highp vec3 col = tex.rgb;

    highp float s = renderNoise(vec2(iTime * 2.5, iTime * 1.5));

    // Range of glitch.
    vec2 rs[3];
    rs[0] = vec2(0.1, 0.3);
    rs[1] = vec2(0.5, 0.55);
    rs[2] = vec2(0.8, 0.9);

    for (int i = 0; i < 3; i++)
    {
        if (s > rs[i].x && s < rs[i].y)
        {
            if (mask > rs[i].x && mask < rs[i].y)
            {
                col = tex2.rgb;
            }
            break;
        }
    }

    return vec4(col, 1.0);

}

// ******************************* GlitchNoise2 *******************************
highp vec3 mod289(highp vec3 x) {
    return x - floor(x * (1.0 / 289.0)) * 289.0;
}

highp vec4 mod289(vec4 x) {
    return x - floor(x * (1.0 / 289.0)) * 289.0;
}

highp vec4 permuteNoise(vec4 x) {
    return mod289(((x*34.0)+1.0)*x);
}

highp vec4 taylorInvSqrtNoise(vec4 r)
{
    return 1.79284291400159 - 0.85373472095314 * r;
}

highp float snoiseGlitcher(vec3 v)
{
    const vec2 C = vec2(1.0/6.0, 1.0/3.0) ;
    const vec4 D = vec4(0.0, 0.5, 1.0, 2.0);
    highp vec3 i  = floor(v + dot(v, C.yyy) );
    highp vec3 x0 =   v - i + dot(i, C.xxx) ;
    
    highp vec3 g = step(x0.yzx, x0.xyz);
    highp vec3 l = 1.0 - g;
    highp vec3 i1 = min( g.xyz, l.zxy );
    highp vec3 i2 = max( g.xyz, l.zxy );
    
    highp vec3 x1 = x0 - i1 + C.xxx;
    highp vec3 x2 = x0 - i2 + C.yyy;
    highp vec3 x3 = x0 - D.yyy;
    
    i = mod289(i);
    highp vec4 p = permuteNoise( permuteNoise( permuteNoise( i.z + vec4(0.0, i1.z, i2.z, 1.0 )) + i.y + vec4(0.0, i1.y, i2.y, 1.0 )) + i.x + vec4(0.0, i1.x, i2.x, 1.0 ));
    
    highp float n_ = 0.142857142857;
    highp vec3  ns = n_ * D.wyz - D.xzx;
    
    highp vec4 j = p - 49.0 * floor(p * ns.z * ns.z);
    
    highp vec4 x_ = floor(j * ns.z);
    highp vec4 y_ = floor(j - 7.0 * x_ );
    
    highp vec4 x = x_ *ns.x + ns.yyyy;
    highp vec4 y = y_ *ns.x + ns.yyyy;
    highp vec4 h = 1.0 - abs(x) - abs(y);
    
    highp vec4 b0 = vec4( x.xy, y.xy );
    highp vec4 b1 = vec4( x.zw, y.zw );
    highp vec4 s0 = floor(b0)*2.0 + 1.0;
    highp vec4 s1 = floor(b1)*2.0 + 1.0;
    highp vec4 sh = -step(h, vec4(0.0));
    
    highp vec4 a0 = b0.xzyw + s0.xzyw*sh.xxyy ;
    highp vec4 a1 = b1.xzyw + s1.xzyw*sh.zzww ;
    
    highp vec3 p0 = vec3(a0.xy,h.x);
    highp vec3 p1 = vec3(a0.zw,h.y);
    highp vec3 p2 = vec3(a1.xy,h.z);
    highp vec3 p3 = vec3(a1.zw,h.w);
    
    highp vec4 norm = taylorInvSqrtNoise(vec4(dot(p0,p0), dot(p1,p1), dot(p2, p2), dot(p3,p3)));
    p0 *= norm.x;
    p1 *= norm.y;
    p2 *= norm.z;
    p3 *= norm.w;
    
    highp vec4 m = max(0.6 - vec4(dot(x0,x0), dot(x1,x1), dot(x2,x2), dot(x3,x3)), 0.0);
    m = m * m;
    return 42.0 * dot( m*m, vec4( dot(p0,x0), dot(p1,x1), dot(p2,x2), dot(p3,x3) ) );
}

highp vec4 GlitchNoise2(highp vec4 color,sampler2D sampler,highp vec2 localUV,highp float iLocalTime,highp float localPower , int type)
{
    highp vec2 uv = localUV.xy;
    
    highp vec2 muv1 = vec2(floor(uv.x * 2.0) / 2.0, floor(uv.y * 10.0) / 10.0) + iLocalTime * 0.01;
    highp vec2 muv2 = vec2(floor(uv.x * 4.0) / 4.0, floor(uv.y * 16.0) / 16.0) + iLocalTime * 0.98;
    highp vec2 muv3 = vec2(floor(uv.x * 8.0) / 10.0, floor(uv.y * 14.0) / 14.0) + iLocalTime * 0.5;
    
    highp float noise1 = step(0.8, snoiseGlitcher(vec3(muv1 * 4.0, 1.0)));
    highp float noise2 = step(0.8, snoiseGlitcher(vec3(muv2 * 4.0, 1.0)));
    highp float noise3 = step(0.98, snoiseGlitcher(vec3(muv3 * 6.0, 1.0)));
   
    if (type == 1) {
        
        noise1 = step(0.7, snoiseGlitcher(vec3(muv1 * 4.0, 1.0)));
        noise2 = step(0.7, snoiseGlitcher(vec3(muv2 * 4.0, 1.0)));
        noise3 = step(0.9, snoiseGlitcher(vec3(muv3 * 6.0, 1.0)));
    }
    
    if (type == 2) {
        
        noise1 = step(0.7, snoiseGlitcher(vec3(muv1 * 4.0, 1.0)));
        noise2 = step(0.6, snoiseGlitcher(vec3(muv2 * 4.0, 1.0)));
        noise3 = step(0.7, snoiseGlitcher(vec3(muv3 * 6.0, 1.0)));
    }
    
    highp float mergeNoise = noise1 + noise2 + noise3;
    
    highp vec2 mergeUv = uv + mergeNoise * 0.1;
    highp vec4 textur = vec4(texture2D(sampler, mergeUv));
    
    return textur;
}

// ******************************* Glitch TANWAVE *******************************
highp vec4 GlitchTan(highp vec4 color,sampler2D sampler,highp vec2 localUV,highp float iLocalTime,highp float localPower , int type)
{
    highp float power = 0.06;
    highp float verticalSpread = 2.0;
    highp float animationSpeed = 0.1;

    vec2 uv = localUV.xy;
    float y = (uv.y + iLocalTime * animationSpeed) * verticalSpread;

    uv.x += (
             cos(y)
             + sin(y * 10.0) * 0.2
             + sin(y * 50.0) * 0.03
             )
    * power  * sin(uv.y * 3.14) * tan(iLocalTime);

    return texture2D(sampler, uv);

}

// ******************************* GlitchEffects *******************************
highp vec4 GlitchEffects(sampler2D sampler,highp vec2 localUV,highp float iLocalTime,highp float localPower , int type)
{
    highp vec4 finalColor = texture2D(sampler,localUV);
    
    if(type == 1) {
        finalColor = GlitchRGBEffects(finalColor,sampler,localUV,iLocalTime,localPower,1);
    }
    else if(type == 2) {
        finalColor = GlitchRGBEffects(finalColor,sampler,localUV,iLocalTime,localPower,2);
    }
    else if(type == 3) {
        finalColor = GlitchRGBEffects(finalColor,sampler,localUV,iLocalTime,localPower,3);
    }
    else if(type == 4) {
        finalColor = NoiseGlitch(finalColor,sampler,localUV,iLocalTime,localPower ,1);
        finalColor = ColorNoise(finalColor,sampler,localUV,iLocalTime);
    }
    else if(type == 5) {
        finalColor = GlitchPixel(sampler,localUV,iLocalTime/2.0,0.5);
    }
    else if(type == 6) {
        finalColor = RandomZoomBlur(sampler,localUV,iLocalTime,1,localPower);
        finalColor = ColorNoise(finalColor,sampler,localUV,iLocalTime);
    }
    else if(type == 7) {
        finalColor = ChiccoGlitch(finalColor,sampler,localUV,iLocalTime,localPower,1);
    }
    else if(type == 8) {
        finalColor = GlitchImage(finalColor,sampler,localUV,iLocalTime,localPower,1);
    }
    else if(type == 9) {
        finalColor = GlitchImage(finalColor,sampler,localUV,iLocalTime,localPower,2);
    }
    else if(type == 10) {
        finalColor = GlitchImage(finalColor,sampler,localUV,iLocalTime,localPower,3);
    }
    else if(type == 11) {
        finalColor = GlitchTan(finalColor,sampler,localUV,iLocalTime,localPower,3);
    }
    else if(type == 12) {
        finalColor = GlitchNoise2(finalColor,sampler,localUV,iLocalTime,localPower,1);
    }
    else if(type == 13) {
        finalColor = GlitchNoise2(finalColor,sampler,localUV,iLocalTime,localPower,2);
    }
    
    return finalColor;
}

// ******************************* BlackNWhiteEffects *******************************
highp vec4 BlackNWhiteEffects(sampler2D sampler,highp vec2 localUV,highp float iLocalTime,highp float localPower , int type)
{
    highp vec4 finalColor = texture2D(sampler,localUV);
    
    if (type == 1) {
        highp vec3 c_r = vec3(0.1, 0.7, 0.1);
        highp vec3 c_g = vec3(0.1, 0.7, 0.1);
        highp vec3 c_b = vec3(0.1, 0.7, 0.1);
        
        highp vec3 rgb = vec3( dot(finalColor.rgb,c_r), dot(finalColor.rgb,c_g), dot(finalColor.rgb,c_b) );
        finalColor = vec4( rgb, 1.0 );
    }
    
    if(type == 2) {
        highp vec3 c_r = vec3(0.1, 0.8, 0.1);
        highp vec3 c_g = vec3(0.1, 0.8, 0.1);
        highp vec3 c_b = vec3(0.1, 0.8, 0.1);
        
        highp vec3 rgb = vec3( dot(finalColor.rgb,c_r), dot(finalColor.rgb,c_g), dot(finalColor.rgb,c_b) );
        finalColor = vec4( rgb, 1.0 );
    }
    
    if (type == 3) {
        highp vec3 c_r = vec3(0.5, 0.7, 0.1);
        highp vec3 c_g = vec3(0.5, 0.7, 0.1);
        highp vec3 c_b = vec3(0.5, 0.7, 0.1);
        
        highp vec3 rgb = vec3( dot(finalColor.rgb,c_r), dot(finalColor.rgb,c_g), dot(finalColor.rgb,c_b) );
        finalColor = vec4( rgb, 1.0 );
    }
    
    if (type == 4) {
        highp vec3 c_r = vec3(0.6, 0.8, 0.2);
        highp vec3 c_g = vec3(0.6, 0.8, 0.2);
        highp vec3 c_b = vec3(0.6, 0.8, 0.2);
        
        highp vec3 rgb = vec3( dot(finalColor.rgb,c_r), dot(finalColor.rgb,c_g), dot(finalColor.rgb,c_b) );
        finalColor = vec4( rgb, 1.0 );
    }
    
    if (type == 5) {
        highp vec3 c_r = vec3(0.2, 0.6, 0.4);
        highp vec3 c_g = vec3(0.2, 0.6, 0.4);
        highp vec3 c_b = vec3(0.2, 0.6, 0.4);
        
        highp vec3 rgb = vec3( dot(finalColor.rgb,c_r), dot(finalColor.rgb,c_g), dot(finalColor.rgb,c_b) );
        finalColor = vec4( rgb, 1.0 );
        
        highp float scanline = sin(localUV.y*400.0)*0.08;
        finalColor -= scanline;
    }
    
    
    if (type == 6) {
        highp vec3 c_r = vec3(0.8, 0.5, 0.1);
        highp vec3 c_g = vec3(0.8, 0.5, 0.1);
        highp vec3 c_b = vec3(0.8, 0.5, 0.1);
        
        highp vec3 rgb = vec3( dot(finalColor.rgb,c_r), dot(finalColor.rgb,c_g), dot(finalColor.rgb,c_b) );
        finalColor = vec4( rgb, 1.0 );
    }
    
    if (type == 7) {
        highp vec3 c_r = vec3(0.1, 0.7, 0.1);
        highp vec3 c_g = vec3(0.1, 0.7, 0.1);
        highp vec3 c_b = vec3(0.1, 0.7, 0.1);
        
        highp vec3 rgb = vec3( dot(finalColor.rgb,c_r), dot(finalColor.rgb,c_g), dot(finalColor.rgb,c_b) );
        
        finalColor = vec4( rgb, 1.0 );
        finalColor = SmallPixelNoiseEffect(finalColor,localUV,iLocalTime,2);
    }
    
    if (type == 8) {
        highp vec3 c_r = vec3(0.5, 0.7, 0.1);
        highp vec3 c_g = vec3(0.5, 0.7, 0.1);
        highp vec3 c_b = vec3(0.5, 0.7, 0.1);
        
        highp vec3 rgb = vec3( dot(finalColor.rgb,c_r), dot(finalColor.rgb,c_g), dot(finalColor.rgb,c_b) );
        finalColor = vec4( rgb, 1.0 );
        
        highp float scanline = sin(localUV.y*400.0)*0.08;
        finalColor -= scanline;
        
        finalColor = SmallPixelNoiseEffect(finalColor,localUV,iLocalTime,0);
    }
    
    if (type == 9) {
        highp vec3 c_r = vec3(0.6, 0.8, 0.2);
        highp vec3 c_g = vec3(0.6, 0.8, 0.2);
        highp vec3 c_b = vec3(0.6, 0.8, 0.2);
        
        highp vec3 rgb = vec3( dot(finalColor.rgb,c_r), dot(finalColor.rgb,c_g), dot(finalColor.rgb,c_b) );
        finalColor = vec4( rgb, 1.0 );
        
        finalColor = SmallPixelNoiseEffect(finalColor,localUV,iLocalTime,0);
    }
    
    if (type == 10) {
        highp vec3 c_r = vec3(0.2, 0.6, 0.4);
        highp vec3 c_g = vec3(0.2, 0.6, 0.4);
        highp vec3 c_b = vec3(0.2, 0.6, 0.4);
        
        highp vec3 rgb = vec3( dot(finalColor.rgb,c_r), dot(finalColor.rgb,c_g), dot(finalColor.rgb,c_b) );
        finalColor = vec4( rgb, 1.0 );
        
        finalColor = SmallPixelNoiseEffect(finalColor,localUV,iLocalTime,0);
    }
    
    if (type == 11) {
        highp vec3 c_r = vec3(0.8, 0.5, 0.1);
        highp vec3 c_g = vec3(0.8, 0.5, 0.1);
        highp vec3 c_b = vec3(0.8, 0.5, 0.1);
        
        highp vec3 rgb = vec3( dot(finalColor.rgb,c_r), dot(finalColor.rgb,c_g), dot(finalColor.rgb,c_b) );
        finalColor = vec4( rgb, 1.0 );
        
        finalColor = SmallPixelNoiseEffect(finalColor,localUV,iLocalTime,1);
    }
    
    if (type == 12) {
        highp vec3 c_r = vec3(0.4, 0.6, 0.8);
        highp vec3 c_g = vec3(0.4, 0.6, 0.8);
        highp vec3 c_b = vec3(0.4, 0.6, 0.8);
        
        highp vec3 rgb = vec3( dot(finalColor.rgb,c_r), dot(finalColor.rgb,c_g), dot(finalColor.rgb,c_b) );
        finalColor = vec4( rgb, 1.0 );
        
        finalColor = SmallPixelNoiseEffect(finalColor,localUV,iLocalTime,1);
    }
    
    if (type == 13) {
        highp vec3 c_r = vec3(0.4, 0.6, 0.1);
        highp vec3 c_g = vec3(0.4, 0.6, 0.1);
        highp vec3 c_b = vec3(0.4, 0.6, 0.1);
        
        highp vec3 rgb = vec3( dot(finalColor.rgb,c_r), dot(finalColor.rgb,c_g), dot(finalColor.rgb,c_b) );
        finalColor = vec4( rgb, 1.0 );
        
        highp float scanline = sin(localUV.y*400.0)*0.08;
        finalColor -= scanline;
        
        finalColor = SmallPixelNoiseEffect(finalColor,localUV,iLocalTime,1);
    }
    
    return finalColor;
    
}


// ******************************* Flasher *******************************
highp vec4 Flasher(highp vec4 color,sampler2D sampler,highp vec2 localUV,highp float iLocalTime,highp float localPower , int type)
{
    highp vec4 finalColor = color;
    
    highp float value = .5 + atan(sin(iLocalTime) / .1)/3.;
    highp float shake = sin(value*10.)/100.;
    highp vec2 shift = vec2(shake, 0.);
    finalColor = texture2D(sampler, localUV) * (1. - value) - texture2D(sampler, localUV - shift) * abs(shake) * 50. * (1. - value) + texture2D(sampler, localUV) * value - texture2D(sampler, localUV - shift) * abs(shake) * 50. * value;
    
    return finalColor;
}

// ******************************* MirrorSides *******************************
highp vec4 MirrorSides(highp vec4 color,sampler2D sampler,highp vec2 localUV,highp float iLocalTime,highp float localPower , int type)
{
    highp vec4 finalColor = color;
    
    if (type==0) {
        localUV.x -= step(0.5, localUV.x) * (localUV.x-0.5) * 2.0;
        finalColor = texture2D(sampler, localUV);
    }
    
    if (type==1) {
        localUV.x += step(localUV.x, 0.5) * (0.5-localUV.x) * 2.0;
        finalColor = texture2D(sampler, localUV);
    }
    
    if (type==2) {
        localUV.y -= step(localUV.y, 0.5) * (localUV.y-.5) * 2.0;
        finalColor = texture2D(sampler, localUV);
    }
    
    if (type==3) {
        localUV.y += step(0.5, localUV.y) * (0.5-localUV.y) * 2.0;
        finalColor = texture2D(sampler, localUV);
    }
    
    if (type==4) {
        localUV.x += step(localUV.x, 0.5) * (0.5-localUV.x) * 2.0;
        localUV.y += step(0.5, localUV.y) * (0.5-localUV.y) * 2.0;
        finalColor = texture2D(sampler, localUV);
    }
    
    if (type==5) {
        localUV = localUV - 0.5;
        localUV *= 2. + sin(iLocalTime);
        localUV = fract(localUV);
        localUV = abs((localUV - 0.5) * 2.);
        finalColor = texture2D(sampler, localUV);
    }
    
    return finalColor;
    
}

// ******************************* FrameMirror *******************************
highp vec4 FrameMirror(highp vec4 color,sampler2D sampler,highp vec2 localUV,highp float iLocalTime,highp float localPower , int type)
{
    highp vec4 finalColor = color;
    
    if (type==1) {
        float div = step(0.5, (localUV.y));
        if (div == 1.0){
            localUV.y -=  0.2;
        } else{
            localUV.y += 0.2;
        }
        finalColor = texture2D(sampler, localUV);
    }
    
    if (type==2) {
        if(localUV.y <= 0.33) {
            localUV.y +=  0.2;
        } else if(localUV.y <= 0.66) {
            
        } else {
            localUV.y -=  0.2;
        }
        finalColor = texture2D(sampler, localUV);
    }
    
    if (type==3) {
        localUV = localUV - 0.5;
        localUV *= 2.;
        localUV = fract(localUV);
        finalColor = texture2D(sampler, localUV);
    }
    
    if (type==4) {
        localUV = localUV;
        localUV *= 3.;
        localUV = fract(localUV);
        finalColor = texture2D(sampler, localUV);
    }
    
    if (type==5) {
        localUV = localUV - 0.5;
        localUV *= 4.;
        localUV = fract(localUV);
        finalColor = texture2D(sampler, localUV);
    }
    
     return finalColor;
}

// ******************************* ViewPort *******************************
#define F_PI (3.14152965359)
highp float gray(highp vec3 c ) {
    return 0.4 * c.x + 0.587 * c.y + 0.114 * c.z;
}

highp vec4 ViewPort(highp vec4 color,sampler2D sampler,highp vec2 localUV,highp float iLocalTime,highp float localPower , int type)
{
    highp float borderOfLife  = 1.0;
    highp float colorPower    = 0.6;
    highp float scrollTween   = iLocalTime;
    highp float zoomDist      = 10.0;
    highp float zoomToCenter  = 0.8;
    
    highp float portParam     = 1.0;
    highp float singularity   = 0.3;

    highp vec2 uv = localUV;
    uv = mod((uv - 0.5) * zoomToCenter + 0.5, 1.0);
    uv = (2.0*uv-1.0) * singularity;

    highp float quadrature = atan(uv.y, uv.x);
    highp float fromRadius = sqrt(uv.x*uv.x + uv.y*uv.y);

    bool hDom = false;
    if (abs(uv.x) > abs(uv.y))
    {
        hDom = true;
    }
    
    highp float signer = float(uv.y + uv.x > 0.0) * 2.0 - 1.0;

    highp vec2 pq;
    if (hDom)
    {
        pq.x = fromRadius;
        pq.y = fromRadius * tan(quadrature);
    }
    else
    {
        pq.y = fromRadius ;
        pq.x = fromRadius / tan(quadrature);
    }
    pq = pq * signer;

    highp float reps = portParam + zoomDist;
    highp float ease = pow(fromRadius, 2.0);
    highp vec2 mn = uv;

    highp vec2 uvView = pq*ease + mn*(1.0-ease)*reps;
    highp vec2 uvScroll = uvView + vec2(0.5, 0.5 + scrollTween);
    uvView = mod(uvScroll, 1.0);

    highp float reps2 = 12.0;
    highp float scrambler = sqrt(12.0);
    highp vec2 uvIndex = floor(uvScroll);
    highp float indexView = scrambler * (uvIndex.x*reps2 + uvIndex.y);
    highp vec3 col = vec3(mod(0.5+indexView/11.0,1.0), mod(indexView/5.0,1.0), mod(indexView/7.0,1.0));
    highp vec4 tex = texture2D(sampler, uvView);

    col = col / max(max(col.x, col.y), col.z);
    highp vec3 colBlend = 1.0 - (1.0 - col*colorPower)*(1.0 - tex.xyz);
    highp float borderSetter = pow(abs(2.0*uvView.x-1.0), 6.0) + pow(abs(2.0*uvView.y-1.0), 6.0);
    highp vec4 fragColor = vec4(colBlend * clamp(1.0-borderSetter*borderOfLife,0.0,1.0),1.0);
    return fragColor;
}

// ******************************* MirrorEffects *******************************
highp vec4 MirrorEffects(sampler2D sampler,highp vec2 localUV,highp float iLocalTime,highp float localPower , int type)
{
    highp vec4 finalColor = texture2D(sampler,localUV);
    
    if (type == 1) {
        finalColor = Flasher(finalColor,sampler,localUV,iLocalTime,localPower,0);
    }
    
    if (type == 2) {
        finalColor = MirrorSides(finalColor,sampler,localUV,iLocalTime,localPower,0);
    }
    
    if (type == 3) {
        finalColor = MirrorSides(finalColor,sampler,localUV,iLocalTime,localPower,1);
    }
    
    if (type == 4) {
        finalColor = MirrorSides(finalColor,sampler,localUV,iLocalTime,localPower,2);
    }
    
    if (type == 5) {
        finalColor = MirrorSides(finalColor,sampler,localUV,iLocalTime,localPower,3);
    }
    
    if (type == 6) {
        finalColor = MirrorSides(finalColor,sampler,localUV,iLocalTime,localPower,4);
    }
    
    if (type == 7) {
        finalColor = ViewPort(finalColor,sampler,localUV,iLocalTime,localPower,0);
    }
    
    if (type == 8) {
        finalColor = FrameMirror(finalColor,sampler,localUV,iLocalTime,localPower,1);
    }
    
    if (type == 9) {
        finalColor = FrameMirror(finalColor,sampler,localUV,iLocalTime,localPower,2);
    }
    
    if (type == 10) {
        finalColor = FrameMirror(finalColor,sampler,localUV,iLocalTime,localPower,3);
    }
    
    if (type == 11) {
        finalColor = FrameMirror(finalColor,sampler,localUV,iLocalTime,localPower,4);
    }
    
    if (type == 12) {
        finalColor = FrameMirror(finalColor,sampler,localUV,iLocalTime,localPower,5);
    }

    if (type == 13) {
        finalColor = MirrorSides(finalColor,sampler,localUV,iLocalTime,localPower,5);
    }

    return finalColor;
    
}

// ******************************* VintageEffects *******************************
highp vec4 VintageEffects(sampler2D sampler,highp vec2 localUV,highp float iLocalTime,highp float localPower , int type)
{
    
    highp vec4 finalColor = texture2D(sampler,localUV);
    
    vec3 c_r = vec3(0.6, 0.5, 0.1);
    vec3 c_g = vec3(0.5, 0.6, 0.1);
    vec3 c_b = vec3(0.5, 0.5, 0.6);
    
    if(type == 2) {
        c_r = vec3(0.5, 0.6, 0.4);
        c_g = vec3(0.5, 0.6, 0.1);
        c_b = vec3(0.5, 0.6, 0.1);
    }
    
    if(type == 3) {
        c_r = vec3(0.5, 0.6, 0.1);
        c_g = vec3(0.8, 0.5, 0.2);
        c_b = vec3(0.5, 0.5, 0.1);
    }
    
    if(type == 4) {
        c_r = vec3(0.5, 0.6, 0.1);
        c_g = vec3(0.8, 0.5, 0.2);
        c_b = vec3(0.8, 0.5, 0.2);
    }
    
    if(type == 5) {
        c_r = vec3(0.2, 0.2, 0.2);
        c_g = vec3(0.5, 0.4, 0.3);
        c_b = vec3(0.1, 0.5, 0.2);
    }
    
    if(type == 6) {
        c_r = vec3(0.5, 0.5, 0.2);
        c_g = vec3(0.5, 0.4, 0.3);
        c_b = vec3(0.1, 0.5, 0.2);
    }
    
    if(type == 7) {
        c_r = vec3(0.7, 0.9, 0.5);
        c_g = vec3(0.5, 0.5, 0.5);
        c_b = vec3(0.2, 0.5, 0.4);
    }
    
    if(type == 8) {
        c_r = vec3(0.7, 0.1, 0.0);
        c_g = vec3(1.0, 0.1, 0.0);
        c_b = vec3(0.3, 0.6, 0.3);
    }
    
    if(type == 9) {
        c_r = vec3(0.4, 0.2, 0.0);
        c_g = vec3(1.0, 0.1, 0.3);
        c_b = vec3(0.3, 0.3, 0.3);
    }
    
    if(type == 10) {
        c_r = vec3(0.7, 0.6, 0.7);
        c_g = vec3(0.5, 0.7, 0.3);
        c_b = vec3(0.6, 0.5, 0.4);
    }
    
    if(type == 11) {
        c_r = vec3(0.1, 0.5, 0.5);
        c_g = vec3(0.6, 0.5, 0.8);
        c_b = vec3(0.5, 0.6, 0.4);
    }
    
    if(type == 12) {
        c_r = vec3(0.6, 0.6, 0.4);
        c_g = vec3(0.5, 0.5, 0.5);
        c_b = vec3(0.5, 0.5, 0.4);
    }
    
    if(type == 13) {
        c_r = vec3(0.2, 0.2, 0.6);
        c_g = vec3(0.3, 0.3, 0.4);
        c_b = vec3(0.1, 0.1, 0.8);
    }
    
    vec3 rgb = vec3( dot(finalColor.rgb,c_r), dot(finalColor.rgb,c_g), dot(finalColor.rgb,c_b) );
    
    finalColor = vec4( rgb, 1.0 );
    
    return finalColor;
}

//uv - textureCoordinate
//sampler - inputImageTexture
void main()
{
    
    highp vec4 inputColor = texture2D(inputImageTexture, textureCoordinate);
    highp vec2 newCoordinate =  textureCoordinate;
    
    if(category == 0)
    {
        inputColor = texture2D(inputImageTexture, newCoordinate);
    }
    else if (category == 1) //VHS Effects
    {
        inputColor = VHSEffects(inputImageTexture,newCoordinate,MainTime,power,type);
    }
    else if (category == 2) //Glitch Effects
    {
        inputColor = GlitchEffects(inputImageTexture,newCoordinate,MainTime,power,type);
    }
    else if (category == 3) //Vintage Effects
    {
        if (type != 0) {
            inputColor = VintageEffects(inputImageTexture,newCoordinate,MainTime,power,type);
        }
    }
    else if (category == 4) //Black N White Effects
    {
        inputColor = BlackNWhiteEffects(inputImageTexture,newCoordinate,MainTime,power,type);
    }
    else if (category == 5) //Mirror Effects
    {
        inputColor = MirrorEffects(inputImageTexture,newCoordinate,MainTime,power,type);
    }
    
    
    gl_FragColor = inputColor;
  
}
