

import UIKit
import AVKit
import MediaPlayer

protocol RingtoneListVCDelegage : class{
    func didSelectLocalMusic(audioModel: AudioModel)
}

class RingtoneListVC: UIViewController , UITableViewDataSource, UITableViewDelegate, RingtoneListCellDelegate{
    
    //MARK:- Outlets
    
    @IBOutlet weak var listTableView: UITableView!
    @IBOutlet weak var lblTitle: UILabel!
    
    
    //MARK:- Properties
    var titleStr = ""
    var soundsResult = [AnyHashable: Any]()
    var actInd: UIActivityIndicatorView!
    var isPurchaseDone = false
    weak var delegate: RingtoneListVCDelegage?
    
    
    var localMusicArray = [AudioModel]()
    
    //MARK:- Lifecycle methods
    
    @IBAction func backButtonAction(_ sender: UIButton) {
        
        self.dismiss(animated: true, completion: nil)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        lblTitle.text = titleStr
        self.navigationController?.navigationBar.isHidden = false
        self.tabBarController?.tabBar.isHidden = true
        self.tabBarController?.tabBar.isTranslucent = true
        loadLibraryArray()
    }
    
    func loadLibraryArray()  {
        let query = MPMediaQuery.songs()
        query.addFilterPredicate(MPMediaPropertyPredicate(value: 0, forProperty: MPMediaItemPropertyIsCloudItem))
        var albumArray = query.collections
        if(albumArray == nil){
            return;
        }
        localMusicArray.removeAll()
        DispatchQueue.main.async {
            self.listTableView.reloadData()
        }
        for i in 0..<albumArray!.count {
            let music = AudioModel()
            music.type = "LibrarySong"
            
            let currentCollction = albumArray![i]
            music.title = currentCollction.representativeItem?.title
            let mItem = query.items![i]
            let artWork = mItem.value(forProperty: MPMediaItemPropertyArtwork) as? MPMediaItemArtwork
            if artWork == nil {
                music.artWork = UIImage(named: "itunes")
            } else {
                music.artWork = artWork?.image(at: CGSize(width: 100, height: 100))
            }
            music.type = "Library"
            var selectedSong = currentCollction.items
            if (selectedSong.count ) > 0 {
                let songItem: MPMediaItem? = selectedSong[0]
                let pathURL = songItem?.value(forProperty: MPMediaItemPropertyAssetURL) as? URL
                if pathURL != nil {
                    music.audioUrl = pathURL
                }
                music.duration = CGFloat(selectedSong[0].playbackDuration)
            }
            localMusicArray.append(music)
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                self.listTableView.reloadData()
            }
        }
       
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
    }
    
    override func viewWillLayoutSubviews() {
        listTableView.reloadData()
    }

    override func viewWillDisappear(_ animated: Bool) {

        
    }
    
    //=====================================================================================
    //MARK:- Table View Methods

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return localMusicArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let sound = localMusicArray[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "RingtoneListCell") as! RingtoneListCell
        cell.setupData(with: sound)
        cell.indexPath = indexPath
        cell.delegate = self
        return cell
    }
    
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        if isPurchaseDone{
            return nil
        }
        return indexPath
    }
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

    }
    
    //=====================================================================================
    //MARK:- Private Methods======================================================================

    //MARK:- Cell delegate method
    func buttonClicked(at indexPath: IndexPath){
            onDownloadClicked(at: indexPath)
    }
    
    
    
    //=====================================================================================
    
    

    func onDownloadClicked(at indexPath: IndexPath){
        
        let songModel = localMusicArray[indexPath.row]
        delegate?.didSelectLocalMusic(audioModel: songModel)
        self.dismiss(animated: true, completion: nil)
    }
 
 
   

}
