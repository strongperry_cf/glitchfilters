
import UIKit

class Utility: NSObject {
    
    
    static func showAlertViewController(title: String, message: String, btnTitle1: String, ok handler: ((UIAlertAction) -> Void)?, viewController: UIViewController){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: btnTitle1, style: .default, handler: handler));
        viewController.present(alert, animated: true, completion: nil);
    }
    
    //===========================================================================================
    static func showAlertViewController(title: String, message: String, viewController: UIViewController){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil));
        viewController.present(alert, animated: true, completion: nil);
    }
    
    static func showAlertViewController(title: String, message: String, buttonTitle: String, viewController: UIViewController){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: buttonTitle, style: .cancel, handler: nil));
        viewController.present(alert, animated: true, completion: nil);
    }
    
    static func showAlertViewController(title: String, message: String, btnTitle1: String, btnTitle2: String, ok handler: ((UIAlertAction) -> Void)?,cancel handler1: ((UIAlertAction) -> Void)?, viewController: UIViewController){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: btnTitle1, style: .default, handler: handler1));
        var action2: UIAlertAction!
        if btnTitle2 == "Delete"{
            action2 = UIAlertAction(title: btnTitle2, style: .destructive, handler: handler)
        }else{
            action2 = UIAlertAction(title: btnTitle2, style: .default, handler: handler)
        }
        alert.addAction(action2);
        viewController.present(alert, animated: true, completion: nil);
    }
    
    static func setGradient(_ view:UIView) {
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.colors = [UIColor.init(red: 255/255.0, green: 56/255.0, blue: 127/255.0, alpha: 1.0).cgColor, UIColor.init(red: 255/255.0, green: 136/255.0, blue: 109/255.0, alpha: 1.0).cgColor]
        gradient.locations = [0.0 , 1.0]
        gradient.startPoint = CGPoint(x: 0.0, y: 1.0)
        gradient.endPoint = CGPoint(x: 1.0, y: 1.0)
        gradient.frame = CGRect(x: 0.0, y: 0.0, width: view.frame.size.width, height: view.frame.size.height)
        view.layer.insertSublayer(gradient, at: 0)
    }
}
