
import UIKit
import AVFoundation


struct VideoModel{
    var composition: AVMutableComposition!
    var videoComposition: AVVideoComposition!
    var timeRange: CMTimeRange!
    var asset: AVAsset!
    var rotationAngle: CGFloat = 0.0
}


class VideoUtility: NSObject {
    
    
    public static let shared = VideoUtility()
    
    
    func getComposition(videoAsset: AVAsset,from startTime: CGFloat,to endTime: CGFloat) -> VideoModel?{
        // Get video track
        let asset = videoAsset
        
        guard let videoTrack = asset.tracks(withMediaType: AVMediaType.video).first else {
            return nil
        }
        
        var audioTrack: AVAssetTrack?
        if let aTrack = asset.tracks(withMediaType: AVMediaType.audio).first{
            audioTrack = aTrack
        }
        // 1 - Create AVMutableComposition object. This object will hold your AVMutableCompositionTrack instances.
        
        
        let mixComposition = AVMutableComposition()
        let sTime = CMTime(seconds: Double(startTime), preferredTimescale: videoAsset.duration.timescale)
        let eTime = CMTime(seconds: Double(endTime), preferredTimescale: videoAsset.duration.timescale)
        let timeRange = CMTimeRange(start: sTime, end: eTime)
        
        
        // Init video & audio composition track
        let videoCompositionTrack = mixComposition.addMutableTrack(withMediaType: AVMediaType.video,
                                                                   preferredTrackID: Int32(kCMPersistentTrackID_Invalid))
        
        let audioCompositionTrack = mixComposition.addMutableTrack(withMediaType: .audio, preferredTrackID: Int32(kCMPersistentTrackID_Invalid))
        
        do {
            // ***** 1
            // Add video track to video composition at specific time
            try videoCompositionTrack?.insertTimeRange(timeRange, of: videoTrack, at: .zero)
            // ***** 2
            // Add video track to video composition at specific time
            if audioTrack != nil{
                try audioCompositionTrack?.insertTimeRange(timeRange, of: audioTrack!, at: .zero)
            }
            
        } catch {
            print("Load track error")
        }
        var videoSize = videoTrack.naturalSize
        videoSize = videoSize.applying(videoTrack.preferredTransform)
        videoSize = CGSize(width: abs(videoSize.width), height: abs(videoSize.height))
        let frontLayerInstruction = AVMutableVideoCompositionLayerInstruction(assetTrack: videoCompositionTrack!)
        
        //for Time-lapse videos
        let (transform,  _) = correctTransform(asset: asset, standardSize: videoSize)
        frontLayerInstruction.setTransform(transform, at: .zero)
        
        
        let videoComposition = AVMutableVideoComposition()
        videoComposition.frameDuration = CMTimeMake(value: 1, timescale: 30)
        videoComposition.renderScale  = 1.0
        videoComposition.renderSize = videoSize
        mixComposition.naturalSize = videoSize
        
        
        let mainInstruction = AVMutableVideoCompositionInstruction()
        mainInstruction.timeRange =  (videoCompositionTrack?.timeRange)!
        mainInstruction.layerInstructions = [frontLayerInstruction]
        videoComposition.instructions = [mainInstruction]
        
        var model = VideoModel()
        model.composition = mixComposition
        model.timeRange = timeRange
        model.videoComposition = videoComposition
        return model
    }
    
    
    func correctTransform(asset: AVAsset, standardSize:CGSize ) -> (CGAffineTransform, Bool) {
        
        let assetTrack = asset.tracks(withMediaType: AVMediaType.video)[0]
        let transform = assetTrack.preferredTransform
        var finalTransform = assetTrack.preferredTransform
        
        let assetInfo = orientation(from: transform)
        
        var aspectFillRatio:CGFloat = 1
        
        var videoSize = assetTrack.naturalSize;
        videoSize = videoSize.applying(assetTrack.preferredTransform)
        videoSize = CGSize(width: abs(videoSize.width), height: abs(videoSize.height))
        
        //  if videoSize.height > videoSize.width //Aspect Fit
        if videoSize.height < videoSize.width  //Aspect Fill
        {
            aspectFillRatio = standardSize.height / videoSize.height
        }
        else {
            aspectFillRatio = standardSize.width / videoSize.width
        }
        
        if assetInfo.isPortrait {
            let scaleFactor = CGAffineTransform(scaleX: aspectFillRatio, y: aspectFillRatio)
            
            let posX = standardSize.width/2 - (videoSize.width * aspectFillRatio)/2
            let posY = standardSize.height/2 - (videoSize.height * aspectFillRatio)/2
            let moveFactor = CGAffineTransform(translationX: posX, y: posY)
            finalTransform = assetTrack.preferredTransform.concatenating(scaleFactor).concatenating(moveFactor);
            
        } else {
            let scaleFactor = CGAffineTransform(scaleX: aspectFillRatio, y: aspectFillRatio)
            
            let posX = standardSize.width/2 - (videoSize.width * aspectFillRatio)/2
            let posY = standardSize.height/2 - (videoSize.height * aspectFillRatio)/2
            let moveFactor = CGAffineTransform(translationX: posX, y: posY)
            
            let concat = assetTrack.preferredTransform.concatenating(scaleFactor).concatenating(moveFactor)
            
            if assetInfo.orientation == .down {
                // let fixUpsideDown = CGAffineTransform(rotationAngle: CGFloat(Double.pi))
                //concat = fixUpsideDown.concatenating(scaleFactor).concatenating(moveFactor)
            }
            
            finalTransform = concat;
        }
        
        return (finalTransform,assetInfo.isPortrait) ;
    }
    
    
    func orientation(from transform: CGAffineTransform) -> (orientation: UIImage.Orientation, isPortrait: Bool) {
        var assetOrientation = UIImage.Orientation.up
        var isPortrait = false
        if transform.a == 0 && transform.b == 1.0 && transform.c == -1.0 && transform.d == 0 {
            assetOrientation = .right
            isPortrait = true
        } else if transform.a == 0 && transform.b == -1.0 && transform.c == 1.0 && transform.d == 0 {
            assetOrientation = .left
            isPortrait = true
        } else if transform.a == 1.0 && transform.b == 0 && transform.c == 0 && transform.d == 1.0 {
            assetOrientation = .up
        } else if transform.a == -1.0 && transform.b == 0 && transform.c == 0 && transform.d == -1.0 {
            assetOrientation = .down
        }
        return (assetOrientation, isPortrait)
    }
    
}
