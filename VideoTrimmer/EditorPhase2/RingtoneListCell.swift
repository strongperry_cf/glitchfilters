

import UIKit

public enum ListTasks :Int {
    case play = 0,
    pause,
    share,
    download
};


protocol RingtoneListCellDelegate : class{
    func buttonClicked(at indexPath: IndexPath)
}

class RingtoneListCell: UITableViewCell {

    
    @IBOutlet weak var btnDownload: UIButton!
    @IBOutlet weak var btnPlayPause: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var lblUserName: UILabel!
    
    weak var delegate: RingtoneListCellDelegate?
    var indexPath:IndexPath!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }


    @IBAction func actionDownload(_ sender: UIButton) {
        delegate?.buttonClicked(at: self.indexPath)
    }
    
    
    func setupData(with model: AudioModel){
        lblTitle.text = model.title
        lblUserName.text = ""
        btnPlayPause.setImage(model.artWork, for: .normal)
    }
    
}
