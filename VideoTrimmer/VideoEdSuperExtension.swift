
//  SuperExtension.swift
//  VideoTrimmer
//
//  Created by strongapps on 03/04/19.
//  Copyright © 2019 strongapps. All rights reserved.
//

import UIKit
import StoreKit
import AVFoundation
extension UIApplication {
    class func topViewController(controller: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let navigationController = controller as? UINavigationController {
            return topViewController(controller: navigationController.visibleViewController)
        }
        if let tabController = controller as? UITabBarController {
            if let selected = tabController.selectedViewController {
                return topViewController(controller: selected)
            }
        }
        if let presented = controller?.presentedViewController {
            return topViewController(controller: presented)
        }
        return controller
    }
}
extension UIBezierPath {
    convenience init(heartIn rect: CGRect) {
        self.init()
        
        //Calculate Radius of Arcs using Pythagoras
        let sideOne = rect.width * 0.4
        let sideTwo = rect.height * 0.3
        let arcRadius = sqrt(sideOne*sideOne + sideTwo*sideTwo)/2
        
        //Left Hand Curve
        self.addArc(withCenter: CGPoint(x: rect.width * 0.3, y: rect.height * 0.35), radius: arcRadius, startAngle: 135.degreesToRadians, endAngle: 315.degreesToRadians, clockwise: true)
        
        //Top Centre Dip
        self.addLine(to: CGPoint(x: rect.width/2, y: rect.height * 0.2))
        
        //Right Hand Curve
        self.addArc(withCenter: CGPoint(x: rect.width * 0.7, y: rect.height * 0.35), radius: arcRadius, startAngle: 225.degreesToRadians, endAngle: 45.degreesToRadians, clockwise: true)
        
        //Right Bottom Line
        self.addLine(to: CGPoint(x: rect.width * 0.5, y: rect.height * 0.95))
        self.fill()
        UIColor.yellow.setFill()
        //Left Bottom Line
        self.close()
    }
}

extension UIColor {
    static func randomColor() -> UIColor {
        return UIColor(red:   .random(),
                       green: .random(),
                       blue:  .random(),
                       alpha: 1.0)
    }
}

extension CGFloat {
    static func random() -> CGFloat {
        return CGFloat(arc4random()) / CGFloat(UInt32.max)
    }
    func convertSecondsToDurationString() -> String {
        let total : Float = Float(self)
        let totalMins : Int = Int(total / 60)
        let remainSeconds : Int = Int(total.truncatingRemainder(dividingBy: 60))
        let formatedString = NSString(format:"%02d:%02d",totalMins,remainSeconds)
        
        return formatedString as String
        
    }
    func convertSecondsToDurationString2() -> String {
        var total : Float = Float(self)
        if total < 0{
            total = 0
        }
        
        let totalMins : Int = Int(total / 60)
        let remainSeconds : Int = Int(total.truncatingRemainder(dividingBy: 60))
        let miliSeconds = UInt((Float(total - Float(Int(total))))*10)
        let formatedString1 = NSString(format:"%f",miliSeconds).character(at: 0)
        let formatedString = NSString(format:"%02d:%02d.%01d",totalMins,remainSeconds,miliSeconds)
        print(formatedString)
        return formatedString as String
        
    }
    
    func cmTimeValue() -> CMTime{
        let timevalue = CMTime(seconds: Double(self), preferredTimescale: 1000)
        return timevalue
    }
}
extension Double{
    func convertSecondsToDurationString() -> String {
        let total : Float = Float(self)
        let totalMins : Int = Int(total / 60)
        let remainSeconds : Int = Int(total.truncatingRemainder(dividingBy: 60))
        let formatedString = NSString(format:"%02d:%02d",totalMins,remainSeconds)
        return formatedString as String
        
    }
}
extension Int{
    func convertSecondsToDurationString() -> String {
        let total : Float = Float(self)
        let totalMins : Int = Int(total / 60)
        let remainSeconds : Int = Int(total.truncatingRemainder(dividingBy: 60))
        let formatedString = NSString(format:"%02d:%02d",totalMins,remainSeconds)
        return formatedString as String
        
    }
}
extension Float{
    func convertSecondsToDurationString() -> String {
        let total : Float = Float(self)
        let totalMins : Int = Int(total / 60)
        let remainSeconds : Int = Int(total.truncatingRemainder(dividingBy: 60))
        let formatedString = NSString(format:"%02d:%02d",totalMins,remainSeconds)
        return formatedString as String
        
    }
}
extension CGPoint {
    
    func scalePoint(scale: CGFloat) -> CGPoint {
        
        let originX =  self.x * scale
        let originY = self.y * scale
        return CGPoint(x: originX, y: originY)
        
    }
    
}
extension CGRect {
    
    func scaleRect(scale: CGFloat) -> CGRect {
        
        let originX =  self.minX * scale
        let originY = self.minY * scale
        let originW = self.width * scale
        let originH = self.height * scale
        return CGRect(x: originX, y: originY, width: originW, height: originH)
        
    }
    
}
extension UIImage {
    
    func blurImage() ->  UIImage{
        
        let inputImage = self

        let currentFilter = CIFilter(name: "CIGaussianBlur")
        let beginImage = CIImage(image: inputImage)
        currentFilter!.setValue(beginImage, forKey: kCIInputImageKey)
        currentFilter!.setValue(60, forKey: kCIInputRadiusKey)
        
        let cropFilter = CIFilter(name: "CICrop")
        cropFilter!.setValue(currentFilter!.outputImage, forKey: kCIInputImageKey)
        cropFilter!.setValue(CIVector(cgRect: beginImage!.extent), forKey: "inputRectangle")
        let context = CIContext(options: nil)
        
        let output = cropFilter!.outputImage
        let cgimg = context.createCGImage(output!, from: output!.extent)
        let processedImage = UIImage(cgImage: cgimg!)
        return processedImage
        
        
    }
    func grayImage()-> UIImage {
        let image = self
        // Create image rectangle with current image width/height
        let imageRect:CGRect = CGRect(x:0, y:0, width:image.size.width, height: image.size.height)
        
        // Grayscale color space
        let colorSpace = CGColorSpaceCreateDeviceGray()
        let width = image.size.width
        let height = image.size.height
        
        // Create bitmap content with current image size and grayscale colorspace
        let bitmapInfo = CGBitmapInfo(rawValue: CGImageAlphaInfo.none.rawValue)
        
        // Draw image into current context, with specified rectangle
        // using previously defined context (with grayscale colorspace)
        let context = CGContext(data: nil, width: Int(width), height: Int(height), bitsPerComponent: 8, bytesPerRow: 0, space: colorSpace, bitmapInfo: bitmapInfo.rawValue)
        context?.draw(image.cgImage!, in: imageRect)
        let imageRef = context!.makeImage()
        
        // Create a new UIImage object
        let newImage = UIImage(cgImage: imageRef!)
        
        return newImage
    }
    var noir: UIImage? {
        let context = CIContext(options: nil)
        guard let currentFilter = CIFilter(name: "CIPhotoEffectNoir") else { return nil }
        currentFilter.setValue(CIImage(image: self), forKey: kCIInputImageKey)
        if let output = currentFilter.outputImage,
            let cgImage = context.createCGImage(output, from: output.extent) {
            return UIImage(cgImage: cgImage, scale: scale, orientation: imageOrientation)
        }
        return nil
    }
    func imageByApplyingMaskingBezierPath(_ path: UIBezierPath, _ pathFrame: CGRect) -> UIImage {
        
        UIGraphicsBeginImageContext(self.size)
        let context = UIGraphicsGetCurrentContext()!
        context.addPath(path.cgPath)
        context.clip()
        draw(in: CGRect(x: 0, y: 0, width: self.size.width, height: self.size.height))
        let maskedImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        
        return maskedImage
    }
    func resizeImage(targetSize: CGSize) -> UIImage {
        
        let image = self
        let size = image.size
        
        let widthRatio  = targetSize.width  / size.width
        let heightRatio = targetSize.height / size.height
        
        // Figure out what our orientation is, and use that to form the rectangle
        var newSize: CGSize = targetSize
        if(widthRatio > heightRatio) {
            newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
        } else {
            newSize = CGSize(width: size.width * widthRatio,  height: size.height * widthRatio)
        }
        
        // This is the rect that we've calculated out and this is what is actually used below
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
        
        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        image.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    func squareimage() -> UIImage? {
        
        let image : UIImage? = self
        
        var newCropWidth: Double
        var newCropHeight: Double
        
        //=== To crop more efficently =====//
        if (image?.size.width ?? 0.0) < (image?.size.height ?? 0.0) {
            if (image?.size.width ?? 0.0) < size.width {
                newCropWidth = Double(size.width)
            } else {
                newCropWidth = Double(image?.size.width ?? 0.0)
            }
            newCropHeight = (newCropWidth * Double(size.height)) / Double(size.width)
        } else {
            if (image?.size.height ?? 0.0) < size.height {
                newCropHeight = Double(size.height)
            } else {
                newCropHeight = Double(image?.size.height ?? 0.0)
            }
            newCropWidth = (newCropHeight * Double(size.width)) / Double(size.height)
        }
        let x: Double = Double((image?.size.width)! / 2.0) - newCropWidth / 2.0
        let y: Double = Double((image?.size.height)! / 2.0) - newCropHeight / 2.0
        
        let cropRect = CGRect(x: Double(x), y: Double(y), width: newCropWidth, height: newCropHeight)
        let imageRef = image?.cgImage?.cropping(to: cropRect)
        
        var cropped: UIImage? = nil
        if let imageRef = imageRef {
            cropped = UIImage(cgImage: imageRef)
        }
        
        return cropped
    }
    func squareimage(size: CGSize) -> UIImage? {
        
        let image : UIImage? = self
        
        var newCropWidth: Double
        var newCropHeight: Double
        
        //=== To crop more efficently =====//
        if (image?.size.width ?? 0.0) < (image?.size.height ?? 0.0) {
            if (image?.size.width ?? 0.0) < size.width {
                newCropWidth = Double(size.width)
            } else {
                newCropWidth = Double(image?.size.width ?? 0.0)
            }
            newCropHeight = (newCropWidth * Double(size.height)) / Double(size.width)
        } else {
            if (image?.size.height ?? 0.0) < size.height {
                newCropHeight = Double(size.height)
            } else {
                newCropHeight = Double(image?.size.height ?? 0.0)
            }
            newCropWidth = (newCropHeight * Double(size.width)) / Double(size.height)
        }
        let x: Double = Double((image?.size.width)! / 2.0) - newCropWidth / 2.0
        let y: Double = Double((image?.size.height)! / 2.0) - newCropHeight / 2.0
        
        let cropRect = CGRect(x: Double(x), y: Double(y), width: newCropWidth, height: newCropHeight)
        let imageRef = image?.cgImage?.cropping(to: cropRect)
        
        var cropped: UIImage? = nil
        if let imageRef = imageRef {
            cropped = UIImage(cgImage: imageRef)
        }
        
        
        return cropped?.resizeImage(targetSize: size)
    }
}
extension CGPath {
    
    func resizeCGPath(Fitin frame : CGRect) -> CGPath{
        
        let path = self
        let boundingBox = path.boundingBox
        let boundingBoxAspectRatio = boundingBox.width / boundingBox.height
        let viewAspectRatio = frame.width  / frame.height
        var scaleFactor : CGFloat = 1.0
        if (boundingBoxAspectRatio > viewAspectRatio) {
            // Width is limiting factor
            
            scaleFactor = frame.width / boundingBox.width
        } else {
            // Height is limiting factor
            scaleFactor = frame.height / boundingBox.height
        }
        
        var scaleTransform = CGAffineTransform.identity
        scaleTransform = scaleTransform.scaledBy(x: scaleFactor, y: scaleFactor)
        scaleTransform.translatedBy(x: -boundingBox.minX, y: -boundingBox.minY)
        
        let scaledSize = boundingBox.size.applying(CGAffineTransform (scaleX: scaleFactor, y: scaleFactor))
        let centerOffset = CGSize(width: (frame.width - scaledSize.width ) / scaleFactor * 2.0, height: (frame.height - scaledSize.height) /  scaleFactor * 2.0 )
        scaleTransform = scaleTransform.translatedBy(x: centerOffset.width, y: centerOffset.height)
        let  scaledPath = path.copy(using: &scaleTransform)
        return scaledPath!
    }
}
extension FileManager {
    func removeItemIfExisted(_ url:URL) -> Void
    {
        if FileManager.default.fileExists(atPath: url.path) {
            do {
                try FileManager.default.removeItem(atPath: url.path)
            }
            catch {
                print("Failed to delete item")
            }}
    }
}
public extension UIDevice {
    

    static let modelNameString: String = {
        var systemInfo = utsname()
        uname(&systemInfo)
        let machineMirror = Mirror(reflecting: systemInfo.machine)
        let identifier = machineMirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8, value != 0 else { return identifier }
            return identifier + String(UnicodeScalar(UInt8(value)))
        }
        
        func mapToDevice(identifier: String) -> String { // swiftlint:disable:this cyclomatic_complexity
            #if os(iOS)
            switch identifier {
            case "iPod5,1":                                 return "iPod Touch 5"
            case "iPod7,1":                                 return "iPod Touch 6"
            case "iPhone3,1", "iPhone3,2", "iPhone3,3":     return "iPhone 4"
            case "iPhone4,1":                               return "iPhone 4s"
            case "iPhone5,1", "iPhone5,2":                  return "iPhone 5"
            case "iPhone5,3", "iPhone5,4":                  return "iPhone 5c"
            case "iPhone6,1", "iPhone6,2":                  return "iPhone 5s"
            case "iPhone7,2":                               return "iPhone 6"
            case "iPhone7,1":                               return "iPhone 6 Plus"
            case "iPhone8,1":                               return "iPhone 6s"
            case "iPhone8,2":                               return "iPhone 6s Plus"
            case "iPhone9,1", "iPhone9,3":                  return "iPhone 7"
            case "iPhone9,2", "iPhone9,4":                  return "iPhone 7 Plus"
            case "iPhone8,4":                               return "iPhone SE"
            case "iPhone10,1", "iPhone10,4":                return "iPhone 8"
            case "iPhone10,2", "iPhone10,5":                return "iPhone 8 Plus"
            case "iPhone10,3", "iPhone10,6":                return "iPhone X"
            case "iPhone11,2":                              return "iPhone XS"
            case "iPhone11,4", "iPhone11,6":                return "iPhone XS Max"
            case "iPhone11,8":                              return "iPhone XR"
            case "iPad2,1", "iPad2,2", "iPad2,3", "iPad2,4":return "iPad 2"
            case "iPad3,1", "iPad3,2", "iPad3,3":           return "iPad 3"
            case "iPad3,4", "iPad3,5", "iPad3,6":           return "iPad 4"
            case "iPad4,1", "iPad4,2", "iPad4,3":           return "iPad Air"
            case "iPad5,3", "iPad5,4":                      return "iPad Air 2"
            case "iPad6,11", "iPad6,12":                    return "iPad 5"
            case "iPad7,5", "iPad7,6":                      return "iPad 6"
            case "iPad11,4", "iPad11,5":                    return "iPad Air (3rd generation)"
            case "iPad2,5", "iPad2,6", "iPad2,7":           return "iPad Mini"
            case "iPad4,4", "iPad4,5", "iPad4,6":           return "iPad Mini 2"
            case "iPad4,7", "iPad4,8", "iPad4,9":           return "iPad Mini 3"
            case "iPad5,1", "iPad5,2":                      return "iPad Mini 4"
            case "iPad11,1", "iPad11,2":                    return "iPad Mini 5"
            case "iPad6,3", "iPad6,4":                      return "iPad Pro (9.7-inch)"
            case "iPad6,7", "iPad6,8":                      return "iPad Pro (12.9-inch)"
            case "iPad7,1", "iPad7,2":                      return "iPad Pro (12.9-inch) (2nd generation)"
            case "iPad7,3", "iPad7,4":                      return "iPad Pro (10.5-inch)"
            case "iPad8,1", "iPad8,2", "iPad8,3", "iPad8,4":return "iPad Pro (11-inch)"
            case "iPad8,5", "iPad8,6", "iPad8,7", "iPad8,8":return "iPad Pro (12.9-inch) (3rd generation)"
            case "AppleTV5,3":                              return "Apple TV"
            case "AppleTV6,2":                              return "Apple TV 4K"
            case "AudioAccessory1,1":                       return "HomePod"
            case "i386", "x86_64":                          return "Simulator \(mapToDevice(identifier: ProcessInfo().environment["SIMULATOR_MODEL_IDENTIFIER"] ?? "iOS"))"
            default:                                        return identifier
            }
            #elseif os(tvOS)
            switch identifier {
            case "AppleTV5,3": return "Apple TV 4"
            case "AppleTV6,2": return "Apple TV 4K"
            case "i386", "x86_64": return "Simulator \(mapToDevice(identifier: ProcessInfo().environment["SIMULATOR_MODEL_IDENTIFIER"] ?? "tvOS"))"
            default: return identifier
            }
            #endif
        }
        
        return mapToDevice(identifier: identifier)
    }()
    
}
public extension CMTime {
    
    var durationInSeconds: CGFloat {
        return CGFloat(CMTimeGetSeconds(self))
    }
    var durationInFloat: Float {
        return Float(CMTimeGetSeconds(self))
    }
    var durationInDouble: Double {
        return Double(CMTimeGetSeconds(self))
    }
    
    var durationInInt: Int {
        return Int(CMTimeGetSeconds(self))
    }
    
}
extension CGAffineTransform
{
    func xscale() -> CGFloat {
        return sqrt(self.a * self.a + self.c * self.c)
    }
    
    func yscale() -> CGFloat {
        return sqrt(self.b * self.b + self.d * self.d)
    }
    
    func zRotaion() -> CGFloat {
        let radians = atan2f(Float(self.b), Float(self.a))
        return CGFloat(radians)
    }
    
    
    
    
}
extension Date {
    
    // Convert local time to UTC (or GMT)
    func toGlobalTime() -> Date {
        let timezone = TimeZone.current
        let seconds = -TimeInterval(timezone.secondsFromGMT(for: self))
        return Date(timeInterval: seconds, since: self)
    }
    
    // Convert UTC (or GMT) to local time
    func toLocalTime() -> Date {
        let timezone = TimeZone.current
        let seconds = TimeInterval(timezone.secondsFromGMT(for: self))
        return Date(timeInterval: seconds, since: self)
    }


}
