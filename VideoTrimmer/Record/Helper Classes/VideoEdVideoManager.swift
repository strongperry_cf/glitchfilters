//
//  VideoEdVideoManager.swift
//  VideoTrimmer
//
//  Created by strongapps on 08/04/19.
//  Copyright © 2019 strongapps. All rights reserved.


import UIKit
import Photos

class VideoEdVideoManager: NSObject {

    func getDurationOfUrl(url:URL) -> Double {
        
            let asset = AVAsset(url: url)
            return asset.duration.seconds
        /*
        let audioAsset = AVURLAsset.init(url: url, options: nil)
        audioAsset.loadValuesAsynchronously(forKeys: ["duration"]) {
            var error: NSError? = nil
            let status = audioAsset.statusOfValue(forKey: "duration", error: &error)
            switch status {
            case .loaded: // Sucessfully loaded. Continue processing.
                let duration = audioAsset.duration
                let durationInSeconds = CMTimeGetSeconds(duration)
                print(Double (duration.seconds))
                break
            default:
                break // Handle all other cases
            }
        }   */
    }
    
    func stringFromat(seconds:TimeInterval) -> String {
        
        let formatter = DateComponentsFormatter()
        formatter.zeroFormattingBehavior = .pad
        // formatter.allowedUnits = [.hour, .minute, .second]
        formatter.allowedUnits = [.minute, .second]
        let str = formatter.string(from: seconds)
//        print(str)
        return str!

    }
    
    func resolutionOfVideo(videoUrl:URL) -> CGSize? {
        
        guard let track = AVAsset(url: videoUrl as URL).tracks(withMediaType: AVMediaType.video).first else { return nil }
        let size = track.naturalSize.applying(track.preferredTransform)
        return CGSize(width: abs(size.width), height: abs(size.height))
    }

    func isUrlContainsAudioTrack(url:URL) -> Bool {
        let asset: AVURLAsset? = AVURLAsset(url: url as URL, options: nil)
        if ((asset)?.tracks(withMediaType: .audio))!.count != 0 {
            return true;
        }
        return false;
    }
    
    func isAssetContainsAudioTrack(asset:AVAsset) -> Bool {
        if ((asset).tracks(withMediaType: .audio)).count != 0 {
            return true;
        }
        return false;
    }
    
    func isUrlContainsVideoTrack(url:URL) -> Bool {
        let asset: AVURLAsset? = AVURLAsset(url: url as URL, options: nil)
        if ((asset)?.tracks(withMediaType: .video))!.count != 0 {
            return true;
        }
        return false;
    }
    
    func greenLineRemoval(videoSize:CGSize) ->CGSize {
        
        var width: Int = Int(videoSize.width)
        var height: Int = Int(videoSize.height)
        while width % 4 > 0 {
            width = width + 1        }
        while height % 4 > 0 {
            height = height + 1        }
        return CGSize(width: CGFloat(width), height: CGFloat(height));
    }
    
    func getAverageSizeOfVideos(videoUrlArr:NSArray) -> CGSize {
        
        var outputSize = CGSize.zero;
        for case let videoUrl as URL in videoUrlArr
        {
//            debugPrint("videoUrl is :",videoUrl as Any);
            if videoUrl == nil {
                continue;            }
            let videoSize: CGSize = resolutionOfVideo(videoUrl: videoUrl )!
            if (videoSize.height) > outputSize.height {
                outputSize.height = (videoSize.height);
            }
            if (videoSize.width) > outputSize.width {
                outputSize.width = (videoSize.width) ;
            }
        }
        return outputSize;
    }
    
    func generateThumbnail(asset: AVAsset,_ videoComposition :AVMutableVideoComposition? = nil,at time: CMTime = .zero,of size: CGSize = .zero) -> UIImage? {
        do {
            let imgGenerator = AVAssetImageGenerator(asset: asset)
            if videoComposition != nil{
                imgGenerator.videoComposition = videoComposition            }
            imgGenerator.appliesPreferredTrackTransform = true
            if size != .zero{
                imgGenerator.maximumSize = size
            }
            let cgImage = try imgGenerator.copyCGImage(at: time, actualTime: nil)
            let thumbnail = UIImage(cgImage: cgImage)
            return thumbnail
        } catch let error {
            print("*** Error generating thumbnail: \(error.localizedDescription)")
            return nil
        }
    }
    
    /*
    func averageSizeOfVideos_FromModal(videoData:NSMutableArray) -> CGSize {
        
        var outputSize = CGSize.zero;
        let videoUrlArray:NSMutableArray =  NSMutableArray();
        for case  let videoModal_Data as VideoData in videoData
        {
            let videoUrl:URL = videoModal_Data.videoUrl!;
            videoUrlArray.add(videoUrl);
        }
        outputSize = getAverageSizeOfVideos(videoUrlArr: videoUrlArray);
        return outputSize;
    }*/
    
    
    // *********************************************************************************************** //
    // ************************     Speed Control of Videos      ************************************ //
    // *********************************************************************************************** //
    
    // Merge Multiple Videos with Specific Speeds (All Videos Having Same Resolution)
    
    
//    func abc(asfdf:UIImage,block: @escaping((UIImage)->(Int))) -> Int {
//        //body
//
//    }
    
    func mergeVideos_WithSpeed(videoArray :[VideoData]/*[VideoWithSpeed]*/, maxSize:CGSize = CGSize.zero,isAspectFit:Bool = true ,_ orietation : videoTrimAngleEnum? ,complitionBlock:@escaping (_ composition : AVMutableComposition?, _ videoComposition: AVMutableVideoComposition?, _ audioMix: AVAudioMix? ) -> ()) {
        
        //        self.abc(asfdf: UIImage()) { (image) -> (Int) in
        //            return 2
        //        }
        
        // 1 - Create AVMutableComposition object. This object will hold your AVMutableCompositionTrack instances.
        let mixComposition = AVMutableComposition()
        var arrayLayerInstructions:[AVMutableVideoCompositionLayerInstruction] = []
        
        // Calculate Final Average Size Of Videos
        var outputSize =  (maxSize != CGSize.zero) ? maxSize : CGSize.zero
        var isAudioContains = false
        //        var isVideoContains = false
        
        if maxSize == CGSize.zero {
            for case  let videoModal as VideoData in videoArray
            {
                let url:URL = videoModal.videoUrl!
                if url == nil {
                    continue;
                    
                }
                if (isUrlContainsVideoTrack(url: url) == false)    {
                    complitionBlock (nil,nil,nil);
                    break
                }
                else{
                    //                    isVideoContains = true
                    
                }
                
                let videoSize: CGSize = resolutionOfVideo(videoUrl: url )!
                if (videoSize.height) > outputSize.height {
                    outputSize.height = (videoSize.height)
                }
                if (videoSize.width) > outputSize.width {
                    outputSize.width = (videoSize.width)
                }
                
                if (isUrlContainsAudioTrack(url: url) == true)    {
                    isAudioContains = true
                    
                }
            }
        }
        
        // *** Green Removal ***
        outputSize =  self.greenLineRemoval(videoSize: outputSize)
        //        outputSize = CGSize(width: 1280, height: 1280)
        
        
        // Init video & audio composition track
        /*
         let videoCompositionTrack = mixComposition.addMutableTrack(withMediaType: AVMediaType.video,
         preferredTrackID: Int32(kCMPersistentTrackID_Invalid))
         var audioCompositionTrack:AVMutableCompositionTrack? ;
         if isAudioContains {
         //audioCompositionTrack = nil
         audioCompositionTrack = mixComposition.addMutableTrack(withMediaType: AVMediaType.audio,
         preferredTrackID: Int32(kCMPersistentTrackID_Invalid));
         }
         */
        /*
         let assetInfo = orientationFromTransform(transform: (videoTrack.preferredTransform))
         outputSize = videoTrack.naturalSize
         if assetInfo.isPortrait == true {
         outputSize.width = (videoTrack.naturalSize.height)
         outputSize.height = (videoTrack.naturalSize.width)
         }
         */
        
        
        // ************ Adding Multiple Video Tracks With Different Different Speed ***************
        
        var currentTime:CMTime = CMTime.zero;
        
        for i in 0 ..< videoArray.count {
            
            //  let videoSpeedObj:VideoWithSpeed  =  videoArray.object(at: i) as! VideoWithSpeed;
            let videoSpeedObj  =  videoArray[i] ;
            //            debugPrint("videoSpeedObj.videoUrl is ",videoSpeedObj.videoUrl as Any);
            let videoUrl = videoSpeedObj.videoUrl;
            
            var videoAsset: AVURLAsset?
            videoAsset = AVURLAsset(url: videoUrl!, options: nil)
            
            // Get video track
            guard let videoTrack = videoAsset?.tracks(withMediaType: AVMediaType.video).first else { continue }
            
            // Get audio track
            var audioTrack:AVAssetTrack?
            if (videoAsset?.tracks(withMediaType: AVMediaType.audio).count)! > 0 {
                audioTrack = videoAsset?.tracks(withMediaType: AVMediaType.audio).first
                isAudioContains = true
            }
            else            {
                isAudioContains = false;
            }
            
            let videoCompositionTrack = mixComposition.addMutableTrack(withMediaType: AVMediaType.video,
                                                                       preferredTrackID: Int32(kCMPersistentTrackID_Invalid))
            var audioCompositionTrack:AVMutableCompositionTrack? ;
            if isAudioContains {
                //audioCompositionTrack = nil
                audioCompositionTrack = mixComposition.addMutableTrack(withMediaType: AVMediaType.audio,
                                                                       preferredTrackID: Int32(kCMPersistentTrackID_Invalid));
            }
            
            // **** calculation Timings
            let SpeedMuliplier:Float = videoSpeedObj.videoSpeed ;
            var currentVideoDuration:CMTime = (videoAsset?.duration)!;
            var currentVideoStartTime:CMTime = CMTime.zero;
            
            if (videoSpeedObj.trimStartTime > currentVideoStartTime )            {
                currentVideoStartTime = videoSpeedObj.trimStartTime
            }
            
            if (videoSpeedObj.trimEndTime > CMTime.zero)            {
                currentVideoDuration = videoSpeedObj.trimEndTime
            }
            
            currentVideoDuration = CMTimeSubtract(currentVideoDuration, currentVideoStartTime)
            //            debugPrint("currentVideoDuration is :",currentVideoDuration)
            
            
            let durationFloat :Float =  Float(CMTimeGetSeconds(currentVideoDuration));
            let calDuration:Float = durationFloat/SpeedMuliplier;
            let calDurationInt:Int64 = Int64(calDuration*100);
            let calDurationCMTime:CMTime = CMTimeMake(value: calDurationInt, timescale: 100);
            _ =  CMTimeGetSeconds(calDurationCMTime);
            
            // Init video & audio composition track
            do {
                
                // ***** 1
                // Add video track to video composition at specific time
                
                try videoCompositionTrack?.insertTimeRange(CMTimeRangeMake(start: currentVideoStartTime, duration: currentVideoDuration),
                                                           of: videoTrack,
                                                           at: currentTime)
                // Add audio track to audio composition at specific time
                if let audioTrack = audioTrack {
                    try audioCompositionTrack!.insertTimeRange(CMTimeRangeMake(start: currentVideoStartTime, duration: currentVideoDuration),
                                                               of: audioTrack,
                                                               at: currentTime)
                    audioCompositionTrack?.preferredVolume = videoSpeedObj.volume
                    // ---  Audio Time scale  ----
                    audioCompositionTrack!.scaleTimeRange(CMTimeRangeMake(start: CMTimeSubtract((audioCompositionTrack?.timeRange.duration)!, currentVideoDuration) , duration: currentVideoDuration), toDuration: calDurationCMTime); // Org
                }
                
                // ---  Video Time scale  ----
                videoCompositionTrack?.scaleTimeRange(CMTimeRangeMake(start: CMTimeSubtract((videoCompositionTrack?.timeRange.duration)!, currentVideoDuration) , duration: currentVideoDuration), toDuration: calDurationCMTime); // ORG
                
                // ---  LayerInstructions  ---
                let layerInstruction = AVMutableVideoCompositionLayerInstruction(assetTrack: videoCompositionTrack!)
                
                // ----- Transform ---
                let trans = correctTransform(asset: videoAsset!, standardSize: outputSize, isAspectFit: isAspectFit)
                
                if VideoEdVideoMakeManager.currentEditorMode == .VIDEOEDITOR && orietation != nil{
                    
                    let (transformNew,  isPortrait) = VideoEdCompositionManager.videoTransformAffineAndPortrait(asset: videoAsset!, standardSize: outputSize)
                    let newTransform = VideoEdCompositionManager.videoRotateComposition(for: transformNew, isP: isPortrait, with: orietation!, size: outputSize)
                    if i == 0 {
                        outputSize = newTransform.1
                        
                    }
                    layerInstruction.setTransform(newTransform.0, at: currentTime)
                }else{
                    layerInstruction.setTransform(trans, at: currentTime)
                    
                }
                //                layerInstruction.setTransform(trans, at: currentTime)
                
                arrayLayerInstructions.append(layerInstruction)
                
                let transitionType = videoSpeedObj.transitionType
                //                transitionType = 1
                
                if (transitionType == 0)                {
                    // ----- Opacity (Hide Individual Video Track when it Completed) -----
                    layerInstruction.setOpacity(0, at: (videoCompositionTrack?.timeRange.duration)!)
                } else      {
                    
                    let rampTime :CMTime = CMTime(seconds: 1.2, preferredTimescale: 60)
                    let resolutionSize = resolutionOfVideo(videoUrl: videoUrl!)
                    let maxLength = max(max(outputSize.width ,outputSize.height),max(resolutionSize!.width ,resolutionSize!.height))
                    
                    // Opacity Ramp
                    //layerInstruction.setOpacityRamp(fromStartOpacity: 1.0, toEndOpacity: 0.0, timeRange: timeRange);\
                    if  (transitionType == 1 )                  {
                        let timeRange:CMTimeRange = CMTimeRange(start:(videoCompositionTrack?.timeRange.duration)!, duration: rampTime)
                        layerInstruction.setOpacityRamp(fromStartOpacity: 1.0, toEndOpacity: 0.0, timeRange: timeRange);
                    }
                    else if (transitionType < 7)     {
                        // Push Transitions
                        applyTransform(layerInstruction, trans, videoTrack, maxLength, currentTime, (videoCompositionTrack?.timeRange.duration)!, rampTime, type: transitionType - 1)
                    }
                    
                }
                
            } // Do
            catch {
                print("Load track error")
            }
            
            currentTime = (videoCompositionTrack?.timeRange.duration)!;
            //  prevTime = CMTimeAdd(prevTime, trimDuration);
            //  debugPrint("current Total Video Duratoion is",prevTime);
            
            //            let duration = videoCompositionTrack?.timeRange.duration;
            //            debugPrint("current Total Video Duratoion is",duration as Any);
            
        }
        
        
        // *********************** Video Composition ****************
        
        // **** Reverse The Layer Isntruction Array If overlay Come Transition is Applied ****
        //        var reverseArry:[AVMutableVideoCompositionLayerInstruction] = []
        //        reverseArry = arrayLayerInstructions.reversed()
        
        // Main video composition instruction
        let mainInstruction = AVMutableVideoCompositionInstruction()
        mainInstruction.timeRange = CMTimeRangeMake(start: CMTime.zero, duration: mixComposition.duration)
        mainInstruction.layerInstructions = arrayLayerInstructions
        //        mainInstruction.backgroundColor = UIColor.red.cgColor
        
        // Main video composition
        let videoComposition = AVMutableVideoComposition()
        videoComposition.instructions = [mainInstruction]
        videoComposition.frameDuration = CMTimeMake(value: 1, timescale: 30)
        videoComposition.renderSize = outputSize;
        
        /*
         // let filter = CIFilter(
         let filter = CIFilter(name: "CIGaussianBlur")!
         let composition = AVMutableVideoComposition(asset: mixComposition, applyingCIFiltersWithHandler: { request in
         
         // Clamp to avoid blurring transparent pixels at the image edges
         let source = request.sourceImage.clampedToExtent()
         filter.setValue(source, forKey: kCIInputImageKey)
         
         // Vary filter parameters based on video timing
         let seconds = CMTimeGetSeconds(request.compositionTime)
         filter.setValue(seconds * 10.0, forKey: kCIInputRadiusKey)
         
         // Crop the blurred output to the bounds of the original image
         let output = filter.outputImage!.cropped(to: request.sourceImage.extent)
         
         // Provide the filter output to the composition
         request.finish(with: output, context: nil)
         })
         */
        //composition.instructions = [mainInstruction]
        
        /*
         // 2 Get video track
         guard let videoTrack = asset.tracks(withMediaType: AVMediaType.video).first else {
         complitionBlock (nil,nil,nil);
         return;
         }
         */
        
        complitionBlock (mixComposition,videoComposition,nil);
        // complitionBlock (mixComposition,composition,nil);
        return;
        
        
        
        // *********************** Testing Video Composition Filter ********************
        /*
         let filter = CIFilter(name: "CIGaussianBlur")!
         var composition = AVMutableVideoComposition(asset: mixComposition) { (request) in
         // Clamp to avoid blurring transparent pixels at the image edges
         let source = request.sourceImage.clampedToExtent()
         filter.setValue(source, forKey: kCIInputImageKey)
         // Vary filter parameters based on video timing
         let seconds = CMTimeGetSeconds(request.compositionTime)
         filter.setValue(seconds * 10.0, forKey: kCIInputRadiusKey)
         // Crop the blurred output to the bounds of the original image
         let output = filter.outputImage!.cropped(to: request.sourceImage.extent)
         // Provide the filter output to the composition
         request.finish(with: output, context: nil)
         }
         // composition.instructions = [mainInstruction]
         //composition.animationTool =  AVVideoCompositionCoreAnimationTool.init(postProcessingAsVideoLayer: videoLayer, in: parentUILayer)
         composition.frameDuration = CMTimeMake(value: 1, timescale: 30)
         composition.renderSize = outputSize;
         */
        
        // *********************** Testing Video Composition Filter  END ********************
        
        
        // ****************************  Export  Testing ************************************
        
        /*
         
         // Export to file
         let path = NSTemporaryDirectory().appending("mergedVideo.mp4")
         let exportURL = URL.init(fileURLWithPath: path)
         
         // Remove file if existed
         FileManager.default.removeItemIfExisted(exportURL)
         
         // Init exporter
         let exporter = AVAssetExportSession.init(asset: mixComposition, presetName: AVAssetExportPresetHighestQuality)
         exporter?.outputURL = exportURL
         exporter?.outputFileType = AVFileType.mp4
         exporter?.shouldOptimizeForNetworkUse = true
         // exporter?.timeRange = (videoCompositionTrack?.timeRange)!
         // exporter?.timeRange = (videoCompositionTrack?.timeRange)!
         exporter?.videoComposition = videoComposition
         
         // Do export
         print(" Start  Start  Start  Start  Start  Start  Start  Start  Start  Start  Start");
         
         exporter?.exportAsynchronously(completionHandler: {
         DispatchQueue.main.async {
         // self.exportDidFinish(exporter: exporter, videoURL: exportURL, completion: complitionBlock)
         
         print(" Done  Done  Done  Done  Done  Done  Done  Done  Done  Done  Done ");
         
         if exporter?.status == AVAssetExportSession.Status.completed {
         print("Exported file: \(exportURL.absoluteString)")
         
         complitionBlock (mixComposition,videoComposition,nil);
         
         PHPhotoLibrary.shared().performChanges({
         PHAssetChangeRequest.creationRequestForAssetFromVideo(atFileURL: exportURL)
         }) { saved, error in
         if saved {
         let fetchOptions = PHFetchOptions()
         fetchOptions.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: false)]
         let fetchResult = PHAsset.fetchAssets(with: .video, options: fetchOptions).firstObject
         // fetchResult is your latest video PHAsset
         // To fetch latest image  replace .video with .image
         DispatchQueue.main.async {
         self.UIAlert(title: "Saved", msg: "Video Save Successfully");
         }
         }
         }
         }
         else if exporter?.status == AVAssetExportSession.Status.failed {
         debugPrint("exporter?.error ",exporter?.error);
         //completion(exportURL,exporter?.error)
         }
         }
         })
         */
    }
    func mergeVideos_WithSpeed(videoArray :[VideoData]/*[VideoWithSpeed]*/, maxSize:CGSize = CGSize.zero,isAspectFit:Bool = true  ,complitionBlock:@escaping (_ composition : AVMutableComposition?, _ videoComposition: AVMutableVideoComposition?, _ audioMix: AVAudioMix? ) -> ()) {
        
//        self.abc(asfdf: UIImage()) { (image) -> (Int) in
//            return 2
//        }
        
        // 1 - Create AVMutableComposition object. This object will hold your AVMutableCompositionTrack instances.
        let mixComposition = AVMutableComposition()
        var arrayLayerInstructions:[AVMutableVideoCompositionLayerInstruction] = []
        
        // Calculate Final Average Size Of Videos
        var outputSize =  (maxSize != CGSize.zero) ? maxSize : CGSize.zero
        var isAudioContains = false
//        var isVideoContains = false
        
        if maxSize == CGSize.zero {
            for case  let videoModal as VideoData in videoArray
            {
                let url:URL = videoModal.videoUrl!
                if url == nil {
                    continue;
                    
                }
                if (isUrlContainsVideoTrack(url: url) == false)    {
                    complitionBlock (nil,nil,nil);
                    break
                }
                else{
//                    isVideoContains = true
                    
                }
                
                let videoSize: CGSize = resolutionOfVideo(videoUrl: url )!
                if (videoSize.height) > outputSize.height {
                    outputSize.height = (videoSize.height)
                }
                if (videoSize.width) > outputSize.width {
                    outputSize.width = (videoSize.width)
                }
                
                if (isUrlContainsAudioTrack(url: url) == true)    {
                    isAudioContains = true
                    
                }
            }
        }
        
        // *** Green Removal ***
        outputSize =  self.greenLineRemoval(videoSize: outputSize)
//        outputSize = CGSize(width: 1280, height: 1280)

        
        // Init video & audio composition track
        /*
         let videoCompositionTrack = mixComposition.addMutableTrack(withMediaType: AVMediaType.video,
                                                                   preferredTrackID: Int32(kCMPersistentTrackID_Invalid))
        var audioCompositionTrack:AVMutableCompositionTrack? ;
        if isAudioContains {
            //audioCompositionTrack = nil
            audioCompositionTrack = mixComposition.addMutableTrack(withMediaType: AVMediaType.audio,
            preferredTrackID: Int32(kCMPersistentTrackID_Invalid));
        }
        */
        /*
        let assetInfo = orientationFromTransform(transform: (videoTrack.preferredTransform))
        outputSize = videoTrack.naturalSize
        if assetInfo.isPortrait == true {
            outputSize.width = (videoTrack.naturalSize.height)
            outputSize.height = (videoTrack.naturalSize.width)
        }
        */
        
        
        // ************ Adding Multiple Video Tracks With Different Different Speed ***************
        
        var currentTime:CMTime = CMTime.zero;
        
        for i in 0 ..< videoArray.count {
            
            //  let videoSpeedObj:VideoWithSpeed  =  videoArray.object(at: i) as! VideoWithSpeed;
            let videoSpeedObj  =  videoArray[i] ;
//            debugPrint("videoSpeedObj.videoUrl is ",videoSpeedObj.videoUrl as Any);
            let videoUrl = videoSpeedObj.videoUrl;
            
            var videoAsset: AVURLAsset?
            videoAsset = AVURLAsset(url: videoUrl!, options: nil)
            
            // Get video track
            guard let videoTrack = videoAsset?.tracks(withMediaType: AVMediaType.video).first else { continue }
            
            // Get audio track
            var audioTrack:AVAssetTrack?
            if (videoAsset?.tracks(withMediaType: AVMediaType.audio).count)! > 0 {
                audioTrack = videoAsset?.tracks(withMediaType: AVMediaType.audio).first
                isAudioContains = true
            }
            else            {
                isAudioContains = false;
            }
            
            let videoCompositionTrack = mixComposition.addMutableTrack(withMediaType: AVMediaType.video,
                                                                       preferredTrackID: Int32(kCMPersistentTrackID_Invalid))
            var audioCompositionTrack:AVMutableCompositionTrack? ;
            if isAudioContains {
                //audioCompositionTrack = nil
                audioCompositionTrack = mixComposition.addMutableTrack(withMediaType: AVMediaType.audio,
                                                                       preferredTrackID: Int32(kCMPersistentTrackID_Invalid));
            }
            
            // **** calculation Timings
            let SpeedMuliplier:Float = videoSpeedObj.videoSpeed ;
            var currentVideoDuration:CMTime = (videoAsset?.duration)!;
            var currentVideoStartTime:CMTime = CMTime.zero;
            
            if (videoSpeedObj.trimStartTime > currentVideoStartTime )            {
                currentVideoStartTime = videoSpeedObj.trimStartTime
            }
            
            if (videoSpeedObj.trimEndTime > CMTime.zero)            {
                currentVideoDuration = videoSpeedObj.trimEndTime
            }
            
            currentVideoDuration = CMTimeSubtract(currentVideoDuration, currentVideoStartTime)
//            debugPrint("currentVideoDuration is :",currentVideoDuration)
            
            
            let durationFloat :Float =  Float(CMTimeGetSeconds(currentVideoDuration));
            let calDuration:Float = durationFloat/SpeedMuliplier;
            let calDurationInt:Int64 = Int64(calDuration*100);
            let calDurationCMTime:CMTime = CMTimeMake(value: calDurationInt, timescale: 100);
            _ =  CMTimeGetSeconds(calDurationCMTime);
            
            // Init video & audio composition track
            do {
                
                // ***** 1
                // Add video track to video composition at specific time
                
                try videoCompositionTrack?.insertTimeRange(CMTimeRangeMake(start: currentVideoStartTime, duration: currentVideoDuration),
                                                           of: videoTrack,
                                                           at: currentTime)
                // Add audio track to audio composition at specific time
                if let audioTrack = audioTrack {
                    try audioCompositionTrack!.insertTimeRange(CMTimeRangeMake(start: currentVideoStartTime, duration: currentVideoDuration),
                                                               of: audioTrack,
                                                               at: currentTime)
                    audioCompositionTrack?.preferredVolume = videoSpeedObj.volume
                    // ---  Audio Time scale  ----
                    audioCompositionTrack!.scaleTimeRange(CMTimeRangeMake(start: CMTimeSubtract((audioCompositionTrack?.timeRange.duration)!, currentVideoDuration) , duration: currentVideoDuration), toDuration: calDurationCMTime); // Org
                }
                
                // ---  Video Time scale  ----
                videoCompositionTrack?.scaleTimeRange(CMTimeRangeMake(start: CMTimeSubtract((videoCompositionTrack?.timeRange.duration)!, currentVideoDuration) , duration: currentVideoDuration), toDuration: calDurationCMTime); // ORG
                
                // ---  LayerInstructions  ---
                let layerInstruction = AVMutableVideoCompositionLayerInstruction(assetTrack: videoCompositionTrack!)
                
                // ----- Transform ---
                let trans = correctTransform(asset: videoAsset!, standardSize: outputSize, isAspectFit: isAspectFit)
                
//                if VideoEdVideoMakeManager.currentEditorMode == .VIDEOEDITOR && orietation != nil{
//
//                    let (transformNew,  isPortrait) = VideoEdCompositionManager.videoTransformAffineAndPortrait(asset: videoAsset!, standardSize: outputSize)
//                    let newTransform = VideoEdCompositionManager.videoRotateComposition(for: transformNew, isP: isPortrait, with: orietation!, size: outputSize)
//                    if i == 0 {
//                        outputSize = newTransform.1
//
//                    }
//                    layerInstruction.setTransform(newTransform.0, at: currentTime)
//                }else{
//                    layerInstruction.setTransform(trans, at: currentTime)
//
//                }
                
                layerInstruction.setTransform(trans, at: currentTime)

                arrayLayerInstructions.append(layerInstruction)
                
                let transitionType = videoSpeedObj.transitionType
//                transitionType = 1
                
                if (transitionType == 0)                {
                    // ----- Opacity (Hide Individual Video Track when it Completed) -----
                    layerInstruction.setOpacity(0, at: (videoCompositionTrack?.timeRange.duration)!)
                } else      {
                    
                    let rampTime :CMTime = CMTime(seconds: 1.2, preferredTimescale: 60)
                    let resolutionSize = resolutionOfVideo(videoUrl: videoUrl!)
                    let maxLength = max(max(outputSize.width ,outputSize.height),max(resolutionSize!.width ,resolutionSize!.height))
                    
                    // Opacity Ramp
                    //layerInstruction.setOpacityRamp(fromStartOpacity: 1.0, toEndOpacity: 0.0, timeRange: timeRange);\
                    if  (transitionType == 1 )                  {
                        let timeRange:CMTimeRange = CMTimeRange(start:(videoCompositionTrack?.timeRange.duration)!, duration: rampTime)
                        layerInstruction.setOpacityRamp(fromStartOpacity: 1.0, toEndOpacity: 0.0, timeRange: timeRange);
                    }
                    else if (transitionType < 7)     {
                        // Push Transitions
                        applyTransform(layerInstruction, trans, videoTrack, maxLength, currentTime, (videoCompositionTrack?.timeRange.duration)!, rampTime, type: transitionType - 1)
                    }
                    
                }
                
            } // Do
            catch {
                print("Load track error")
            }
            
            currentTime = (videoCompositionTrack?.timeRange.duration)!;
            //  prevTime = CMTimeAdd(prevTime, trimDuration);
            //  debugPrint("current Total Video Duratoion is",prevTime);
            
//            let duration = videoCompositionTrack?.timeRange.duration;
//            debugPrint("current Total Video Duratoion is",duration as Any);
            
        }
 
        
        // *********************** Video Composition ****************
        
        // **** Reverse The Layer Isntruction Array If overlay Come Transition is Applied ****
//        var reverseArry:[AVMutableVideoCompositionLayerInstruction] = []
//        reverseArry = arrayLayerInstructions.reversed()
        
        // Main video composition instruction
        let mainInstruction = AVMutableVideoCompositionInstruction()
        mainInstruction.timeRange = CMTimeRangeMake(start: CMTime.zero, duration: mixComposition.duration)
        mainInstruction.layerInstructions = arrayLayerInstructions
//        mainInstruction.backgroundColor = UIColor.red.cgColor
        
        // Main video composition
        let videoComposition = AVMutableVideoComposition()
        videoComposition.instructions = [mainInstruction]
        videoComposition.frameDuration = CMTimeMake(value: 1, timescale: 30)
        videoComposition.renderSize = outputSize;
        
        /*
         // let filter = CIFilter(
        let filter = CIFilter(name: "CIGaussianBlur")!
        let composition = AVMutableVideoComposition(asset: mixComposition, applyingCIFiltersWithHandler: { request in
            
            // Clamp to avoid blurring transparent pixels at the image edges
            let source = request.sourceImage.clampedToExtent()
            filter.setValue(source, forKey: kCIInputImageKey)
            
            // Vary filter parameters based on video timing
            let seconds = CMTimeGetSeconds(request.compositionTime)
            filter.setValue(seconds * 10.0, forKey: kCIInputRadiusKey)
            
            // Crop the blurred output to the bounds of the original image
            let output = filter.outputImage!.cropped(to: request.sourceImage.extent)
            
            // Provide the filter output to the composition
            request.finish(with: output, context: nil)
        })
       */
        //composition.instructions = [mainInstruction]
        
        /*
         // 2 Get video track
         guard let videoTrack = asset.tracks(withMediaType: AVMediaType.video).first else {
         complitionBlock (nil,nil,nil);
         return;
         }
         */
        
        complitionBlock (mixComposition,videoComposition,nil);
        // complitionBlock (mixComposition,composition,nil);
        return;
        
        
        
        // *********************** Testing Video Composition Filter ********************
        /*
        let filter = CIFilter(name: "CIGaussianBlur")!
        var composition = AVMutableVideoComposition(asset: mixComposition) { (request) in
            // Clamp to avoid blurring transparent pixels at the image edges
            let source = request.sourceImage.clampedToExtent()
            filter.setValue(source, forKey: kCIInputImageKey)
            // Vary filter parameters based on video timing
            let seconds = CMTimeGetSeconds(request.compositionTime)
            filter.setValue(seconds * 10.0, forKey: kCIInputRadiusKey)
            // Crop the blurred output to the bounds of the original image
            let output = filter.outputImage!.cropped(to: request.sourceImage.extent)
            // Provide the filter output to the composition
            request.finish(with: output, context: nil)
        }
        // composition.instructions = [mainInstruction]
        //composition.animationTool =  AVVideoCompositionCoreAnimationTool.init(postProcessingAsVideoLayer: videoLayer, in: parentUILayer)
        composition.frameDuration = CMTimeMake(value: 1, timescale: 30)
        composition.renderSize = outputSize;
        */
        
        // *********************** Testing Video Composition Filter  END ********************

      
        // ****************************  Export  Testing ************************************
        
        /*
         
        // Export to file
        let path = NSTemporaryDirectory().appending("mergedVideo.mp4")
        let exportURL = URL.init(fileURLWithPath: path)
        
        // Remove file if existed
        FileManager.default.removeItemIfExisted(exportURL)
        
        // Init exporter
        let exporter = AVAssetExportSession.init(asset: mixComposition, presetName: AVAssetExportPresetHighestQuality)
        exporter?.outputURL = exportURL
        exporter?.outputFileType = AVFileType.mp4
        exporter?.shouldOptimizeForNetworkUse = true
        // exporter?.timeRange = (videoCompositionTrack?.timeRange)!
        // exporter?.timeRange = (videoCompositionTrack?.timeRange)!
        exporter?.videoComposition = videoComposition
        
        // Do export
        print(" Start  Start  Start  Start  Start  Start  Start  Start  Start  Start  Start");
        
        exporter?.exportAsynchronously(completionHandler: {
            DispatchQueue.main.async {
                // self.exportDidFinish(exporter: exporter, videoURL: exportURL, completion: complitionBlock)
                
                print(" Done  Done  Done  Done  Done  Done  Done  Done  Done  Done  Done ");
                
                if exporter?.status == AVAssetExportSession.Status.completed {
                    print("Exported file: \(exportURL.absoluteString)")
                    
                    complitionBlock (mixComposition,videoComposition,nil);
                    
                    PHPhotoLibrary.shared().performChanges({
                        PHAssetChangeRequest.creationRequestForAssetFromVideo(atFileURL: exportURL)
                    }) { saved, error in
                        if saved {
                            let fetchOptions = PHFetchOptions()
                            fetchOptions.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: false)]
                            let fetchResult = PHAsset.fetchAssets(with: .video, options: fetchOptions).firstObject
                            // fetchResult is your latest video PHAsset
                            // To fetch latest image  replace .video with .image
                            DispatchQueue.main.async {
                                self.UIAlert(title: "Saved", msg: "Video Save Successfully");
                            }
                        }
                    }
                }
                else if exporter?.status == AVAssetExportSession.Status.failed {
                    debugPrint("exporter?.error ",exporter?.error);
                    //completion(exportURL,exporter?.error)
                }
            }
        })
        */
    }

    
    func applyTransform(_ layerInstruction:AVMutableVideoCompositionLayerInstruction,_ initialTrans:CGAffineTransform ,_  videoTrack:AVAssetTrack,_ greatestLength:CGFloat,_ trackBeginTime:CMTime,_ trackDuration:CMTime,_ rampTime:CMTime,type:Int )  {
       
        let assetInfo = orientationFromTransform(transform: videoTrack.preferredTransform)
//        print("assetInfo.isPortrait is = ",assetInfo.isPortrait)
//        print("assetInfo.orientation is = ",assetInfo.orientation.rawValue )

        // let timeRange:CMTimeRange = CMTimeRange(start:CMTimeSubtract(trackDuration, rampTime), duration: rampTime)
        let timeRange:CMTimeRange = CMTimeRange(start:trackDuration, duration: rampTime)
        
        var finalTrans = initialTrans
        
//        finalTrans = finalTrans.translatedBy(x: greatestLength, y: 0)
        // Transform Ramping

        switch type {
        case 1:
            //  To Left
            if assetInfo.orientation == .up {
                finalTrans = finalTrans.translatedBy(x: -1.0 * greatestLength  , y: 0.0 * greatestLength)
            }else if assetInfo.orientation == .down {
                finalTrans = finalTrans.translatedBy(x: 1.0 * greatestLength  , y: 0.0 * greatestLength)
            }else if assetInfo.orientation == .left {
                finalTrans = finalTrans.translatedBy(x: 0.0 * greatestLength  , y: -1.0 * greatestLength)
            }else if assetInfo.orientation == .right {
                finalTrans = finalTrans.translatedBy(x: 0.0 * greatestLength  , y: 1.0 * greatestLength)
            }
        case 2:
            // To Right
            if assetInfo.orientation == .up {
                finalTrans = finalTrans.translatedBy(x: 1.0 * greatestLength , y: 0.0 * greatestLength)
            }else if assetInfo.orientation == .down {
                finalTrans = finalTrans.translatedBy(x: -1.0 * greatestLength , y: 0.0 * greatestLength)
            }else if assetInfo.orientation == .left {
                finalTrans = finalTrans.translatedBy(x: 0.0 * greatestLength  , y: 1.0 * greatestLength)
            }else if assetInfo.orientation == .right {
                finalTrans = finalTrans.translatedBy(x: 0.0 * greatestLength  , y: -1.0 * greatestLength)
            }
        case 3:
            // To Top
            if assetInfo.orientation == .up{
                finalTrans = finalTrans.translatedBy(x: greatestLength * 0.0, y: -1.0 * greatestLength)
            }else if assetInfo.orientation == .down {
                finalTrans = finalTrans.translatedBy(x: greatestLength * 0.0, y: 1.0 * greatestLength)
            }else if assetInfo.orientation == .left {
                finalTrans = finalTrans.translatedBy(x: 1.0 * greatestLength  , y: greatestLength * 0.0)
            }else if assetInfo.orientation == .right {
                finalTrans = finalTrans.translatedBy(x: -1.0 * greatestLength  , y: greatestLength * 0.0)
            }
        case 4:
            // To Bottom
            if assetInfo.orientation == .up{
                finalTrans = finalTrans.translatedBy(x: greatestLength * 0.0, y: 1.0 * greatestLength)
            }else if assetInfo.orientation == .down {
                finalTrans = finalTrans.translatedBy(x: greatestLength * 0.0, y: -1.0 * greatestLength)
            }else if assetInfo.orientation == .left {
                finalTrans = finalTrans.translatedBy(x: -1.0 * greatestLength  , y: 0.0 * greatestLength)
            }else if assetInfo.orientation == .right {
                finalTrans = finalTrans.translatedBy(x: 1.0 * greatestLength  , y: 0.0 * greatestLength)
            }
        case 5:
            
//            let timeRangeT:CMTimeRange = CMTimeRange(start:CMTimeSubtract(trackDuration, rampTime), duration: rampTime)
            let timeRangeT:CMTimeRange = CMTimeRange(start:trackDuration, duration: rampTime)
            var finalTrans = initialTrans
            finalTrans = finalTrans.scaledBy(x: 0.01, y: 0.01)
            
            if assetInfo.orientation == .up{
                finalTrans = finalTrans.translatedBy(x: greatestLength * 0.0, y: 1.0 * greatestLength)
            }else if assetInfo.orientation == .down {
                finalTrans = finalTrans.translatedBy(x: greatestLength * 0.0, y: -1.0 * greatestLength)
            }else if assetInfo.orientation == .left {
                finalTrans = finalTrans.translatedBy(x: -1.0 * greatestLength  , y: 0.0 * greatestLength)
            }else if assetInfo.orientation == .right {
                finalTrans = finalTrans.translatedBy(x: 1.0 * greatestLength  , y: 0.0 * greatestLength)
            }
            
            layerInstruction.setTransformRamp(fromStart: initialTrans, toEnd: finalTrans, timeRange: timeRangeT)
            layerInstruction.setOpacity(0, at: CMTimeAdd(trackDuration, rampTime))
            return

        default:
            finalTrans = initialTrans
        }
        layerInstruction.setTransformRamp(fromStart: initialTrans, toEnd: finalTrans, timeRange: timeRange)
        
    }

 
    /*
    func correctTransform(asset: AVAsset, standardSize:CGSize ) -> CGAffineTransform {
        
        let assetTrack = asset.tracks(withMediaType: AVMediaType.video)[0]
        let transform = assetTrack.preferredTransform
        var finalTransform = assetTrack.preferredTransform
        
        let assetInfo = orientationFromTransform(transform: transform)
        
        var aspectFillRatio:CGFloat = 1
        
        var videoTrackSize = assetTrack.naturalSize.applying(assetTrack.preferredTransform)
        videoTrackSize = CGSize(width: abs(videoTrackSize.width), height: abs(videoTrackSize.height));

        let maxAspectRatio = standardSize.width/standardSize.height
        let videoAspectRatio = videoTrackSize.width / videoTrackSize.height;

        
        if assetTrack.naturalSize.height < assetTrack.naturalSize.width {
            aspectFillRatio = standardSize.height / assetTrack.naturalSize.height
        }
        else {
            aspectFillRatio = standardSize.width / assetTrack.naturalSize.width
        }
       
        if assetInfo.isPortrait {
            let scaleFactor = CGAffineTransform(scaleX: aspectFillRatio, y: aspectFillRatio)
            
            let posX = standardSize.width/2 - (assetTrack.naturalSize.height * aspectFillRatio)/2
            let posY = standardSize.height/2 - (assetTrack.naturalSize.width * aspectFillRatio)/2
            let moveFactor = CGAffineTransform(translationX: posX, y: posY)
            finalTransform = assetTrack.preferredTransform.concatenating(scaleFactor).concatenating(moveFactor);
            
        } else {
            let scaleFactor = CGAffineTransform(scaleX: aspectFillRatio, y: aspectFillRatio)
            
            let posX = standardSize.width/2 - (assetTrack.naturalSize.width * aspectFillRatio)/2
            let posY = standardSize.height/2 - (assetTrack.naturalSize.height * aspectFillRatio)/2
            let moveFactor = CGAffineTransform(translationX: posX, y: posY)
            
            let concat = assetTrack.preferredTransform.concatenating(scaleFactor).concatenating(moveFactor)
            
            if assetInfo.orientation == .down {
                // let fixUpsideDown = CGAffineTransform(rotationAngle: CGFloat(Double.pi))
                //concat = fixUpsideDown.concatenating(scaleFactor).concatenating(moveFactor)
            }
            
            finalTransform = concat;
        }
        
        return finalTransform;
    }
    
    */
    
    func correctTransform(asset: AVAsset, standardSize:CGSize ,isAspectFit:Bool = true ) -> CGAffineTransform {
        
        let assetTrack = asset.tracks(withMediaType: AVMediaType.video)[0]
        let transform = assetTrack.preferredTransform
        var finalTransform = assetTrack.preferredTransform
        
        let assetInfo = orientationFromTransform(transform: transform)
        
        var aspectFillRatio:CGFloat = 1
        var videoTrackSize = assetTrack.naturalSize.applying(assetTrack.preferredTransform)
        videoTrackSize = CGSize(width: abs(videoTrackSize.width), height: abs(videoTrackSize.height));
        
        //if videoTrackSize.height < videoTrackSize.width
        //if videoTrackSize.height > videoTrackSize.width
        let maxAspectRatio = standardSize.width/standardSize.height
        let videoAspectRatio = videoTrackSize.width / videoTrackSize.height;
        
        if isAspectFit == true {
            if maxAspectRatio > videoAspectRatio  {
                aspectFillRatio = standardSize.height / videoTrackSize.height
            }            else {
                aspectFillRatio = standardSize.width / videoTrackSize.width
            }
        } else{
            if maxAspectRatio < videoAspectRatio  {
                aspectFillRatio = standardSize.height / videoTrackSize.height
            }            else {
                aspectFillRatio = standardSize.width / videoTrackSize.width
            }
        }
        
        let scaleFactor = CGAffineTransform(scaleX: aspectFillRatio, y: aspectFillRatio)
        let posX = standardSize.width/2 - (videoTrackSize.width * aspectFillRatio)/2
        let posY = standardSize.height/2 - (videoTrackSize.height * aspectFillRatio)/2
        //        posX = 00;
        
        let moveFactor = CGAffineTransform(translationX: posX, y: posY)
        
//        let rotation = CGAffineTransform(rotationAngle: CGFloat(GlobalHelper.shared.degreeToRadian(70)));
        var concat = assetTrack.preferredTransform.concatenating(scaleFactor).concatenating(moveFactor)//.concatenating(rotation)
        
//        if assetInfo.orientation == .down {
//            let fixUpsideDown = CGAffineTransform(rotationAngle: CGFloat(Double.pi))
            //concat = fixUpsideDown.concatenating(scaleFactor).concatenating(moveFactor)
//        }
        finalTransform = concat;
        
        if assetInfo.orientation == .right  &&  finalTransform.tx == 0.0 {
            
            finalTransform = finalTransform.translatedBy(x: 0, y: -videoTrackSize.width)
            
        }else if finalTransform.b == -1 && finalTransform.c == -1{
//            finalTransform = finalTransform.rotated(by: -270) //finalTransform.translatedBy(x: -1, y: 0)
//            finalTransform = CGAffineTransform(scaleX: 0, y: -1)// not final solution found for this condition

        }
        //new code for rotation
        
//        let (testTransform,  isPortrait) = VideoEdCompositionManager.videoTransformAffineAndPortrait(asset: asset, standardSize: standardSize)
//        let transformUpdate = VideoEdCompositionManager.videoRotateComposition(for: testTransform, isP: isPortrait, with: .k90, size: standardSize)
//        let newSizeForVideo = transformUpdate.1
//        let newtransform = transformUpdate.0
//        let posX2 = newSizeForVideo.width/2 - (videoTrackSize.width * aspectFillRatio)/2
//        let posY2 = newSizeForVideo.height/2 - (videoTrackSize.height * aspectFillRatio)/2
//        let moveFactor2 = CGAffineTransform(translationX: posX2, y: posY2)
//        let concat2 = newtransform.concatenating(scaleFactor).concatenating(moveFactor2)//.concatenating(rotation)
//        finalTransform = concat2;
//
//        if assetInfo.orientation == .right  &&  finalTransform.tx == 0.0 {
//            finalTransform = finalTransform.translatedBy(x: 0, y: -videoTrackSize.width)
//        }

 
        return finalTransform;
    }
    
    func orientationFromTransform(transform: CGAffineTransform) -> (orientation: UIImage.Orientation, isPortrait: Bool) {
        var assetOrientation = UIImage.Orientation.up
        var isPortrait = false
        
        if transform.a == 0 && transform.b == 1.0 && transform.c == -1.0 && transform.d == 0 {
            assetOrientation = .right
            isPortrait = true
        } else if transform.a == 0 && transform.b == -1.0 && transform.c == 1.0 && transform.d == 0 {
            assetOrientation = .left
            isPortrait = true
        } else if transform.a == 1.0 && transform.b == 0 && transform.c == 0 && transform.d == 1.0 {
            assetOrientation = .up
        } else if transform.a == -1.0 && transform.b == 0 && transform.c == 0 && transform.d == -1.0 {
            assetOrientation = .down
        }
        return (assetOrientation, isPortrait)
    }
    var savingVideoUrl : URL? {
        get {
            var passUrl = false
            var url: URL?
            
            while !passUrl {
                
                let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
                
                let completePath = documentsPath.appendingFormat(String(format: "/VideoDoc_%lld.mov", Int64(arc4random()) % 1000000000000000))
                //                let completePath = NSTemporaryDirectory().appendingFormat(String(format: "VideoDoc_%lld.mov", Int64(arc4random()) % 1000000000000000))
                url =  URL(fileURLWithPath: completePath)
                
                let exists: Bool = FileManager.default.fileExists(atPath: completePath)
                if !exists {
                    passUrl = true
                }
            }
            return url
        }
        
    }
    func exportSession(mixComposition:AVMutableComposition,videoComposition:AVMutableVideoComposition?, audioMix:AVAudioMix?,_ exportPresetName:String =  AVAssetExportPresetHighestQuality ,videoName:String,progress:@escaping((Int)->()),complitionBlock:@escaping((AVAssetExportSession?)->()) ) -> AVAssetExportSession? {
        
        let exportURL = savingVideoUrl
        
        // Init exporter
        let exporter = AVAssetExportSession.init(asset: mixComposition, presetName: exportPresetName)
        exporter?.outputURL = exportURL
        exporter?.outputFileType = AVFileType.mp4
        exporter?.shouldOptimizeForNetworkUse = true
        exporter?.timeRange = CMTimeRangeMake(start: .zero, duration: mixComposition.duration);
 
        if videoComposition != nil        {
            exporter?.videoComposition = videoComposition
            
        }
        if audioMix != nil          {
            exporter?.audioMix = audioMix
            
        }

        var progressTimer:Timer?
        progressTimer = Timer.scheduledTimer(withTimeInterval: 0.1, repeats: true) { timer in
            // print("progressTimer is:",timer);
            let percentage:Int = Int(exporter!.progress * Float (100.0))
            //  print("export Timer :",percentage);
            progress(percentage)
        }
        exporter?.exportAsynchronously(completionHandler: {
            DispatchQueue.main.async {
                // self.exportDidFinish(exporter: exporter, videoURL: exportURL, completion: complitionBlock)
                progressTimer?.invalidate()
                progressTimer = nil
                complitionBlock(exporter);
            }
        })
        

       return exporter
    }
  
}

