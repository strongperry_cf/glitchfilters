

#import "GPUCustomColorControlFilter.h"

#if TARGET_IPHONE_SIMULATOR || TARGET_OS_IPHONE
NSString *const kGPUColorCustomControlFragmentShaderString = SHADER_STRING
(
 varying highp vec2 textureCoordinate;
 
 uniform sampler2D inputImageTexture;
 
 uniform lowp float brightness;
 uniform lowp float contrast;
 uniform highp float exposure;
 
 uniform lowp float saturation;
 // Values from "Graphics Shaders: Theory and Practice" by Bailey and Cunningham
 const mediump vec3 luminanceWeighting = vec3(0.2125, 0.7154, 0.0721);

 uniform lowp float gamma;

 
 uniform lowp float temperature;
 uniform lowp float tint;
 const lowp vec3 warmFilter = vec3(0.93, 0.54, 0.0);
 const mediump mat3 RGBtoYIQ = mat3(0.299, 0.587, 0.114, 0.596, -0.274, -0.322, 0.212, -0.523, 0.311);
 const mediump mat3 YIQtoRGB = mat3(1.0, 0.956, 0.621, 1.0, -0.272, -0.647, 1.0, -1.105, 1.702);

 // Hue Parameters
 uniform mediump float hueAdjust;
 const lowp  vec4  kRGBToYPrime = vec4 (0.299, 0.587, 0.114, 0.0);
 const lowp  vec4  kRGBToI     = vec4 (0.595716, -0.274453, -0.321263, 0.0);
 const lowp  vec4  kRGBToQ     = vec4 (0.211456, -0.522591, 0.31135, 0.0);
 
 const lowp  vec4  kYIQToR   = vec4 (1.0, 0.9563, 0.6210, 0.0);
 const lowp  vec4  kYIQToG   = vec4 (1.0, -0.2721, -0.6474, 0.0);
 const lowp  vec4  kYIQToB   = vec4 (1.0, -1.1070, 1.7046, 0.0);
 
 // GPUImageVignetteFilter.h
 uniform lowp vec2 vignetteCenter;
 uniform lowp vec3 vignetteColor;
 uniform highp float vignetteStart;
 uniform highp float vignetteEnd;

 
 
 void main()
 {
     lowp vec4 textureColor = texture2D(inputImageTexture, textureCoordinate);
     //gl_FragColor = vec4((textureColor.rgb + vec3(brightness)), textureColor.w);
     lowp vec4 textureBrightnes = vec4((textureColor.rgb + vec3(brightness)), textureColor.w); // brightness
     lowp vec4 textureContrast = vec4(((textureBrightnes.rgb - vec3(0.5)) * contrast + vec3(0.5)), textureBrightnes.w); // contrast
     lowp vec4 textureExposure = vec4(textureContrast.rgb * pow(2.0, exposure), textureContrast.w);  // exposure
     // Saturation
     lowp float luminance = dot(textureExposure.rgb, luminanceWeighting);
     lowp vec3 greyScaleColor = vec3(luminance);
     lowp vec4 textureSaturation = vec4(mix(greyScaleColor, textureExposure.rgb, saturation), textureExposure.w);
     lowp vec4 textureGama = vec4(pow(textureSaturation.rgb, vec3(gamma)), textureSaturation.w); // Gama Effect

     
     // White Balance Filter
     mediump vec3 yiq = RGBtoYIQ * textureGama.rgb; //adjusting tint
     yiq.b = clamp(yiq.b + tint*0.5226*0.1, -0.5226, 0.5226);
     lowp vec3 rgb = YIQtoRGB * yiq;
     
     lowp vec3 processed = vec3(
                                (rgb.r < 0.5 ? (2.0 * rgb.r * warmFilter.r) : (1.0 - 2.0 * (1.0 - rgb.r) * (1.0 - warmFilter.r))), //adjusting temperature
                                (rgb.g < 0.5 ? (2.0 * rgb.g * warmFilter.g) : (1.0 - 2.0 * (1.0 - rgb.g) * (1.0 - warmFilter.g))),
                                (rgb.b < 0.5 ? (2.0 * rgb.b * warmFilter.b) : (1.0 - 2.0 * (1.0 - rgb.b) * (1.0 - warmFilter.b))));
     
     lowp vec4 textureWhiteBalance = vec4(mix(rgb, processed, temperature), textureGama.a);
     
     
     // ***** Hue  ****
     // Sample the input pixel
     //lowp vec4 color   = texture2D(inputImageTexture, textureCoordinate);
     
     lowp vec4 color = textureWhiteBalance;
     
     // Convert to YIQ
     lowp float   YPrime  = dot (textureWhiteBalance, kRGBToYPrime);
     lowp float   I      = dot (textureWhiteBalance, kRGBToI);
     lowp float   Q      = dot (textureWhiteBalance, kRGBToQ);
     
     // Calculate the hue and chroma
     lowp float   hue     = atan (Q, I);
     lowp float   chroma  = sqrt (I * I + Q * Q);
     
     // Make the user's adjustments
     hue += (-hueAdjust); //why negative rotation?
     
     // Convert back to YIQ
     Q = chroma * sin (hue);
     I = chroma * cos (hue);
     
     // Convert back to RGB
     lowp vec4    yIQ   = vec4 (YPrime, I, Q, 0.0);
     color.r = dot (yIQ, kYIQToR);
     color.g = dot (yIQ, kYIQToG);
     color.b = dot (yIQ, kYIQToB);
     
     // Save the result
     // gl_FragColor = color;
     
     
     ///-*********** GPUImageVignetteFilter.h
     lowp vec4 sourceImageColor = texture2D(inputImageTexture, textureCoordinate);
     
     lowp float d = distance(textureCoordinate, vec2(vignetteCenter.x, vignetteCenter.y));
     lowp float percent = smoothstep(vignetteStart, vignetteEnd, d);
     gl_FragColor = vec4(mix(color.rgb, vignetteColor, percent), color.a);

     
 }
 );
// not For Mac 
/*#else
NSString *const kGPUImageBrightnessFragmentShaderString = SHADER_STRING
(
 varying vec2 textureCoordinate;
 
 uniform sampler2D inputImageTexture;
 uniform float brightness;
 
 void main()
 {
     vec4 textureColor = texture2D(inputImageTexture, textureCoordinate);
     
     gl_FragColor = vec4((textureColor.rgb + vec3(brightness)), textureColor.w);
 }
 );*/

#endif


@implementation GPUCustomColorControlFilter

@synthesize brightness = _brightness;
@synthesize contrast = _contrast;
@synthesize exposure = _exposure;
@synthesize saturation = _saturation;
@synthesize gamma = _gamma;
@synthesize temperature = _temperature;
@synthesize tint = _tint;
@synthesize hue;

@synthesize vignetteCenter = _vignetteCenter;
@synthesize vignetteColor = _vignetteColor;
@synthesize vignetteStart =_vignetteStart;
@synthesize vignetteEnd = _vignetteEnd;

#pragma mark -
#pragma mark Initialization and teardown

- (id)init;
{
    if (!(self = [super initWithFragmentShaderFromString:kGPUColorCustomControlFragmentShaderString]))
    {
        return nil;
    }
    
    brightnessUniform = [filterProgram uniformIndex:@"brightness"];
    self.brightness = 0.0;
    
    contrastUniform = [filterProgram uniformIndex:@"contrast"];
    self.contrast = 1.0;

    exposureUniform = [filterProgram uniformIndex:@"exposure"];
    self.exposure = 0.0;
    
    saturationUniform = [filterProgram uniformIndex:@"saturation"];
    self.saturation = 1.0;
    
    gammaUniform = [filterProgram uniformIndex:@"gamma"];
    self.gamma = 1.0;

    temperatureUniform = [filterProgram uniformIndex:@"temperature"];
    tintUniform = [filterProgram uniformIndex:@"tint"];
    self.temperature = 5000.0;
    self.tint = 0.0;
    
    hueAdjustUniform = [filterProgram uniformIndex:@"hueAdjust"];
    //self.hue = 90;
    self.hue = 0;

    vignetteCenterUniform = [filterProgram uniformIndex:@"vignetteCenter"];
    vignetteColorUniform = [filterProgram uniformIndex:@"vignetteColor"];
    vignetteStartUniform = [filterProgram uniformIndex:@"vignetteStart"];
    vignetteEndUniform = [filterProgram uniformIndex:@"vignetteEnd"];
    
    self.vignetteCenter = (CGPoint){ 0.5f, 0.5f };
    self.vignetteColor = (GPUVector3){ 0.0f, 0.0f, 0.0f };
//    self.vignetteStart = 0.3;
//    self.vignetteEnd = 0.75;
    
    self.vignetteStart = 1.0;
    self.vignetteEnd = 1.0;

    
    return self;
}

#pragma mark -
#pragma mark Accessors


- (void)setBrightness:(CGFloat)newValue;
{
    _brightness = newValue;
    
    [self setFloat:_brightness forUniform:brightnessUniform program:filterProgram];
}
- (void)setContrast:(CGFloat)newValue;
{
    _contrast = newValue;
    
    [self setFloat:_contrast forUniform:contrastUniform program:filterProgram];
}

- (void)setExposure:(CGFloat)newValue;
{
    _exposure = newValue;
    
    [self setFloat:_exposure forUniform:exposureUniform program:filterProgram];
}
- (void)setSaturation:(CGFloat)newValue;
{
    _saturation = newValue;
    
    [self setFloat:_saturation forUniform:saturationUniform program:filterProgram];
}

- (void)setTemperature:(CGFloat)newValue;
{
    _temperature = newValue;
    
    [self setFloat:_temperature < 5000 ? 0.0004 * (_temperature-5000.0) : 0.00006 * (_temperature-5000.0) forUniform:temperatureUniform program:filterProgram];
}

- (void)setTint:(CGFloat)newValue;
{
    _tint = newValue;
    
    [self setFloat:_tint / 100.0 forUniform:tintUniform program:filterProgram];
}
- (void)setGamma:(CGFloat)newValue;
{
    _gamma = newValue;
    
    [self setFloat:_gamma forUniform:gammaUniform program:filterProgram];
}
- (void)setHue:(CGFloat)newHue
{
    // Convert degrees to radians for hue rotation
    hue = fmodf(newHue, 360.0) * M_PI/180;
    [self setFloat:hue forUniform:hueAdjustUniform program:filterProgram];
}

- (void)setVignetteCenter:(CGPoint)newValue
{
    _vignetteCenter = newValue;
    
    [self setPoint:newValue forUniform:vignetteCenterUniform program:filterProgram];
}

- (void)setVignetteColor:(GPUVector3)newValue
{
    _vignetteColor = newValue;
    
    [self setVec3:newValue forUniform:vignetteColorUniform program:filterProgram];
}

- (void)setVignetteStart:(CGFloat)newValue;
{
    _vignetteStart = newValue;
    
    [self setFloat:_vignetteStart forUniform:vignetteStartUniform program:filterProgram];
}

- (void)setVignetteEnd:(CGFloat)newValue;
{
    _vignetteEnd = newValue;
    
    [self setFloat:_vignetteEnd forUniform:vignetteEndUniform program:filterProgram];
}

-(void)resetAllParameters
{
    self.brightness = 0.0;
    self.contrast = 1.0;
    self.exposure = 0.0;
    self.saturation = 1.0;
    self.gamma = 1.0;
    self.temperature = 5000.0;
    self.tint = 0.0;
    self.hue = 0;
    self.vignetteCenter = (CGPoint){ 0.5f, 0.5f };
    self.vignetteColor = (GPUVector3){ 0.0f, 0.0f, 0.0f };
    self.vignetteStart = 1.0;
    self.vignetteEnd = 1.0;
    
}


@end
