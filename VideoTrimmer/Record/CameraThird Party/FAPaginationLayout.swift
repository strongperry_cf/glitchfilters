//
//  FAFlowLayout.swift
//  FAPaginationLayout
//
//  Created by Fahid Attique on 14/06/2017.
//  Copyright © 2017 Fahid Attique. All rights reserved.
//

import UIKit


public class FAPaginationLayout: UICollectionViewFlowLayout {
    
    //  Class properties
    
    var insertingTopCells: Bool = false
    var sizeForTopInsertions: CGSize = CGSize.zero
    
    
    //  Preparing the layout
    
    override public func prepare() {
        
        super.prepare()

        let oldSize: CGSize = sizeForTopInsertions
        debugPrint("self.scrollDirection is :",self.scrollDirection);

        
        if insertingTopCells {
            
            if(self.scrollDirection == .horizontal)
            {
                let newSize: CGSize  = collectionViewContentSize
                let xOffset: CGFloat = collectionView!.contentOffset.x + (newSize.width - oldSize.width)
                let newOffset: CGPoint = CGPoint(x: xOffset, y: collectionView!.contentOffset.y)
                collectionView!.contentOffset = newOffset
            }
            else{
                let newSize: CGSize  = collectionViewContentSize
                let yOffset: CGFloat = collectionView!.contentOffset.y + (newSize.height - oldSize.height)
                let newOffset: CGPoint = CGPoint(x: collectionView!.contentOffset.x, y: yOffset)
                collectionView!.contentOffset = newOffset
            }
        }
        else {
            insertingTopCells = false;
        }

        sizeForTopInsertions = collectionViewContentSize
    }
    
    override public func targetContentOffset(forProposedContentOffset proposedContentOffset: CGPoint, withScrollingVelocity velocity: CGPoint) -> CGPoint {
        
        if let cv = self.collectionView {
            
            let cvBounds = cv.bounds
            
            if (self.scrollDirection == .horizontal)
            {
                let halfWidth = cvBounds.size.width * 0.5;
                let proposedContentOffsetCenterX = proposedContentOffset.x + halfWidth;
                
                if let attributesForVisibleCells = self.layoutAttributesForElements(in: cvBounds) {
                    
                    var candidateAttributes : UICollectionViewLayoutAttributes?
                    for attributes in attributesForVisibleCells {
                        
                        // == Skip comparison with non-cell items (headers and footers) == //
                        // if attributes.representedElementCategory != UICollectionElementCategory.cell
                        if attributes.representedElementCategory != UICollectionView.ElementCategory.cell
                        {
                            continue
                        }
                        
                        if let candAttrs = candidateAttributes {
                            let a = attributes.center.x - proposedContentOffsetCenterX
                            let b = candAttrs.center.x - proposedContentOffsetCenterX
                            
                            if fabsf(Float(a)) < fabsf(Float(b)) {
                                candidateAttributes = attributes;
                            }
                        }
                        else { // == First time in the loop == //
                            
                            candidateAttributes = attributes;
                            continue;
                        }
                    }
                    return CGPoint(x : candidateAttributes!.center.x - halfWidth, y : proposedContentOffset.y);
                    }
            }else
            {
                
                    let halfHeight = cvBounds.size.height * 0.5;
                    let proposedContentOffsetCenterY = proposedContentOffset.y + halfHeight;
                    
                    if let attributesForVisibleCells = self.layoutAttributesForElements(in: cvBounds) {
                        
                        var candidateAttributes : UICollectionViewLayoutAttributes?
                        for attributes in attributesForVisibleCells {
                            
                            // == Skip comparison with non-cell items (headers and footers) == //
                            if attributes.representedElementCategory != UICollectionView.ElementCategory.cell {
                                continue
                            }
                            
                            if let candAttrs = candidateAttributes {
                                let a = attributes.center.y - proposedContentOffsetCenterY
                                let b = candAttrs.center.y - proposedContentOffsetCenterY
                                
                                if fabsf(Float(a)) < fabsf(Float(b)) {
                                    candidateAttributes = attributes;
                                }
                            }
                            else { // == First time in the loop == //
                                
                                candidateAttributes = attributes;
                                continue;
                            }
                        }
                        //return CGPoint(x : candidateAttributes!.center.x - halfWidth, y : proposedContentOffset.y);
                        return CGPoint(x :proposedContentOffset.x, y : candidateAttributes!.center.y - halfHeight);
                    }
                
            }
        }
        
        
        // Fallback
        //return super.targetContentOffsetForProposedContentOffset(proposedContentOffset)
        //return CGPoint(x: firstAttribute.center.x - collectionView!.bounds.size.width * 0.5, y: firstAttribute.center.y - collectionView!.bounds.size.height * 0.5)
        return CGPoint(x : proposedContentOffset.x, y : proposedContentOffset.y);
    }
    
    /*
    //  configuring the content offsets relative to the scroll velocity
    
    override public func targetContentOffset(forProposedContentOffset proposedContentOffset: CGPoint, withScrollingVelocity velocity: CGPoint) -> CGPoint {
        
        debugPrint("proposedContentOffset is :",proposedContentOffset,"with Velocity :",velocity);
        
        var layoutAttributes: Array = layoutAttributesForElements(in: collectionView!.bounds)!
        
        if layoutAttributes.count == 0 {
            return proposedContentOffset
        }
        
        var firstAttribute: UICollectionViewLayoutAttributes = layoutAttributes[0]
        
        var change :Bool ;//=  false;
        
        debugPrint("self.scrollDirection is :",self.scrollDirection);
        if (self.scrollDirection == .horizontal)
        {
                for attribute: UICollectionViewLayoutAttributes in layoutAttributes {
                    
                    if attribute.representedElementCategory != .cell {
                        continue
                    }
                    
                    if((velocity.x > 0.0 && attribute.center.x > firstAttribute.center.x) ||
                        (velocity.x <= 0.0 && attribute.center.x < firstAttribute.center.x)) {
                        firstAttribute = attribute;
                    }
            }
            return CGPoint(x: firstAttribute.center.x - collectionView!.bounds.size.width * 0.5, y: proposedContentOffset.y)
        }
        else{
            
            for attribute: UICollectionViewLayoutAttributes in layoutAttributes {
                
                if attribute.representedElementCategory != .cell {
                    continue
                }
                
                if((velocity.y > 0.0 && attribute.center.y > firstAttribute.center.y) ||
                    (velocity.y <= 0.0 && attribute.center.y < firstAttribute.center.y)) {
                    change =  true;
                    firstAttribute = attribute;
                }
            }
            
            return CGPoint(x: proposedContentOffset.y, y: firstAttribute.center.y - collectionView!.bounds.size.height * 0.5)
        }
 
         //return CGPoint(x: firstAttribute.center.x - collectionView!.bounds.size.width * 0.5, y: proposedContentOffset.y)
        
        return CGPoint(x: firstAttribute.center.x - collectionView!.bounds.size.width * 0.5, y: firstAttribute.center.y - collectionView!.bounds.size.height * 0.5)
        
    }
    */
   
}
