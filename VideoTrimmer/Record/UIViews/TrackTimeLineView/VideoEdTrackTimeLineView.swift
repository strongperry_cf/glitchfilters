//
//  VideoEdTrackTimeLineView.swift
//  VideoTrimmer
//
//  Created by strongapps on 08/04/19.
//  Copyright © 2019 strongapps. All rights reserved.

import UIKit

class VideoEdTrackTimeLineView: UIView {
    
    @objc public var currentShapeLayer :CAShapeLayer? = nil
    @objc public var layerArray :NSMutableArray? = nil
    @objc public var lastProgress :CGFloat = 0.0
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        debugPrint("init(frame: CGRect)");
        createUI();
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func createUI()  {
        layerArray = NSMutableArray();
    }
    
    // func addLayer(progress:Float)
    @objc public func addLayer()
    {
        let chooseFrom = ["FF1744","FFFF00","FF6F00","0F9D58","CE93D8","00E676","00B0FF","76FF03","304FFE","E040FB"] //Add your colors here
        
        let screenHeight =  UIScreen.main.nativeBounds.height;
        let screenWidth =  UIScreen.main.nativeBounds.width;
        var newShapeLayer = CAShapeLayer();
        newShapeLayer.backgroundColor = UIColor.clear.cgColor;
        newShapeLayer.fillColor = UIColor.clear.cgColor;
        newShapeLayer.borderColor = UIColor.clear.cgColor;
        newShapeLayer.shadowColor = UIColor.clear.cgColor;
        newShapeLayer.masksToBounds = true;
        
        var notchPath  = createNotchBezierPath();
        notchPath.usesEvenOddFillRule =  true;
        
        newShapeLayer.frame  = CGRect(x: 0, y: 0, width: screenWidth/2.0, height: 40)
        newShapeLayer.path = notchPath.cgPath
        // newShapeLayer.strokeColor = UIColor.red.cgColor;
        // newShapeLayer.strokeColor = UIColor.randomHexColor().cgColor;
        
        let index:Int = (layerArray?.count)! % chooseFrom.count;
        let hexStr = chooseFrom[index];
        
        newShapeLayer.strokeColor = UIColor.hexStringToUIColor(hex: hexStr).cgColor ;
        
        // newShapeLayer.lineWidth = 14.0;
        newShapeLayer.lineWidth = 8.0;
        newShapeLayer.lineCap = CAShapeLayerLineCap.round;
        //        newShapeLayer.lineJoin = CAShapeLayerLineJoin.round
        
        newShapeLayer.strokeEnd = lastProgress;
        // newShapeLayer.strokeEnd = 1.0;
        
        // self.layer.addSublayer(newShapeLayer);
        layerArray?.add(newShapeLayer as Any);
        self.layer.insertSublayer(newShapeLayer, at: 0);
        currentShapeLayer = newShapeLayer ;
     
        
        
    }
    
    func createNotchBezierPath () -> UIBezierPath {
        
        let modalStr:String = UIDevice.modelName
        debugPrint("str is ", modalStr);
        let bezierPath = UIBezierPath()
        
        if ( modalStr == "iPhone X" || modalStr == "iPhone XS" ) // X
        {
            bezierPath.move(to: CGPoint(x: 12.5, y: 12.5))
            bezierPath.addCurve(to: CGPoint(x: 40, y: 0.5), controlPoint1: CGPoint(x: 21.88, y: 2.93), controlPoint2: CGPoint(x: 40, y: 0.5))
            bezierPath.addLine(to: CGPoint(x: 60.5, y: 0.5))
            bezierPath.addLine(to: CGPoint(x: 77, y: 0.0))
            bezierPath.addCurve(to: CGPoint(x: 81.5, y: 2.5), controlPoint1: CGPoint(x: 77, y: 0.0), controlPoint2: CGPoint(x: 80, y: 1.13))
            bezierPath.addCurve(to: CGPoint(x: 83, y: 6), controlPoint1: CGPoint(x: 83, y: 3.87), controlPoint2: CGPoint(x: 83, y: 6))
            bezierPath.addLine(to: CGPoint(x: 83, y: 10))
            //        bezierPath.addCurve(to: CGPoint(x: 87.5, y: 25.5), controlPoint1: CGPoint(x: 83, y: 10), controlPoint2: CGPoint(x: 83.5, y: 20.5))
            bezierPath.addCurve(to: CGPoint(x: 89, y: 24), controlPoint1: CGPoint(x: 83, y: 10), controlPoint2: CGPoint(x: 83.5, y: 20.5))
            bezierPath.addCurve(to: CGPoint(x: 101.5, y: 30), controlPoint1: CGPoint(x: 91.5, y: 30.5), controlPoint2: CGPoint(x: 101.5, y: 30))
            bezierPath.addLine(to: CGPoint(x: 272, y: 30))
            //        bezierPath.addCurve(to: CGPoint(x: 287.5, y: 25.5), controlPoint1: CGPoint(x: 272, y: 30), controlPoint2: CGPoint(x: 282.5, y: 30.5))
            bezierPath.addCurve(to: CGPoint(x: 286, y: 24.0), controlPoint1: CGPoint(x: 272, y: 30), controlPoint2: CGPoint(x: 282.5, y: 30.5))
            bezierPath.addCurve(to: CGPoint(x: 292, y: 10), controlPoint1: CGPoint(x: 292.5, y: 20.5), controlPoint2: CGPoint(x: 292, y: 10))
            bezierPath.addLine(to: CGPoint(x: 292, y: 6))
            bezierPath.addCurve(to: CGPoint(x: 293.5, y: 0.0), controlPoint1: CGPoint(x: 292, y: 6), controlPoint2: CGPoint(x: 292, y: 2.99))
            bezierPath.addCurve(to: CGPoint(x: 298, y: 0.0), controlPoint1: CGPoint(x: 295, y: 0.01), controlPoint2: CGPoint(x: 298, y: 0.03))
            bezierPath.addLine(to: CGPoint(x: 335, y: 0.0))
            bezierPath.addCurve(to: CGPoint(x: 363.5, y: 12.5), controlPoint1: CGPoint(x: 335, y: 0.0), controlPoint2: CGPoint(x: 354.75, y: 2.68))
        }
        else if modalStr == "Simulator iPhone XR" || modalStr == "iPhone XR"
        {
            let yT = 0.0;
            /*
            bezierPath.move(to: CGPoint(x: 11, y: 11 + yT))
            bezierPath.addCurve(to: CGPoint(x: 40, y: 0.5 + yT), controlPoint1: CGPoint(x: 18.61, y: 2.0 + yT), controlPoint2: CGPoint(x: 27.45, y: 0.5 + yT))
            bezierPath.addCurve(to: CGPoint(x: 77, y: -0 + yT), controlPoint1: CGPoint(x: 60, y: 0.5 + yT), controlPoint2: CGPoint(x: 77, y: -0 + yT))
            bezierPath.addCurve(to: CGPoint(x: 83, y: 6 + yT), controlPoint1: CGPoint(x: 77, y: -0 + yT), controlPoint2: CGPoint(x: 83, y: 0 + yT))
            bezierPath.addCurve(to: CGPoint(x: 111.5, y: 30 + yT), controlPoint1: CGPoint(x: 83, y: 12 + yT), controlPoint2: CGPoint(x: 82.5, y: 30 + yT))
            bezierPath.addCurve(to: CGPoint(x: 272, y: 30 + yT), controlPoint1: CGPoint(x: 140.5, y: 30 + yT), controlPoint2: CGPoint(x: 272, y: 30 + yT))
            
            bezierPath.addCurve(to: CGPoint(x: 286, y: 24.0 + yT), controlPoint1: CGPoint(x: 272, y: 30 + yT), controlPoint2: CGPoint(x: 282.5, y: 30.5 + yT))
            bezierPath.addCurve(to: CGPoint(x: 292, y: 10 + yT), controlPoint1: CGPoint(x: 292.5, y: 20.5 + yT), controlPoint2: CGPoint(x: 292, y: 10 + yT))
            bezierPath.addLine(to: CGPoint(x: 292, y: 6 + yT))
            bezierPath.addCurve(to: CGPoint(x: 293.5, y: 0.0 + yT), controlPoint1: CGPoint(x: 292, y: 6 + yT), controlPoint2: CGPoint(x: 292, y: 2.99 + yT))
            bezierPath.addCurve(to: CGPoint(x: 298, y: 0.0 + yT), controlPoint1: CGPoint(x: 295, y: 0.01 + yT), controlPoint2: CGPoint(x: 298, y: 0.03 + yT))
            bezierPath.addLine(to: CGPoint(x: 335, y: 0.0 + yT))
            bezierPath.addCurve(to: CGPoint(x: 362.0, y: 11.0 + yT), controlPoint1: CGPoint(x: 335, y: 0.0 + yT), controlPoint2: CGPoint(x: 354.75, y: 2.68 + yT))
            */
            
            bezierPath.move(to: CGPoint(x: 11, y: 13 + yT))
            bezierPath.addCurve(to: CGPoint(x: 40, y: 0.5 + yT), controlPoint1: CGPoint(x: 18.61, y: 3.0 + yT), controlPoint2: CGPoint(x: 27.45, y: 3.0 + yT))
            bezierPath.addCurve(to: CGPoint(x: 83 + 8, y: -0 + yT), controlPoint1: CGPoint(x: 68 + 8, y: 0.5 + yT), controlPoint2: CGPoint(x: 83 + 8, y: -0 + yT))

            bezierPath.addCurve(to: CGPoint(x: 83 + 8, y: 6 + yT), controlPoint1: CGPoint(x: 77 + 8, y: 0 + yT), controlPoint2: CGPoint(x: 83 + 8, y: 0 + yT))
            bezierPath.addCurve(to: CGPoint(x: 111.5, y: 33 + yT), controlPoint1: CGPoint(x: 93, y: 12 + yT), controlPoint2: CGPoint(x: 93, y: 29 + yT))
            bezierPath.addCurve(to: CGPoint(x: 272, y: 33 + yT), controlPoint1: CGPoint(x: 140.5, y: 33 + yT), controlPoint2: CGPoint(x: 272, y: 33 + yT))
            
            bezierPath.addCurve(to: CGPoint(x: 286 + 30, y: 24.0 + yT), controlPoint1: CGPoint(x: 272 + 30, y: 33 + yT), controlPoint2: CGPoint(x: 282.5 + 30, y: 31 + yT))
            
            bezierPath.addCurve(to: CGPoint(x: 292 + 30, y: 10 + yT), controlPoint1: CGPoint(x: 292.5 + 30, y: 16 + yT), controlPoint2: CGPoint(x: 292 + 30, y: 10 + yT))
            bezierPath.addLine(to: CGPoint(x: 292 + 30, y: 6 + yT))
            
            bezierPath.addCurve(to: CGPoint(x: 293.5 + 30, y: 0.0 + yT), controlPoint1: CGPoint(x: 292 + 30, y: 6 + yT), controlPoint2: CGPoint(x: 292 + 30, y: 2.99 + yT))
            bezierPath.addCurve(to: CGPoint(x: 298 + 30 + 2, y: 0.0 + yT), controlPoint1: CGPoint(x: 295 + 30 + 2, y: 0.01 + yT), controlPoint2: CGPoint(x: 298 + 30 + 2, y: 0.03 + yT))
            
            bezierPath.addLine(to: CGPoint(x: 335 + 38, y: 0.0))
            bezierPath.addCurve(to: CGPoint(x: 362.0 + 38, y: 11.0), controlPoint1: CGPoint(x: 335 + 38, y: 0.0), controlPoint2: CGPoint(x: 354.75 + 38, y: 2.68))

        }
        else if  modalStr == "Simulator iPhone XS Max" || modalStr == "iPhone XS Max"
        {
            let yT = 0.0;
            let xT = 0.0 ;
            
            // orginal
            /*
            bezierPath.move(to: CGPoint(x: 11, y: 11 + yT))
            
            bezierPath.addCurve(to: CGPoint(x: 40, y: 0.5 + yT), controlPoint1: CGPoint(x: 18.61, y: 2.0 + yT), controlPoint2: CGPoint(x: 27.45, y: 0.5 + yT))
            bezierPath.addCurve(to: CGPoint(x: 77 + xT, y: -0 + yT), controlPoint1: CGPoint(x: 60 + xT, y: 0.5 + yT), controlPoint2: CGPoint(x: 77 + xT, y: -0 + yT))
            bezierPath.addCurve(to: CGPoint(x: 77 + xT, y: -0 + yT), controlPoint1: CGPoint(x: 60 + xT, y: 0.5 + yT), controlPoint2: CGPoint(x: 77 + xT, y: -0 + yT))
            
            bezierPath.addCurve(to: CGPoint(x: 83 + xT, y: 6 + yT), controlPoint1: CGPoint(x: 77 + xT, y: -0 + yT), controlPoint2: CGPoint(x: 83 + xT, y: 0 + yT))
            bezierPath.addCurve(to: CGPoint(x: 111.5 + xT, y: 30 + yT), controlPoint1: CGPoint(x: 83 + xT, y: 12 + yT), controlPoint2: CGPoint(x: 82.5 + xT, y: 30 + yT))
            
            bezierPath.addCurve(to: CGPoint(x: 272 - xT, y: 30 + yT), controlPoint1: CGPoint(x: 140.5 - xT, y: 30 + yT), controlPoint2: CGPoint(x: 272 - xT, y: 30 + yT))
            bezierPath.addCurve(to: CGPoint(x: 286 - xT, y: 24.0 + yT), controlPoint1: CGPoint(x: 272 - xT, y: 30 + yT), controlPoint2: CGPoint(x: 282.5 - xT, y: 30.5 + yT))
            
            //        bezierPath.addCurve(to: CGPoint(x: 287.5, y: 25.5), controlPoint1: CGPoint(x: 272, y: 30), controlPoint2: CGPoint(x: 282.5, y: 30.5))
            //            bezierPath.addCurve(to: CGPoint(x: 286, y: 24.0), controlPoint1: CGPoint(x: 272, y: 30), controlPoint2: CGPoint(x: 282.5, y: 30.5))
            
            bezierPath.addCurve(to: CGPoint(x: 292 - xT, y: 10), controlPoint1: CGPoint(x: 292.5 - xT, y: 20.5), controlPoint2: CGPoint(x: 292 - xT, y: 10))
            bezierPath.addLine(to: CGPoint(x: 292 - xT, y: 6))
            bezierPath.addCurve(to: CGPoint(x: 293.5 - xT, y: 0.0), controlPoint1: CGPoint(x: 292 - xT, y: 6), controlPoint2: CGPoint(x: 292 - xT, y: 2.99))
            bezierPath.addCurve(to: CGPoint(x: 298 - xT, y: 0.0), controlPoint1: CGPoint(x: 295 - xT, y: 0.01), controlPoint2: CGPoint(x: 298 - xT, y: 0.03))
            bezierPath.addLine(to: CGPoint(x: 335, y: 0.0))
            bezierPath.addCurve(to: CGPoint(x: 362.0, y: 11.0), controlPoint1: CGPoint(x: 335, y: 0.0), controlPoint2: CGPoint(x: 354.75, y: 2.68))
            
            */
            
            bezierPath.move(to: CGPoint(x: 11, y: 11 + yT+1))
            
            bezierPath.addCurve(to: CGPoint(x: 40, y: 0.5 + yT), controlPoint1: CGPoint(x: 18.61, y: 2.0 + yT), controlPoint2: CGPoint(x: 27.45, y: 0.5 + yT))
            bezierPath.addCurve(to: CGPoint(x: 85 + xT, y: -0 + yT), controlPoint1: CGPoint(x: 68 + xT, y: 0.5 + yT), controlPoint2: CGPoint(x: 85 + xT, y: -0 + yT))
            bezierPath.addCurve(to: CGPoint(x: 85 + xT, y: -0 + yT), controlPoint1: CGPoint(x: 68 + xT, y: 0.5 + yT), controlPoint2: CGPoint(x: 85 + xT, y: -0 + yT))
            
            // change
            bezierPath.addCurve(to: CGPoint(x: 91 + 10 + xT, y: 6 + yT), controlPoint1: CGPoint(x: 85 + xT + 10, y: -0 + yT), controlPoint2: CGPoint(x: 91 + xT + 10, y: 0 + yT))
            bezierPath.addCurve(to: CGPoint(x: 119.5 + xT, y: 30 + yT), controlPoint1: CGPoint(x: 91 + xT + 10, y: 12 + yT), controlPoint2: CGPoint(x: 90.5 + xT + 10, y: 25 + yT))
            
            
            bezierPath.addCurve(to: CGPoint(x: 272 + 20 + xT, y: 30 + yT), controlPoint1: CGPoint(x: 140.5 + 20 + xT, y: 30 + yT), controlPoint2: CGPoint(x: 272 + xT + 20, y: 30 + yT))
            bezierPath.addCurve(to: CGPoint(x: 286 + xT+20, y: 24.0 + yT), controlPoint1: CGPoint(x: 272 + xT+20, y: 30 + yT), controlPoint2: CGPoint(x: 282.5 + xT+20, y: 30.5 + yT))
            
            bezierPath.addCurve(to: CGPoint(x: 293 + xT + 20, y: 10), controlPoint1: CGPoint(x: 293 + xT + 18, y: 20.5), controlPoint2: CGPoint(x: 293 + xT + 20, y: 10)) //
            
            bezierPath.addLine(to: CGPoint(x: 293 + xT + 20, y: 6))
            bezierPath.addCurve(to: CGPoint(x: 293.5 + xT + 20, y: 0.0), controlPoint1: CGPoint(x: 293 + xT + 20 , y: 6), controlPoint2: CGPoint(x: 293 + xT + 20, y: 2.99))
            
            bezierPath.addCurve(to: CGPoint(x: 298 + xT + 38, y: 0.0), controlPoint1: CGPoint(x: 295 + xT + 38, y: 0.01), controlPoint2: CGPoint(x: 298 + xT + 38, y: 0.03))
            bezierPath.addLine(to: CGPoint(x: 335 + 38, y: 0.0))
            bezierPath.addCurve(to: CGPoint(x: 362.0 + 38, y: 11.0), controlPoint1: CGPoint(x: 335 + 38, y: 0.0), controlPoint2: CGPoint(x: 354.75 + 38, y: 2.68))
            
        }
        else
        {
            bezierPath.move(to: CGPoint(x: 12.5, y: 5))
            bezierPath.addLine(to: CGPoint(x: CGFloat(SCREEN_WIDTH) - 12.5, y: 5))
        }
        
        return bezierPath;
    }
    
}

extension UIColor {
    static var random: UIColor {
        return UIColor(red: .random(in: 0...1),
                       green: .random(in: 0...1),
                       blue: .random(in: 0...1),
                       alpha: 1.0)
    }
    
    
    static func hexStringToUIColor (hex:String) -> UIColor {
        
        var colorString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (colorString.hasPrefix("#")) {
            colorString.remove(at: colorString.startIndex)
        }
        
        if ((colorString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: colorString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    static func randomHexColor() -> UIColor {
        
        let chooseFrom = ["FFFF00","FF6F00","0F9D58","FF6F00","FFFF00","CE93D8","FF4081","90CAF9","E6EE9C"] //Add your colors here
        let hexStr = chooseFrom.randomElement();
        let color:UIColor
        color =  UIColor.hexStringToUIColor(hex: hexStr!);
        
        return color;
        
    }
}

public extension UIDevice {
    
    static let modelName: String = {
        var systemInfo = utsname()
        uname(&systemInfo)
        let machineMirror = Mirror(reflecting: systemInfo.machine)
        let identifier = machineMirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8, value != 0 else { return identifier }
            return identifier + String(UnicodeScalar(UInt8(value)))
        }
        
        func mapToDevice(identifier: String) -> String { // swiftlint:disable:this cyclomatic_complexity
            #if os(iOS)
            switch identifier {
            case "iPod5,1":                                 return "iPod Touch 5"
            case "iPod7,1":                                 return "iPod Touch 6"
            case "iPhone3,1", "iPhone3,2", "iPhone3,3":     return "iPhone 4"
            case "iPhone4,1":                               return "iPhone 4s"
            case "iPhone5,1", "iPhone5,2":                  return "iPhone 5"
            case "iPhone5,3", "iPhone5,4":                  return "iPhone 5c"
            case "iPhone6,1", "iPhone6,2":                  return "iPhone 5s"
            case "iPhone7,2":                               return "iPhone 6"
            case "iPhone7,1":                               return "iPhone 6 Plus"
            case "iPhone8,1":                               return "iPhone 6s"
            case "iPhone8,2":                               return "iPhone 6s Plus"
            case "iPhone9,1", "iPhone9,3":                  return "iPhone 7"
            case "iPhone9,2", "iPhone9,4":                  return "iPhone 7 Plus"
            case "iPhone8,4":                               return "iPhone SE"
            case "iPhone10,1", "iPhone10,4":                return "iPhone 8"
            case "iPhone10,2", "iPhone10,5":                return "iPhone 8 Plus"
            case "iPhone10,3", "iPhone10,6":                return "iPhone X"
            case "iPhone11,2":                              return "iPhone XS"
            case "iPhone11,4", "iPhone11,6":                return "iPhone XS Max"
            case "iPhone11,8":                              return "iPhone XR"
            case "iPad2,1", "iPad2,2", "iPad2,3", "iPad2,4":return "iPad 2"
            case "iPad3,1", "iPad3,2", "iPad3,3":           return "iPad 3"
            case "iPad3,4", "iPad3,5", "iPad3,6":           return "iPad 4"
            case "iPad4,1", "iPad4,2", "iPad4,3":           return "iPad Air"
            case "iPad5,3", "iPad5,4":                      return "iPad Air 2"
            case "iPad6,11", "iPad6,12":                    return "iPad 5"
            case "iPad7,5", "iPad7,6":                      return "iPad 6"
            case "iPad2,5", "iPad2,6", "iPad2,7":           return "iPad Mini"
            case "iPad4,4", "iPad4,5", "iPad4,6":           return "iPad Mini 2"
            case "iPad4,7", "iPad4,8", "iPad4,9":           return "iPad Mini 3"
            case "iPad5,1", "iPad5,2":                      return "iPad Mini 4"
            case "iPad6,3", "iPad6,4":                      return "iPad Pro (9.7-inch)"
            case "iPad6,7", "iPad6,8":                      return "iPad Pro (12.9-inch)"
            case "iPad7,1", "iPad7,2":                      return "iPad Pro (12.9-inch) (2nd generation)"
            case "iPad7,3", "iPad7,4":                      return "iPad Pro (10.5-inch)"
            case "iPad8,1", "iPad8,2", "iPad8,3", "iPad8,4":return "iPad Pro (11-inch)"
            case "iPad8,5", "iPad8,6", "iPad8,7", "iPad8,8":return "iPad Pro (12.9-inch) (3rd generation)"
            case "AppleTV5,3":                              return "Apple TV"
            case "AppleTV6,2":                              return "Apple TV 4K"
            case "AudioAccessory1,1":                       return "HomePod"
            case "i386", "x86_64":                          return "Simulator \(mapToDevice(identifier: ProcessInfo().environment["SIMULATOR_MODEL_IDENTIFIER"] ?? "iOS"))"
            default:                                        return identifier
            }
            #elseif os(tvOS)
            switch identifier {
            case "AppleTV5,3": return "Apple TV 4"
            case "AppleTV6,2": return "Apple TV 4K"
            case "i386", "x86_64": return "Simulator \(mapToDevice(identifier: ProcessInfo().environment["SIMULATOR_MODEL_IDENTIFIER"] ?? "tvOS"))"
            default: return identifier
            }
            #endif
        }
        
        return mapToDevice(identifier: identifier)
    }()
    
}
