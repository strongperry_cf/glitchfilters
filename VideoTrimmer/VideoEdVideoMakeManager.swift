//
//  VideoEdVideoMakeManager.swift
//  VideoTrimmer
//
//  Created by strongapps on 04/04/19.
//  Copyright © 2019 strongapps. All rights reserved.
//

import UIKit

enum EDITOR_MODE {
    case VIDEOEDITOR
    case SLIDESHOW
    case CAMERA
    case SETTINGS
    case REACTCAMERA
}
enum SLIDESHOWMENUMODE {
    case LIST
    case DURATION
    case MUSIC
    case IMAGESLIST
}
class VideoEdVideoMakeManager: NSObject {

    static let shared = VideoEdVideoMakeManager()

    static var currentMenuMode  : SLIDESHOWMENUMODE = .LIST
    static var currentEditorMode  : EDITOR_MODE = .VIDEOEDITOR
    static var durationForOnePic : CGFloat = 2
//    static var contentsGravity : CALayerContentsGravity = .resizeAspect // preview working for some
    static var contentsGravity  : CALayerContentsGravity = .resizeAspectFill


    static var audioUrl         : URL! = Bundle.main.url(forResource: "SampleAudio", withExtension: "mp3")!
    static var audioStartTime   : CGFloat = 0.0
    static var audioEndTime     : CGFloat = 0.0
    static var repeatAudio      : Bool = false
    
    static var someAudioChanges      : Bool = false

    static var silenceAudioURL: URL {
        get {
            return URL(fileURLWithPath: Bundle.main.path(forResource: "silenceAudio", ofType: "mp3")!)
        }
    }
    func cleanDocumentDirectory(){
        
        let folderPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        
        var _: Error? = nil
        do {
            for file in try FileManager.default.contentsOfDirectory(atPath: folderPath) {
                
                if file.contains("mp4") || file.contains("mov"){
                    debugPrint("file to remove-<> ", file)

                    try FileManager.default.removeItem(atPath: URL(fileURLWithPath: folderPath).appendingPathComponent(file).absoluteString)
                    
                }
            }
        } catch {
            debugPrint(error)

        }

    }


}
