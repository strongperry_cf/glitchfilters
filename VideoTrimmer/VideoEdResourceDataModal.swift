
import UIKit
import CoreMedia

// ************************************************************************************************************** //
// *** This Model Is Used for Store CALayer Items ,Mute,Audio Trim, The Speed and Time Slots for Create Slomo Effect on Video *** //
// ************************************************************************************************************** //

@objcMembers class VideoSpeedCheckPoints: NSObject{
    var speed:Float = 1.0 ;
    var timePoint:CMTime = CMTime.zero;
}

/*
@objcMembers class VideoWithSpeed: NSObject{
    var videoSpeed:Float = 1.0 ;
    var videoUrl        :URL? = nil;
    var timePoint:CMTime = CMTime.zero;
    
    init(url:URL,speed:Float) {
        videoUrl = url;
        videoSpeed = speed;
    }
}
*/

@objcMembers class VideoData: NSObject {
    
    var videoUrl        :URL? = nil
    var trimStartTime   :CMTime = CMTime.zero
    var trimEndTime     :CMTime = CMTime.zero
    var transitionType  :Int  = 0
    var videoIndex      :Int = 0
    var volume          :Float = 1
    var thumbnailImage  :UIImage? = nil
    var videoSpeed      :Float = 1.0 ;
    var timePoint       :CMTime = CMTime.zero;

    // var trimStartTime   :CMTime? = CMTimeMake(4, 1);
    // var trimEndTime     :CMTime? = CMTimeMake(8, 1);

    //*****************************************//
    //** Mute is working on Inbuild Audio ******//
    //*****************************************//
    
    //var muteStart       :CMTime? = CMTime.zero;
    //var muteDuration    :CMTime? = CMTime.zero;
    
    var muteStart       :CMTime? = CMTime.zero;
    var muteDuration    :CMTime? = CMTime.zero;
    
    
    var rotation        :Float = 0.0;
    var vFlip           :Bool  = false;
    var hFlip           :Bool  = false;
    
    //*****************************************//
    //** Yet not use , but use in Future ******//
    //*****************************************//

    var brightness      :Float?;
    var constrast       :Float?;
    var temperature     :Float?;
    var saturation      :Float?;
    var sharpness       :Float?;
    var tint            :Float?;
    
    var vignetteStart   :Float?;
    var vignetteEnd     :Float?;
    var vignetteColor   :UIColor?;
    var vignetteCenter  :UIColor?;
    var collageType     :Int = 0;
    
    init(url:URL) {
        videoUrl = url;
    }
}


// struct AudioData
 @objcMembers class AudioData: NSObject{

    var audioUrl :URL?  =  nil;
    var audioAddingTime:CMTime? = CMTime.zero;
    var trimStartTime:CMTime?  = CMTime.zero;
    var trimEndTime:CMTime? = CMTime.zero;
    var repeatMusic = false
    init(url:URL) {
        self.audioUrl = url
    }
}

// struct TextData
 @objcMembers class TextData: NSObject
 {
    var identifier      :String? = "text";
    var string          :String = "";

    var fontColor       :UIColor    = UIColor.white;
    var fontColorIndex  : Int    = 0
    var fontName        :String    = FONT_SEMIBOLD
    var fontNameIndex  : Int    = 0

    var fontSize        :CGFloat      = 30.0;
    var zAngle          :CGFloat      = 0.0;
    var transform       :CATransform3D = CATransform3DIdentity;
    var centerPt        :CGPoint    = CGPoint(x: 0.0, y: 0.0);
    var size            :CGSize     = CGSize(width: 0.0, height: 0.0);
    var opacity         :Float      = 1.0;
    
    // shadow
    var shadowColor     :UIColor    = UIColor.black ;
    var shadowColorIndex: Int    = 0
    var shadowOffset    :CGSize     = CGSize(width: 0.0, height: 0.0);
    var shadowRadius    :CGFloat    = 5.0;
    var shadowAlpha     :Float      = 0.0;
    var scale           :CGFloat    = 1.0;
    var editable        :Bool       = true;
    
    //Animation
    var animationIN_Type:Int        = 0;
    var animationOut_Type:Int       = 0;
    var startTime       :CGFloat = 0.0;   // animation IN Duration is 0.3;
    var duration        :CGFloat = 5.0;   // animation Out Duration is 0.3.
        
    // is edited check Point
    var fontEdited      :Bool       = false;
    var colorEdited     :Bool       = false;
    var animationEdited :Bool       = false;
    var shadowEdited    :Bool       = false;
    var blendFilter     :String     = "normalBlendMode";

    init(_ text:String) {
        self.string = text
    }
}


//struct StickerData
 @objcMembers class StickerData: NSObject
 {
    var identifier      :String? = "sticker";
    var image           :UIImage? = nil;
    var startTime       :CGFloat  = 0;
    var duration        :CGFloat  = 5;
    var zAngle          :CGFloat    = 0.0;
    var transform       :CATransform3D  = CATransform3DIdentity;
    var centerPt        :CGPoint        = CGPoint(x: 0.0, y: 0.0);
    var size            :CGSize         = CGSize(width: 0.0, height: 0.0);
    var opacity         :Float          = 1.0;
    var vFlip           :Bool  = false;
    var hFlip           :Bool  = false;
    var scale           :CGFloat  = 1.0;
    var editable        :Bool       = true;
    var blendFilter     :String     = "normalBlendMode";
    var shadowColor     :UIColor!   = UIColor.clear
    
 // deleted
 //    var fontSize: CGFloat!
 //    var fontName: String!
 //    var text: String!
 //    var placeHolderText: String!

    init( image:UIImage) {
        self.image = image
    }
    
 }


// struct ImojiData
@objcMembers class ImojiData: NSObject
 {
    var identifier      :String? = "Imoji";
    var image           :UIImage? = nil;
    //var startTime       :CMTime? = CMTime.zero;
    //var duration        :CMTime? = CMTime.zero;
    var startTime       :CGFloat = 0.0;
    var duration        :CGFloat = 5.0;
    var zAngle          :CGFloat   = 0.0;
    var transform       :CATransform3D = CATransform3DIdentity;
    var centerPt        :CGPoint        = CGPoint(x: 0.0, y: 0.0);
    var size            :CGSize         = CGSize(width: 0.0, height: 0.0);
    var opacity         :Float          = 1.0;
    var scale           :CGFloat  = 1.0;
    var editable        :Bool       = true;
    var blendFilter     :String     = "normalBlendMode";
}

//struct ActionFxData
@objcMembers class ActionFxData: NSObject
{
    var identifier      :String? = "ActionFx";
    var url             :URL?  = nil;
    var thumbnail       :UIImage? = nil;
    //var startTime       :CMTime?  = CMTime.zero;
    //var duration        :CMTime?  = CMTime.zero;
    var startTime       :CGFloat = 0.0;
    var duration        :CGFloat = 10.0;
    var zAngle          :CGFloat = 0.0;
    var transform       :CATransform3D  = CATransform3DIdentity;
    var centerPt        :CGPoint        = CGPoint(x: 0.0, y: 0.0);
    var size            :CGSize         = CGSize(width: 0.0, height: 0.0);
    var opacity         :Float          = 1.0;
    var scale           :CGFloat  = 1.0;
    var editable        :Bool       = true;
    var blendFilter     :String     = "normalBlendMode";
    var vFlip           :Bool  = false;
    var hFlip           :Bool  = false;
    
    init( thumbnail:UIImage , url :URL) {
        self.thumbnail = thumbnail
        self.url = url
    }
}


@objcMembers class StickerGifData: NSObject // Gif and Giphy
{
    var identifier      :String? = "stickergif";
    var url             :URL?  = nil;
    var thumbnail       :UIImage? = nil;
    //var startTime       :CMTime?  = CMTime.zero;
    //var duration        :CMTime?  = CMTime.zero;
    var startTime       :CGFloat = 0.0;
    var duration        :CGFloat = 5.0;
    var zAngle          :CGFloat = 0.0;
    var transform       :CATransform3D  = CATransform3DIdentity;
    var centerPt        :CGPoint        = CGPoint(x: 0.0, y: 0.0);
    var size            :CGSize         = CGSize(width: 0.0, height: 0.0);
    var opacity         :Float          = 1.0;
    var scale           :CGFloat  = 1.0;
    var editable        :Bool       = true;
    var blendFilter     :String     = "normalBlendMode";
    var vFlip           :Bool  = false;
    var hFlip           :Bool  = false;
    
    init( thumbnail:UIImage , url :URL) {
        self.thumbnail = thumbnail
        self.url = url
    }
}


@objcMembers class VideoGifData: NSObject // Gif and Giphy
{
    var identifier      :String? = "videogif";
    var url             :URL?  = nil;
    var thumbnail       :UIImage? = nil;
    //var startTime       :CMTime?  = CMTime.zero;
    //var duration        :CMTime?  = CMTime.zero;
    var startTime       :CGFloat = 0.0;
    var duration        :CGFloat = 5.0;
    var zAngle          :CGFloat = 0.0;
    var transform       :CATransform3D  = CATransform3DIdentity;
    var centerPt        :CGPoint        = CGPoint(x: 0.0, y: 0.0);
    var size            :CGSize         = CGSize(width: 0.0, height: 0.0);
    var opacity         :Float          = 1.0;
    var scale           :CGFloat  = 1.0;
    var editable        :Bool       = true;
    var blendFilter     :String     = "normalBlendMode";
    var vFlip           :Bool  = false;
    var hFlip           :Bool  = false;
    
    init( thumbnail:UIImage , url :URL) {
        self.thumbnail = thumbnail
        self.url = url
    }
}

////-*-*-*-*-*-*-*-*

// Blur BackGround
// Blur Effect
// BackGround Color
// FX Filters.

////-*-*-*-*-*-*-*-*

// var videoArray      : Array<VideoData>?;
// var audioArray      : Array<AudioData>?;

// var itemsArray      : Array<Any>?;

//var textArray       : Array<TextData>?;
//var stickerArray    : Array<StickerData>?;
//var imojiArray      : Array<ImojiData>?;
//var actionFXArray   : Array<ActionFxData>?;



@objcMembers class VideoEdResourceDataModal: NSObject {

    public var tag :Int = 0;
    //var actionsData:ActionFxData?

    /*
    var videoDataArray      : [VideoData]? ;
    var audioDataArray      : [AudioData]?;
    var itemsDataArray      : [Any]?;
    */
    
    var videoDataArray      : NSMutableArray? = [];// NSMutableArray();
    var audioDataArray      : NSMutableArray? = [];// NSMutableArray();
    var itemsDataArray      : NSMutableArray? = [];// NSMutableArray();

    /*
    var videoDataArray      : Array<VideoData>? = [];// Array();
    var audioDataArray      : Array<AudioData>? = [];// Array();
    var itemsDataArray      : Array<Any>? = [];// Array();
    */
//    var FxData : ActionFxData = ActionFxData();
    
    
    //itemsDataArray.append(FxData);

    //itemsDataArray.insert


    /*var videoDataArray      : NSMutableArray? = NSMutableArray();
    var audioDataArray      : NSMutableArray? = NSMutableArray();
    var itemsDataArray      : NSMutableArray? = NSMutableArray();
     */
    
    
    //  videoDataArray addobj
    
    // let filterType = GlitchFilterType(rawValue: indexPath.item)
    // ActionFxData

    // var currentActiveGlitchFilter       =  FilterType()  // Containing All Parameters ..
    /*
    var currentActiveGlitchFilter:TextData       =  TextData() ;
    var currentActiveGlitchFilter2:VideoData      =  VideoData() ;
    var currentActiveGlitchFilter3:AudioData      =  AudioData() ;
   */
    
    // let newData = ActionFxData(
    
//    ActionFxData
//    let fxData = ActionFxData(image: <#UIImage?#>);
//    fxData

    
}

// MARK: Copy Extension - NSCopying Protocols

extension VideoData: NSCopying {
    func copy(with zone: NSZone? = nil) -> Any {
        let copy = VideoData(url: videoUrl!)
        copy.trimStartTime    = trimStartTime
        copy.trimEndTime      = trimEndTime
        copy.transitionType   = transitionType
        copy.videoIndex       = videoIndex
        copy.volume           = volume
        copy.thumbnailImage   = thumbnailImage
        copy.videoSpeed       = videoSpeed
        copy.timePoint        = timePoint
        
        copy.muteStart        = muteStart
        copy.muteDuration     = muteDuration
        
        copy.rotation         = rotation
        copy.vFlip            = vFlip
        copy.hFlip            = hFlip
        
        copy.brightness       = brightness
        copy.constrast        = constrast
        copy.temperature      = temperature
        copy.saturation       = saturation
        copy.sharpness        = sharpness
        copy.tint             = tint
        copy.vignetteStart    = vignetteStart
        copy.vignetteEnd      = vignetteEnd
        copy.vignetteColor    = vignetteColor
        copy.vignetteCenter   = vignetteCenter
        copy.collageType      = collageType
        return copy
    }
}
