# HSCenterSlider

[![License](https://img.shields.io/cocoapods/l/HSCenterSlider.svg?style=flat)](http://cocoapods.org/pods/HSCenterSlider)
[![Platform](https://img.shields.io/cocoapods/p/HSCenterSlider.svg?style=flat)](http://cocoapods.org/pods/HSCenterSlider)

## Demo
![HSCenterSlider](https://github.com/hitendradeveloper/HSCenterSlider/blob/master/HSCenterSlider.gif)


## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

HSCenterSlider is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'HSCenterSlider'
```
##### Possible Error:
`[!] Unable to find a specification for 'HSCenterSlider'` 
##### Solution:

```ruby
pod setup
```


## Author

Hitendra Solanki, hitendra.developer@gmail.com | twitter: @hitendrahckr

## License

HSCenterSlider is available under the MIT license. See the LICENSE file for more info.
